var item = document.getElementById('items')
  var bar = document.getElementById('bar')
  Sortable.create(item, {
    group: "words",
    animation: 150,
    store: {
      get: function (sortable) {
        var order = localStorage.getItem(sortable.options.group);
        return order ? order.split('|') : [];
      },
      set: function (sortable) {
        var order = sortable.toArray();
        localStorage.setItem(sortable.options.group, order.join('|'));
      }
    },
    onAdd: function (evt){  
      brandList.reIndex();
      updateTotalBrand();
      console.log('onAdd.foo:', [evt.item, evt.from]); 
    },
    onUpdate: function (evt){
      brandList.reIndex();
      updateTotalBrand();
      console.log('onUpdate.foo:', [evt.item, evt.from]); 
    },
    onRemove: function (evt){
      brandList.reIndex();
      updateTotalBrand();
     console.log('onRemove.foo:', [evt.item, evt.from]); 
   },
    onStart:function(evt){
      brandList.reIndex();
      updateTotalBrand();
     console.log('onStart.foo:', [evt.item, evt.from]);
   },
    onSort:function(evt){
      brandList.reIndex();
     console.log('onStart.foo:', [evt.item, evt.from]);
   },
    onEnd: function(evt){
      brandList.reIndex();
      updateTotalBrand();
     console.log('onEnd.foo:', [evt.item, evt.from]);
    }
  });


  Sortable.create(bar, {
    group: "words",
    animation: 150,
    onAdd: function (evt){ 
      brandList.reIndex();
      updateTotalBrand();
      updateAssoc();
    },
    onUpdate: function (evt){
    brandList.reIndex();
    updateTotalBrand();
     console.log('onUpdate.bar:', evt.item); 
   },
    onRemove: function (evt){
      brandList.reIndex();
      updateTotalBrand();
      updateAssoc();
    },
    onStart:function(evt){
      brandList.reIndex();
      updateTotalBrand();
     console.log('onStart.foo:', evt.item);
   },
    onEnd: function(evt){ 
      brandList.reIndex();
      updateTotalBrand();
     console.log('onEnd.foo:', evt.item);
    }
  });
