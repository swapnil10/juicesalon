var windowFocus ;
$(window).focus(function() {
    windowFocus = true;
}).blur(function() {
    windowFocus = false;
});
var uri = apiadd+'/contents/franchise';
var container = {
  brandDetails        : $('.brand-details-section'),
  brands              : $("ul.brands-collection"),
  brandsTotal         : $('.total-brands')
}

// var brandSearch= $("#brands-section input.search")
var form = {
  name                : $('#title'),
  whydes              : $('#unecom-why'),
  whycri              : $('#unecom-criteria'),
  addBtn              : $('#add-btn'),
  saveBtn             : $('#save-btn')
}




//to view all brand details on page load
// init();


  
form.addBtn.click(function(){
  var whydes = $('#unecom-why').val().trim().trim();
  var whycri = $('#unecom-criteria').val().trim();
  if(!whydes.length || !whycri.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }

  $.ajax({
    url:uri,
    type : 'PATCH',
    data : {whydes:whydes,whycri:whycri},
    success : function(data){
      console.log(data);
      if(data.error){
      new PNotify({
          title: 'Something went wrong',
          text:  data.error,
          type: 'error',
      });
      return false;
    }
    new PNotify({
      title:'Content Updated',
      text:'Content Update Success',
      type:'success'
    });
    }
  });
  imageUploader.processQueue();
});



  // var url = uri;
  $.ajax({
    url:uri,
    type:'GET',
    success:function(data){
      console.log(data);
      if(data.error)
        return error(data.error);
      form.whydes.data('wysihtml5').editor.setValue(data.why);
      form.whycri.data('wysihtml5').editor.setValue(data.criteria);

      if(data.imageId == 0)
        return false;
      var mockFile = {
        name: 'Franchisee', 
        size: 500,
        status: Dropzone.ADDED,
        url:'/image/'+data.imageId,
      };
      // Call the default addedfile event handler
      imageUploader.emit("addedfile", mockFile);
      imageUploader.createThumbnailFromUrl( mockFile, mockFile['url']);
      imageUploader.files.push(mockFile);
      }
    
  });

/***********************************************************************************
***************************************Image Processing****************************/
Dropzone.autoDiscover = false;

var imageUploader = new Dropzone('div#brand-image',{ url: uri+'/image',maxFiles:1,paramName:'image',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:false,createImageThumbnails:true,acceptedFiles:'image/png,image/jpeg'});
imageUploader.options.cleaningUp = false;
imageUploader.options.cleanUp = function(){
  this.cleaningUp = true;
  imageUploader.removeAllFiles();
  this.cleaningUp = false;
}


imageUploader.on('success',function(file,image){
 console.log(image);
  if(image.success){
    new PNotify({
      title : "Content Uploaded Successfully",
      text : "Content Successfully Updated",
      type : 'success'
    });
  }
  if(image.error){
    new PNotify({
      title : "Error",
      text : image.error,
      type : 'error'
    });
  }
  return false;
});

imageUploader.on('removedfile',function(file){
  if(imageUploader.options.cleaningUp)
    return false;

  $.ajax({
    url : uri+'/image',
    type : 'DELETE',
    success : function(data){
      console.log(data);
    }
  });
});

imageUploader.on('sending', function(file, xhr, formData){
  var token = $("meta[name='csrf-token']").attr('content');
  formData.append('_token',token);
});

imageUploader.on('error',function(file,errorMessage,jqXhr=null){

  if(jqXhr == null){
    if(errorMessage == "You can not upload any more files."){
      imageUploader.options.cleaningUp = true;
      imageUploader.removeFile(file);
      imageUploader.options.cleaningUp = false;
      error("Multiple file not allowed.");
    }
    return false;
  }

  if(jqXhr.status == 422){
    $.each(errorMessage.image,function(i,v){
      error(v);
    });
  }
});