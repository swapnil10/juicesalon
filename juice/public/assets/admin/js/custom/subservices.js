$("select").select2();
var options = {
	valueNames: [
		'brand',
		{data:['id']}
	],
	item:'brand-list-template'
};
var brandList = new List('subservice-section',options);

var container = {
	brands: 			$("ul.brands-collection"),
	brandsTotal: 		$('.total-brands'),
	brandAddBtnGroup: 	$('.brand-add-btn-group'),
	brandEditBtnGroup: 	$('.brand-edit-btn-group'),
}

var form = {
	addBtn: 			$('#add-btn'),
	saveBtn: 			$('#save-btn'),
	title: 				$("#title"),
	enabled : 			$("#enabled"),
	id: 				$("#id"),
	service: 			$("#unecom-service"),
	description: 		$("#unecom-description"),
	price: 				$("#price")

}

var templates = {
	brand: 			$(".unecom-template .unecom-brands li#brand-list-template"),
};

var uri = apiadd+"/subservice";
//to view all brand details on page load
var response = $.ajax(apiadd+"/service");
	response.done(function(data){
	   	if(data.error){
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
		else{
			$.each(data , function( i, v ){
				form.service.append("<option value="+v.id+">"+v.title+"</option>");
			});
		}
	});
init();

brandList.on("updated",function(){
	container.brandsTotal.html($('.brands-collection li').length);
});

$('body').on('click','ul.brands-collection li a',function(e){
	e.preventDefault();
	var parentLi = $(this).closest('li');
	if(parentLi.hasClass('active'))
		return false;

	$(this).closest('ul').find('li').removeClass('active');
	parentLi.addClass('active');

	if(parentLi.hasClass('add-list-ui')){
		document.getElementById("type").reset();
		form.id.val("");
		container.brandAddBtnGroup.removeClass('hide');
		container.brandEditBtnGroup.addClass('hide');
	}else{
		$(brandList.items[0].elm).removeClass('active');
		container.brandEditBtnGroup.removeClass('hide');
		container.brandAddBtnGroup.addClass('hide');

		var id = parentLi.attr('data-id');
		form.id.val(id);
		var url = uri+'/'+id;
		var response = $.get(url);
		response.done(function(data){
			if(data.error){
				new PNotify({
				    title: 'Something went wrong',
				    text:  data.error,
				    type: 'error',
				});
				return false;
			}else{
				console.log(data.serviceId);
				form.title.val(data.title);
				form.service.val(data.serviceId).trigger('change');
				form.price.val(data.price);
				form.description.data("wysihtml5").editor.setValue(data.description);
				form.enabled.prop('checked',data.enabled == '1'?true:false);
			}
		});
	}
});



form.addBtn.click(function(event){
	event.stopPropagation();
	var data = {title:form.title.val(),service:form.service.val(),desc:form.description.val(),price:form.price.val(),enabled:form.enabled.is(":checked")?1:0};
	var response = $.ajax({url:uri,type:"POST",data:data});
	response.fail(function(jqXhr){
		if(jqXhr.status == 422){
	        var errors = jqXhr.responseJSON;
	        console.log(errors);
	        $.each( errors , function( key, value ) {
	          new PNotify({
	            title: 'Something went wrong',
	            text:  value[0],
	            type: 'error',
	          });
	        });
      		return false;
      	}
      new PNotify({
        title:'Error Code : '+jqXhr.status,
        text: 'Something went wrong',
        type:'error'
      });
      return false;
	});
	response.done(function(data){
		if(data.error){
			console.log(data);
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
			
		new PNotify({
			title: 'Added',
			text: 'Service is successfully added.',
			type: 'success',
		});
 		brandList.add({id:data.id,brand:data.name});
 		container.brandsTotal.html(brandList.items.length-1);
	});
	$("#subservices")[0].reset();
	form.service.val(0).trigger('change');
	form.description.data("wysihtml5").editor.setValue("");
	return false;
});

form.saveBtn.click(function(event){
	event.stopPropagation();
	var title = form.title.val();
	var service = form.service.val();
	var desc = form.description.val();
	var price = form.price.val();
	var enabled = form.enabled.is(':checked')?1:0;
	var data = {title:title,service:service,desc:desc,price:price,enabled:enabled};
	var url = uri+'/'+form.id.val();
	var response = $.ajax({url:url,type:"PATCH",data:data});
	response.fail(function(jqXhr){
		if(jqXhr.status == 422){
	        var errors = jqXhr.responseJSON;
	        console.log(errors);
	        $.each( errors , function( key, value ) {
	          new PNotify({
	            title: 'Something went wrong',
	            text:  value[0],
	            type: 'error',
	          });
	        });
      		return false;
      	}
      new PNotify({
        title:'Error Code : '+jqXhr.status,
        text: 'Something went wrong',
        type:'error'
      });
      return false;
	});
	response.done(function(data){
		console.log(data);
		if(data.error){
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
		new PNotify({
			title:'Service Updated',
			text:'Service Update Success',
			type:'success'
		});
		$("li[data-id='"+data.id+"']").find("span.brand").html(data.name);
	});
	form.service.val(0).trigger('change');
	form.description.data("wysihtml5").editor.setValue("");
	return false;
});



function init(){
	var response = $.ajax("/admin/api/subservice");
	response.done(function(data){
	   	if(data.error){
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
		$.each(data,function(i,brand){
			brandList.add({id:brand.id,brand:brand.title});
		});
		/*container.brandsTotal.html(data.coupons.length);*/
		container.brandsTotal.html(brandList.items.length-1);
	});
}
