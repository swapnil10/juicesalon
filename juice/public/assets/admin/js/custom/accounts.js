	$("select").niceSelect();
	var currAcc = 0;

	var options = {
	valueNames: [
		'brand',
		{data:['id']}
	],
	item:'brand-list-template'
};

var uri = '/admin/api/accounts';

var brandList = new List('accounts-section',options);

var container = {
	brands: 			$("ul.brands-collection"),
	brandsAssoc: 		$("ul.brands-associated"),
	rightPanel: 		$(".right-panel"),
	brandsTotal: 		$('.total-brands'),
	brandAddBtnGroup: 	$('.brand-add-btn-group'),
	brandEditBtnGroup: 	$('.brand-edit-btn-group'),
}

var form = {
	id : $("#id"),
	name 		: $("#name"),
	mobile :$("#mobile"),
	email : $("#email"),
	phone :$("#phone"),
	aadhar:$("#aadhar"),
	pan : $("#pan"),
	address : $("#address"),
	type : $("#type"),
	branch : $("#branch"),
	password : $("#password"),
	addBtn: $('#add-btn'),
	saveBtn: $('#save-btn'),
	deleteBtn:$("#delete-btn")
}

var templates = {
	brand: 			$(".unecom-template .unecom-brands li#brand-list-template"),
	brandAssoc: 	$(".unecom-template .unecom-brands li#brand-associated-template"),
};

//to view all brand details on page load
init();

brandList.on("updated",function(){
	container.brandsTotal.html($('.brands-collection li').length);
});

	$('body').on('click','ul.brands-collection li a',function(e){
	imageUploader.options.cleanUp();
	e.preventDefault();
	var parentLi = $(this).closest('li');
	if(parentLi.hasClass('active'))
		return false;

	$(this).closest('ul').find('li').removeClass('active');
	parentLi.addClass('active');

	if(parentLi.hasClass('add-list-ui')){	
		document.getElementById("branchAdmin").reset();
		$("select").niceSelect('update');
		form.addBtn.removeClass("hide");
		$("#password-box").removeClass("hide");
		form.saveBtn.addClass("hide");
		form.deleteBtn.addClass('hide');
	}else{
		$(brandList.items[0].elm).removeClass('active');
		container.brandEditBtnGroup.removeClass('hide');
		container.brandAddBtnGroup.addClass('hide');
		form.addBtn.addClass("hide");
		$("#password-box").addClass("hide");
		form.saveBtn.removeClass("hide");
		form.deleteBtn.removeClass('hide');
		var id = parentLi.attr('data-id');
		var url = uri+'/'+id;
		var response = $.get(url);
		response.done(function(data){
			var branch = data;
			if(data.error){
				new PNotify({
				    title: 'Something went wrong',
				    text:  data.error,
				    type: 'error',
				});
				return false;
			}else{
				form.id.val(branch.id);
				form.name.val(branch.name);
				form.email.val(branch.email);
				form.mobile.val(branch.mobile);
				form.phone.val(branch.phoneNo);
				form.pan.val(branch.panNo);
				form.aadhar.val(branch.aadharNo);
				form.address.val(branch.address);
				/*form.type.find("option[value='"+branch.type+"']").trigger('change');
				form.branch.find("option[value='"+branch.branchId+"']").trigger('change');*/
				form.type.find("option[value='"+branch.type+"']").prop('selected', true);
				form.branch.find("option[value='"+branch.branchId+"']").prop('selected', true);
				$('select').niceSelect('update');
				if(branch.imageId){
					
					var mockFile = {
					    name: branch.name, 
					    size: 500,
					    status: Dropzone.ADDED,
					    url:'/admin/image/'+branch.imageId,
					};
				  
				  	imageUploader.emit("addedfile", mockFile);
				  	imageUploader.createThumbnailFromUrl(mockFile, mockFile['url']);
				  	imageUploader.files.push(mockFile);

				  	imageUploader.on('removedfile',function(file){
				  		if(imageUploader.options.cleaningUp)
				  			return false;
					    var response = $.ajax({url:uri+'/'+id+'/image',type:'DELETE'});
					    response.done(function(data){
					      console.log(data);
					    });
					});
				}
			}
		});
	}
});

	$.get('/admin/api/branch',function(branch){
		$.each(branch,function(i,v){
			var temp = "<option value='"+v.id+"'>"+v.name+"</option>";
			$("#branch").append(temp);
		});
		$("select#branch").niceSelect('update');
	});

	$(document).on('click','#add-btn',function(){
    	var data = $("#branchAdmin").serializeArray();
    	console.log(data);
		var ths = $(this);
		ths.attr('disabled',true);
		ths.html('Please wait...');
		

		$.post(uri,data,function(res){
			console.log(res);
			if(res.error){
				notify('Something went wrong',res.error,'error');
				ths.removeAttr('disabled');
				ths.html('Add');
				return false;
			}
			id = res.id;
			currAcc = res.id;
			imageUploader.processQueue();
			$("#branchAdmin")[0].reset();
			notify('Succesessfully added','','success');
			brandList.add({id:res.id,brand:res.name});
			container.brandsTotal.html(brandList.items.length-1);
			ths.removeAttr('disabled');
			ths.html('Add');
		}).fail(function(e){
			if(e.status == 422){
				var error = e.responseJSON;
				$.each(error,function(i,v){
          			console.log(v[0]);
					notify('Something went wrong',v[0],'error');
					return false;
				});
				ths.removeAttr('disabled');
				ths.html('Submit');
				return false;
			}
			ths.removeAttr('disabled');
			ths.html('Add');
			notify('Something went wrong','Something went wrong','error');
		});
		
		return false;
	});

	$(document).on('click','#save-btn',function(){
    	var data = $("#branchAdmin").serializeArray();
    	var id = $("#id").val();
    	console.log(data);
		var ths = $(this);
		ths.attr('disabled',true);
		ths.html('Please wait...');
		
		var url = uri+'/'+id;

		var response = $.ajax({url:url,type:"PATCH",data:data});
		
		response.done(function(data){
			ths.removeAttr('disabled');
			ths.html('Save');
			console.log(data);
			if(data.error){
				new PNotify({
				    title: 'Something went wrong',
				    text:  data.error,
				    type: 'error',
				});
				return false;
			}
			id = data.id;
			currAcc = data.id;
			imageUploader.processQueue();
			$("#branchAdmin")[0].reset();
			new PNotify({
				title:'Account Updated',
				text: 'Account Updated Successesfully',
				type:'success'
			});

		});

		response.fail(function(err){
			ths.removeAttr('disabled');
			ths.html('Save');
			if(err.status == 422){
				var error = err.responseJSON;
				$.each(error,function(i,v){
					notify("Something went wrong",v[0],"error");
					return false;
				});
			}
		});
		return false;
	});
		

	
	///DROPZONE
	Dropzone.autoDiscover = false;
	var imageUploader = new Dropzone('div#brand-image',{ url: uri+'/image',maxFiles:1,parallelUploads:1,paramName:'image',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:false,createImageThumbnails:true,acceptedFiles:'image/png,image/jpeg'});

	imageUploader.options.cleaningUp = false;
	imageUploader.options.cleanUp = function(){
	  this.cleaningUp = true;
	  imageUploader.removeAllFiles();
	  this.cleaningUp = false;
	}

	imageUploader.on('success',function(file,image){
		if(image.error){
			notify('Something went wrong',image.error,'error');
			imageUploader.options.cleanUp();
			return false;
		}
	});

	imageUploader.on('sending', function(file, xhr, formData){
	  var token = $("meta[name='csrf-token']").attr('content');
	  formData.append('_token',token);
	  formData.append('id',currAcc);
	});

	function init(){
		var response = $.ajax(uri);
		response.done(function(data){
		   	if(data.error){
				new PNotify({
				    title: 'Something went wrong',
				    text:  data.error,
				    type: 'error',
				});
				return false;
			}

			$.each(data,function(i,brand){
				brandList.add({id:brand.id,brand:brand.name});
			});
			container.brandsTotal.html(brandList.items.length-1);
		});

	//brandsInit();
	}
	$(document).on('click','.delete',function(){
		if(!confirm("Are you sure you want to delete this branch admin?"))
			return false;
		var id = $(this).closest('li').attr('data-id');
		var url = uri+'/'+id;

		var response = $.ajax({url:url,type:"DELETE"});
		
		response.done(function(data){
			console.log(data);
			if(data.error){
				new PNotify({
				    title: 'Something went wrong',
				    text:  data.error,
				    type: 'error',
				});
				return false;
			}
			new PNotify({
				title:'Branch Admin Deleted Successesfully',
				text:'Branch Admin Deleted',
				type:'success'
			});
			brandList.remove({id:id});
		});
		// location.href = window.location.href();

	});
