var options = {
	valueNames: [
		'brand',
		{data:['id']}
	],
	item:'brand-list-template'
};
var brandList = new List('coupons-section',options);

var container = {
	brands: 			$("ul.brands-collection"),
	brandsAssoc: 		$("ul.brands-associated"),
	rightPanel: 		$(".right-panel"),
	brandsTotal: 		$('.total-brands'),
	brandAddBtnGroup: 	$('.brand-add-btn-group'),
	brandEditBtnGroup: 	$('.brand-edit-btn-group'),
}

var form = {
	addBtn: 			$('#add-btn'),
	saveBtn: 			$('#save-btn'),
	saveBrandAssoc: 	$('#save-brand-assoc'),
	brandAssoc: 		$("#brand-assoc-form"),
}

var templates = {
	brand: 			$(".unecom-template .unecom-brands li#brand-list-template"),
	brandAssoc: 	$(".unecom-template .unecom-brands li#brand-associated-template"),
};

//to view all brand details on page load
init();

brandList.on("updated",function(){
	container.brandsTotal.html($('.brands-collection li').length);
});

$('body').on('click','ul.brands-collection li a',function(e){
	e.preventDefault();
	var parentLi = $(this).closest('li');
	if(parentLi.hasClass('active'))
		return false;

	$(this).closest('ul').find('li').removeClass('active');
	parentLi.addClass('active');

	if(parentLi.hasClass('add-list-ui')){
		$('#code').removeAttr('disabled');	
		document.getElementById("coupon").reset();
		
		container.brandAddBtnGroup.removeClass('hide');
		container.brandEditBtnGroup.addClass('hide');
		container.rightPanel.addClass('hide');
	}else{
		$(brandList.items[0].elm).removeClass('active');
		$('#code').attr('disabled','disabled');
		container.brandEditBtnGroup.removeClass('hide');
		container.brandAddBtnGroup.addClass('hide');

		var id = parentLi.attr('data-id');
		var url = 'api/coupons/'+id;
		var response = $.get(url);
		response.done(function(data){
			var coupon = data;
			if(data.error){
				new PNotify({
				    title: 'Something went wrong',
				    text:  data.error,
				    type: 'error',
				});
				return false;
			}else{
				$("#code").val(coupon.code);
				$("#amount").val(coupon.amount);
				$("#discountType").val(coupon.discountType);
				$("#startDate").val(coupon.startDate);
				$("#expiryDate").val(coupon.expiryDate);
				$("#enabled").prop('checked',coupon.enabled == '1'?true:false);
			}
		});

		//Get associated brands
		// response = $.get('/admin/api/coupons/'+id+'/productType');
		// response.done(function(data){
		// 	console.log();
		// 	container.brandsAssoc.find('li input[type=checkbox]').prop('checked',0);
		// 	$.each(data,function(i,brand){
		// 		container.brandsAssoc.find('li[id='+brand.id+'] input[type=checkbox]').prop('checked',true);
		// 	});
		// 	container.rightPanel.removeClass('hide');
		// });
	}
});



form.addBtn.click(function(event){
	event.stopPropagation();

	var url = 'api/coupons';
	var data = $('#coupon').serializeArray();
	delete data['enabled'];
	if($('#enabled').prop('checked')){
		data.push({name:'enabled',value:1});
	}else{
		data.push({name:'enabled',value:0});
	}
	var code = $('#code').val();
	var flag=false; 	
	$("#error").fadeOut();
	var response = $.ajax({url:url,type:"POST",data:data});
	response.fail(function(jqXhr){
		if(jqXhr.status == 422){
	        var errors = jqXhr.responseJSON;
	        console.log(errors);
	        $.each( errors , function( key, value ) {
	          new PNotify({
	            title: 'Something went wrong',
	            text:  value[0],
	            type: 'error',
	          });
	        });
      		return false;
      	}
      new PNotify({
        title:'Error Code : '+jqXhr.status,
        text: 'Something went wrong',
        type:'error'
      });
      return false;
	});
	response.done(function(data){
		if(data.error){
			console.log(data);
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
			
		new PNotify({
			title: 'success',
			text: 'Coupons are successfully added.',
			type: 'success',
		});
 		brandList.add({id:data.id,brand:code});
 		container.brandsTotal.html(brandList.items.length-1);
	});
	return false;
});

form.saveBtn.click(function(event){
	event.stopPropagation();
	var code = $('#code').val();
	var data = $('#coupon').serializeArray();
	delete data['enabled'];
	if($('#enabled').prop('checked')){
		data.push({name:'enabled',value:1});
	}else{
		data.push({name:'enabled',value:0});
	}
	var url = 'api/coupons/'+code;

	var response = $.ajax({url:url,type:"PATCH",data:data});
	
	response.done(function(data){
		console.log(data);
		if(data.error){
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
		new PNotify({
			title:'Coupon Updated Successesfully',
			text:'Coupon Updated',
			type:'success'
		});

	});
 
	return false;
});

// form.saveBrandAssoc.click(function(){
// 	var brands =[];
// 	$.each(container.brandsAssoc.find('li'),function(){
// 		if($(this).find('input[type=checkbox]').is(":checked"))
// 			brands.push($(this).attr('id'));
// 	});
// 	$.ajax({
// 		url:'/admin/api/coupons/'+$('#code').val()+'/productType',
// 		type:'POST',
// 		data:{ptid:brands},
// 		dataType: 'json',
// 		success:function(data){
// 			console.log(data);
// 			new PNotify({
// 			    title: 'Successful',
// 			    text:  'Coupon was added to Product Type',
// 			    type: 'success',
// 			});
// 		}
// 	});
// });


function init(nextUrl=''){
	var response = $.ajax('api/coupons'+nextUrl);
	response.done(function(data){
	   	if(data.error){
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
		$.each(data,function(i,brand){
			brandList.add({id:brand.code,brand:brand.code});
		});
		/*container.brandsTotal.html(data.coupons.length);*/
		container.brandsTotal.html(brandList.items.length-1);

		if(typeof data.next !== 'undefined')
			init(data.next);
	});

	//brandsInit();
}

// function brandsInit(nextUrl=''){
// 	var response = $.get('/admin/api/product-type'+nextUrl);
// 	response.done(function(data){
// 		$.each(data,function(i,brand){
// 			if(brand.enabled == 0)
// 				return true;
// 			var template = templates.brandAssoc.clone();
// 			template.find('.brand').html(brand.title);
// 			template.attr('id',brand.id);
// 			container.brandsAssoc.append(template);
// 		});

// 		if(typeof data.next !== 'undefined')
// 			brandsInit(data.next);
// 	});
// }