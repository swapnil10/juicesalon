var options = {
  valueNames: [
    'brand',
    {data:['id']}
  ],
  item:'brand-list-template'
};

var uri = apiadd+'/contents/slider/academy'
var brandList = new List('variants-section',options);

var container = {
  brands:       $("ul.brands-collection"),
  brandsTotal:    $('.total-brands'),
  brandAddBtnGroup:   $('.brand-add-btn-group'),
  brandEditBtnGroup:  $('.brand-edit-btn-group'),
}

var form = {
  id : $("#id"),
  content : $("#unecom-content"),
  link : $("#link"),
  saveBtn:$('#save-btn'),
  addBtn : $('#add-btn'),
  deleteBtn : $("#delete-btn")
}

var templates = {
  brand:      $(".unecom-template .unecom-brands li#brand-list-template"),
};

//to view all brand details on page load
init();

brandList.on("updated",function(){
  var sliderCount = 1;
  container.brandsTotal.html($('.brands-collection li').length-1);

  $('.brands-collection li').each(function(){
    $(this).find("span.brand").html("Slider "+eval(sliderCount-1));
    sliderCount++;
  });
});

$('body').on('click','ul.brands-collection li a',function(e){
  e.preventDefault();
  var parentLi = $(this).closest('li');
  if(parentLi.hasClass('active'))
    return false;

  $(this).closest('ul').find('li').removeClass('active');
  parentLi.addClass('active');
  if(parentLi.hasClass('add-list-ui')){
    container.brandEditBtnGroup.addClass('hide');
    container.brandAddBtnGroup.removeClass('hide');
    resetForm();
    return false;
  }
  container.brandAddBtnGroup.addClass('hide');
  container.brandEditBtnGroup.removeClass('hide');
  var id = parentLi.attr('data-id');
  var url = uri+'/'+id;
  var response = $.ajax(url,"get");
  var title = parentLi.find("span.brand").html();
  response.done(function(data){
    imageUploader.options.cleanUp();
    if(data.error){
      new PNotify({
        title:'Something went wrong.',
        text:data.error,
        type:'error'
      });
      return false;
    }
    form.id.val(data.id);
    form.content.data('wysihtml5').editor.setValue(data.content);
    form.link.val(data.link);
    if(data.imageId != 0){
    var mockFile = {
      name: title, 
      size: 500,
      status: Dropzone.ADDED,
      url:'/image/'+data.imageId,
    }
      imageUploader.emit("addedfile", mockFile);
      imageUploader.createThumbnailFromUrl( mockFile, mockFile['url']);
      imageUploader.files.push(mockFile);
    }
  });
  
  response.fail(function(jqXhr){
    new PNotify({
          title: 'Something went wrong',
          text:  data.error,
          type: 'error',
      });
  });
  
  return false;
});

function resetForm(){
  form.id.val("");
  form.content.data('wysihtml5').editor.setValue("");
  form.link.val("");
  imageUploader.options.cleanUp();
}

form.saveBtn.click(function(event){
  event.stopPropagation();
  var content = $('#unecom-content').val();
  var link = form.link.val();
  var obj = {content:content,link:link};
  var id = $(".brands-collection > .active").attr('data-id');

  $.ajax({
    url:uri+'/'+id,
    type:"PATCH",
    data:obj,
    success:function(data){
      if(data.error){
        new PNotify({
          title: 'Something went wrong',
          text:  data.error,
          type: 'error',
        });
      //$(".brands-collection > .active > a > .brand").html(data.name);
      return false;
      }
      new PNotify({
      title: 'success',
      text: 'Slider Content are successfully updated.',
      type: 'success',
      });
      imageUploader.processQueue();
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
        var errors = jqXhr.responseJSON;
        $.each( errors , function( key, value ) {
          new PNotify({
            title: 'Something went wrong',
            text:  value[0],
            type: 'error',
          });
        });
      return false;
      }
    }
  });
  return false;
});

form.addBtn.click(function(){
  var content = $('#unecom-content').val().trim();
  var link = form.link.val().trim();
  if(!content.length){

    new PNotify({
      title: 'Empty Fields',
      text: 'All fields are required',
      type: 'error',
    });
    return false;
  }
  var obj = {content:content,link:link};
  console.log(obj);
  $.ajax({
    url:uri,
    type:"POST",
    data:obj,
    success:function(data){
      if(data.error){
        new PNotify({
          title: 'Something went wrong',
          text:  data.error,
          type: 'error',
        });
      //$(".brands-collection > .active > a > .brand").html(data.name);
      return false;
      }
      new PNotify({
      title: 'success',
      text: 'Slider Content are successfully updated.',
      type: 'success',
      });
      brandList.add({id:data.id});
      form.id.val(data.id);
      imageUploader.processQueue();
      resetForm();
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
        var errors = jqXhr.responseJSON;
        $.each( errors , function( key, value ) {
          new PNotify({
            title: 'Something went wrong',
            text:  value[0],
            type: 'error',
          });
        });
      return false;
      }
    }
  });

  return false;
});


form.deleteBtn.click(function(){
  (new PNotify({
        title: 'Delete Confirmation',
        text: 'Are you sure?<br>This will delete every related details from database',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        hide: false,
        confirm: {
            confirm: true
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        after_close: function(notice, timer_hide){
          $('.ui-pnotify-modal-overlay').remove();
        }
    })).get().on('pnotify.confirm', function(){
      var id = form.id.val();
      var response = $.ajax({url:uri+'/'+id,type:'DELETE'});
      response.fail(function(jqXhr){
        return error("");
      });
      response.done(function(data){
        console.log(data);
        if(data.error)
          return error(data.error);

        notify("Slider Deleted","","success");
        deleteFromList(id);
      });
  });

  // var cnf = confirm('Are you sure?<br>This will delete every related details from database');
  return false;
});

function deleteFromList(id){
  brandList.remove('id',id);
  resetForm();
  container.brands.find('li:first-child a').trigger('click');
}

function init(){
  var response = $.get(uri);
  response.fail(function( jqXhr) {
        new PNotify({
          title:'Something went wrong',
          text:'Error:'+jqXhr.status,
          type:'error'
        });return false;
    });

  response.done(function(data){
    
    $.each(data,function(i,brand){
      brandList.add({id:brand.id});
    });
    container.brandsTotal.html($('.brands-collection li').length-1);
    $("ul.brands-collection li:first a").trigger('click');
  });
}

/***********************************************************************************
***************************************Image Processing****************************/
Dropzone.autoDiscover = false;

var imageUploader = new Dropzone('div#brand-image',{ url: uri+'/image',maxFiles:1,paramName:'image',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:false,createImageThumbnails:true,acceptedFiles:'image/png,image/jpeg'});
imageUploader.options.cleaningUp = false;

imageUploader.options.cleanUp = function(){
  this.cleaningUp = true;
  imageUploader.removeAllFiles();
  this.cleaningUp = false;
}


imageUploader.on('success',function(file,image){
  if(image.error)
    return error(image.error);
});

imageUploader.on('removedfile',function(file){
  
  if(imageUploader.options.cleaningUp)
    return false;

  var response = $.ajax({url:uri+'/'+$("#id").val()+'/image',type:'DELETE'});
  response.done(function(data){
    if(data.error)
      return error(data.error);
  });

});

imageUploader.on('sending', function(file, xhr, formData){
  var token = $("meta[name='csrf-token']").attr('content');
  formData.append('_token',token);
  formData.append('id',$("#id").val());
});

imageUploader.on('error',function(file,errorMessage,jqXhr=null){

  if(jqXhr == null){
    if(errorMessage == "You can not upload any more files."){
      imageUploader.options.cleaningUp = true;
      imageUploader.removeFile(file);
      imageUploader.options.cleaningUp = false;
      error("Multiple file not allowed.");
    }
    return false;
  }

  if(jqXhr.status == 422){
    $.each(errorMessage.image,function(i,v){
      error(v);
    });
  }
});