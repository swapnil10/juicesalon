
var form  = {
	id 				: false,
	brands 			: $('#bar'),
	name 			: $('#unecom-brandassoc-name'),
	description 	: $("#unecom-brandassoc-description")
}

var templates = {
	association 	: $('.unecom-template .unecom-association li'),
	brand 			: $('.unecom-template .unecom-brands li')
}
var total={
	association: 	$("#total-association"),
	brands: 		$("#total-brands")
}

//list options
var assocListOptions = {
	valueNames: [
	'assoc',
	{data:['id']}
	],
	item:'assoc-list-template',
	searchClass:'search-assoc'
};

var brandListOptions = {
	valueNames:[
	'brand',
	{data:['id']}
	],
	item:'brand-list-template',
	searchClass:'search-brand'
}

//List objects
var associationList = new List('association-section',assocListOptions);
var brandList = new List('brand-section',brandListOptions);


var updateBtn = $('.unecom-update button').clone();
var addBtn = $('.unecom-add button').clone();
var formData = [];

//methods
var updateTotalAssociation = function(){
	var length = $('ul.association li.unecom-association').length;
	total.association.html(length===1?length+" Brand":length+" Brands");
}
var updateTotalBrand = function(){
	var length = $('ul.brands li.unecom-brand').length;
	total.brands.html(length===1?length+" Variant Detail":length+" Variant Details");
}
var formReset = function(){
	form.brands.html('');
	form.name.val('');
	form.description.html('');
} 
var reloadBrandList = function(brandArray=null){
	brandList.clear();
	if(brandArray!=null){
		$.each(brandArray,function(i,brand){
				if(brand.enabled == 0)
					return true;
				brandList.add({id:brand.id,brand:brand.title});
		});
	}
	else{
		/*var response = $('/admin/api/category');
		response.done(function(data){
			if(data.error){
				new PNotify({
					title:'Something went wrong.',
					text:data.error,
					type:'error'
				});
				return false;
			}else{
				$.each(data,function(i,brand){
					if(brand.enabled == 0)
						return true;
					brandList.add({id:brand.id,brand:brand.title});
				});
			}
			updateTotalBrand();
		});
		response.error(function(err){
			new PNotify({
				title:'Something went wrong.',
				text :'Error : '+err.status,
				type:'error'
			});
			return false;
		});*/
	}
}

var brandListInit = function (nextUrl=''){
	/*var response = $.get('/admin/api/category'+nextUrl);
	response.done(function(data){
		if(data.error){
			new PNotify({
				title:'Something went wrong.',
				text:data.error,
				type:'error'
			});
			return false;
			//error if  there is no data to show
		}
		$.each(data,function(i,brand){
			//console.log(brand);
			if(brand.enabled == 0)
				return true;
			brandList.add({id:brand.id,brand:brand.title});
		});
		updateTotalBrand();

		if(typeof data.next !== 'undefined')
			brandListInit(data.next);
	});*/
}



//   to display all associations
var url = '/admin/api/brands/';
var response = $.get(url);
response.done(function(data){
	if(data.error){
		new PNotify({
			title:'Something went wrongh.',
			text:data.error,
			type:'error'
		});
		return false;
	}
	$.each(data,function(i,category){
		associationList.add({id:category.id,assoc:category.title})
	});
	updateTotalAssociation();
});
response.error(function(jqXhr){
	new PNotify({
		title:'Something went wrong',
		text:'Error : '+jqXhr.status,
		type:'error'
	});
	return false;
});


//  to display all brands
brandListInit();
//to view all details of association
$(document).on('click','li.unecom-association',function(){
	$('#association-section').find('.active').removeClass('active');
	$('.dragi').removeClass('hide');
	var li = $(this).closest('li');
	li.addClass('active');
	var id = li.attr('data-id');
	form.id = id;
	formReset();
	var response = $.get('/admin/api/association/'+id);
	response.done(function(data){
		console.log(data);
		if(data.error){
			new PNotify({
				title:'Something went wrong.',
				text:data.error,
				type:'error'
			});
			return false;
		}
		(data.nonGroupBrands==null)?brandList.clear():reloadBrandList(data.nonGroupBrands);
		$.each(data.groupBrands,function(i,brand){
			if(brand.enabled == 0)
				return true;
			brands = templates.brand.clone();
			brands.attr('data-id',brand.id);
			brands.find('a').append(brand.title);
			form.brands.append(brands);
		});
		updateTotalBrand();
	});
	response.fail(function(jqXhr){
		new PNotify({
			title:'Something went wrong.',
			text:'Error : '+jqXhr.status,
			type:'error'
		});
	});
});
// function deleteAssoc(id){
// 	$.ajax({
// 		url:'/admin/api/association',
// 		type:'DELETE',
// 		data:{cid:form.id,ptid:id},
// 		success:function(data){
// 			console.log(data);
// 		},
// 		error:function(jqXhr){
// 			console.log(jqXhr.status);
// 		}
// 	});
// }
function updateAssoc(){
	var brandArray = [];
	var brands = form.brands.children()
	$.each(brands,function(i){
		brandArray[i] = $(this).attr('data-id');	
	});
	$.ajax({
		url:'/admin/api/association',
		type:'POST',
		data:{brandId:form.id,vdid:brandArray},
		success:function(data){
			console.log(data);
		},
		error:function(jqXhr){
			console.log(jqXhr.status);
		}
	});
}
// function updateAssoc(id){
// 	var brandArray = [];
// 	var brands = form.brands.children()
// 	$.each(brands,function(i){
// 		brandArray[i] = $(this).attr('data-id');	
// 	});
// 	console.log(brandArray);
// }
//List events
brandList.on('updated',function(){
	updateTotalBrand();
});
associationList.on('updated',function(){
	updateTotalAssociation();
})