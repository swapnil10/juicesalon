var windowFocus ;
$(window).focus(function() {
    windowFocus = true;
}).blur(function() {
    windowFocus = false;
});


var container = {
  brandDetails        : $('.details-section'),
  addDetails          : $('.add-section'),
  brands              : $("ul.brands-collection"),
  brandsTotal         : $('.total-brands'),
}
var form = {
  addForm             : {
    price             : container.addDetails.find('#price'),
    quantity          : container.addDetails.find('#quantity'),
  },
  detailsForm         : {
    price             : container.brandDetails.find('#price'),
    quantity          : container.brandDetails.find('#quantity'),
  },
  product             : $("#product-id"),
  addBtn              : $('#add-btn'),
  saveBtn             : $('#save-btn'),
  deleteBtn           : $('#delete-btn'),
}
var options = {
  valueNames:[
    'brand',
    {data:['id']}
  ],
  item:'brand-list-template'
};
var brandList = new List('brands-section',options);
var templates = {
  brand:  $(".unecom-template .unecom-brands li"),
  varaintBox: $('.unecom-template .unecom-variantBox .input-group')
};
//to view all variants box
$.ajax({
  url:'/admin/api/variants',
  type:'GET',
  success:function(data){
    if(data.error){
      new PNotify({
        title:'Something went wrong.',
        text:data.error,
        type:'error'
      });
      return false;
    }var k=0;
    $.each(data,function(i,v){
      if(v.enabled == 0)
        return true;
      k++;
      
      $('.variant-container').each(function(){
        var cloneTemp = templates.varaintBox.clone();
        console.log(k);
        var a = cloneTemp.appendTo(this).find('input');
        a.attr('placeholder',v.name);
        var vari = 'variant_'+ k;
        a.attr('id',vari);
        a.attr('name',vari);
      });
    });
  }
});
//to view product details
$.ajax({
  url:'/admin/api/products/'+form.product.val(),
  type:'GET',
  success:function(data){
    if(data.error){
      new PNotify({
        title:'Something went wrong',
        text:data.error,
        type:'error',
      });
      return false;
    }
    $('.product-title').html(data.title);
    $('.product-desc').html(data.description);
  },
  error:function(jqXhr){
    new PNotify({
      title:'Something went wrong.',
      text:'Error code :'+jqXhr.status,
      type:'error',
    });
    return false;
  }
});
//to view all brand details on page load
var response = $.get('/admin/api/products/'+form.product.val()+'/variants');
response.done(function(data){
  // console.log(data);
  if(data.error){
    new PNotify({
      title:'Something went wrong',
      text:data.error,
      type:'error',
    });
  }else{
    var varianti='';
    $.each(data,function(i,v){
      var vari1 = v.variant1;
      var vari2 = v.variant2;
      var vari3 = v.variant3;
      var concatVari = $.grep([vari1, vari2, vari3], Boolean).join("+ ");
      brandList.add({id:v.id,brand:concatVari});
    });
    container.brandsTotal.html(brandList.items.length-1);
    container.brands.find('li:first-child a').trigger('click');
  }
});

form.addBtn.click(function(){
  var variants = [];
  $.each(container.addDetails.find('.product-variant'),function(i){
    var variant = {};
    $.each($(this).find('input'),function(){
      if($(this).attr('name') == 'variant_1'){
        variant['variant1'] = $(this).val();
        return true;
      }
      if($(this).attr('name') == 'variant_2'){
        variant['variant2'] = $(this).val();
        return true;
      }
      if($(this).attr('name') == 'variant_3'){
        variant['variant3'] = $(this).val();
        return true;
      }
      variant[$(this).attr('name')] = $(this).val();
    })
    variants[i] = variant;
    
  });
  var response = addProductVariants(form.product.val(),variants);
  response.fail(function(jqXhr){
    if(jqXhr.status == 422){
      var errors = jqXhr.responseJSON;
      $.each( errors , function( key, value ) {
        new PNotify({
          title: 'Something went wrong',
          text:  value[0],
          type: 'error',
        });
      });
      return false;
    }
    new PNotify({
      title:'Something went wrong.',
      text:'Error : '+jqXhr.status,
      type:'error'
    });
  });
  response.done(function(data){
    console.log(data);
    if(data.error){
      new PNotify({
            title: 'Variant addition failed',
            text: data.error,
            type: 'error',
        });
      return false;
    }
    new PNotify({
        title: 'Variant added successfully',
        text: 'The variant was added successfully',
        type: 'success',
    });
    var brandName = $.grep([data.variant1, data.variant2, data.variant3], Boolean).join("+ ");
    brandList.add({id:data.id,brand:brandName});
    container.brandsTotal.html(brandList.items.length-1);
    container.addDetails.find(".product-variant")[0].reset();
  });
});

form.saveBtn.click(function(){
  var variants = [];
  ////
  $.each(container.brandDetails.find('.product-variant'),function(i){
    var variant = {};
    $.each($(this).find('input'),function(){
      variant[$(this).attr('name')] = $(this).val();
    });
    variants[i] = variant;
  });

  var response = updateProductVariants(variants[0]);
  response.fail(function(jqXhr){
    if(jqXhr.status == 422){
      var errors = jqXhr.responseJSON;
      $.each( errors , function( key, value ) {
        new PNotify({
          title: 'Something went wrong',
          text:  value[0],
          type: 'error',
        });
      });
      return false;
    }
    new PNotify({
      title:'Something went wrong.',
      text:'Error : '+jqXhr.status,
      type:'error'
    });
  });
  response.done(function(data){
    console.log(data);
    if(data.error){
      new PNotify({
            title: 'Variant addition failed',
            text: data.error,
            type: 'error',
        });
      return false;
    }
    var brandName = $.grep([data.variant1, data.variant2, data.variant3], Boolean).join("+ ");
    $(".brands-collection > [data-id='"+data.id+"'] > a > span").html(brandName);
    new PNotify({
        title: 'Variant updated successfully',
        text: 'The variant was updated successfully',
        type: 'success',
    });
  });
});

form.deleteBtn.click(function(){
  (new PNotify({
      title: 'Delete Confirmation',
      text: 'Are you sure?<br>This will delete every related details from database',
      icon: 'glyphicon glyphicon-question-sign',
      hide: false,
      hide: false,
      confirm: {
          confirm: true
      },
      addclass: 'stack-modal',
      stack: {
          'dir1': 'down',
          'dir2': 'right',
          'modal': true
      },
      buttons: {
          closer: false,
          sticker: false
      },
      history: {
          history: false
      },
      after_close: function(notice, timer_hide){
        $('.ui-pnotify-modal-overlay').remove();
      }
  })).get().on('pnotify.confirm', function(){
      var id = form.product.val();
      var vid = $('.brands-collection > .active').data('id');
      var response = $.ajax({
        url:'/admin/api/products/'+id+'/variant/'+vid,
        type:'DELETE'
      });
      response.done(function(data){
        if(data.error){
          new PNotify({
              title: 'Variant delete failed',
              text: data.error,
              type: 'error',
          });
          return false;
        }
        new PNotify({
            title: 'Product variant deleted',
            text: 'Product variant deleted from database',
            type: 'success',
        });
        deleteFromList(vid);
      });
  })
});

// $('body').on('click','ul.brands-collection li a',function(e){
//   e.preventDefault();
//   var parentLi = $(this).closest('li');
//   if(parentLi.hasClass('active'))
//     return false;
//   $(this).closest('ul').find('li').removeClass('active');
//   parentLi.addClass('active');

//   if(parentLi.hasClass('add-list-ui')){
//     // container.codes.addClass('hide');
//     container.brandDetails.hide();
//     container.addDetails.show();
//     // resetAddForm();
//     return false;
//   }
//   $(brandList.items[0].elm).removeClass('active');
//   var id = form.product.val();
//   var vid = $(this).closest("li").attr("data-id");
  
//   $.ajax({
//     url:'/admin/api/products/'+id+'/variants/'+vid,
//     type:'GET',
//     success:function(data){
//       if(data.error){
//         new PNotify({
//           title:'Something went wrong.',
//           text:data.error,
//           type:'error'
//         });
//         return false;
//       }
//       $.each(data,function(i,v){
//         form.price.val(v.changeOfPrice);
//         form.quantity.val(v.quantity);
//         $('.variant-container').find('#variant_1').val(v.variant1);
//         $('.variant-container').find('#variant_2').val(v.variant2);
//         $('.variant-container').find('#variant_3').val(v.variant3);
//       });
//     },
//     error:function(jqXhr){
//       new PNotify({
//         title:'Something went wrong.',
//         text:'Error :'+jqXhr.status,
//         type:'error'
//       });
//       return false;
//     }
//   });
// });
$('body').on('click','ul.brands-collection li a',function(e){
  e.preventDefault();
  var parentLi = $(this).closest('li');
  if(parentLi.hasClass('active'))
    return false;
  $(this).closest('ul').find('li').removeClass('active');
  parentLi.addClass('active');

  if(parentLi.hasClass('add-list-ui')){
    //container.codes.addClass('hide');
    container.brandDetails.hide();
    container.addDetails.show();
    // resetAddForm();
    return false;
  }
  $(brandList.items[0].elm).removeClass('active');
  //container.codes.removeClass('hide');
  container.brandDetails.show();
  container.addDetails.hide();

  var id = form.product.val();
  var vid = $(this).closest("li").attr("data-id");
  
  $.ajax({
    url:'/admin/api/products/'+id+'/variants/'+vid,
    type:'GET',
    success:function(data){
      console.log(data);
      if(data.error){
        new PNotify({
          title:'Something went wrong',
          text:data.error,
          type:'error'
        });
        return false;
      }
      $.each(data,function(i,v){
        console.log(v.changeOfPrice);
        form.detailsForm.price.val(v.changeOfPrice);
        form.detailsForm.quantity.val(v.quantity);
        container.brandDetails.find('#variant_1').val(v.variant1);
        container.brandDetails.find('#variant_2').val(v.variant2);
        container.brandDetails.find('#variant_3').val(v.variant3);
      });
    }
  });

});
function addProductVariants(id,variants){
  return $.ajax({
    url:'/admin/api/products/'+id+'/variants',
    type:'POST',
    data:JSON.stringify(variants),
    contentType: "application/json"
  });
}
function updateProductVariants(variants){
  var vid = $('.brands-collection > .active').data('id');
  return $.ajax({
    url:'/admin/api/products/'+form.product.val()+'/variant/'+vid,
    type:'PATCH',
    data:JSON.stringify(variants),
    contentType: "application/json"
  });
}
function deleteFromList(vid){
  brandList.remove("id",vid);
  brandList.search('');
  container.brands.find('.search').val('');
  $('ul.brands-collection li.add-list-ui a').trigger('click');
}