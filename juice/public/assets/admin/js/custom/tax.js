// PNotify.desktop.permission();
// PNotify.prototype.options.styling = "bootstrap3";


var form = {
	taxId 			: $('form #tax-id'),
	name 			: $('form #name'),
	type 			: $('form #type'),
	amount 			: $('form #amount'),
	description 	: $('form #description'),
	resetBtn 		: $('#reset-btn'),
	addBtn 			: $('#add-btn'),
	updateBtn 		: $('#save-btn')
}
var container = {
	taxCount 			: $('.total-tax'),
	taxes 				: $('ul.taxes-collection'),
	addBtnGroup 		: $('.add-btn-group'),
	editBtnGroup 		: $('.edit-btn-group'),
};

var option = {
	valueNames: [
		'name',
		{data:['id']}
	],
	item:'tax-list-template'
};
var taxList = new List('tax-section',option);


var templates = {
	taxList : $('.unecom-template ul.unecom-tax li'),
};

//   to display all tax 
var url = '/admin/api/tax';
var response = $.get(url);
response.done(function(data){
	if(data.error){
		console.log(data);
	}else{
		if(data == 0){
			container.taxCount.html(0);
			return false;
		}
		$.each(data,function(i,tax){
			taxList.add({id:tax.id,name:tax.name});
			container.taxCount.html(container.taxes.find('li').length-1);
		});
	}
});
response.error(function(jqXhr){
	console.log(jqXhr.status);
});


// when click on delete button
$('body').on('click','ul.taxes-collection li .delete',function(){
	var li = $(this).closest('li');
	var id = li.attr('data-id');

	(new PNotify({
	    title: 'Delete Confirmation',
	    text: 'Are you sure?',
	    icon: 'glyphicon glyphicon-question-sign',
	    hide: false,
	    hide: false,
	    confirm: {
	        confirm: true
	    },
	    addclass: 'stack-modal',
	    stack: {
	        'dir1': 'down',
	        'dir2': 'right',
	        'modal': true
	    },
	    buttons: {
	        closer: false,
	        sticker: false
	    },
	    history: {
	        history: false
	    },
	    after_close: function(notice, timer_hide){
	     	$('.ui-pnotify-modal-overlay').remove();
	    }
	})).get().on('pnotify.confirm', function(){
	    
		var response = $.ajax({url:url+'/'+id,type:'DELETE'});
		response.done(function(data){
			if(data.error){
				new PNotify({
				    title: 'Tax failed to be deleted',
				    text: data.error,
				    type: 'error',
				});
			}else{
				$('ul.taxes-collection li:first-child').trigger('click');
				taxList.remove("id",id);
				container.taxCount.html(container.taxes.find('li').length-1);
				new PNotify({
				    title: 'Tax Deleted',
				    text: 'Tax deleted from database',
				    type: 'success',
				});
			}
		});
	})
	
});

$('body').on('click','ul.taxes-collection li',function(e){
	e.preventDefault();
	if($(this).hasClass('active'))
		return false;
	
	$(this).closest('ul').find('li').removeClass('active');
	$(this).addClass('active');


	if($(this).is(":first-child")){
		resetForm();
		container.addBtnGroup.removeClass('hide');
		container.editBtnGroup.addClass('hide');
	}else{
		container.editBtnGroup.removeClass('hide');
		container.addBtnGroup.addClass('hide');
		
		var request = $.get('/admin/api/tax/'+$(this).attr('data-id'));
		request.done(function(data){
			resetForm(data.id,data.name,data.description,data.type,data.amount);
		});
	}
});

function resetForm(id='',name='',description='',type='none',amount=null){
	form.taxId.val(id);
	form.name.val(name);
	form.description.val(description);
	form.amount.val(amount);
	form.type.val(type);
}

// when click on add new button
form.addBtn.click(function(){
	var formData = new FormData();
	$.each($('#tax-form .form-control,#tax-form textarea'),function(){
		formData.append($(this).attr('name'),$(this).val());
	});
	$.ajax({
		url:url,
		type:'POST',
		data:formData,
		processData:false,
		contentType:false,
		error:function(jqXhr){
			if(jqXhr.status == 422){
		        var errors = jqXhr.responseJSON;
		        console.log(errors);
		        $.each( errors , function( key, value ) {
		          new PNotify({
		            title: 'Something went wrong',
		            text:  value[0],
		            type: 'error',
		          });
		        });
	      		return false;
      		}
		},
		success:function(data){
			console.log(data);
			if(data.error){
				console.log(data);
				new PNotify({
				    title: 'Tax addition failed',
				    text: data.error,
				    type: 'error',
				});
			}else{
				var tax = data;
				taxList.add({id:tax.id,name:tax.name});
				container.taxCount.html(container.taxes.find('li').length-1);
				new PNotify({
				    title: 'Tax added',
				    text: 'Tax '+tax.name+' added to database',
				    type: 'success',
				});
				resetForm();
			}
		}
	});
	return false;
});


//when click on upadte button
form.updateBtn.click(function(){
	var formData = {};
	$.each($('#tax-form .form-control,#tax-form textarea'),function(){
		formData[$(this).attr('name')] = $(this).val();
	});

	$.ajax({
		url:url+'/update/'+form.taxId.val(),
		type:'POST',
		data:JSON.stringify(formData),
		contentType: "application/json",
		error:function(){
			if(jqXhr.status == 422){
		        var errors = jqXhr.responseJSON;
		        console.log(errors);
		        $.each( errors , function( key, value ) {
		          new PNotify({
		            title: 'Something went wrong',
		            text:  value[0],
		            type: 'error',
		          });
		        });
	      		return false;
      		}
		},
		success:function(data){
			console.log(data);
			if(data.error){
				console.log(data);
				new PNotify({
				    title: 'Tax updation failed',
				    text: data.Error,
				    type: 'error',
				});
			}else{
				data = data;
				resetForm(data.id,data.name,data.description,data.type,data.amount);
				new PNotify({
				    title: 'Tax updated',
				    text: 'Tax <b>'+data.name+'</b> updated successfully',
				    type: 'success',
				});
			}
		}
	});
	return false;
});