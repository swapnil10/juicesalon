var windowFocus ;
$(window).focus(function() {
    windowFocus = true;
}).blur(function() {
    windowFocus = false;
});
var uri = apiadd+"/look/";
var container = {
  brandDetails        : $('.brand-details-section'),
  brands              : $("ul.brands-collection"),
  brandsTotal         : $('.total-brands')
}

// var brandSearch= $("#brands-section input.search")
var form = {
  id                  : $("#product-id"),
  name                : $('#title'),
  priority            : $('#priority'),
  weekly			        : $('#weeklyTop'),
  addBtn              : $('#add-btn'),
  saveBtn             : $('#save-btn'),
  deleteBtn           : $('#delete-btn'),
  enabled             : $('#enabled')
}


var options = {
  valueNames:[
    'brand',
    {data:['id']}
  ],
  item:'brand-list-template'
};
var brandList = new List('brands-section',options);

var templates = {
  brand:  $(".unecom-template .unecom-brands li")
};

//to view all brand details on page load
init();

form.saveBtn.click(function(){
  var formData = {};// = new FormData();
  var id = form.id.val().trim();
  var name = form.name.val().trim().trim();
  var priority = form.priority.val().trim().trim();

  if(!name.length){
    new PNotify({
      title: 'Empty Fields',
      text: 'All fields are required',
      type: 'error'
    });
    return false;
  }

  formData['title'] = name;
  formData['priority'] = priority;
  formData['enabled'] = form.enabled.is(':checked')?1:0;
  formData['weeklyTop'] = form.weekly.is(':checked')?1:0;
  $.ajax({
    url:uri+id,
    type:'PATCH',
    data:formData,
    success:function(data){
      if(data.error)
        return error(data.error);
      
      notify('Look Updated','Look successfully updated.','success');
      
      brandList.remove('id',data.id);
      brandList.add({id:data.id,brand:data.name});
      container.brands.find('li[data-id='+data.id+']').addClass('active');
      form.id.val(data.id);
      imageUploader.processQueue();
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
        var errors = jqXhr.responseJSON;
        $.each( errors , function( key, value ) {
          return error(value[0]);
        });
        return false;
      }
      return error("Contact website administrator.");
    }
  });
});

form.deleteBtn.click(function(){
  (new PNotify({
      title: 'Delete Confirmation',
      text: 'Are you sure?<br>This will delete every related details from database',
      icon: 'glyphicon glyphicon-question-sign',
      hide: false,
      hide: false,
      confirm: {
          confirm: true
      },
      addclass: 'stack-modal',
      stack: {
          'dir1': 'down',
          'dir2': 'right',
          'modal': true
      },
      buttons: {
          closer: false,
          sticker: false
      },
      history: {
          history: false
      },
      after_close: function(notice, timer_hide){
        $('.ui-pnotify-modal-overlay').remove();
      }
  })).get().on('pnotify.confirm', function(){
      var id = form.id.val();
      var response = $.ajax({url:uri+id,type:'DELETE'});
      response.fail(function(jqXhr){
        return error("");
      });
      response.done(function(data){
        console.log(data);
        if(data.error)
          return error(data.error);

        notify("Product Deleted","","success");
        deleteFromList(id);
      });
  })
});

form.addBtn.click(function(){
  var formData = {};
  
  var name = form.name.val().trim().trim();
  var priority = form.priority.val().trim().trim();
  if(!name.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }

  formData['title'] = name;
  formData['priority'] = priority;
  formData['enabled'] = form.enabled.is(':checked')?1:0;
  formData['weeklyTop'] = form.enabled.is(':checked')?1:0;
  console.log(formData);
  $.ajax({
    url:uri,
    type:'POST',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.error)
        return error(data.error);
      
      notify("Look added",'Look Added','success');
      brandList.remove('id',data.id);
      brandList.add({id:data.id,brand:data.name});
      form.id.val(data.id);
      imageUploader.processQueue();
      resetForm();
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          $.each( errors , function( key, value ) {
            return error(value[0]);
          });
          return false;
        }
        return error("");
    }
  });
});

function resetForm(){
    form.name.val("");
    form.enabled.prop("checked",false);
    form.weekly.prop("checked",false);
    form.saveBtn.addClass("hide");
    form.deleteBtn.addClass("hide");
    form.addBtn.removeClass("hide");
    return false;
}

$('body').on('click','ul.brands-collection li a',function(e){
  e.preventDefault();
  imageUploader.options.cleanUp();
  var parentLi = $(this).closest('li');
  if(parentLi.hasClass('active'))
    return false;
  $(this).closest('ul').find('li').removeClass('active');
  parentLi.addClass('active');
  console.log();
  var id = $(this).closest("li").attr("data-id");
  if(id=="add")
    return resetForm();

  form.addBtn.addClass("hide");
  form.saveBtn.removeClass("hide");
  form.deleteBtn.removeClass("hide");
  var url = uri+id;
  $.ajax({
    url:url,
    type:'GET',
    success:function(data){
      if(data.error)
        return error(data.error);

      form.id.val(data.id);
      form.name.val(data.title);
      form.priority.val(data.priority);
      form.enabled.prop('checked',data.enabled==1?true:false);
      form.weekly.prop('checked',data.weeklyTop==1?true:false);


      if(data.imageId != 0 || data.imageId != ''){
        var mockFile = {
        name: data.title,
        status: Dropzone.ADDED,
        url:'/image/'+data.imageId,
      };

      // Call the default addedfile event handler
      imageUploader.emit("addedfile", mockFile);
      imageUploader.createThumbnailFromUrl( mockFile, mockFile['url']);
      imageUploader.files.push(mockFile);
      }
    }
  });
});

function deleteFromList(id){
  brandList.remove('id',id);
  resetForm();
  var count = brandList.items.length;
  container.brandsTotal.html(count);
  if(!count)
    container.brandDetails.hide();
  container.brands.find('li:first-child a').trigger('click');
}

function init(){
  var response = $.ajax("/admin/api/look")
  response.done(function(data){
    if(data.error){
      return error(data.error);
    }else{
      $.each(data,function(i,product){
        brandList.add({id:product.id,brand:product.title});
      });
      container.brandsTotal.html(brandList.items.length-1);
      container.brands.find('li:first-child a').trigger('click');
    }
  });
}


/***********************************************************************************
***************************************Image Processing****************************/
Dropzone.autoDiscover = false;

var imageUploader = new Dropzone('div#brand-image',{ url: uri+'image',maxFiles:1,paramName:'image',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:false,createImageThumbnails:true,acceptedFiles:'image/png,image/jpeg'});
imageUploader.options.cleaningUp = false;
imageUploader.options.cleanUp = function(){
  this.cleaningUp = true;
  imageUploader.removeAllFiles();
  this.cleaningUp = false;
}


imageUploader.on('success',function(file,image){
  if(image.error)
    return error(image.error);
});

imageUploader.on('removedfile',function(file){
  
  if(imageUploader.options.cleaningUp)
    return false;

  var response = $.ajax({url:uri+form.id.val()+'/image',type:'DELETE'});
  response.done(function(data){
    if(data.error)
      return error(data.error);
  });

});

imageUploader.on('sending', function(file, xhr, formData){
  var token = $("meta[name='csrf-token']").attr('content');
  formData.append('_token',token);
  formData.append('id',form.id.val());
});

imageUploader.on('error',function(file,errorMessage,jqXhr=null){

  if(jqXhr == null){
    if(errorMessage == "You can not upload any more files."){
      imageUploader.options.cleaningUp = true;
      imageUploader.removeFile(file);
      imageUploader.options.cleaningUp = false;
      error("Multiple file not allowed.");
    }
    return false;
  }

  if(jqXhr.status == 422){
    $.each(errorMessage.image,function(i,v){
      error(v);
    });
  }
});

