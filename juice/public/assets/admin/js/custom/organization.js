
var form = {
	name : $("#name"),
	gst : $("#gst"),
	address : $("#address"),
	saveBtn: $('#save-btn'),
}

var uri ='/admin/api/organisation/';


form.saveBtn.click(function(event){
  event.stopPropagation();
  var formData = $("#organization").serializeArray();
  $.ajax({
    url:uri,
    type:"PATCH",
    data:formData,
    success:function(data){
      console.log(data);
      if(data.error){
       	notify('Something went wrong',data.error,'error');
      	return false;
      }
      notify('Updated','Company Details Updated','success');
      imageUploader.processQueue();
    },
    error:function(jqXhr){

      if(jqXhr.status == 422){
        var errors = jqXhr.responseJSON;
        $.each( errors , function( key, value ) {
          notify('Errors',v[0],'error');
        });
      	return false;
      }
    }
  });
  return false;
});
	///DROPZONE
	Dropzone.autoDiscover = false;
	var imageUploader = new Dropzone('div#brand-image',{ url: uri+'logo',maxFiles:1000,parallelUploads:1000,paramName:'image',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:false,createImageThumbnails:true,acceptedFiles:'image/png,image/jpeg'});

	imageUploader.on('success',function(file,image){
		if(image.error){
			notify('Something went wrong',image.error,'error');
			return false;
		}
	});

	imageUploader.on('sending', function(file, xhr, formData){
	  	var token = $("meta[name='csrf-token']").attr('content');
	  	formData.append('_token',token);
	});

	imageUploader.options.cleaningUp = false;
		imageUploader.options.cleanUp = function(){
		  this.cleaningUp = true;
		  imageUploader.removeAllFiles();
		  this.cleaningUp = false;
	}

	function init(){
		imageUploader.options.cleanUp();
		var response = $.ajax(uri);
		response.done(function(data){
		   	if(data.error){
				new PNotify({
				    title: 'Something went wrong',
				    text:  data.error,
				    type: 'error',
				});
				return false;
			}

			form.name.val(data.name);
			form.address.val(data.address);
			form.gst.val(data.gstCode);

			var mockFile = {
			    name: "Company logo", 
			    size: 500,
			    status: Dropzone.ADDED,
			    url:'/admin/image/'+data.logo,
			};
		  
		  	imageUploader.emit("addedfile", mockFile);
		  	imageUploader.createThumbnailFromUrl(mockFile, mockFile['url']);
		  	imageUploader.files.push(mockFile);

		  	imageUploader.on('removedfile',function(file){
		  		if(imageUploader.options.cleaningUp)
		  			return false;
			    var response = $.ajax({url: uri+'logo',type:'DELETE'});
			    response.done(function(data){
			      console.log(data);
			    });
			});
		});
	}

	init();
