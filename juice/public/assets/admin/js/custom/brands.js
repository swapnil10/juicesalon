var options = {
	valueNames: [
		'brand',
		{data:['id']}
	],
	item:'brand-list-template'
};
var brandList = new List('brands-section',options);

var container = {
	brands: 			$("ul.brands-collection"),
	brandsAssoc: 		$("ul.brands-associated"),
	rightPanel: 		$(".right-panel"),
	brandsTotal: 		$('.total-brands'),
	brandAddBtnGroup: 	$('.brand-add-btn-group'),
	brandEditBtnGroup: 	$('.brand-edit-btn-group'),
}

var form = {
	addBtn: 			$('#add-btn'),
	saveBtn: 			$('#save-btn'),
	saveBrandAssoc: 	$('#save-brand-assoc'),
	brandAssoc: 		$("#brand-assoc-form"),
}

var templates = {
	brand: 			$(".unecom-template .unecom-brands li#brand-list-template"),
	brandAssoc: 	$(".unecom-template .unecom-brands li#brand-associated-template"),
};

//to view all brand details on page load
init();

brandList.on("updated",function(){
	container.brandsTotal.html($('.brands-collection li').length);
});

$('body').on('click','ul.brands-collection li a',function(e){
	e.preventDefault();
	var parentLi = $(this).closest('li');
	if(parentLi.hasClass('active'))
		return false;

	$(this).closest('ul').find('li').removeClass('active');
	parentLi.addClass('active');

	if(parentLi.hasClass('add-list-ui')){
		$('#code').removeAttr('disabled');	
		document.getElementById("brand").reset();
		
		container.brandAddBtnGroup.removeClass('hide');
		container.brandEditBtnGroup.addClass('hide');
		container.rightPanel.addClass('hide');
	}else{
		$(brandList.items[0].elm).removeClass('active');
		$('#code').attr('disabled','disabled');
		container.brandEditBtnGroup.removeClass('hide');
		container.brandAddBtnGroup.addClass('hide');

		var id = parentLi.attr('data-id');
		var url = '/admin/api/brands/'+id;
		var response = $.get(url);
		response.done(function(data){
			var brand = data;
			if(data.error){
				new PNotify({
				    title: 'Something went wrong',
				    text:  data.error,
				    type: 'error',
				});
				return false;
			}else{
				$("#title").val(brand.title);
				$("#title").attr("data-id",brand.id);
				$("#categoryId").val(brand.categoryId).trigger('change');
				$("#enabled").prop('checked',brand.enabled == '1'?true:false);
			}
		});
	}
});



form.addBtn.click(function(event){
	event.stopPropagation();

	var url = '/admin/api/brands';
	var data = $('#brand').serializeArray();
	console.log(data);
	delete data['enabled'];
	if($('#enabled').prop('checked')){
		data.push({name:'enabled',value:1});
	}else{
		data.push({name:'enabled',value:0});
	}
	$("#error").fadeOut();
	var response = $.ajax({url:url,type:"POST",data:data});
	response.fail(function(jqXhr){
		if(jqXhr.status == 422){
	        var errors = jqXhr.responseJSON;
	        console.log(errors);
	        $.each( errors , function( key, value ) {
	          new PNotify({
	            title: 'Something went wrong',
	            text:  value[0],
	            type: 'error',
	          });
	        });
      		return false;
      	}
      new PNotify({
        title:'Error Code : '+jqXhr.status,
        text: 'Something went wrong',
        type:'error'
      });
      return false;
	});
	response.done(function(data){
		if(data.error){
			console.log(data);
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
			
		new PNotify({
			title: 'Success',
			text: 'Brand is successfully added.',
			type: 'success',
		});
 		brandList.add({id:data.id,brand:data.title});
 		container.brandsTotal.html(brandList.items.length-1);
	});
	return false;
});

form.saveBtn.click(function(event){
	event.stopPropagation();
	var id = $("#title").attr("data-id");
	var data = $('#brand').serializeArray();
	delete data['enabled'];
	if($('#enabled').prop('checked')){
		data.push({name:'enabled',value:1});
	}else{
		data.push({name:'enabled',value:0});
	}
	var url = '/admin/api/brands/'+id;

	var response = $.ajax({url:url,type:"PATCH",data:data});
	
	response.done(function(data){
		if(data.error){
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
		new PNotify({
			title:'Brand Updated Successesfully',
			text:'Brand Updated',
			type:'success'
		});

	});
 
	return false;
});


function init(nextUrl=''){
	var response = $.ajax('/admin/api/brands'+nextUrl);
	response.done(function(data){
	   	if(data.error){
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
			return false;
		}
		$.each(data,function(i,brand){
			brandList.add({id:brand.id,brand:brand.title});
		});
		/*container.brandsTotal.html(data.brands.length);*/
		container.brandsTotal.html(brandList.items.length-1);

		if(typeof data.next !== 'undefined')
			init(data.next);
	});

	categoryInit();
}

function categoryInit(){
	var response = $.get('/admin/api/category');
	response.done(function(data){
		$template = '<option value="" disabled selected>Choose Category</option>';
		$.each(data,function(i,category){
			$template += '<option value="'+category.id+'">'+category.title+'</option>';	
		});
		$("#categoryId").append($template);
		$("#categoryId").select2();
	});
}