$('select').not('.unecom-template select').select2();
 var flag = 0;
 var form = {
  title                 : $("#unecom-name"),
  description           : $("#unecom-description"),
  category              : $("#unecom-category"),
  base_price            : $("#base_price"),
  productType           : $("#unecom-product_type"),
  weight                : $("#weight"),
  variants              : $('#unecom-variants'),
  addBtn                : $('#add-btn'),
  resetBtn              : $('#reset-btn'),
  enabled               : $('#enabled'),
  image                 : $('img.brand-image'),
  imageIcon             : $('.brand-image-icon'),
  imageDescription      : $('#image-description'),
};
var currProduct = 0;
var container = {
  variant               : $('.variants-collection'),
  imageBox              : $('.img-box'),
  images                : $('.img-container'),
}

var templates = {
  variant               : $(".unecom-template .variant-template .variant-container")
};
function variantsReset(){
	container.variant.html('');
	$('.variant-boxs').find('.variant-heading').each(function(i,v){
		v.remove();
	});
	$('.variant-boxs').find('.variant-theme').each(function(i,v){
		v.remove();
	});
	$('.variant-boxs').addClass('hide');
	$('.variant-button').show();
}
form.addBtn.click(function(){
  var title = form.title.val().trim();
  var base_price = form.base_price.val().trim();
  var category = form.category.find('option:selected').val().trim();
  var product_type = form.productType.find('option:selected').val().trim();
  var weight = form.weight.val().trim();
  var description = form.description.val().trim();
  //var desc =  form.description.data("wysihtml5").editor;
   if(!title.length || !base_price.length || category=='none' || product_type=='none' || !description.length){
    new PNotify({
        title: 'All fields required',
        text:  'Some of the fields are empty. Please fill them',
        type: 'error',
    });
    return false;
  }

  var formData = new FormData();

  formData.append('title',title);
  formData.append('category',category);
  formData.append('base_price',base_price);
  formData.append('product_type',product_type);
  formData.append('weight',weight);
  formData.append('description',description);
  formData.append('enabled',form.enabled.is(':checked')?1:0);
  $.ajax({
    url:'/admin/api/products/add',
    type:'POST',
    data:formData,
    processData:false,
    contentType:false,
    success:function(data){
      console.log(data)
      if(data.error){

        new PNotify({
          title: 'Failed to add product',
          text:  data.error,
          type: 'error',
        });
        return false;
      }
      product_sucess_process();
     /***/
     currProduct = data.id;
      imageUploader.processQueue();
     if(flag == 0){
     	  variantsReset();
        return false;
    }
      var variants = {};
      var i = 0;
      $('.variants-collection .variant-container').each(function(){
        $(this).find('form').each(function(){
          var variant = {};
          $.each($(this).find('input'),function(){
            variant[$(this).attr('name')] = $(this).val();
          });
          variants[i++] = variant;
        });
      });
      var variantResponse = addProductVariants(data.id,variants);
      variantResponse.done(function(data){
      	variantsReset();
        console.log(data);
        if(data.error){
          new PNotify({
          title: 'Failed to add product variants',
          text:  data.error,
          type: 'error',
          });
          return false;
        }
        new PNotify({
          title:'Variants was successfully added.',
          text: 'Product Variants was successfully save.',
          type:'success'
        });

      });
      variantResponse.fail(function(jqXhr){
      	variantsReset();
        if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          $.each( errors , function( key, value ) {
            console.log(value);
            new PNotify({
              title: 'Something went wrong',
              text:  value[0],
              type: 'error',
            });
          });
          return false;
        }
        new PNotify({
          title: 'Something went wrong',
          text:  "Error Code: "+jqXhr.status,
          type: 'error',
        });
      });
      /***/
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
        var errors = jqXhr.responseJSON;
        console.log(errors);
        $.each( errors , function( key, value ) {
          new PNotify({
            title: 'Something went wrong',
            text:  value[0],
            type: 'error',
          });
        });
      return false;
      }
      new PNotify({
        title:'Error Code : '+jqXhr.status,
        text: 'Something went wrong',
        type:'error'
      });
    }
  });
});

function addProductVariants(pid,variants){
  console.log(JSON.stringify(variants));
  return $.ajax({
    url:'/admin/api/products/'+pid+'/variants',
    type:'POST',
    data:JSON.stringify(variants),
    contentType: "application/json"
  });
}
function addVariantTemplate(variantName,i){
  $('.variant-boxs').removeClass("hide");
  $('#variantText .variant-heading').clone().appendTo('.variant-boxs').find('div > span').html("Add "+variantName);
  $('#variantText .variant-theme').clone().appendTo('.variant-boxs').attr('id',variantName);
  $('#'+variantName +' > .unecom-variants').tagsInput({
    height:'34px',
    width:'100%',
    interactive:true,
    onAddTag:updateTags,
    onRemoveTag:updateTags
  });
}
function updateTags(){
  var arr = [];
  var arrVal = [];
  var temp=1;
  var variant = $(".variant-boxs > .variant-theme > input[type=text]").each(function(){
    arr.push($(this).val());
  });
  $.each(arr,function(key,valuee){
    arrVal.push(valuee.split(','));
  });
  var variant1 = [],variant2= [],variant3=[];
  if(arrVal[0]){
    $.each(arrVal[0],function(iv,vi){
      variant1.push(vi);
    });
  }
  if(arrVal[1]){
    $.each(arrVal[1],function(iv,vi){
      variant2.push(vi);
    });
  }
  if(arrVal[2]){
    $.each(arrVal[2],function(iv,vi){
      variant3.push(vi);
    });
  }

  $(".variants-collection").html("");
  var tag1="",tag2="",tag3="";
  for(var i=0;i<variant1.length;i++){
  	tag1="";
  	if(variant1[i])
  		tag1=variant1[i];
    if(!variant2.length){
      var copy = templates.variant.clone().appendTo(".variants-collection");
      copy.find("form").data("form",i);
      $hiddenInput = "<input type='hidden' name='variant1' value='"+variant1[i]+"' /><input type='hidden' name='variant2' value='' /><input type='hidden' name='variant3' value=''/>";
      copy.find(".variant").html(tag1+$hiddenInput);
    }
  	for(var j=0;j<variant2.length;j++){
  		tag2="";
  		if(variant2[j])
  			if(tag1)
  				tag2=tag1+"+"+variant2[j];
  			else
  				tag2=variant2[j];
  		else
  			tag2=tag1;
      if(!variant3.length){
        var copy = templates.variant.clone().appendTo(".variants-collection");
        copy.find("form").data("form",i);
        $hiddenInput = "<input type='hidden' name='variant1' value='"+variant1[i]+"' /><input type='hidden' name='variant2' value='"+variant2[j]+"' /><input type='hidden' name='variant3' value=''/>";
        copy.find(".variant").html(tag2+$hiddenInput);
      }
  		for(var k=0;k<variant3.length;k++){
  			tag3="";
  			if(variant3[k])
  				if(tag2)
  					tag3=tag2+"+"+variant3[k];
  				else
  					tag3=variant3[k];
  			else
  				tag3=tag2;
  			if(tag3){
  				var copy = templates.variant.clone().appendTo(".variants-collection");
  				copy.find("form").data("form",i);
          $hiddenInput = "<input type='hidden' name='variant1' value='"+variant1[i]+"' /><input type='hidden' name='variant2' value='"+variant2[j]+"' /><input type='hidden' name='variant3' value='"+variant3[k]+"' />";
  				copy.find(".variant").html(tag3+$hiddenInput);
  			}
  		}
  	}
  }
return false;
}
function addVariant(){
  $.ajax({
    url:'/admin/api/variants',
    type:"GET",
    success:function(data){
      if(data.error){
        console.log(data.error);
        return false;
      }
      $.each(data,function(key,value){
        if(value.enabled == 0)
          return true;
        addVariantTemplate(value.name,key+1);
      });
    }
  });
}
function product_sucess_process(){
  new PNotify({
    'title':'Product was added.',
    'text': 'Product was added.',
    'type':'success'
  });
  form.title.val('');
  form.productType.select2('val','none');
  form.category.select2('val','none');
  form.base_price.val('');
  form.description.data('wysihtml5').editor.setValue('');
  //form.description.val('');
}
// $(document).ready(function(){
//    $('#add_variant')[0].trigger('click'); 
// });
// $(document).on('click','#add_variant',function(){
//   console.log("Add Variant");
//   $(".variant-button").hide();
//   addVariant();
//   flag = 1;
//   return false;
// });
/***********Add Variant Trigger replace*************/
$(".variant-button").hide();
  addVariant();
  flag = 1;
/***********\Add ***********************************/
$.ajax({
  url:"/admin/api/category",
  type:"GET",
  success:function(data){
    form.category.html("<option value='none' disabled selected>Select Product Category</option>");
    $.each(data,function(i,category){
      console.log(category.id+category.title+category.parent_id);
      var option = $("<option>").attr("value",category.id).html(/*addSpace(category.level)+*/category.title);
      if(category.parent_id==0){
        form.category.append(option);
      }else{
        form.category.find("option[value="+category.parent_id+"]").addClass('strong-text').after(option);   
      }
    });
  }
});
form.category.change(function(){
  var catId = $(this).find('option:selected').val();
  var response = $.get('/admin/api/association/'+catId);
  response.done(function(data){
    console.log(data);
    if(data.error){
      console.log(data.error);
      return false;
    }
    // if(data.error){
    //   new PNotify({
    //     title:'Something went wrong.',
    //     text:data.error,
    //     type:'error'
    //   });
    //   return false;
    // }
    if(!data.groupBrands.length){
      form.productType.select2('destroy');
      form.productType.html('');
      form.productType.html("<option value='none' disabled selected>No Product Type Found.");
      form.productType.select2();
      return false;
    }
    form.productType.select2('destroy');
    form.productType.html('');
    form.productType.html("<option value='none' disabled selected>Select Product Type</option>");
    $.each(data.groupBrands,function(i,type){
      if(type.enabled == 0)
        return true;
      console.log(type.id+type.title);
      var option = $("<option>").attr("value",type.id).html(type.title);
      form.productType.append(option);
    });
    form.productType.select2();
    return false;
  });
});
/***********************************************************************************
***************************************Image Processing****************************/
Dropzone.autoDiscover = false;
var imageUploader = new Dropzone('div#brand-image',{ url: "/admin/api/image",maxFiles:5,parallelUploads:5,paramName:'image',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:true,createImageThumbnails:true,acceptedFiles:'image/png,image/jpeg'});

imageUploader.options.cleaningUp = false;
imageUploader.options.cleanUp = function(){
  this.cleaningUp = true;
  imageUploader.removeAllFiles();
  this.cleaningUp = false;
}

// imageUploader.on('success',function(file,image){
//   console.log(image);
//   if(image.error){
//     new PNotify({
//       title:'Something went wrong',
//       text:image.error,
//       type:'error',
//     });
//     return false;
//   }
//   var url = "/admin/api/image/"+currProduct+'/'+image.id;
//   console.log(url);
//   $.ajax({
//     url:url,
//     type:'POST',
//     data:{image:image.id},
//     success:function(data){
//       console.log(data);
//       if(data.error){
//         new PNotify({
//             title: 'Couldn\'t upload image',
//             text:  data.error,
//             type: 'error',
//         });
//       }
//     },
//     error:function(req,xhr){
//       console.log(xhr.status);
//     }
//   });
// });
imageUploader.on('successmultiple',function(file,image){
  console.log(image);
  $.each(image.id,function(i,v){
    var url = "/admin/api/image/"+currProduct+'/'+v;
    console.log(url);
    $.ajax({
      url:url,
      type:'POST',
      data:{image:image.id},
      success:function(data){
        console.log(data);
      },
      error:function(req,xhr){
        console.log(xhr.status);
      }
    });
  });
});
imageUploader.on('sending', function(file, xhr, formData){
  //console.log($("meta[name='csrf-token']").attr('content'));
  var token = $("meta[name='csrf-token']").attr('content');
  formData.append('_token',token);
});
imageUploader.on('queuecomplete',function(){
  imageUploader.removeAllFiles();
});

// imageUploader.on('removedfile',function(file){
//   if(imageUploader.options.cleaningUp || typeof file.id == 'undefined')
//     return false;

//   var response = sendAjax('/admin/api/products/'+form.id.val()+'/images/'+file.id,'DELETE');
//   response.done(function(data){
//     console.log(data);
//   });
// });

imageUploader.on('error',function(file,errorMessage,jqXhr){
  console.log(jqXhr.status);
  console.log(errorMessage);
  if(jqXhr.status == 422){
    $.each(errorMessage.image,function(i,v){
      new PNotify({
        title:'Something went wrong',
        text:v,
        type:'error'
      });
    });
  }
});


// $.ajax({
//   url:"/admin/api/product-type",
//   type:"GET",
//   success:function(data){
//     form.productType.html("<option value='none' disabled selected>Select Product Type</option>");
//     $.each(data,function(i,type){
// 		if(type.enabled == 0)
// 			return true;
//       console.log(type.id+type.title);
//       var option = $("<option>").attr("value",type.id).html(type.title);
//       form.productType.append(option);
//     });
//   }
// });
