var categoryContainer = $(".category-list");

//to view all categories
var response = $.get(apiadd+'/category');
response.done(function(data){
	if(data.error){
		new PNotify({
				title: 'Error',
				text: data.error,
				type: 'error',
		});
		return false;
	}
	for (var i = 0; i < 5; i++) {
		$.each(data,function(i, category){
			addCategory(category,categoryContainer);
		});
	}
	
});

//to add category
$("#add").on('click',function(){  
	var categoryName = $("#name");
	var url = apiadd+'/category/add';
	var response = $.ajax({url:url,data:{title:categoryName.val()},type:"POST"});
	response.done(function(data){
		if(data.error){
			console.log(data);
			new PNotify({
				title: 'Category',
				text: data.error,
				type: 'Error',
			});
			return false;
		}
		data.parentId = 0;
		categoryName.val('');
		new PNotify({
			title: 'Category',
			text: 'Category Successfully Added',
			type: 'success',
		});
		addCategory(data,categoryContainer);
	});

	response.error(function(jqXhr){
			console.log(jqXhr.status);
			if(jqXhr.status == 422){
				var errors = jqXhr.responseJSON;
				$.each( errors , function( key, value ) {
					new PNotify({
				    title: 'Something went wrong',
				    text:  value[0],
				    type: 'error',
					});
				});
			return false;
			}
		});
	return false;
});

//to update category
$("#update-btn").click(function(){
	var id = $(this).attr('data-id');
	var name = $("#catName").val();
	var url=apiadd+"/category/"+id;
	var response = $.ajax({url:url,type:"PATCH",data:{title:name}});
	response.done(function(data){
		if(data.error){
			console.log(data);
			new PNotify({
				title: 'Category',
				text: data.error,
				type: 'error',
			});
		}else{
			new PNotify({
				title: 'Category',
				text: 'Category Successfully Updated',
				type: 'success',
			});
			$('.modal .close').trigger('click');
			$('.dd-item[data-id='+id+']>.dd-handle').html(name);
		}
	});

});
//to open modal
$(document).on('click','.dd-edit',function(){
	var id = $(this).closest('li').attr('data-id');
	var	url=apiadd+'/category/'+id;
	var response = $.ajax({url:url,type:'GET'});
	response.done(function(data){
		if(!data.error){
			$('.modal').show();
			$('#catName').val(data.title);
			$('#update-btn').attr('data-id',data.id);
		}
		else
			alert("Please try again later!");
	});

});


function addCategory(category,categoryContainer){
	//console.log(category.parentId);
	var a = $(".category-list").find("li.dd-item[data-id='"+category.id+"']").length;

	if(a > 0)
		return false;

	var categoryList = $("<li class='dd-item'>");
	categoryList.attr('data-id',category.id);
	
	var editBtn = $('<span class="dd-edit">');
	var deleteBtn = $('<span class="dd-delete">');
	// deleteBtn.html('Delete');

	deleteBtn.click(function(e){
		e.stopPropagation();
		var categoryToDelete = $(this).closest('.dd-item');
		
		deleteCategory(categoryToDelete);
	});

	categoryList.html($("<div class='dd-handle'>").html(category.title));
	categoryList.append(deleteBtn).append(editBtn);
	
	if(category.parentId != 0){
		var childContainer = categoryContainer.find('li[data-id="'+category.parentId+'"]');
		if(!childContainer.find('.dd-list').length){
			childContainer.append($('<ol class="dd-list">'));
		}
		
		childContainer.find('ol').append(categoryList);
	}
	else	
		categoryContainer.append(categoryList);
}

function deleteCategory(category){
	console.log("delete function called")
	if(!confirm("Are you sure to delete?"))return false;
	var url = apiadd+'/category/'+category.attr('data-id');
	var response = $.ajax({url:url,type:'DELETE'});
	response.done(function(data){
		console.log(data);
		if(data.error){
			new PNotify({
				title: 'Category',
				text: data.error,
				type: 'error',
			});
			btn.closest("tr").remove();
		}
		else{
			new PNotify({
				title: 'Category',
				text: 'Category Successfully Deleted',
				type: 'success',
			});
		}
		category.remove();
	});
}
$(document).on('click','.close',function(){
	$('.modal').hide();
});

$('.dd').nestable({group:1,maxDepth:5});

$(document).on('change','.dd',function(){
	var categoryList = $(this).nestable('serialize');
	console.log(categoryList);
	updateCategoryParent(categoryList);
});

function updateCategoryParent(categoryList,parentId=0){
	$.each(categoryList,function(i,category){
		var url = apiadd+'/category/'+category.id+'/parent/'+parentId;
		console.log(url);
		$.ajax({url:url,type:'PATCH'}).error(function(req,err){
			console.log(req);
		}).done(function(data){
			console.log(data);
		});
		console.log(category.children);
		if(category.children){
			updateCategoryParent(category.children,category.id);
		}
	});
}
