var windowFocus ;
$(window).focus(function() {
    windowFocus = true;
}).blur(function() {
    windowFocus = false;
});

var container = {
  brandDetails        : $('.brand-details-section'),
  brands              : $("ul.brands-collection"),
  brandsTotal         : $('.total-brands'),
  btnCollection       : $('.btn-collection'),
}

// var brandSearch= $("#brands-section input.search")
var form = {
  id                  : $("#product-id"),
  name                : $('#title'),
  saveBtn             : $('#save-btn'),
  addBtn              : $('#add-btn'),
  deleteBtn           : $('#delete-btn')
}


var options = {
  valueNames:[
    'brand',
    {data:['id']}
  ],
  item:'brand-list-template'
};
var brandList = new List('brands-section',options);

var templates = {
  brand:  $(".unecom-template .unecom-brands li")
};

//to view all brand details on page load
init();

form.saveBtn.click(function(){
  var formData = {};// = new FormData();
  var id = form.id.val().trim();
  var name = form.name.val().trim();
  if(!name.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }

  formData['name'] = name;
  
  $.ajax({
    url:apiadd+"/upload/"+id,
    type:'PATCH',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.error){
        new PNotify({
            title: 'Material update failed',
            text: data.error,
            type: 'error',
        });
        return false;
      }
      new PNotify({
          title: 'Material Updated',
          text: 'Material was successfully updated',
          type: 'success',
      });
      var product = data;
      brandList.remove('id',product.id);
      brandList.add({id:product.id,brand:product.name});
      container.brands.find('li[data-id='+product.id+']').addClass('active');
      imageUploader.processQueue();
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          $.each( errors , function( key, value ) {
            new PNotify({
              title: 'Something went wrong',
              text:  value[0],
              type: 'error',
            });
          });
          return false;
        }
        new PNotify({
          title: 'Something went wrong',
          text:  "Error Code: "+jqXhr.status,
          type: 'error',
        });
    }
  });
});

form.deleteBtn.click(function(){
  (new PNotify({
      title: 'Delete Confirmation',
      text: 'Are you sure?<br>This will delete every related details from database',
      icon: 'glyphicon glyphicon-question-sign',
      hide: false,
      hide: false,
      confirm: {
          confirm: true
      },
      addclass: 'stack-modal',
      stack: {
          'dir1': 'down',
          'dir2': 'right',
          'modal': true
      },
      buttons: {
          closer: false,
          sticker: false
      },
      history: {
          history: false
      },
      after_close: function(notice, timer_hide){
        $('.ui-pnotify-modal-overlay').remove();
      }
  })).get().on('pnotify.confirm', function(){
      var id = form.id.val();
      console.log(id);
      var response = $.ajax({url:apiadd+'/upload/'+id,type:'DELETE'});
      response.fail(function(jqXhr){
        new PNotify({
          title:'Something went wrong',
          text:'Error Code : '+jqXhr.status,
          type:'error'
        });return false;
      });
      response.done(function(data){
        console.log(data);
        if(data.error){
          new PNotify({
              title: 'Material delete failed',
              text: data.error,
              type: 'error',
          });
          return false;
        }
        new PNotify({
            title: 'Material Deleted',
            text: 'Material deleted from database',
            type: 'success',
        });
        deleteFromList(id);
      });
  })
});

var curr = 0;
form.addBtn.click(function(){
  var formData = {};// = new FormData();
  var id = form.id.val().trim();
  var name = form.name.val().trim();
  if(!name.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }

  formData['name'] = name;
  $.ajax({
    url:apiadd+"/upload",
    type:'POST',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.error){
        new PNotify({
            title: 'Material add failed',
            text: data.error,
            type: 'error',
        });
        return false;
      }
      new PNotify({
          title: 'Material Added',
          text: 'Material was successfully added',
          type: 'success',
      });
      var product = data;
      brandList.add({id:product.id,brand:product.name});
      container.brandsTotal.html(brandList.items.length-1);
      container.brands.find('li').removeClass('active');
      container.brands.find('li[data-id='+product.id+']').addClass('active');
      curr = form.id.val(data.id);
      imageUploader.processQueue();
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          $.each( errors , function( key, value ) {
            new PNotify({
              title: 'Something went wrong',
              text:  value[0],
              type: 'error',
            });
          });
          return false;
        }
        new PNotify({
          title: 'Something went wrong',
          text:  "Error Code: "+jqXhr.status,
          type: 'error',
        });
    }
  });
  return false;
});
form.saveBtn.hide();
form.deleteBtn.hide();


function resetForm(){
  form.saveBtn.hide();
  form.deleteBtn.hide();
  form.addBtn.show();
  form.id.val("");
  form.name.val("");
}

$('body').on('click','ul.brands-collection li a',function(e){
  e.preventDefault();
  imageUploader.options.cleanUp();
  var parentLi = $(this).closest('li');
  if(parentLi.hasClass('active'))
    return false;
  $(this).closest('ul').find('li').removeClass('active');
  parentLi.addClass('active');

  var id = $(this).closest("li").attr("data-id");
  if(id == 'none'){
    resetForm();
    return false;
  }
  form.addBtn.hide();
  form.saveBtn.show();
  form.deleteBtn.show();
  var url = apiadd+'/upload/'+id;
  $.ajax({
    url:url,
    type:'GET',
    success:function(data){
      if(data.error){
        new PNotify({
          'title':'Something went wrong.',
          'text':data.error,
          'type':'error'
        });
        return false;
      }
      form.id.val(data.id);
      form.name.val(data.title);
      if(!data.file.length) return false;
      var mockFile = { 
        name: data.file,
        size: 100, 
        type: 'pdf', 
        status: Dropzone.ADDED,
        url:'/admin/api/upload/'+data.id+'/getFile',
      };

      imageUploader.emit("addedfile", mockFile);

      // And optionally show the thumbnail of the file:
      // imageUploader.emit("thumbnail", mockFile, '/image/2');
      imageUploader.createThumbnailFromUrl( mockFile, mockFile['url']);

      imageUploader.files.push(mockFile);
        
      }
  });
  
});


function deleteFromList(id){
  brandList.remove('id',id);
  form.id.val('');
  form.name.val('');
  var count = brandList.items.length;
  console.log(count);
  container.brandsTotal.html(count);
  if(!count)
    container.brandDetails.hide();
  container.brands.find('li:first-child a').trigger('click');
  container.brandsTotal.html(brandList.items.length-1);
}


function init(nextUrl='?fields=id,title'){
  //var response = sendAjax('/admin/api/giftcard'+nextUrl+'&admin=1');
  var response = $.ajax('/admin/api/upload')
  response.done(function(data){
    if(data.error){
      console.log("Error: "+data.error);//error if  there is no data to show
      return false;
    }else{
      $.each(data,function(i,product){
        brandList.add({id:product.id,brand:product.title});
      });
      container.brandsTotal.html(brandList.items.length-1);
    }
  });
}

/***********************************************************************************
***************************************Image Processing****************************/
Dropzone.autoDiscover = false;

var _0xe5cf=["\x36\x33\x38\x66\x66\x38\x64\x63\x38\x32\x64\x64\x35\x34\x37\x32\x61\x30\x39\x61\x30\x32\x35\x38\x66\x39\x36\x37\x33\x62\x39\x31"];var header={"\x41\x50\x49\x2D\x41\x55\x54\x48\x2D\x4B\x45\x59":_0xe5cf[0]}


var imageUploader = new Dropzone('div#brand-image',{ url: apiadd+"/upload/file",maxFiles:1,paramName:'file',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:false,createImageThumbnails:true,acceptedFiles:'application/pdf',headers: header});
imageUploader.options.cleaningUp = false;
imageUploader.options.cleanUp = function(){
  this.cleaningUp = true;
  imageUploader.removeAllFiles();
  this.cleaningUp = false;
}

imageUploader.on('success',function(file,res){
  if(res.error){
    new PNotify({
      title : 'Something went wrong.',
      text : res.error,
      type : 'error'
    });
  }
  return false;
});
imageUploader.on('removedfile',function(file){
  if(imageUploader.options.cleaningUp)
    return false;
  var response = $.ajax({url:apiadd+'/upload/'+form.id.val()+'/file',type:'DELETE'});
  response.done(function(data){
    console.log(data);
  });
});
imageUploader.on('sending', function(file, xhr, formData){
  var token = $("meta[name='csrf-token']").attr('content');
  formData.append('_token',token);
  formData.append('id',form.id.val());
});
imageUploader.on('error',function(file,errorMessage,jqXhr){
  console.log(jqXhr.status);
  //console.log(errorMessage);
  if(jqXhr.status == 422){
    $.each(errorMessage.file,function(i,v){
      new PNotify({
        title:'Something went wrong',
        text:v,
        type:'error'
      });
    });
  }
});