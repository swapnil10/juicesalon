var windowFocus ;
$(window).focus(function() {
    windowFocus = true;
}).blur(function() {
    windowFocus = false;
});
var uri = '/admin/api/product/';
var container = {
  brandDetails        : $('.brand-details-section'),
  brands              : $("ul.brands-collection"),
  brandsTotal         : $('.total-brands')
}

// var brandSearch= $("#brands-section input.search")
var form = {
  id                  : $("#product-id"),
  name                : $('#title'),
  hsn                 : $('#hsn'),
  description         : $('#unecom-description'),
  variants            : $('#unecom-variants'),
  variant             : $('#unecom-variant'),
  base_price          : $('#unecom-base-price'),
  addBtn              : $('#add-btn'),
  saveBtn             : $('#save-btn'),
  deleteBtn           : $('#delete-btn'),
  enabled             : $('#enabled'),
  type                : $('#unecom-type'),
  tax                 : $('#tax'),
  webclip             : $("#webclip"),
  webclipBox          : $("#web-clip-box"),
  videoCategory       : $("#unecom-video-category"),
  videoCategoryBox    : $('#video-category-box')
}


var options = {
  valueNames:[
    'brand',
    {data:['id']}
  ],
  item:'brand-list-template'
};
var brandList = new List('brands-section',options);

var templates = {
  brand:  $(".unecom-template .unecom-brands li")
};

//to view all brand details on page load
init();

form.saveBtn.click(function(){
  var formData = {};// = new FormData();
  var id = form.id.val().trim();
  var name = form.name.val().trim().trim();
  var hsn = form.hsn.val().trim().trim();
  var price = form.base_price.val().trim();
  var description = form.description.val().trim();
  var type = form.type.find("option:selected").val();
  var tax = form.tax.find("option:selected").val();
  if(!name.length || !description.length || type=='0' || !price.length){
    new PNotify({
      title: 'Empty Fields',
      text: 'All fields are required',
      type: 'error'
    });
    return false;
  }

  formData['title'] = name;
  formData['hsn'] = hsn;
  formData['description'] = description;
  formData['type'] = type;
  formData['tax'] = tax;
  formData['price'] = price;
  formData['enabled'] = form.enabled.is(':checked')?1:0;
  if(type == 1){
    formData['webclip'] = form.webclip.val();
    formData['category'] = form.videoCategory.val();
  }
  $.ajax({
    url:uri+id,
    type:'PATCH',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.error)
        return error(data.error);
      
      notify('Product Updated','Product successfully updated.','success');
      
      brandList.remove('id',data.id);
      brandList.add({id:data.id,brand:data.name});
      container.brands.find('li[data-id='+data.id+']').addClass('active');
      form.id.val(data.id);
      imageUploader.processQueue();
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
        var errors = jqXhr.responseJSON;
        $.each( errors , function( key, value ) {
          return error(value[0]);
        });
        return false;
      }
      return error("Contact website administrator.");
    }
  });
});

form.deleteBtn.click(function(){
  (new PNotify({
      title: 'Delete Confirmation',
      text: 'Are you sure?<br>This will delete every related details from database',
      icon: 'glyphicon glyphicon-question-sign',
      hide: false,
      hide: false,
      confirm: {
          confirm: true
      },
      addclass: 'stack-modal',
      stack: {
          'dir1': 'down',
          'dir2': 'right',
          'modal': true
      },
      buttons: {
          closer: false,
          sticker: false
      },
      history: {
          history: false
      },
      after_close: function(notice, timer_hide){
        $('.ui-pnotify-modal-overlay').remove();
      }
  })).get().on('pnotify.confirm', function(){
      var id = form.id.val();
      var response = $.ajax({url:uri+id,type:'DELETE'});
      response.fail(function(jqXhr){
        return error("");
      });
      response.done(function(data){
        console.log(data);
        if(data.error)
          return error(data.error);

        notify("Product Deleted","","success");
        deleteFromList(id);
      });
  })
});

form.addBtn.click(function(){
  var formData = {};
  
  var name = form.name.val().trim().trim();
  var price = form.base_price.val().trim();
  var description = form.description.val().trim();
  var hsn = form.hsn.val().trim();
  var type = form.type.find("option:selected").val().trim();
  var tax = form.tax.find("option:selected").val().trim();
  if(!name.length || !description.length || type=='' || !price.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }

  formData['title'] = name;
  formData['hsn'] = hsn;
  formData['description'] = description;
  formData['type'] = type;
  formData['tax'] = tax;
  formData['price'] = price;
  formData['enabled'] = form.enabled.is(':checked')?1:0;
  if(type == 1){
    formData['webclip'] = form.webclip.val();
    formData['category'] = form.videoCategory.val();
  }
  
  $.ajax({
    url:"api/product",
    type:'POST',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.error)
        return error(data.error);
      
      
      notify("Product added",'Product Added','success');
      brandList.remove('id',data.id);
      brandList.add({id:data.id,brand:data.name});
      form.id.val(data.id);
      imageUploader.processQueue();
      resetForm();
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          $.each( errors , function( key, value ) {
            return error(value[0]);
          });
          return false;
        }
        return error("");
    }
  });
});

function resetForm(){
    form.name.val("");
    form.description.data('wysihtml5').editor.setValue("");
    form.base_price.val("");
    form.enabled.prop("checked",true);
    form.type.val('0').trigger('change');
    form.saveBtn.addClass("hide");
    form.deleteBtn.addClass("hide");
    form.addBtn.removeClass("hide");
    return false;
}

$('body').on('click','ul.brands-collection li a',function(e){
  e.preventDefault();
  imageUploader.options.cleanUp();
  var parentLi = $(this).closest('li');
  if(parentLi.hasClass('active'))
    return false;
  $(this).closest('ul').find('li').removeClass('active');
  parentLi.addClass('active');
  console.log();
  var id = $(this).closest("li").attr("data-id");
  if(id=="add")
    return resetForm();

  form.addBtn.addClass("hide");
  form.saveBtn.removeClass("hide");
  form.deleteBtn.removeClass("hide");
  var url = uri+id;
  $.ajax({
    url:url,
    type:'GET',
    success:function(data){
      console.log(data);
      if(data.error)
        return error(data.error);

      form.id.val(data.id);
      form.name.val(data.title);
      form.hsn.val(data.hsn);
      form.description.data('wysihtml5').editor.setValue(data.description);
      form.base_price.val(data.price);
      form.type.val(data.productTypeId).trigger('change');
      form.tax.val(data.tax).trigger('change');
      form.enabled.prop('checked',data.enabled==1?true:false);

      if(data.size != 0 || data.size != ''){
        var mockFile = {
        name: data.title, 
        size: data.size,
        status: Dropzone.ADDED,
        url:'/image/'+data.imageId,
      };

      // Call the default addedfile event handler
      imageUploader.emit("addedfile", mockFile);
      imageUploader.createThumbnailFromUrl( mockFile, mockFile['url']);
      imageUploader.files.push(mockFile);
      }
    }
  });
});

function deleteFromList(id){
  brandList.remove('id',id);
  resetForm();
  var count = brandList.items.length;
  container.brandsTotal.html(count);
  if(!count)
    container.brandDetails.hide();
  container.brands.find('li:first-child a').trigger('click');
}

function createSpace(len){
  var space = "";
  for(var i=0;i<=len;i++){
    space = space+"&nbsp;";
  }
  return space;
}

var ll = 1;
function pushVideoCategory(data){

  if(ll == 100)
    return false;

  if(!data.length)
    return false;

  
  var findIn = form.videoCategory;

  $.each(data,function(i,v){
    
    try{
    if(v.parentId == 0){
      findIn.append("<option value='"+v.id+"'>"+v.title+"</option>");
      data.splice(i,1);
      return true;
    }
    var search = findIn.find("option[value='"+v.parentId+"']");
    if(!search.length)
      return true;

    var getSpaceInstance  = search.html();
    var createSpaceLength = (getSpaceInstance.match(/&nbsp;/g) || []).length+5;
    var getSpace = createSpace(createSpaceLength);
    search.after("<option value='"+v.id+"'>"+getSpace+v.title+"</option>");
    data.splice(i,1);
    }catch(err){
      //console.log(err);
    }
  });

  if(!data.length)
    return false;
  ll++;
  pushVideoCategory(data);
}

function init(){

  $.get("/admin/api/productType",function(data){
    if(data.error)
      return error(data.error);
    form.type.select2('destroy');
    form.type.html("<option disabled='' selected='' value='0'>Select Product Type</option>");

    $.each(data,function(i,v){
      var a = "<option value='"+v.id+"'>"+v.title+"</option>";
      form.type.append(a);
    });
    form.type.niceSelect();
  });

  $.get("/admin/api/category",function(data){
    if(data.error)
      return error(data.error);
    form.videoCategory.select2('destroy');
    form.videoCategory.html("<option disabled='' selected='' value='0'>Select Video Category</option>");
    var dataArr = [];
    $.each(data,function(i,v){
      dataArr.push(v);
      // var a = "<option value='"+v.id+"'>"+v.title+"</option>";
      // form.videoCategory.append(a);
    });
    pushVideoCategory(dataArr);
    form.videoCategory.niceSelect();
  });

  var response = $.ajax('/admin/api/product')
  response.done(function(data){
    if(data.error){
      return error(data.error);
    }else{
      $.each(data,function(i,product){
        brandList.add({id:product.id,brand:product.title});
      });
      container.brandsTotal.html(brandList.items.length-1);
      container.brands.find('li:first-child a').trigger('click');
    }
  });
}

form.type.on('change',function(){
  var selected = form.type.find("option:selected").val();
  if (selected == 1){
    form.webclipBox.removeClass('hide');
    form.videoCategoryBox.removeClass('hide');
  }
  else{
    form.videoCategoryBox.addClass('hide');
    form.webclipBox.addClass('hide');
  }
});

/***********************************************************************************
***************************************Image Processing****************************/
Dropzone.autoDiscover = false;

var imageUploader = new Dropzone('div#brand-image',{ url: uri+'image',maxFiles:1,paramName:'image',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:false,createImageThumbnails:true,acceptedFiles:'image/png,image/jpeg'});
imageUploader.options.cleaningUp = false;
imageUploader.options.cleanUp = function(){
  this.cleaningUp = true;
  imageUploader.removeAllFiles();
  this.cleaningUp = false;
}


imageUploader.on('success',function(file,image){
  if(image.error)
    return error(image.error);
});

imageUploader.on('removedfile',function(file){
  
  if(imageUploader.options.cleaningUp)
    return false;

  var response = $.ajax({url:uri+form.id.val()+'/image',type:'DELETE'});
  response.done(function(data){
    if(data.error)
      return error(data.error);
  });

});

imageUploader.on('sending', function(file, xhr, formData){
  var token = $("meta[name='csrf-token']").attr('content');
  formData.append('_token',token);
  formData.append('id',form.id.val());
});

imageUploader.on('error',function(file,errorMessage,jqXhr=null){

  if(jqXhr == null){
    if(errorMessage == "You can not upload any more files."){
      imageUploader.options.cleaningUp = true;
      imageUploader.removeFile(file);
      imageUploader.options.cleaningUp = false;
      error("Multiple file not allowed.");
    }
    return false;
  }

  if(jqXhr.status == 422){
    $.each(errorMessage.image,function(i,v){
      error(v);
    });
  }
});