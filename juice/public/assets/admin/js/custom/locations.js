var windowFocus ;
$(window).focus(function() {
    windowFocus = true;
}).blur(function() {
    windowFocus = false;
});
var uri = apiadd+'/location';
var container = {
  brandDetails        : $('.brand-details-section'),
  brands              : $("ul.brands-collection"),
  brandsTotal         : $('.total-brands')
}

// var brandSearch= $("#brands-section input.search")
var form = {
  id                  : $("#location-id"),
  name                : $('#name'),
  address             : $('#unecom-address'),
  phone               : $("#phone"),
  email               : $("#email"),
  website             : $("#website"),
  longitude           : $("#longitude"),
  latitude            : $("#latitude"),
  addBtn              : $('#add-btn'),
  saveBtn             : $('#save-btn'),
  deleteBtn           : $('#delete-btn'),
  map                 : $('#map-box')
}


var options = {
  valueNames:[
    'brand',
    {data:['id']}
  ],
  item:'brand-list-template'
};
var brandList = new List('brands-section',options);

var templates = {
  brand:  $(".unecom-template .unecom-brands li")
};

//to view all brand details on page load
init();

form.saveBtn.click(function(){
  var formData = {};// = new FormData();
  var id = form.id.val().trim();
  var name = form.name.val().trim().trim();
  var address = form.address.val().trim();
  var phone = form.phone.val().trim();
  var email = form.email.val().trim();
  var website = form.website.val().trim();
  var longitude = form.longitude.val().trim();
  var latitude = form.latitude.val().trim();

  if(!name.length || !address.length || !longitude.length || !latitude.length || !id.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }


  formData['name'] = name;
  formData['address'] =address;
  formData['phone'] =phone;
  formData['email'] =email;
  formData['website'] =website;
  formData['longitude'] =longitude;
  formData['latitude'] =latitude;
  
  $.ajax({
    url:uri+'/'+id,
    type:'PATCH',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.error)
        return error(data.error);
      
      notify('Product Updated','Product successfully updated.','success');
      
      brandList.remove('id',data.id);
      brandList.add({id:data.id,brand:data.name});
      container.brands.find('li[data-id='+data.id+']').addClass('active');
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
        var errors = jqXhr.responseJSON;
        $.each( errors , function( key, value ) {
          return error(value[0]);
        });
        return false;
      }
      return error("Contact website administrator.");
    }
  });
});

form.deleteBtn.click(function(){
  (new PNotify({
      title: 'Delete Confirmation',
      text: 'Are you sure?<br>This will delete every related details from database',
      icon: 'glyphicon glyphicon-question-sign',
      hide: false,
      hide: false,
      confirm: {
          confirm: true
      },
      addclass: 'stack-modal',
      stack: {
          'dir1': 'down',
          'dir2': 'right',
          'modal': true
      },
      buttons: {
          closer: false,
          sticker: false
      },
      history: {
          history: false
      },
      after_close: function(notice, timer_hide){
        $('.ui-pnotify-modal-overlay').remove();
      }
  })).get().on('pnotify.confirm', function(){
      var id = form.id.val();
      var response = $.ajax({url:uri+'/'+id,type:'DELETE'});
      response.fail(function(jqXhr){
        return error("");
      });
      response.done(function(data){
        console.log(data);
        if(data.error)
          return error(data.error);

        notify("Product Deleted","","success");
        deleteFromList(id);
      });
  })
});

form.addBtn.click(function(){
  var formData = {};
  
  var name = form.name.val().trim().trim();
  var address = form.address.val().trim();
  var phone = form.phone.val().trim();
  var email = form.email.val().trim();
  var website = form.website.val().trim();
  var longitude = form.longitude.val().trim();
  var latitude = form.latitude.val().trim();

  if(!name.length || !address.length || !longitude.length || !latitude.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }

  formData['name'] = name;
  formData['address'] =address;
  formData['phone'] =phone;
  formData['email'] =email;
  formData['website'] =website;
  formData['longitude'] =longitude;
  formData['latitude'] =latitude;

  console.log(formData);
  $.ajax({
    url:uri,
    type:'POST',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.error)
        return error(data.error);
      
      
      notify("Location added",'Location Added','success');
      brandList.remove('id',data.id);
      brandList.add({id:data.id,brand:data.name});
      resetForm();
      $("ul.brands-collection li[data-id='add'] a").trigger('click');
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          $.each( errors , function( key, value ) {
            return error(value[0]);
          });
          return false;
        }
        return error("");
    }
  });
});

function resetForm(){
    form.name.val("");
    form.id.val("");
    form.address.data('wysihtml5').editor.setValue("");
    form.map.addClass('hide');
    form.phone.val("");
    form.email.val("");
    form.website.val("");
    form.longitude.val("");
    form.latitude.val("");

    form.saveBtn.addClass("hide");
    form.deleteBtn.addClass("hide");
    form.addBtn.removeClass("hide");
    return false;
}

$('body').on('click','ul.brands-collection li a',function(e){
  e.preventDefault();
  var parentLi = $(this).closest('li');
  if(parentLi.hasClass('active'))
    return false;
  $(this).closest('ul').find('li').removeClass('active');
  parentLi.addClass('active');
  console.log();
  var id = $(this).closest("li").attr("data-id");
  if(id=="add")
    return resetForm();

  form.addBtn.addClass("hide");
  form.saveBtn.removeClass("hide");
  form.deleteBtn.removeClass("hide");
  var url = uri+'/'+id;
  $.ajax({
    url:url,
    type:'GET',
    success:function(data){
      console.log(data);
      if(data.error)
        return error(data.error);

      form.id.val(data.id);
      form.name.val(data.name);
      form.address.data('wysihtml5').editor.setValue(data.address);
      form.phone.val(data.phone);
      form.email.val(data.email);
      form.website.val(data.website);
      form.longitude.val(data.longitude);
      form.latitude.val(data.latitude);
    }
  });
  
});

function deleteFromList(id){
  brandList.remove('id',id);
  resetForm();
  var count = brandList.items.length;
  container.brandsTotal.html(count);
  if(!count)
    container.brandDetails.hide();
  container.brands.find('li:first-child a').trigger('click');
}

function init(){

  var response = $.ajax("/admin/api/location")
  response.done(function(data){
    if(data.error){
      return error(data.error);
    }else{
      $.each(data,function(i,location){
        brandList.add({id:location.id,brand:location.name});
      });
      container.brandsTotal.html(brandList.items.length-1);
      container.brands.find('li:first-child a').trigger('click');
    }
  });
}