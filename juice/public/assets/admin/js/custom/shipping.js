var options = {
	valueNames: [
		'brand',
		{data:['id']}
	],
	item:'brand-list-template'
};
var brandList = new List('variants-section',options);

var container = {
	brands: 			$("ul.brands-collection"),
	brandsTotal: 		$('.total-brands'),
	brandAddBtnGroup: 	$('.brand-add-btn-group'),
	brandEditBtnGroup: 	$('.brand-edit-btn-group'),
}

var form = {
	saveBtn: 			$('#save-btn')
}

var templates = {
	brand: 			$(".unecom-template .unecom-brands li#brand-list-template"),
};

//to view all brand details on page load
init();

brandList.on("updated",function(){
	container.brandsTotal.html($('.brands-collection li').length);
});

$('body').on('click','ul.brands-collection li a',function(e){
	e.preventDefault();
	var parentLi = $(this).closest('li');
	if(parentLi.hasClass('active'))
		return false;

	$(this).closest('ul').find('li').removeClass('active');
	parentLi.addClass('active');
	$('#code').attr('disabled','disabled');
	container.brandEditBtnGroup.removeClass('hide');
	var id = parentLi.attr('data-id');
	var url = '/admin/api/shipping/'+id;
	var response = $.ajax(url,"get");
	response.done(function(data){
		if(data.error){
			new PNotify({
				title:'Something went wrong.',
				text:data.error,
				type:'error'
			});
			return false;
		}
		$("#amount").val(data.amount);
		$("#title").val(data.title);
	});
	response.fail(function(jqXhr){
		new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
			});
	});
	
	return false;
});





form.saveBtn.click(function(event){
	event.stopPropagation();
	var amount = $('#amount').val();
	var obj = {amount:amount};
	var id = $(".brands-collection > .active").data(id);
	var uri = '/admin/api/shipping/'+id.id;
	$.ajax({
		url:uri,
		type:"PATCH",
		data:obj,
		success:function(data){
			if(data.error){
				new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
				});
			//$(".brands-collection > .active > a > .brand").html(data.name);
			return false;
			}
			new PNotify({
			title: 'success',
			text: 'Shipping Rates are successfully updated.',
			type: 'success',
			});
			/*$(".brands-collection > .active").attr('data-id',data.amount);
 			$(".brands-collection > .active > a > .brand").html(data.amount);*/
		},
		error:function(jqXhr){
			if(jqXhr.status == 422){
				var errors = jqXhr.responseJSON;
				$.each( errors , function( key, value ) {
					new PNotify({
				    title: 'Something went wrong',
				    text:  value[0],
				    type: 'error',
					});
				});
			return false;
			}
		}
	});
 	return false;
});
function init(){
	var response = $.ajax('/admin/api/shipping');
	response.fail(function( jqXhr) {
        new PNotify({
        	title:'Something went wrong',
        	text:'Error:'+jqXhr.status,
        	type:'error'
        });return false;
    });
	response.done(function(data){
		$.each(data,function(i,brand){
			brandList.add({id:brand.id,brand:brand.title});
		});
		/*container.brandsTotal.html(data.coupons.length);*/
		container.brandsTotal.html(brandList.items.length);
	});
}