$("select").niceSelect();
var options = {
  valueNames: [
    'brand',
    {data:['id']}
  ],
  item:'brand-list-template'
};
var brandList = new List('customer-section',options);
var uri = api+'crm/';

var container = {
  brands:       $("ul.brands-collection"),
  brandsAssoc:    $("ul.brands-associated"),
  rightPanel:     $(".right-panel"),
  brandsTotal:    $('.total-brands'),
  brandAddBtnGroup:   $('.brand-add-btn-group'),
  brandEditBtnGroup:  $('.brand-edit-btn-group'),
}

var form = {
  addBtn:       $('#add-btn'),
  saveBtn:      $('#save-btn'),
  id: $("#id"),
  name : $("#name"),
  email : $("#email"),
  mobile : $("#mobile"),
  phone : $("#phone"),
  pan : $("#pan"),
  aadhar : $("#aadhar"),
  address : $("#address"),
  branch : $("#branchId")
}

var templates = {
  brand:      $(".unecom-template .unecom-brands li#brand-list-template")
};
  $.get('/admin/api/branch',function(branch){
    console.log(branch);
    $.each(branch,function(i,v){
      var temp = "<option value='"+v.id+"'>"+v.name+"</option>";
      $("select#branchId").append(temp);
    });
    $("select#branchId").niceSelect('update');
  });

brandList.on("updated",function(){
  container.brandsTotal.html($('.brands-collection li').length);
});

function resetForm(){
  form.id.val('');
  form.name.val('');
  form.email.val('');
  form.mobile.val('');
  form.phone.val('');
  form.pan.val('');
  form.aadhar.val('');
  form.address.val('');
  form.branch.find("option[value='0']").prop("selected",true);
  form.branch.niceSelect('update');
  imageUploader.options.cleanUp();
}

$('body').on('click','ul.brands-collection li a',function(e){
  e.preventDefault();
  var parentLi = $(this).closest('li');
  if(parentLi.hasClass('active'))
    return false;

  $(this).closest('ul').find('li').removeClass('active');
  parentLi.addClass('active');

  if(parentLi.hasClass('add-list-ui')){
    resetForm();
    form.addBtn.removeClass('hide');
    form.saveBtn.addClass('hide');
  }else{
    $(brandList.items[0].elm).removeClass('active');
    form.id.attr('disabled','disabled');
    form.addBtn.addClass('hide');
    form.saveBtn.removeClass('hide');


    var id = parentLi.attr('data-id');
    
    var response = $.get(uri+id);
    response.done(function(data){
      
      if(data.error){
        new PNotify({
            title: 'Something went wrong',
            text:  data.error,
            type: 'error',
        });
        return false;
      }else{
        imageUploader.options.cleanUp();
        form.id.val(data.id);
        form.name.val(data.name);
        form.email.val(data.email);
        form.mobile.val(data.mobile);
        form.phone.val(data.phoneNo);
        form.pan.val(data.panNo);
        form.aadhar.val(data.aadharNo);
        form.address.val(data.address);
        form.branch.find("option[value='"+data.branchId+"']").prop("selected",true);
        form.branch.niceSelect('update');
        var mockFile = {
          name: data.name, 
          size: 500,
          status: Dropzone.ADDED,
          url:'/admin/image/'+data.imageId,
        };
      
        imageUploader.emit("addedfile", mockFile);
        imageUploader.createThumbnailFromUrl(mockFile, mockFile['url']);
        imageUploader.files.push(mockFile);
      }
    });
  }
});



form.addBtn.click(function(event){
  event.stopPropagation();
  var data = $('#customer').serializeArray();
  console.log(data); 
  $("#error").fadeOut();
  var response = $.ajax({url:uri,type:"POST",data:data});
  response.fail(function(jqXhr){
    if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          console.log(errors);
          $.each( errors , function( key, value ) {
            new PNotify({
              title: 'Something went wrong',
              text:  value[0],
              type: 'error',
            });
          });
          return false;
        }
      new PNotify({
        title:'Error Code : '+jqXhr.status,
        text: 'Something went wrong',
        type:'error'
      });
      return false;
  });
  response.done(function(data){
    if(data.error){
      console.log(data);
      new PNotify({
          title: 'Something went wrong',
          text:  data.error,
          type: 'error',
      });
      return false;
    }
      
    new PNotify({
      title: 'Success',
      text: 'Customer was added.',
      type: 'success',
    });
    brandList.add({id:data.id,brand:data.name});
    container.brandsTotal.html(brandList.items.length-1);
    form.id.val(data.id);
    imageUploader.processQueue();
    $("ul.brands-collection li[data-id='"+data.id+"'] a").trigger("click");
  });
  return false;
});

form.saveBtn.click(function(event){
  event.stopPropagation();
  var id = form.id.val();
  var data = $('#customer').serializeArray();
  console.log(id);
  var url = uri+id;

  var response = $.ajax({url:url,type:"PATCH",data:data});
  
  response.done(function(data){
    console.log(data);
    if(data.error){
      new PNotify({
          title: 'Something went wrong',
          text:  data.error,
          type: 'error',
      });
      return false;
    }
    new PNotify({
      title:'Customer Updated Successesfully',
      text:'Customer Updated',
      type:'success'
    });
    imageUploader.processQueue();

  });
 
  return false;
});


function init(){
  var response = $.ajax(uri);
  response.done(function(data){
    console.log(data);
      if(data.error){
      new PNotify({
          title: 'Something went wrong',
          text:  data.error,
          type: 'error',
      });
      return false;
    }
    $.each(data,function(i,brand){
      brandList.add({id:brand.id,brand:brand.name});
    });
    
    container.brandsTotal.html(brandList.items.length-1);
  });
}
init();
// IMAGE UPLOADER
///DROPZONE
  Dropzone.autoDiscover = false;
  var imageUploader = new Dropzone('div#brand-image',{ url: uri+'image',paramName:'image',addRemoveLinks:true,previewTemplate:$('#dz-preview-template').html(),previewsContainer:'.dropzone-previews',clickable:true,autoProcessQueue:false, uploadMultiple:false,createImageThumbnails:true,acceptedFiles:'image/png,image/jpeg'});
  
  imageUploader.on('success',function(file,image){
    if(image.error){
      notify('Something went wrong',image.error,'error');
      return false;
    }
  });

  imageUploader.on('sending', function(file, xhr, formData){
    var token = $("meta[name='csrf-token']").attr('content');
    formData.append('_token',token);
    formData.append('id',form.id.val());
  });

  imageUploader.options.cleaningUp = false;
  imageUploader.options.cleanUp = function(){
    this.cleaningUp = true;
    imageUploader.removeAllFiles();
    this.cleaningUp = false;
  }
  imageUploader.options.cleanUp();
  imageUploader.on('removedfile',function(file){
    if(imageUploader.options.cleaningUp)
      return false;

    var response = $.ajax({url: uri+form.id.val()+'/image',type:'DELETE'});
    response.done(function(data){
      console.log(data);
    });
  });