var options = {
	valueNames: [
		'brand',
		{data:['id']}
	],
	item:'brand-list-template'
};
var brandList = new List('variants-section',options);

var container = {
	brands: 			$("ul.brands-collection"),
	brandsTotal: 		$('.total-brands'),
	brandAddBtnGroup: 	$('.brand-add-btn-group'),
	brandEditBtnGroup: 	$('.brand-edit-btn-group'),
	codes:$("#codes-section"),
}

var form = {
	addBtn: 			$('#add-btn'),
	code: 				$('#code'),
	saveBtn: 			$('#save-btn'),
	id : $('#unecom-id'),
}

var codeListOpts = {
  valueNames:['title',{data:['id']}],
  item:'code-item'
};
var codeList = new List('codes-section',codeListOpts);

var templates = {
	brand: 			$(".unecom-template .unecom-brands li#brand-list-template"),
};

//to view all brand details on page load
init();

brandList.on("updated",function(){
	container.brandsTotal.html($('.brands-collection li').length);
});

$('body').on('click','ul.brands-collection li a',function(e){
	e.preventDefault();
	var parentLi = $(this).closest('li');
	if(parentLi.hasClass('active'))
		return false;

	$(this).closest('ul').find('li').removeClass('active');
	parentLi.addClass('active');

	if(parentLi.hasClass('add-list-ui')){
		$('#name').removeAttr('disabled');	
		document.getElementById("variant").reset();
		$("#variantId").val("0");
		container.codes.addClass('hide');
		container.brandAddBtnGroup.removeClass('hide');
		container.brandEditBtnGroup.addClass('hide');
		//container.rightPanel.addClass('hide');
	}else{
		$(brandList.items[0].elm).removeClass('active');
		container.brandEditBtnGroup.removeClass('hide');
		container.brandAddBtnGroup.addClass('hide');
		container.codes.removeClass('hide');

		var id = parentLi.attr('data-id');
		var url = '/admin/api/variants/'+id;
		var response = $.ajax(url,"get");
		response.done(function(data){
			$("#name").val(data.title);
			$("#variantId").val(data.id);
			getCodes(data.variants);
		});
		response.fail(function(jqXhr,textStatus,errorThrown){
			new PNotify({
				    title: 'Something went wrong',
				    text:  errorThrown,
				    type: 'error',
				});
		});
	}
});



form.addBtn.click(function(event){
	event.stopPropagation();

	var uri = '/admin/api/variants/add';
	
	var data =[];
	
	var title = $('#name').val();
	data = {title:title};
	console.log(data);
	var flag=false; 	
	$("#error").fadeOut();
	$.ajax({
		url:uri,
		data:data,
		type:"POST",
		success:function(data){
			console.log(data);
			if(data.error){
				new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
				});
			return false;
			}

			new PNotify({
			title: 'success',
			text: 'Variant are successfully added.',
			type: 'success',
			});
 			brandList.add({id:data.id,brand:data.title});
 			container.brandsTotal.html(brandList.items.length-1);

 			if(data.info){
 				new PNotify({
			    title: 'Warning',
			    text:  data.info,
				});	
 			}
 			$("#variant")[0].reset();
		},
		error:function(jqXhr){
			console.log(jqXhr.status);
			if(jqXhr.status == 422){
				var errors = jqXhr.responseJSON;
				$.each( errors , function( key, value ) {
					new PNotify({
				    title: 'Something went wrong',
				    text:  value[0],
				    type: 'error',
					});
				});
			return false;
			}
		}
	});
	return false;
});

form.saveBtn.click(function(event){
	event.stopPropagation();
	var variants = $('#name').val();
	var obj = {title:variants};
	var id = $(".brands-collection > .active").data(id);
	var uri = '/admin/api/variants/'+id.id;
	console.log(obj);
	$.ajax({
		url:uri,
		type:"PATCH",
		data:obj,
		success:function(data){
			console.log(data);
			if(data.error){
				new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type: 'error',
				});
			//$(".brands-collection > .active > a > .brand").html(data.name);
			return false;
			}
			new PNotify({
			title: 'success',
			text: 'Variant are successfully updated.',
			type: 'success',
			});
			
 			$(".brands-collection > .active > a > .brand").html(data.name);
		},
		error:function(jqXhr){
			if(jqXhr.status == 422){
				var errors = jqXhr.responseJSON;
				$.each( errors , function( key, value ) {
					new PNotify({
				    title: 'Something went wrong',
				    text:  value[0],
				    type: 'error',
					});
				});
			return false;
			}
		}
	});
 	return false;
});
function init(nextUrl=''){
	var response = $.ajax('/admin/api/variants'+nextUrl);
	response.fail(function( jqXhr,textStatus,errorThrown ) {
        if(jqXhr.status === 500){
          new PNotify({
			    title: 'Something went wrong',
			    text:  errorThrown,
			    type: 'error',
			});
          return false;
        }
    });
	response.done(function(data){
		$.each(data,function(i,brand){
			brandList.add({id:brand.id,brand:brand.title});
		});
		/*container.brandsTotal.html(data.coupons.length);*/
		container.brandsTotal.html(brandList.items.length-1);

		if(typeof data.next !== 'undefined')
			init(data.next);
	});
}

$(document).on('click','i.variant-delete',function(){
	var parentLi = $(this).closest('li');
	var vid = parentLi.attr('data-id');

	var response = $.ajax({
		url:'/admin/api/variants/variants/'+vid,
		type:'DELETE'
	});
	response.done(function(data){
		if(data.error){
			console.log(data);
			new PNotify({
			    title: 'Something went wrong',
			    text:  data.error,
			    type:  'Error',
			});
			return false;
		}
		new PNotify({
		    title: 'Variants successfully deleted',
		    text:  'Deleted',
		    type:  'success',
		});
		codeList.remove('id',vid);
	});
});


$("#add-code").on('click',function(){  
	//console.log($("#variantId").val());return false;
  	var url = '/admin/api/variants/variants/';
  	var code = form.code.val();
 	var variantId = $("#variantId").val();
  
  $.ajax({
    url:url,
    type:'POST',
    data:{"title":code,"variantId":variantId},
    success:function(data){
    	console.log(data);
      if(data.error){
      	new PNotify({
            title: 'Something went wrong',
            text:  data.error,
            type:  'error',
        });
      	return false;
    }
    form.code.val('');
    new PNotify({
            title: 'Variants Added',
            text: 'Variants was added successfully',
            type: 'success',
        });
      codeList.add({title:code,id:data.id});
    }
  });
  return false;
});

function getCodes(variants){
  	codeList.clear();
  
  	$.each(variants,function(i,variant){
  		codeList.remove("title",variant.title);
  		codeList.add({title:variant.title,id:variant.id});
	});
  
}
