$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var api = 'api/';

        function cartCount(){
            var showCheckoutButton = false;
            $.get(api+'order/count',function(data){
                if(data.error)
                    return false;
                $("span.cartItemCount").html(data.count);
            });

            $.get(api+'order',function(data){
                $("#cart-container").html("");
                if(data.error)
                    return false;
                $.each(data,function(i,v){
                    if(v.productName == null)
                      return true;
                    showCheckoutButton = true;
                    var a = $("#global-templates > #cart-container-template > article:first").clone().appendTo('#cart-container');
                    a.attr("data-orderDetailId",v.orderDetailId);
                    a.find(".product-title").html(v.productName);
                    a.find("img").attr('src','/image/'+v.productImage);
                    a.find("input.item-quantity").attr('data-orderDetailId',v.orderDetailId).val(v.quantity);
                    a.find("span#price").html(v.price);
                    a.find("span#total").html(+v.price * +v.quantity);
                    a.find(".delete-item").attr('data-orderDetailId',v.orderDetailId);
                    a.find(".edit-item").attr('data-orderDetailId',v.orderDetailId);
                });
                
                if(showCheckoutButton == true)
                  $("#cart-container-checkout-template > article:first").clone().appendTo('#cart-container');
                else{
                  $("#cart-container").html("No product in your cart.");
                }
            });
            return false;
        }

        $(document).on('click','.delete-item',function(e){
          e.preventDefault();
          var orderDetailId = $(this).attr("data-orderDetailId");
          $.ajax({
            url:'/api/order/'+orderDetailId,
            type:'DELETE',
            success:function(data){
              console.log(data);
              cartCount();
            }
          });
        });

        $(document).on('click','.edit-item',function(e){
          e.preventDefault();
          var orderDetailId = $(this).attr("data-orderDetailId");
          var findIn = $("#cart-container").find("article[data-orderDetailId='"+orderDetailId+"']");
          findIn.find("input.item-quantity").removeAttr("disabled").focus();
          return false;
          $.ajax({
            url:'/api/order/'+orderDetailId,
            type:'DELETE',
            success:function(data){
              console.log(data);
              cartCount();
            }
          });
        });

        $(document).on('blur','.item-quantity',function(){
          $(this).attr("disabled",true);
          var quantity = $(this).val();
          var orderDetailId = $(this).attr("data-orderDetailId");
          $.ajax({
            url:'/api/order/'+orderDetailId,
            type:'PATCH',
            data:{quantity:quantity},
            success:function(data){
              if(data.error)
                alert(data.error);
              cartCount();
            }
          });
        });