

cartCount();
var template = {
	box : $("#templates > #category-box-template > div:first"),
	product : $("#templates > #product-box-template > article:first"),
	slider : $("#templates > #page-slider-template > div:first")
}

var container = {
	box : $("#category-box"),
	productBox : $("#product-box"),
	product  : $("#product-inner"),
	slider : $("#academy_page_slider")
}

$.get('/api/slider/academy',function(data){
	if(data.error)
		return false;
	$.each(data,function(i,v){
		var a = template.slider.clone().appendTo(container.slider);

		a.find('#image-slider').css('background-image','url("/image/'+v.imageId+'")');
		a.find("#description").html(v.content);
	});

	initSlider();
});
// Slider Animation
function doAnimations( elems ) {
	//Cache the animationend event in a variable
	var animEndEv = 'webkitAnimationEnd animationend';
	
	elems.each(function () {
		var $this = $(this),
			$animationType = $this.attr('data-animation');

		$this.addClass($animationType).one(animEndEv, function () {
			$this.removeClass($animationType);
		});
	});
}
function initSlider(){
	$("#academy_page_slider").on("initialized.owl.carousel", function(e){
		var $academyPageSlider = $("#academy_page_slider");
		    $academyPageSlider.css({'height': 'initial'});
		    $academyPageSlider.animate({'opacity': 1});
	});


	var academyPageSliderMain = $("#academy_page_slider").owlCarousel({
	    loop:true,
	    margin: 0,
	    nav: false,
	    dots:true, 
	    items: 1,
	    loop: false,
	    responsive:{
	        0:{
	            items:1
	        },

	        600:{
	            items:1
	        },

	        1000:{
	            items:1
	        }
	    }
	});


	academyPageSliderMain .on("changed.owl.carousel", function(e){
		var $animatingElems = $(e.relatedTarget.$element).find("[data-animation ^= 'animated']");
		doAnimations( $animatingElems );
	});
}

var uri = api+'video-category';
var prevId = 0;
var currId = 0;
$(document).ready(function(){
	fetch();
	$(document).on('click','a.list-item',function(){
		fetch($(this).attr('data-id'));
		return false;
	});
	$(document).on('click','.back',function(){
		fetch($(this).attr('data-id'));
		return false;
	});
	$(document).on('click','.closeBox',function(){
		$(".sub-infowindow.infowindow").addClass('hide');
		return false;
	});
});

function fetch(id=null){
	container.box.find(".list-col").remove();
	container.productBox.addClass('hide');
	if(id == null || id == 0){
		$(".back").addClass('hide');
		var url = uri;
	}
	else{
		prevId = currId;
		currId = id;
		var url = uri+'/'+id;
		$(".back").removeClass('hide');
	}
	
	$(".back").attr("data-id",prevId);
	$.get(url,function(data){
		if(data.error)
			return false;
		$.each(data,function(i,v){
			var a = template.box.clone().appendTo(container.box);
			a.find("a").attr('data-id',v.id).attr('title',v.title);
			a.find("#category-title").html(v.title);
		});
	});

	if(id != null)
		getAcademyProducts(id);

	return false;
}

function getAcademyProducts(id){
	container.product.html("");
	$.get(api+'video-products/category/'+id,function(data){
		if(data.error)
			return false;
		if(!data.products.length)
			return false;
		$("#product-category-name").html(data.category+"<a href='#' class='closeBox close-infowindow'><i class='fa fa-close'></i></a>");
		$.each(data.products,function(i,v){
			var a = template.product.clone().appendTo(container.product);
			console.log(a.length);
			a.find("#product-title").html(v.title);
			a.find("#product-description").html(v.description);
			a.find("a.links-btn").attr('data-productId',v.id);
		});
		container.productBox.removeClass('hide');
	});
	return false;
}

$(document).on('click','a.links-btn#add-video',function(){
	var id = $(this).attr('data-productId');
	$.post(api+'order',{productId:id},function(data){
      if(data.error)
        return false;
      if(data.success)
        alert('Video Product was added to cart.');
      cartCount();
    });
});