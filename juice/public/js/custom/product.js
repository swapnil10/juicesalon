 // var uri = 
  cartCount();
  function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
  }

  init(0,'&orderBy=1');

  $.get(api+"slider/product",function(data){
    console.log(data);
    $.each(data,function(i,v){
      template = $("#slider-template .owl-item").clone();
      template.find('#description').html(v.content);
      template.find('#link').attr("href",v.link);
      template.find('#image').attr("src","/image/"+v.imageId+".png");
      template.appendTo("#products_slide_1");
    })
    slider();
  });

  $(document).on('click','.add-to-cart',function(){
    var id = $(this).attr('data-id');
    $.post(api+'order',{productId:id},function(data){
      if(data.error)
        return false;
      if(data.success)
        alert('Product was added to cart.');
      cartCount();
    });
  });

  function slider(){
    $("#products_slide_1").on("initialized.owl.carousel", function(e){
    console.log("initialized owl carousel productsCarousel");
    var $productsSlider = $("#products_page_slide");
        $productsSlider.css({'height': 'initial'});
        $productsSlider.animate({'opacity': 1});
  });

  var productsCarousel = $("#products_slide_1").owlCarousel({
      loop:true,
      margin: 60,
      nav:true,
      dots:false, 
      items: 1,
      loop: false,
      responsive:{
          0:{
              items:1
          },

          600:{
              items:1
          },

          1000:{
              items:1
          }
      }
  });

  productsCarousel.on("changed.owl.carousel", function(e){
    // console.log( e );
    var $animatingElems = $(e.relatedTarget.$element).find("[data-animation ^= 'animated']");
        var $animatingBgElemtns = $("#pink_oval_layer");

    doAnimations( $animatingElems );
    doAnimations( $animatingBgElemtns );
  });
  }
  function doAnimations( elems ) {
    //Cache the animationend event in a variable
    var animEndEv = 'webkitAnimationEnd animationend';
    
    elems.each(function () {
      var $this = $(this),
        $animationType = $this.attr('data-animation');

      $this.addClass($animationType).one(animEndEv, function () {
        $this.removeClass($animationType);
      });
    });
  }

  $.get("/api/productType",function(data){
    if(data.error)
      return false;
    $.each(data,function(i,v){
      if(v.enabled == 0)
        return true;
      if(v.id == 1)
        return true;
      var a = '<option value="'+v.id+'">'+v.title+'</option>';
      $("#productTypeFilter").append(a);
    });
  });

  $(document).on('click','#produceFilterSubmit',function(e){
    e.preventDefault();
    var orderBy = $("#priceSortFilter").find("option:selected").val();
    var productType = $("#productTypeFilter").find("option:selected").val();
    if(productType != "0" ){
      $("#product-type-filter-tag").removeClass('hide');
      $("#product-type-filter-tag").html($("#productTypeFilter").find("option:selected").text().trim());
    }
    else
      $("#product-type-filter-tag").addClass('hide');
    $("#order-filter-tag").html($("#priceSortFilter").find("option:selected").text());
    $(".product-container").html("");
    init(0,'&orderBy='+orderBy+'&productType='+productType);
  });

  function init(offset=0,query=""){
    $.get(api+"products?offset="+offset+query,function(data){
      if(data == "{}")
        return false;
      if(!data.length)
        return false;
      $.each(data,function(i,v){
        var template = $(".unecom-template .product").clone();
        template.find(".title").html(v.title);
        template.find(".description").html(v.description);
        template.find("#price").html(v.price);
        template.find(".overlay-link").attr('href',"#");
        template.find(".add-to-cart").attr('data-id',v.id);
        template.find(".image").attr('src', "/image/"+v.imageId);
        $(".product-container").append(template);
      });
      if(data.length == 30){
        offset = offset+30;
        init(offset,query);
      }
    });
  }