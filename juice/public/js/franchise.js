$.get('api/content/franchisee',function(data){
	$("#criteria").html(data.criteria).attr("property","content2");
	$("#why-us").html(data.why).attr("property","content");
	$("#franchisee-image").attr('src','/bulkimage/'+data.imageId).attr("data-id",data.imageId);
});

$.get('api/materials',function(data){
	console.log(data);
});

$(document).on('click','#submit_enquiry',function(){
	$(".status").fadeOut();

	if($('#select_city').val()=='0'){
		$('.status').html('Please select a city').css({'color':'red'}).fadeIn();
		return false;
	}
	var info = $('#franchise-form').serializeArray();
	$.post('api/franchise-enquiry',info,function(data){
		$('.status').html('Form filled successfully, we\'ll revert back soon..').css({'color':'green'}).fadeIn();
		$('#franchise-form')[0].reset();
	}).fail(function(e){
		if(e.status == 422){
			var err = e.responseJSON;
			$.each(err,function(i,v){
				$(".status").html(v[0]).fadeIn().css({'color':'red'});
				return false;
			});
		}
	});
	return false;
});