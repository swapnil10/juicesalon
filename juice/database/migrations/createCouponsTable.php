<?php 
        if(!Schema::hasTable('admins')){
            Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->dateTime('createdAt');
            $table->dateTime('updatedAt');
        });
        }

        if(!Schema::hasTable('coupons')){
            Schema::create('coupons', function (Blueprint $table) {
                $table->string('code')->unique();
                $table->boolean('discountType');
                $table->integer('amount')->length(10);
                $table->integer('usedTimes')->length(10);
                $table->date('startDate');
                $table->date('expiryDate');
                $table->boolean('enabled')->default(1);
                $table->dateTime('created_at');
            });
        }

        if(!Schema::hasTable('franchisee')){
            Schema::create('franchisee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->float('size',8,2)->comment('What is the size of sapce?(In sq.ft)');
            $table->string('mobile');
            $table->text('comeToKnw')->comment("How did you came to know?");
            $table->string('city');
            $table->text('message');
            $table->dateTime('createdAt');
            $table->dateTime('updatedAt');
        });
        }

        if(!Schema::hasTable('franchisee_content')){
            Schema::create('franchisee_content', function (Blueprint $table) {
                $table->integer('imageId')->length(11);
                $table->text('why');
                $table->text('criteria');
            });
        }

        if(!Schema::hasTable('getthelookpage')){
            Schema::create('getthelookpage', function (Blueprint $table) {
                $table->text('getLook');
                $table->text('darkHeader');
                $table->dateTime('createdAt')->nullable();
                $table->dateTime('updatedAt')->nullable();
            });
        }

        if(!Schema::hasTable('password_resets')){
            Schema::create('password_resets', function (Blueprint $table) {
                $table->string('email');
                $table->string('token');
            });
        }

        if(!Schema::hasTable('images')){
            Schema::create('getthelookpage', function (Blueprint $table) {
                $table->increments('id');
                $table->text('name');
                $table->text('size');
            });
        }

        if(!Schema::hasTable('address')){
            Schema::create('address', function (Blueprint $table) {
                $table->increments('id');
                $table->text('userId')->length(11);
                $table->text('address');
                $table->text('city');
                $table->string('pinCode',6);
                $table->text('state');
            });
        }

        if(!Schema::hasTable('locations')){
            Schema::create('locations', function (Blueprint $table) {
                $table->increments('id');
                $table->text('name');
                $table->text('address');
                $table->double('latitude')->nullable();
                $table->double('longitude')->nullable();
                $table->text('contact');
            });
        }

        if(!Schema::hasTable('looks')){
            Schema::create('looks', function (Blueprint $table) {
                $table->increments('id');
                $table->text('title');
                $table->boolean('weeklyTop');
                $table->boolean('enabled');
                $table->integer('imageId')->length(11);
            });
        }

        if(!Schema::hasTable('material')){
            Schema::create('material', function (Blueprint $table) {
                $table->increments('id');
                $table->text('title');
                $table->text('file');
            });
        }

        if(!Schema::hasTable('orderDetails')){
            Schema::create('orderDetails', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('orderId')->length(11);
                $table->integer('productId')->length(11);
                $table->integer('quantity')->length(11);
            });
        }

        if(!Schema::hasTable('orders')){
            Schema::create('orders', function (Blueprint $table) {
                $table->increments('id');
                $table->text('uid');
                $table->text('couponCode');
                $table->tinyInteger('status')->length(4);
                $table->dateTime('created_at');
                $table->dateTime('completed_at');
                $table->text('razorPayOrderId')->nullable();
            });
        }

        if(!Schema::hasTable('productpageslider')){
            Schema::create('productpageslider', function (Blueprint $table) {
                $table->increments('id');
                $table->text('link');
                $table->text('content');
                $table->integer('imageId')->length(11);
            });
        }

        if(!Schema::hasTable('products')){
            Schema::create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->text('description');
                $table->integer('productTypeId')->length(11);
                $table->float('price');
                $table->integer('imageId')->length(11)->nullable();
                $table->text('webclip');
                $table->integer('category')->length(11)->nullable();
                $table->boolean('enabled');
                $table->dateTime('createdAt')->nullable();
                $table->dateTime('updatedAt')->nullable();
            });
        }

        if(!Schema::hasTable('producttype')){
            Schema::create('producttype', function (Blueprint $table) {
                $table->increments('id');
                $table->text('title');
                $table->boolean('enabled');
            });
        }

        if(!Schema::hasTable('servicepage')){
            Schema::create('servicepage', function (Blueprint $table) {
                $table->text('header');
            });
        }

        if(!Schema::hasTable('servicepageslider')){
            Schema::create('servicepageslider', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('imageId')->length(11);
                $table->text('content');
            });
        }

        if(!Schema::hasTable('services')){
            Schema::create('services', function (Blueprint $table) {
                $table->increments('id');
                $table->text('title');
                $table->boolean('enabled');
            });
        }

        if(!Schema::hasTable('shipping')){
            Schema::create('shipping', function (Blueprint $table) {
                $table->integer('amount');
            });
        }

        if(!Schema::hasTable('staticImages')){
            Schema::create('staticImages', function (Blueprint $table) {
                $table->increments('id');
                $table->text('name');
            });
        }

        if(!Schema::hasTable('subservices')){
            Schema::create('subservices', function (Blueprint $table) {
                $table->increments('id');
                $table->text('title');
                $table->integer('serviceId')->length(11);
                $table->text('description');
                $table->float('price')->length(11);
                $table->boolean('serviceId');
            });
        }

        if(!Schema::hasTable('tempUser')){
            Schema::create('tempUser', function (Blueprint $table) {
                $table->text('fullName');
                $table->text('email');
                $table->text('phone');
                $table->integer('otp')->length(5);
                $table->text('password');
            });
        }

        if(!Schema::hasTable('users')){
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('fullName');
                $table->string('email');
                $table->text('phone');
                $table->string('password');
                $table->tinyInteger('emailVerify')->length(4)->nullable();
                $table->rememberToken()->nullable();
                $table->dateTime('createdAt')->nullable();
                $table->dateTime('updatedAt')->nullable();
            });
        }

        if(!Schema::hasTable('videoCategory')){
            Schema::create('videoCategory', function (Blueprint $table) {
                $table->increments('id');
                $table->text('title');
                $table->integer('parentId')->length(11);
            });
        }

