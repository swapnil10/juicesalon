<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    public $timestamps = false;
    protected $table = 'coupons';
    protected $primaryKey = 'code';
    public $incrementing = false;
    public static function boot(){
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
    // public function association(){
    //     return $this->belongsToMany('App\ProductType','couponsType','couponsId','productTypeId');
    // }
}
