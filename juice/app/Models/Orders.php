<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = "orders";
    public $timestamps = false;

    public static function boot(){
        parent::boot();
        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
    
    public function orderDetails(){
        return $this->hasMany('App\Models\OrderDetails','orderId','id');
    }

    public function coupon(){
        return $this->hasOne('App\Models\Coupon','code','couponCode')->where('enabled',1);
    }
    //Get All Products For this order
    /*public function products(){
    	return $this->belongsToMany('App\Models\Product','orderDetails','orderId','productId');
    }*/
}
