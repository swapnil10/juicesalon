<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model{

    protected $table ='locations';
    public $timestamps = false;
    protected $appends = ['phone','email','website'];
    
    private function extractContact(){
        if(!empty($this->contact))
            return explode(',',$this->contact);
        return ["","",""];
    }

    public function getPhoneAttribute(){
        return $this->extractContact()[0];
    }

    public function getEmailAttribute(){
        return $this->extractContact()[1];
    }

    public function getWebsiteAttribute(){
        return $this->extractContact()[2];
    }

    protected $hidden = ['contact'];
}
