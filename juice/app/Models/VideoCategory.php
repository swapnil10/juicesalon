<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class videoCategory extends Model
{
    public $timestamps = false;
    protected $table = 'videoCategory';
}
