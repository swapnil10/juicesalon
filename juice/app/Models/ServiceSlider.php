<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceSlider extends Model
{
    public $timestamps = false;
    protected $table='servicepageslider';
}
