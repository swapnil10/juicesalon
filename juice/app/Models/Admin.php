<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable{

	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';
	
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($role){
    	
    	if($role == 'admin')
    		$role = 1;
    	else if($role == 'staff')
    		$role = 0;
    	else
    		return false;

    	if($this->type == $role)
    		return true;
    	return false;
    }

    public function branch(){
    	return $this->hasOne('App\Models\Branch','id','branchId');
    }
}
