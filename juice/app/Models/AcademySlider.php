<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcademySlider extends Model
{
    public $timestamps = false;
    protected $table='academyPageSlider';
}
