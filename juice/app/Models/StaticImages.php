<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticImages extends Model{
    protected $table ='staticImages';
    public $timestamps = false;
}
