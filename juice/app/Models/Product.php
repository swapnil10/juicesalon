<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{

    protected $table ='products';
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    protected $appends = ['size'];

    public function images(){
    	return $this->hasOne('App\Models\Image','id','imageId')->select(['size']);
    }

    public function getSizeAttribute(){
        if($this->imageId == 0)
            return 0;
        if(!empty($this->images))
    	   return $this->images->size;
        return 0;
    }

    protected $hidden = ['images'];
}
