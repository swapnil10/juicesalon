<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subservice extends Model{

    protected $table ='subservices';
    public $timestamps = false;
}
