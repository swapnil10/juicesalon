<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Franchise extends Model{

	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';
    protected $table ='franchisee';
}
