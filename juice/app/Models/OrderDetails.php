<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $table = "orderdetails";
    public $timestamps = false;
    protected $hidden = ['orderId'];
    public function products(){
    	return $this->hasOne('App\Models\Product','id','productId');
    }
}