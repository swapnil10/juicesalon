<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    protected $hidden = [
        'password', 'remember_token','updated_at'
    ];
}
