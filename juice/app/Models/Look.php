<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Look extends Model
{
    public $timestamps = false;
    protected $table = 'looks';
    protected $primaryKey = 'id';
}
