<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model{
	public $timestamps = false;
    protected $table ='franchisee_content';
}
