<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Carbon\Carbon;
use Storage;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        /*Storage::disk('s3')->deleteDirectory('logs');
        Storage::disk('local')->delete(['logs/laravel.log']);*/
        if(isset($_SERVER['SERVER_NAME']) && ($_SERVER['SERVER_NAME'] != '0.0.0.0')){
            $error_has_s3_upload = true;
            if($e instanceof AuthorizationException)
                $error_has_s3_upload = false;

            else if($e instanceof HttpException)
                $error_has_s3_upload = false;

            else if($e instanceof ModelNotFoundException)
                $error_has_s3_upload = false;

            else if($e instanceof ValidationException)
                $error_has_s3_upload = false;

            if($error_has_s3_upload === true){
                $logP = 'logs/'.Carbon::now()->format('l_jS_F_Y_h_i_s_A').'.txt';
                $awsDisk = Storage::disk('s3');
                $awsDisk->put($logP,(string)$e);
            }
        }
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {   
        return parent::render($request, $e);
    }
}
