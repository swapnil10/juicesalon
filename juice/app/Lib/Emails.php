<?php

namespace App\Lib;

use SendGrid;
use SendGrid\Email;
use SendGrid\Content;
use SendGrid\Mail;

class Emails{
    private $key = "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";
    
    public function send($to,$from=null,$subject,$views,$arr=[]){
        $compiledView = view("emails.".$views,$arr)->render();
        if($from == null)
            $from = 'support@juicesalon.com';

        $from = new Email("Juice Salon", $from);
        $to = new Email("", $to);
        $content = new Content("text/html", $compiledView);
        $mail = new Mail($from, $subject, $to, $content);
        $sg = new SendGrid($this->key);
        $response = $sg->client->mail()->send()->post($mail);
        return $response;
        return;
   }
   
}