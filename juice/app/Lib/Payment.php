<?php

namespace App\Lib;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\Error;
use Razorpay\Api\Errors\SignatureVerificationError;

class Payment{
	
	public static $key = "rzp_test_SElnjF6BiqrYxP";
	private $secret = "G6QY6rZ8lIM17pPdwVH0ihXI";
	private $razor;

	public function __construct(){
		$this->razor = new Api(self::$key, $this->secret);
	}

	public function createOrder($arr=[]){
		$arr['payment_capture'] = 1;
		try{
			$pay = $this->razor->order->create($arr);
		}catch(Error $e){
			return false;
		}
		return $pay['id'];
	}

	public function fetchOrder($razorOrderId){
		try{
			$pay = $this->razor->order->fetch($id);
		}catch(Error $e){
			return false;
		}
		return $pay;
	}

	public function checkSignature($arr){
		try{
			$this->razor->utility->verifyPaymentSignature($arr);
		}catch(SignatureVerificationError $e){
			return false;
		}
		return true;
	}
}

?>