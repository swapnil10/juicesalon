<?php

	Route::get('/admin/login','AdminAuth\AuthController@showLoginForm');
	Route::post('/admin/login','AdminAuth\LoginController@authenticate');
	Route::get('/admin/logout','AdminAuth\AuthController@logout');
	
	Route::group(["prefix"=>"admin","middleware"=>"admin"],function(){
		
		require_once('api.routes.php');
		Route::get('/','AdminController@index');
		Route::get('{folder}/{page?}','AdminController@pages');
	});
?>