<?php
// 'admin/api/coupons'
Route::group(['prefix'=>'coupons'],function(){
	Route::get('/{code?}','CouponController@show');
	Route::get('/{code}/productType','CouponController@productType');
	Route::post('/','CouponController@store');
	Route::patch('/{code}','CouponController@update');
	Route::post('/{code}/productType','CouponController@assoc');
	// Route::post('/','Admin\CouponController@create');
	// Route::delete('/','Admin\CouponController@remove');
});