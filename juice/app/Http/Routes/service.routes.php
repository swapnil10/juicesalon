<?php
// 'admin/api/service'
Route::group(['prefix'=>'service'],function(){
	Route::post('/','ServiceController@store');
	Route::get('/{id?}','ServiceController@show');
	Route::patch('/{id}','ServiceController@update');
	Route::delete('/{id}','ServiceController@destroy');
});
?>