<?php
// 'admin/api/productType'
Route::group(['prefix'=>'productType'],function(){
	Route::post('/','ProductTypeController@store');
	Route::get('/{id?}','ProductTypeController@show');
	Route::patch('/{id}','ProductTypeController@update');
	Route::delete('/{id}','ProductTypeController@destroy');
});
?>