<?php
// 'admin/api/productType'
Route::group(['prefix'=>'product'],function(){
	Route::post('/','ProductController@store');
	Route::get('/{id}','ProductController@show');
	Route::get('/','ProductController@showAll');
	Route::patch('/{id}','ProductController@update');
	Route::delete('/{id}','ProductController@destroy');
	Route::post('/image','ProductController@addImage');
	Route::delete('/{id}/image','ProductController@destroyImage');
});
?>