<?php
// 'admin/api/look'
Route::group(['prefix'=>'look'],function(){
	Route::get('/{id?}','LookController@show');
	Route::get('/allLooks','LookController@showAll');
	// Route::post('/','LookController@store');
	Route::patch('/','LookController@update');
	Route::post('/image','LookController@imageProduct');
	Route::delete('/{id}/image','LookController@imageRemoveProduct');
	Route::delete('/{id}','LookController@destroyProduct');
});