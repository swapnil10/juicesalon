<?php
// 'admin/api/subservice'
Route::group(['prefix'=>'subservice'],function(){
	Route::post('/','SubserviceController@store');
	Route::get('/{id?}','SubserviceController@show');
	Route::patch('/{id}','SubserviceController@update');
	Route::delete('/{id}','SubserviceController@destroy');
});
?>