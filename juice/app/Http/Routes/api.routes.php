<?php
	Route::group(["prefix"=>"api"],function(){
		include_once('coupons.routes.php');
		include_once('productType.routes.php');
		include_once('shipping.routes.php');
		require('product.routes.php');
		require('location.routes.php');
		include_once('material.routes.php');
		require('service.routes.php');
		require('subservice.routes.php');
		require('look.routes.php');
		require('category.routes.php');

		Route::group(['prefix'=>'bulkimage'],function(){
			Route::post('/','StaticImageController@store');
			Route::get('/','StaticImageController@getAll');
			Route::delete('{id}','StaticImageController@delete');
		});

		Route::group(['prefix'=>'contents'],function(){

			// Slider
			Route::group(['prefix'=>'slider'],function(){
				Route::group(['prefix'=>'service'],function(){
					Route::post('/','SliderContentController@storeService');
					Route::get('/{id?}','SliderContentController@getService');
					Route::patch('{id}','SliderContentController@updateService');
					Route::post('/image','SliderContentController@imageService');
					Route::delete('/{id}/image','SliderContentController@imageRemoveService');
					Route::delete('/{id}','SliderContentController@destroyService');
				});

				// Not yet implemented
				Route::group(['prefix'=>'product'],function(){
					Route::post('/','SliderContentController@storeProduct');
					Route::get('/{id?}','SliderContentController@getProduct');
					Route::patch('{id}','SliderContentController@updateProduct');
					Route::post('/image','SliderContentController@imageProduct');
					Route::delete('/{id}/image','SliderContentController@imageRemoveProduct');
					Route::delete('/{id}','SliderContentController@destroyProduct');
				});

				Route::group(['prefix'=>'academy'],function(){
					Route::post('/','SliderContentController@storeAcademy');
					Route::get('/{id?}','SliderContentController@getAcademy');
					Route::patch('{id}','SliderContentController@updateAcademy');
					Route::post('/image','SliderContentController@imageAcademy');
					Route::delete('/{id}/image','SliderContentController@imageRemoveAcademy');
					Route::delete('/{id}','SliderContentController@destroyAcademy');
				});

				Route::group(['prefix'=>'home'],function(){
					Route::post('/','SliderContentController@storeHome');
					Route::get('/{id?}','SliderContentController@getHome');
					Route::patch('{id}','SliderContentController@updateHome');
					Route::post('/image','SliderContentController@imageHome');
					Route::delete('/{id}/image','SliderContentController@imageRemoveHome');
					Route::delete('/{id}','SliderContentController@destroyHome');
				});
			});


			// GET STATIC CONTENTS OF FOllowing
			Route::get('/franchise','ContentController@getFranchise');
			Route::get('/service-page-header-content','ContentController@getService');
			Route::get('/get-look','ContentController@showGetlook');

			// Store STATIC Image CONTENTS OF FOllowing
			Route::post('/franchise/image','ContentController@imageFranchise');
			Route::post('/getlook/image','ContentController@imageGetlook');

			// Remove STATIC CONTENTS Image OF FOllowing
			Route::delete('/franchise/image','ContentController@imageRemoveFranchise');
			Route::delete('/getlook/image','ContentController@imageRemoveGetlook');

			// Update STATIC CONTENTS OF FOllowing
			Route::patch('/franchise','ContentController@franchise');
			Route::patch('/service-page-header-content','ContentController@service');
			Route::patch('/get-look','ContentController@getlook');
		});
		
		Route::group(['prefix'=>'users'],function(){
			Route::get('/','Auth\UserController@getAllUsers');
			Route::get('/{id}','Auth\UserController@get');
			Route::get('/{id}/address','Auth\UserController@getAddress');
			Route::post('/{id}/is-franchisee','Auth\UserController@isFranchiseeConvert');
		});

		Route::group(['prefix'=>'order'],function(){
			Route::get('/',function(){
				return \App\Http\Controllers\OrderController::getAll();
			});
			Route::get('/{id}',function($id){
				return \App\Http\Controllers\OrderController::getOrderDetail($id);
			});
			Route::post('/{id}/change-status',function(\Illuminate\Http\Request $request,$id){
				return \App\Http\Controllers\OrderController::changeStatus($request,$id);
			});
		});
		
		Route::post('/changeStatus','OrderUserController@changeStatus');
		Route::get('franchise/{id?}','AdminController@franchise');
	});

?>