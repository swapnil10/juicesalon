<?php
	//public api routes
	Route::group(['prefix'=>'api','middleware'=>'api'],function(){
		Route::get('/homepage','HomeController@getall');
		Route::patch('/homepage','HomeController@homePage');
		Route::group(['prefix'=>'auth'],function(){
			Route::post('/send-otp','Auth\AuthController@sendOTP');
			Route::post('/register','Auth\AuthController@register');
			Route::post('/login','Auth\AuthController@login');
			Route::get('/logout','Auth\AuthController@logout');
			Route::post('/forgot-password-otp','Auth\AuthController@forgotPasswordOTP');
			Route::post('/forgot-password','Auth\AuthController@forgotPassword');

			Route::group(['prefix'=>'user','middleware'=>'auth'],function(){
				Route::get('/address','Auth\UserController@getAddress');
				Route::post('/address','Auth\UserController@address');
			});
		});

		Route::get('productType/{id?}','ProductTypeController@show');
		Route::group(['prefix'=>'content'],function(){
			Route::get('/service-page-header-content','ContentController@getService');
			Route::get('/franchisee','ContentController@getFranchise');
			Route::get('/get-look','ContentController@showGetlook');
		});

		Route::group(['prefix'=>'slider'],function(){
			Route::get('/service','SliderContentController@getService');
			Route::get('/product','SliderContentController@getProduct');
			Route::get('/home','SliderContentController@getHome');
			Route::get('/academy','SliderContentController@getAcademy');
		});

		Route::group(['prefix'=>'services'],function(){
			Route::get('/','ServiceController@show');
			Route::get('/sub-services/{id}','SubserviceController@getSubServices');
		});

		// api/order
		Route::group(['prefix'=>'order'],function(){
			// Register Users
			Route::group(['middleware'=>'auth'],function(){
				Route::get('/userOrders','OrderController@userOrders');
				Route::get('/place',"OrderController@confirmOrder");
				Route::get('/payment',"OrderController@payment");
				Route::post('/payment-response',"OrderController@paymentResponse");
			});

			Route::post('/',"OrderController@addProduct"); // Add to cart
			Route::get('/count','OrderController@countProduct');
			Route::delete('/coupon','OrderController@removeCoupon'); //Delete Coupon
			Route::delete('/{orderDetailId}',"OrderController@removeProduct"); // Remove From Cart
			Route::patch('/{orderDetailId}',"OrderController@updateQuantity"); 
			Route::get('/coupon/{cc}','OrderController@coupon'); // Add Coupon
			Route::get('/price','OrderController@finalPrice');
			Route::get('/{orderDetailId?}','OrderController@cartProducts'); // Cart Products
			Route::post('/changePayment/{id}','OrderController@changePaymentMode');
		});

		Route::post('/franchise-enquiry','HomeController@putFranchise');
		Route::get('/video-category/{id?}','CategoryController@show');
		Route::get('/materials','UploadController@show')->middleware('auth');
		Route::get('/materials/{id}/getFile','UploadController@getFile')->middleware('auth');
		Route::get('/products','ProductController@showAll');
		Route::get('/products/{id}','ProductController@show');
		Route::get('/video-products/category/{categoryId}','ProductController@getAllVideoProducts');
		Route::get('/location','LocationController@showloc');
		
	});