<?php
// 'admin/api/shipping'
Route::group(['prefix'=>'shipping'],function(){
	Route::get('/','ShippingController@show');
	Route::patch('/','ShippingController@update');
});