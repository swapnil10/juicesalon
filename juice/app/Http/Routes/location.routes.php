<?php
// 'admin/api/productType'
Route::group(['prefix'=>'location'],function(){
	Route::post('/','LocationController@store');
	Route::get('/{id?}','LocationController@show');
	Route::patch('/{id}','LocationController@update');
	Route::delete('/{id}','LocationController@destroy');
});
?>