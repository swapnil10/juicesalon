<?php
// 'admin/api/categories'
Route::group(['prefix'=>'category'],function(){
	Route::post('/add','CategoryController@store');
	Route::get('/{id?}','CategoryController@show');
	Route::patch('/{id}','CategoryController@update');
	Route::delete('/{id}','CategoryController@destroy');
	Route::patch('/{id}/parent/{parentId}','CategoryController@updateParent');
});