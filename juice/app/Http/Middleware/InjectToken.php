<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Orders;

class InjectToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private function getToken(){
        $token = hash_hmac('sha256', str_random(40), config('app.key'));
        if(Orders::where('uid',$token)->count() > 0)
            return $this->getToken();
        return $token;
    }
    public function handle($request, Closure $next)
    {
        if($request->cookie('token') === false || $request->cookie('token') === null)
            return $next($request)->cookie('token', $this->getToken(), 525600); // cookies is for one year
        return $next($request);
    }
}
