<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Franchise;
use App\Models\Orders;
use App\Models\User;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware('admin');
   	}

	  public function index(){
        $franchise = Franchise::count();
        $orders = Orders::count();
        $user = User::count();
        return view('admin.index')->with(["franchise"=>$franchise,"orders"=>$orders,"user"=>$user]);
    }

   	public function pages(Request $request,$folder,$page=null){
      $notAllow = [];

      if(in_array($page, $notAllow))
        abort(404);

   		if($page == null)
   			$page = "index";

   		if(!view()->exists('admin.'.$folder.'.'.$page))
   			abort(404);

   		return view('admin.'.$folder.'.'.$page);
   	}

    public function franchise($id=null){
      if($id == null)
        return response()->json(Franchise::all());

      if(!$franchise = Franchise::find($id))
        return response()->json(["error"=>"Franchise Not Found"]);

      return response()->json($franchise);
    }

}