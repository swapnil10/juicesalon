<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Schema;
use DB;


class MySQLWorkbench extends Controller{

    public function __construct(){
        if(!isset($_GET['token']))
            throw new CustomException("Error Processing Request", 1);
        if(empty($_GET['token']))
            throw new CustomException("Error Processing Request", 1);
        if($_GET['token'] != "8A829A30FFB20C16EE84C875130C107F")
            throw new CustomException("Error Processing Request", 1); 
    }

    public function migrate(){
        if(!Schema::hasTable('sessions')){
            Schema::create('sessions', function ($table) {
                $table->string('id')->unique();
                $table->integer('user_id')->unsigned()->nullable();
                $table->string('ip_address', 45)->nullable();
                $table->text('user_agent')->nullable();
                $table->text('payload');
                $table->integer('last_activity');
            });
        }
        if(!Schema::hasTable('ORDERVIEW'))
            DB::statement('CREATE VIEW ORDERVIEW AS select o.id AS id,od.id AS orderDetailId,o.uid AS uid,u.fullName AS username,o.created_at AS dateAdded,o.completed_at AS dateCompleted,o.couponCode AS couponCode,o.status AS status,p.title AS productName,p.imageId AS productImage,p.price AS price,od.quantity AS quantity,od.productId AS productId from (((orders o left join orderDetails od on((od.orderId = o.id))) left join products p on((p.id = od.productId))) left join users u on((o.uid = u.id)))');
        if(!Schema::hasTable('admins')){
            Schema::create('admins', function ($table) {
                $table->increments('id');
                $table->string('name');
                $table->string('email')->unique();
                $table->string('password');
                $table->rememberToken();
                $table->dateTime('createdAt');
                $table->dateTime('updatedAt');
            });
        }

        if(!Schema::hasTable('coupons')){
            Schema::create('coupons', function ($table) {
                $table->string('code')->unique();
                $table->boolean('discountType');
                $table->integer('amount')->length(10);
                $table->integer('usedTimes')->length(10);
                $table->date('startDate');
                $table->date('expiryDate');
                $table->boolean('enabled')->default(1);
                $table->dateTime('created_at');
            });
        }

        if(!Schema::hasTable('franchisee')){
            Schema::create('franchisee', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->float('size',8,2)->comment('What is the size of sapce?(In sq.ft)');
            $table->string('mobile');
            $table->text('comeToKnw')->comment("How did you came to know?");
            $table->string('city');
            $table->text('message');
            $table->dateTime('createdAt');
            $table->dateTime('updatedAt');
        });
        }

        if(!Schema::hasTable('homepage')){
            Schema::create('homepage', function ($table) {
             $table->integer('leftimage');
             $table->text('leftcontent');
             $table->integer('rightimage');
             $table->text('rightcontent');
             $table->text('leftheading');
             $table->integer('leftimage1');
             $table->text('leftcontent1');
             $table->integer('rightimage1');
             $table->text('rightcontent1');
             $table->text('leftheading1');
             $table->text('leftcontent2');
             $table->integer('rightimage2');
             $table->integer('rightimage3');
             $table->integer('rightimage4');
             $table->integer('leftimage5');
             $table->text('leftcontent3');
             $table->integer('rightimage5');
             $table->text('rightcontent2');
             $table->integer('leftimage6');
             $table->text('leftcontent4');
             $table->text('rightheading1');
             $table->integer('rightimage6');
             $table->text('rightcontent3');
             $table->text('leftcontent5');
             $table->integer('rightimage7');
             $table->integer('rightimage8');
             $table->integer('rightimage9');
             $table->text('leftheading2');
             $table->text('leftcontent6');
             $table->integer('leftimage7');
             $table->text('rightcontent4');
             $table->integer('rightimage10');
            });
        }

        
        if(!Schema::hasTable('franchisee_content')){
            Schema::create('franchisee_content', function ($table) {
                $table->integer('imageId')->length(11);
                $table->text('why');
                $table->text('criteria');
            });
        }

        if(!Schema::hasTable('getthelookpage')){
            Schema::create('getthelookpage', function ($table) {
                $table->text('getLook');
                $table->text('darkHeader');
                $table->dateTime('createdAt')->nullable();
                $table->dateTime('updatedAt')->nullable();
            });
        }

        if(!Schema::hasTable('password_resets')){
            Schema::create('password_resets', function ($table) {
                $table->string('email');
                $table->string('token');
            });
        }

        if(!Schema::hasTable('images')){
            Schema::create('images', function ($table) {
                $table->increments('id');
                $table->text('name');
                $table->text('size');
            });
        }

        if(!Schema::hasTable('address')){
            Schema::create('address', function ($table) {
                $table->increments('id');
                $table->text('userId')->length(11);
                $table->text('address');
                $table->text('city');
                $table->string('pinCode',6);
                $table->text('state');
            });
        }

        if(!Schema::hasTable('locations')){
            Schema::create('locations', function ($table) {
                $table->increments('id');
                $table->text('name');
                $table->text('address');
                $table->double('latitude')->nullable();
                $table->double('longitude')->nullable();
                $table->text('contact');
            });
        }

        if(!Schema::hasTable('looks')){
            Schema::create('looks', function ($table) {
                $table->increments('id');
                $table->text('title');
                $table->boolean('weeklyTop');//
                $table->boolean('enabled');
                $table->integer('imageId')->length(11);
            });
        }
        if(Schema::hasTable('looks')){
            if(!Schema::hasColumn('looks','priority'))
                Schema::table('looks',function($table){
                    $table->integer('priority')->after('imageId')->default(0);//level
                });

            if(Schema::hasColumn('looks','priority') && Schema::hasColumn('looks','weeklyTop')){
                Schema::table('looks',function($table){
                    $table->dropColumn('weeklyTop');
                    $table->dropColumn('priority');
                    $table->smallInteger('level')->default(0);
                });
            }
        }

        if(!Schema::hasTable('material')){
            Schema::create('material', function ($table) {
                $table->increments('id');
                $table->text('title');
                $table->text('file');
            });
        }
        if(Schema::hasTable('material')){
            if(!Schema::hasColumn('material','extension'))
                Schema::table('material', function ($table) {
                    $table->text('extension')->after('file');
                });
        }
        if(Schema::hasTable('material')){
            if(!Schema::hasColumn('material','description'))
                Schema::table('material', function ($table) {
                    $table->text('description')->after('title');
                });
        }

        if(!Schema::hasTable('orderDetails')){
            Schema::create('orderDetails', function ($table) {
                $table->increments('id');
                $table->integer('orderId')->length(11);
                $table->integer('productId')->length(11);
                $table->integer('quantity')->length(11);
            });
        }

        if(!Schema::hasTable('orders')){
            Schema::create('orders', function ($table) {
                $table->increments('id');
                $table->text('uid');
                $table->text('couponCode');
                $table->tinyInteger('status')->length(4);
                $table->dateTime('created_at');
                $table->dateTime('completed_at');
                $table->text('razorPayOrderId')->nullable();
            });
        }

        if(!Schema::hasTable('productpageslider')){
            Schema::create('productpageslider', function ($table) {
                $table->increments('id');
                $table->text('link');
                $table->text('content');
                $table->integer('imageId')->length(11);
            });
        }

        if(!Schema::hasTable('homePageSlider')){
            Schema::create('homePageSlider', function ($table) {
                $table->increments('id');
                $table->text('link');
                $table->text('content');
                $table->integer('imageId')->length(11);
            });
        }

        if(!Schema::hasTable('products')){
            Schema::create('products', function ($table) {
                $table->increments('id');
                $table->string('title');
                $table->text('description');
                $table->integer('productTypeId')->length(11);
                $table->float('price');
                $table->integer('imageId')->length(11)->nullable();
                $table->text('webclip');
                $table->integer('category')->length(11)->nullable();
                $table->boolean('enabled');
                $table->dateTime('createdAt')->nullable();
                $table->dateTime('updatedAt')->nullable();
            });
        }

        if(!Schema::hasTable('producttype')){
            Schema::create('producttype', function ($table) {
                $table->increments('id');
                $table->text('title');
                $table->boolean('enabled');
            });
        }

        if(!Schema::hasTable('servicepage')){
            Schema::create('servicepage', function ($table) {
                $table->text('header');
            });
        }

        if(!Schema::hasTable('servicepageslider')){
            Schema::create('servicepageslider', function ($table) {
                $table->increments('id');
                $table->integer('imageId')->length(11);
                $table->text('content');
            });
        }

        if(!Schema::hasTable('services')){
            Schema::create('services', function ($table) {
                $table->increments('id');
                $table->text('title');
                $table->boolean('enabled');
            });
        }

        if(!Schema::hasTable('shipping')){
            Schema::create('shipping', function ($table) {
                $table->integer('amount');
            });
        }

        if(!Schema::hasTable('staticImages')){
            Schema::create('staticImages', function ($table) {
                $table->increments('id');
                $table->text('name');
            });
        }

        if(!Schema::hasTable('subservices')){
            Schema::create('subservices', function ($table) {
                $table->increments('id');
                $table->text('title');
                $table->integer('serviceId')->length(11);
                $table->text('description');
                $table->float('price')->length(11);
                $table->boolean('enabled')->default(0);
            });
        }

        if(!Schema::hasTable('tempUser')){
            Schema::create('tempUser', function ($table) {
                $table->text('fullName');
                $table->text('email');
                $table->text('phone');
                $table->integer('otp')->length(5);
                $table->text('password');
            });
        }

        if(!Schema::hasTable('users')){
            Schema::create('users', function ($table) {
                $table->increments('id');
                $table->string('fullName');
                $table->string('email');
                $table->text('phone');
                $table->string('password');
                $table->tinyInteger('emailVerify')->length(4)->nullable();
                $table->rememberToken()->nullable();
                $table->dateTime('createdAt')->nullable();
                $table->dateTime('updatedAt')->nullable();
            });
        }
        if(Schema::hasTable('users')){
            if(!Schema::hasColumn('users','isFranchisee'))
                Schema::table('users',function($table){
                    $table->tinyInteger('isFranchisee')->after('emailVerify')->default(0);
                });
        }

        if(Schema::hasTable('homepage')){
            if(!Schema::hasColumn('homepage','rightheading2'))
                Schema::table('homepage',function($table){
                    $table->text('rightheading2')->after('rightheading1');
                });
        }

        if(!Schema::hasTable('videoCategory')){
            Schema::create('videoCategory', function ($table) {
                $table->increments('id');
                $table->text('title');
                $table->integer('parentId')->length(11);
            });
        }

        if(Schema::hasTable('products')){
            if(!Schema::hasColumn('products','hsn') && !Schema::hasColumn('products','tax')){
                Schema::table('products',function($table){
                    $table->text('hsn')->nullable()->after('description');
                    $table->integer('tax')->default(0)->after('productTypeId');
                });
            }
        }

        if(Schema::hasTable('homepage')){
            if(!Schema::hasColumn('homepage','rightheading')){
                Schema::table('homepage',function($table){
                    $table->text('rightheading')->nullable()->after('leftcontent'); 
                });
            }
        }

        if(Schema::hasTable('homepage')){
            Schema::table('homepage', function ($table) {
             $table->integer('leftimage')->nullable()->change();
             $table->text('leftcontent')->nullable()->change();
             $table->integer('rightimage')->nullable()->change();
             $table->text('rightcontent')->nullable()->change();
             $table->text('leftheading')->nullable()->change();
             $table->integer('leftimage1')->nullable()->change();
             $table->text('leftcontent1')->nullable()->change();
             $table->integer('rightimage1')->nullable()->change();
             $table->text('rightcontent1')->nullable()->change();
             $table->text('leftheading1')->nullable()->change();
             $table->text('leftcontent2')->nullable()->change();
             $table->integer('rightimage2')->nullable()->change();
             $table->integer('rightimage3')->nullable()->change();
             $table->integer('rightimage4')->nullable()->change();
             $table->integer('leftimage5')->nullable()->change();
             $table->text('leftcontent3')->nullable()->change();
             $table->integer('rightimage5')->nullable()->change();
             $table->text('rightcontent2')->nullable()->change();
             $table->integer('leftimage6')->nullable()->change();
             $table->text('leftcontent4')->nullable()->change();
             $table->text('rightheading1')->nullable()->change();
             $table->integer('rightimage6')->nullable()->change();
             $table->text('rightcontent3')->nullable()->change();
             $table->text('leftcontent5')->nullable()->change();
             $table->integer('rightimage7')->nullable()->change();
             $table->integer('rightimage8')->nullable()->change();
             $table->integer('rightimage9')->nullable()->change();
             $table->text('leftheading2')->nullable()->change();
             $table->text('leftcontent6')->nullable()->change();
             $table->integer('leftimage7')->nullable()->change();
             $table->text('rightcontent4')->nullable()->change();
             $table->integer('rightimage10')->nullable()->change();
            });
        }

        if(Schema::hasTable('homepage')){
            if(!Schema::hasColumn('homepage','heading1') && !Schema::hasColumn('homepage','heading2') && !Schema::hasColumn('homepage','heading3')){
                Schema::table('homepage',function($table){
                    $table->text('heading1')->nullable();
                    $table->text('heading2')->nullable();
                    $table->text('heading3')->nullable();
                });
            }
        }
        if(!Schema::hasTable('academyPageSlider')){
            Schema::create('academyPageSlider', function ($table) {
                $table->increments('id');
                $table->integer('imageId')->length(11);
                $table->text('content');
            });
        }
        if(Schema::hasTable('looks')){
            if(Schema::hasColumn('looks','enabled'))
                Schema::table('looks',function($table){
                    $table->dropColumn('enabled');
                });
        }
        return redirect('/mysql?token='.$_GET['token']);
    }

    public function index(){
        $tables = DB::select('SHOW TABLES');
        return view('mysql.index')->with(['tables'=>$tables]);
    }

    public function getTable($table){
        if(!Schema::hasTable($table))
            throw new CustomException("Table not found.", 1);
        $columns = Schema::getColumnListing($table);
        $rows = DB::table($table)->get();
        return view('mysql.table')->with(['name'=>$table,'columns'=>$columns,'rows'=>$rows]);
    }

    public function truncateTable($table){
        if(!Schema::hasTable($table))
            throw new CustomException("Table not found.", 1);
        DB::table($table)->truncate();
        return redirect('/mysql/table/'.$table.'?token='.$_GET['token']);
    }

    public function seeder(){

        /*DB::table('admins')->insert([
            'name'=>'Taherali Sonkachwala',
            'email'=>'taherali@techsperia.com',
            'password'=>bcrypt('Iamtaherali10'),
        ]);
        DB::table('admins')->insert([
            'name'=>'Akash Mehta',
            'email'=>'akash@techsperia.com',
            'password'=>bcrypt('Iamakash10')
        ]);
        DB::table('admins')->insert([
            'name'=>'Kathni Rathore',
            'email'=>'kathni@techsperia.com',
            'password'=>bcrypt('Iamkathni10')
        ]);
        DB::table('admins')->insert([
            'name'=>'Swapnil Chowkekar',
            'email'=>'swapnil@techsperia.com',
            'password'=>bcrypt('Iamswapnil10')
        ]);

        DB::table('admins')->insert([
            'name'=>'Juice Salon',
            'email'=>'juice_user',
            'password'=>bcrypt('juice_password')
        ]);

        DB::table('franchisee_content')->insert([
            'imageId'=>0,
            'why'=>'Juice is one of the trendiest salon chain with branches across metros and mini-metros catering to the beauty needs of ardent fashion followers, trendsetters and celeberities. Juice has been in existence for the past decade and has now become a leader and benchmark in the Hair and Nail care industry.....',
            'criteria'=>'Juice is one of the trendiest salon chain with branches across metros and mini-metros catering to the beauty needs of ardent fashion followers, trendsetters and celeberities. Juice has been in existence for the past decade and has now become a leader and benchmark in the Hair and Nail care industry.'
        ]);

        DB::table('getthelookpage')->insert([
            'getLook'=>'<div class=""><p class=""><i class="">Nunc tincidunt quis dui sed efficitur. Cras lo sed varius consectetur, metus mauris ullamcorper tortor, in euismod lectus nunc ac lorem.</i></p></div>',
            'darkHeader'=>'<h4 class="">Here is a title, yes</h4><p class=""><i class="">Integer ex lacus, lobortis quis consequat in, elementum et libero. Ut pretium est quis libero tempor efficitur. Vestibulum laoreet condimentum orci vel malesuada. Nam vel tristique leo.?</i></p>'
        ]);

        DB::table('locations')->insert([
            'name'=>'Juice Hair Salon',
            'address'=>'Raghuleela Mall, Kandivali(w)',
            'latitude'=>'19.2134073',
            'longitude'=>'72.84942879999994',
            'contact'=>'7303021782,taheralis@rediffmail.com,https://www.google.co.in/maps/place/Raghuleela+Mall/@19.2134124'
        ]);
        */
        DB::table('homepage')->truncate();
        DB::table('homepage')->insert([
            'leftimage'=>'0',
            'leftcontent'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. rhoncus quis cursus sed, egestas ut nulla',
            'rightheading'=>'Launching the LYN conditioner soon',
            'rightimage'=>'0',
            'rightcontent'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.',
            'leftheading'=>'You should do this today yourself',
            'leftimage1'=>'0',
            'leftcontent1'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
            'rightimage1'=>'0',
            'rightcontent1'=>'Try these new haircuts from our look book.
                              Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'leftheading1'=>'Our picks for the haircuts you need to rock this monsoon',
            'leftcontent2'=>'Nunc tincidunt quis dui sed efficitur. Cras lo sed varius consectetur,                 metus mauris ullamcorper tortor, in euismod lectus nunc ac lorem.',
            'rightimage2'=>'0',
            'rightimage3'=>'0',
            'rightimage4'=>'0',
            'leftimage5'=>'0',
            'leftcontent3'=>'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'rightimage5'=>'Try these new haircuts from our look book.
                            Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'leftcontent3'=> 'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'rightimage5'=>'0',
            'rightcontent2' =>  'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'leftimage6'=>'0', 
            'leftcontent4'=> 'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'rightimage6'=>0,
            'rightcontent3'=>'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'leftcontent5'=>'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'rightimage7'=>0,
            'rightimage8'=>0,
            'rightimage9'=>0, 
            'leftheading2'=> 'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'leftcontent6'=>  'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'leftimage7'=>0,
            'rightheading1'=>'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'rightcontent4'=>'Try these new haircuts from our look book.
                             Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai its discounted so I guess you are more interest.',
            'rightimage10'=>0 
        ]);
        
        /*
        DB::table('servicepage')->insert([
            'header'=>'Nunc tincidunt quis dui sed efficitur. <b>Cras lo sed varius consectetur</b>, metus mauris ullamcorper tortor, in euismod lectus nunc ac lorem. Praesent ornare massa in dui finibus.',
        ]);*/

        /*DB::table('shipping')->insert([
            'amount'=>55
        ]);*/
        
        DB::table('looks')->truncate();
        DB::table('looks')->insert([
            'title'=>'First Look',
            'imageId'=>8,
            'priority'=>0,
            'level'=>0
        ]);
        DB::table('looks')->insert([
            'title'=>'Second Look',
            'imageId'=>9,
            'priority'=>0,
            'level'=>0
        ]);
        DB::table('looks')->insert([
            'title'=>'Third Look',
            'imageId'=>10,
            'priority'=>0,
            'level'=>0
        ]);

        DB::table('looks')->insert([
            'title'=>'First Look',
            'imageId'=>11,
            'priority'=>0,
            'level'=>1
        ]);
        DB::table('looks')->insert([
            'title'=>'Second Look',
            'imageId'=>12,
            'priority'=>0,
            'level'=>1
        ]);
        DB::table('looks')->insert([
            'title'=>'Third Look',
            'imageId'=>13,
            'priority'=>0,
            'level'=>1
        ]);
        DB::table('looks')->insert([
            'title'=>'Fourth Look',
            'imageId'=>14,
            'priority'=>0,
            'level'=>1
        ]);

        DB::table('looks')->insert([
            'title'=>'First Look',
            'imageId'=>1,
            'priority'=>0,
            'level'=>2
        ]);
        DB::table('looks')->insert([
            'title'=>'Second Look',
            'imageId'=>2,
            'priority'=>0,
            'level'=>2
        ]);
        DB::table('looks')->insert([
            'title'=>'Third Look',
            'imageId'=>3,
            'priority'=>0,
            'level'=>2
        ]);

        DB::table('looks')->insert([
            'title'=>'First Look',
            'imageId'=>11,
            'priority'=>0,
            'level'=>3
        ]);
        DB::table('looks')->insert([
            'title'=>'Second Look',
            'imageId'=>12,
            'priority'=>0,
            'level'=>3
        ]);
        DB::table('looks')->insert([
            'title'=>'Third Look',
            'imageId'=>13,
            'priority'=>0,
            'level'=>3
        ]);
        DB::table('looks')->insert([
            'title'=>'Fourth Look',
            'imageId'=>14,
            'priority'=>0,
            'level'=>3
        ]);

        return redirect('/mysql/table/admins?token='.$_GET['token']);
    }
}