<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\Material;
use App\Http\Controllers\UploadController;

use Validator;

use Exception;

//use Illuminate\Support\Facades\DB;


class MaterialController extends Controller
{
    public function addFile(Request $request){
        $this->validate($request,[
            'image'=>'bail|required'
        ]);

        

        $res = [];
        try{
            $material = new Material();
            $save = UploadController::save($request,'image');
            if($save === false)
                throw new Exception("File Adding failed", 1);
            UploadController::destroy($material->id);

            $material->id = $save;
            $material->save();
            $res = ["success"=>true];
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }

        return response()->json($res);
    }

    public function destroyFile($id){
        $res = [];
        try{
            if(!$material = Material::find($id))
                throw new Exception("No File Found", 1);
            UploadController::destroy($material->id);
            $material->id = 0;
            $material->save();
            $res = ["success"=>true];
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }
        return response()->json($res);
    }

    public function show(Request $request,$id=null){

        if($id != null || $id != "" || is_int($id)){
            if(!$material = Material::find($id))
                return response()->json(['error'=>'Product not found.']);
            else
                return response()->json($material); 
        }
        $material = Material::all();
        return response()->json($material);
    }

}
