<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\Location;

use Exception;

class LocationController extends Controller{

	public function store(Request $request){
		$this->validate($request,[
			'name'=>'required',
			'address'=>'required',
			'longitude'=>'required|numeric',
			'latitude'=>'required|numeric',
			'phone'=>'required|digits:10',
			'email'=>'required|email',
			'website'=>'required|url'
		]);

		$location = new Location();
		$location->name = $request->name;
		$location->address = $request->address;
		$location->longitude = $request->longitude;
		$location->latitude = $request->latitude;
		$location->contact = $request->phone.','.$request->email.','.$request->website;
		$location->save();
		return response()->json(["name"=>$location->name,"id"=>$location->id]);
	}

	public function show($id=null){
		if($id == null)
			return response()->json(Location::select('id','name')->get());
		if(!$location = Location::find($id))
			return response()->json(["error"=>"Location not found."]);
		return response($location);
	}
	public function showloc(){
			return response()->json(Location::get());
	}

	public function update(Request $request,$id){
		$this->validate($request,[
			'name'=>'required',
			'address'=>'required',
			'longitude'=>'required|numeric',
			'latitude'=>'required|numeric',
			'phone'=>'required|digits:10',
			'email'=>'required|email',
			'website'=>'required|url'
		]);

		if(!$location = Location::find($id))
			return response()->json(["error"=>"Location not found."]);
		$location->name = $request->name;
		$location->address = $request->address;
		$location->longitude = $request->longitude;
		$location->latitude = $request->latitude;
		$location->contact = $request->phone.','.$request->email.','.$request->website;
		$location->save();
		return response()->json(["name"=>$location->name,"id"=>$location->id]);
	}

	public function destroy($id){
		if(!$location = Location::find($id))
			return response()->json(["error"=>"Location not found."]);
		$location->delete();
		return response()->json(["success"=>true]);
	}
}
?>