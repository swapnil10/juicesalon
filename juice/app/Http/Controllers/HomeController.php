<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Franchise;
use App\Models\HomePage;
use DB;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller{

 	public function pages($page=null){
    $notAllow = [];
    if(in_array($page, $notAllow))
      abort(404);

 		if($page == null)
 			$page = "index";

 		if(!view()->exists($page))
 			abort(404);

 		return view($page);
 	}

 	public function putFranchise(Request $request){
 		$this->validate($request,[
 			'name' => 'required',
 			'email'  => 'required|email',
 			'number' => 'required|numeric|digits:10',
 			'city' =>'required',
 			'ownPlace' => 'required|boolean',
 			'size' => 'present',
 			'foundSalon' => 'present',
 			'message' => 'required'
 		]);

 		$franchise = new Franchise();
 		$franchise->name = $request->name;
 		$franchise->email = $request->email;
 		$franchise->mobile = $request->number;
 		$franchise->size = $request->size;
 		$franchise->comeToKnw = $request->foundSalon;
 		$franchise->city = $request->city;
 		$franchise->message = $request->message;
 		$franchise->ownPlace = $request->ownPlace;

 		$franchise->save();
 	}

 	public function getall(){
 	 return response()->json(HomePage::all());
 	}

 	public function homePage(Request $request){
 		/*$this->validate($request,[
 			'leftimage' => 'required',
 			'leftcontent' => '',
 			'rightimage' => '',
 			'rightcontent' => '',
 			'leftheading' => '',
 			'leftimage1' => '',
 			'leftcontent1' => '',
 			'rightimage1' => '',
 			'rightcontent1' => '',
 			'leftheading1' => '',
 			'leftcontent2' => '',
 			'rightimage2' => '',
 			'rightimage3' => '',
 			'rightimage4' => '',
 			'leftimage5' => '',
 			'leftcontent3' => '',
 			'rightimage5' => '',
 			'rightcontent2' => '',
 			'leftimage6' => '',
 			'leftcontent4' => '',
 			'rightheading' => '',
 			'rightimage6' => '',
 			'rightcontent3' => '',
 			'leftcontent5' => '',
 			'rightimage7' => '',
 			'rightimage8' => '',
 			'rightimage9' => '',
 			'leftheading2' => '',
 			'leftcontent6' => '',
 			'leftimage7' => '',
 			'rightheading1' => '',
 			'rightcontent4' => '',
 			'rightimage10' => '',
 		]);*/
 		DB::table('homepage')->update(Input::all());
 		return response()->json(['success'=>true]);
 		/*$homePage->leftimage = $request->leftimage;
 		$homePage->leftcontent = "jj";//$request->leftcontent;
 		$homePage->rightimage = $request->rightimage;
 		$homePage->rightcontent = $request->rightcontent;
 		$homePage->leftheading = $request->leftheading;
 		$homePage->leftimage1 = $request->leftimage1;
 		$homePage->leftcontent1 = $request->leftcontent1;
 		$homePage->rightimage1 = $request->rightimage1;
 		$homePage->rightcontent1 = $request->rightcontent1;
 		$homePage->leftheading1 = $request->leftheading1;
 		$homePage->leftcontent2 = $request->leftcontent2;
 		$homePage->rightimage2 = $request->rightimage2;
 		$homePage->rightimage3 = $request->rightimage3;
 		$homePage->rightimage4 = $request->rightimage4;
 		$homePage->leftimage5 = $request->leftimage5;
 		$homePage->leftcontent3 = $request->leftcontent3;
 		$homePage->rightimage5 = $request->rightimage5;
 		$homePage->rightcontent2 = $request->rightcontent2;
 		$homePage->leftimage6 = $request->leftimage6;
 		$homePage->leftcontent4 = $request->leftcontent4;
 		$homePage->rightheading = $request->rightheading;
 		$homePage->rightimage6 = $request->rightimage6;
 		$homePage->rightcontent3 = $request->rightcontent3;
 		$homePage->leftcontent5 = $request->leftcontent5;
 		$homePage->rightimage7 = $request->rightimage7;
 		$homePage->rightimage8 = $request->rightimage8;
 		$homePage->rightimage9 = $request->rightimage9;
 		$homePage->leftheading2 = $request->leftheading2;
 		$homePage->leftcontent6 = $request->leftcontent6;
 		$homePage->leftimage7 = $request->leftimage7;
 		$homePage->rightheading1 = $request->rightheading1;
 		$homePage->rightcontent4 = $request->rightcontent4;
 		$homePage->rightimage10 = $request->rightimage10;
 		$homePage->save();*/
 	}
}