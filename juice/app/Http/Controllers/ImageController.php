<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\Image;

use Validator;

use Exception;

use Storage;

// use App\Exceptions\CustomException;

class ImageController extends Controller{
	
	public static function save($request,$fieldName){
		if(!$request->hasFile($fieldName))
			throw new Exception("File is missing or corrupted.", 1);

		$extAllow = ['jpg','jpeg','png'];
		$file = $request->file($fieldName);
		$ext = $file->getClientOriginalExtension();

		if(!in_array($ext, $extAllow))
			throw new Exception("Invalid File Extention.", 1);

		$md5 = md5_file($file);
		$name = $md5.'.'.$ext;
		$size = filesize($file);
		if(Storage::exists('images/'.$name)){
			$name.=$name.'_copy'.$ext;
		}
		

		Storage::put('images/'.$name,file_get_contents($file->getRealPath()));
		$image = new Image();
		$image->name = $name;
		$image->size = $size;
		$image->save();
		return $image->id;
	}

	public function get($id){
		if(!$image = Image::find($id))
			abort(404);
		$file = 'images/'.$image->name;
		if(!Storage::exists($file))
			abort(404);
		$content = Storage::get($file);
    	$type = Storage::mimeType($file);
    	return response()->make($content,200)->header('Content-Type',$type);
	}

	public static function destroy($id){
		if($id == 0)
			return;
		if(!$image = Image::find($id))
    		throw new Exception("Image Not Found.", 1);

    	$file ='images/'.$image->name;
    	if(Storage::exists($file)){
    		Storage::delete($file);
    	}

    	$image->delete();

    	return response()->json(["success"=>true]);	
	}
}