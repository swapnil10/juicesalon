<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ServiceSlider;
use App\Models\ProductSlider;
use App\Models\AcademySlider;
use App\Models\HomeSlider;

use Validator;

class SliderContentController extends Controller{

    /**********************SERVICE************************/
    public function storeService(Request $request){
        $this->validate($request,[
            'content'=>'required'
        ]);
        $service = new ServiceSlider();
        $service->content = $request->content;
        $service->save();
        return response()->json(["id"=>$service->id]);
    }

    public function getService($id=null){

        if($id != null){
            if(!$slider = ServiceSlider::find($id))
                return response()->json(['error'=>'Service Slider details not found.']);
            return response()->json($slider);
        }
        $slider = ServiceSlider::all();
        return response()->json($slider);
    }

    public function updateService(Request $request,$id){
        if(!$slider = ServiceSlider::find($id))
            return response()->json(['error'=>'Service Slider details not found.']);
        
        $this->validate($request,[
        	'content' => 'required'
        ]);

        $slider->content = $request->content;
        
        if($slider->save())
            return response()->json(['id'=>$slider->id]);

        return response()->json(['error'=>"Something went wrong."]);
    }

    public function imageService(Request $request){
        // return response()->file($request->image);
        $this->validate($request,[
            'id'=>'required|numeric',
            'image'=>'required'
        ]);

        if(!$bt = ServiceSlider::find($request->id))
            return response()->json(["error"=>"Service Slider Not Found."]);
        $res = ["success"=>true];
        try{
            ImageController::destroy($bt->imageId);
            $bt->imageId = 0;
            $imageId = ImageController::save($request,'image');
            $bt->imageId = $imageId;
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }
        $bt->save();
        return response()->json($res);
    }

    public function imageRemoveService($id){
        if(!$bt = ServiceSlider::find($id))
            return response()->json(['error'=>'Service Slider not found.']);
        try{
            ImageController::destroy($bt->imageId);
        }catch(Exception $e){
            return response()->json(["error"=>$e->getMessage()]);
        }
        $bt->imageId = 0;
        return response()->json(["success"=>$bt->save()]);
    }

    public function destroyService($id){
        if(!$bt = ServiceSlider::find($id))
            return response()->json(["error"=>"Service Slider Not Found."]);
        try{
            ImageController::destroy($bt->imageId);
        }
        catch(Exception $e){/*do Nothing */}
        return response()->json(["success"=>$bt->delete()]);
    }

    /**********************HOME************************/
    public function storeHome(Request $request){
        $this->validate($request,[
            'content'=>'required',
            'link'=>'present|url'
        ]);
        $service = new HomeSlider();
        $service->content = $request->content;
        $service->link = $request->link;
        $service->save();
        return response()->json(["id"=>$service->id]);
    }

    public function getHome($id=null){

        if($id != null){
            if(!$slider = HomeSlider::find($id))
                return response()->json(['error'=>'Home Slider details not found.']);
            return response()->json($slider);
        }
        $slider = HomeSlider::all();
        return response()->json($slider);
    }

    public function updateHome(Request $request,$id){
        if(!$slider = HomeSlider::find($id))
            return response()->json(['error'=>'Home Slider details not found.']);
        
        $this->validate($request,[
            'content' => 'required',
            'link'=>'present|url'
        ]);

        $slider->content = $request->content;
        $slider->link = $request->link;
        if($slider->save())
            return response()->json(['id'=>$slider->id]);

        return response()->json(['error'=>"Something went wrong."]);
    }

    public function imageHome(Request $request){
        // return response()->file($request->image);
        $this->validate($request,[
            'id'=>'required|numeric',
            'image'=>'required'
        ]);

        if(!$bt = HomeSlider::find($request->id))
            return response()->json(["error"=>"Home Slider Not Found."]);
        $res = ["success"=>true];
        try{
            ImageController::destroy($bt->imageId);
            $bt->imageId = 0;
            $imageId = ImageController::save($request,'image');
            $bt->imageId = $imageId;
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }
        $bt->save();
        return response()->json($res);
    }

    public function imageRemoveHome($id){
        if(!$bt = HomeSlider::find($id))
            return response()->json(['error'=>'Home Slider not found.']);
        try{
            ImageController::destroy($bt->imageId);
        }catch(Exception $e){
            return response()->json(["error"=>$e->getMessage()]);
        }
        $bt->imageId = 0;
        return response()->json(["success"=>$bt->save()]);
    }

    public function destroyHome($id){
        if(!$bt = HomeSlider::find($id))
            return response()->json(["error"=>"Home Slider Not Found."]);
        try{
            ImageController::destroy($bt->imageId);
        }
        catch(Exception $e){/*do Nothing */}
        return response()->json(["success"=>$bt->delete()]);
    }

    /**********************Product************************/
    public function storeProduct(Request $request){
        $this->validate($request,[
            'content'=>'required',
            'link'=>'present|url'
        ]);
        $service = new ProductSlider();
        $service->content = $request->content;
        $service->link = $request->link;
        $service->save();
        return response()->json(["id"=>$service->id]);
    }

    public function getProduct($id=null){

        if($id != null){
            if(!$slider = ProductSlider::find($id))
                return response()->json(['error'=>'Product Slider details not found.']);
            return response()->json($slider);
        }
        $slider = ProductSlider::all();
        return response()->json($slider);
    }

    public function updateProduct(Request $request,$id){
        if(!$slider = ProductSlider::find($id))
            return response()->json(['error'=>'Product Slider details not found.']);
        
        $this->validate($request,[
            'content' => 'required',
            'link'=>'required|url'
        ]);

        $slider->content = $request->content;
        $slider->link = $request->link;
        if($slider->save())
            return response()->json(['id'=>$slider->id]);

        return response()->json(['error'=>"Something went wrong."]);
    }

    public function imageProduct(Request $request){
        // return response()->file($request->image);
        $this->validate($request,[
            'id'=>'required|numeric',
            'image'=>'required'
        ]);

        if(!$bt = ProductSlider::find($request->id))
            return response()->json(["error"=>"Service Slider Not Found."]);
        $res = ["success"=>true];
        try{
            ImageController::destroy($bt->imageId);
            $bt->imageId = 0;
            $imageId = ImageController::save($request,'image');
            $bt->imageId = $imageId;
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }
        $bt->save();
        return response()->json($res);
    }

    public function imageRemoveProduct($id){
        if(!$bt = ProductSlider::find($id))
            return response()->json(['error'=>'Service Slider not found.']);
        try{
            ImageController::destroy($bt->imageId);
        }catch(Exception $e){
            return response()->json(["error"=>$e->getMessage()]);
        }
        $bt->imageId = 0;
        return response()->json(["success"=>$bt->save()]);
    }

    public function destroyProduct($id){
        if(!$bt = ProductSlider::find($id))
            return response()->json(["error"=>"Service Slider Not Found."]);
        try{
            ImageController::destroy($bt->imageId);
        }
        catch(Exception $e){/*do Nothing */}
        return response()->json(["success"=>$bt->delete()]);
    }


    /**********************ACADEMY************************/
    public function storeAcademy(Request $request){
        $this->validate($request,[
            'content'=>'required'
        ]);
        $service = new AcademySlider();
        $service->content = $request->content;
        $service->save();
        return response()->json(["id"=>$service->id]);
    }

    public function getAcademy($id=null){

        if($id != null){
            if(!$slider = AcademySlider::find($id))
                return response()->json(['error'=>'Academy Slider details not found.']);
            return response()->json($slider);
        }
        $slider = AcademySlider::all();
        return response()->json($slider);
    }

    public function updateAcademy(Request $request,$id){
        if(!$slider = AcademySlider::find($id))
            return response()->json(['error'=>'Academy Slider details not found.']);
        
        $this->validate($request,[
            'content' => 'required'
        ]);

        $slider->content = $request->content;
        
        if($slider->save())
            return response()->json(['id'=>$slider->id]);

        return response()->json(['error'=>"Something went wrong."]);
    }

    public function imageAcademy(Request $request){
        // return response()->file($request->image);
        $this->validate($request,[
            'id'=>'required|numeric',
            'image'=>'required'
        ]);

        if(!$bt = AcademySlider::find($request->id))
            return response()->json(["error"=>"Service Slider Not Found."]);
        $res = ["success"=>true];
        try{
            ImageController::destroy($bt->imageId);
            $bt->imageId = 0;
            $imageId = ImageController::save($request,'image');
            $bt->imageId = $imageId;
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }
        $bt->save();
        return response()->json($res);
    }

    public function imageRemoveAcademy($id){
        if(!$bt = AcademySlider::find($id))
            return response()->json(['error'=>'Service Slider not found.']);
        try{
            ImageController::destroy($bt->imageId);
        }catch(Exception $e){
            return response()->json(["error"=>$e->getMessage()]);
        }
        $bt->imageId = 0;
        return response()->json(["success"=>$bt->save()]);
    }

    public function destroyAcademy($id){
        if(!$bt = AcademySlider::find($id))
            return response()->json(["error"=>"Academy Slider Not Found."]);
        try{
            ImageController::destroy($bt->imageId);
        }
        catch(Exception $e){/*do Nothing */}
        return response()->json(["success"=>$bt->delete()]);
    }
}