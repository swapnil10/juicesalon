<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;



use App\Http\Requests;



use App\Http\Controllers\Controller;

use App\Models\Material;
use Auth;

use Storage;


use Validator;



class UploadCOntroller extends Controller{

	public function show($id=null){

		if($id == null)
			return response()->json(Material::all());

		if(!$brands = Material::find($id))
			return response()->json(['error'=>'No Material found.']);

		return response()->json($brands);

	}

	public function destroy($id){
		if(!$brand = Material::find($id))
			return response()->json(["error"=>"Material Not Found."]);
		$this->removeImage($brand->file);
		$brand->delete();
		return response()->json(["success"=>true]);
	}

	public function file(Request $request){

		$this->validate($request,[
			'id'=>'required|numeric',
			'file'=>'required'
		]);

		if(!$material = Material::find($request->id))
			return response()->json(["error"=>"Material Not Found."]);

		
		if(!$request->hasFile('file') || !$request->file('file')->isValid())
            return response()->json(["error"=>"Invalid File."]);

        $ext =  $request->file->extension();

        if($ext != 'pdf')
        	return response()->json(["error"=>"Invalid File."]);

        $file = $request->file('file');
        $size = filesize($file);

        if($size > 5242880)
        	return response()->json(["error"=>"File size is too large."]);
       
       $fileName = $material->title.'-'.$material->id.'-juice.pdf';
		// $material->save('/storage/materials/'.$file);
        //$filePath = $file->store('materials');
        Storage::put('materials/'.$fileName,file_get_contents($request->file('file')->getRealPath()));
        
		$this->removeImage($material->file);

		$material->file = $fileName;
		$material->extension = $ext;
		$material->save();
		return response()->json(["success"=>true]);
	}

	private function removeImage($file){
		if(empty($file))
			return;
		$file = 'materials/'.$file;
		if(Storage::exists($file))
			Storage::delete($file);
		return;
	}

	public function store(Request $request){

		$this->validate($request,[
			'name' => 'required',
		]);

		$material = new Material;
		$material->title = $request->name;
		if(!$material->save())
			return response()->json(['error'=>'Material\'s Adding Failed.']);

		return response()->json(['id'=>$material->id,'name'=>$material->title]);

	}

	public function update(Request $request, $id){

		$this->validate($request,[
			'name'=>'required'
		]);

		if(!$material = Material::find($id))
			return response()->json(['error'=>'No Material found.']);

		$material->title = $request->name;
		if(!$material->save())
			return response()->json(['error'=>'Material Update Failed.']);

		return response()->json(['id'=>$material->id,"name"=>$material->title]);

	}

	public function fileRemove($id){
		if(!$material = Material::find($id))
			return response()->json(['error'=>'No Material found.']);

		$this->removeImage($material->file);

		$material->file = "";

		return response()->json(["success"=>$material->save()]);
	}

	public function getFile(Request $request,$id){
		$allow = false;
		if($request->is('admin/*'))
			$allow = true;
		else{
			$user = Auth::user();
			if($user->isFranchisee)
				$allow = true;
			else
				$allow = false;
		}

		if($allow == false)
			abort(404);
		if(!$material = Material::find($id))
			return response()->json(['error'=>'Material not found.']);
		$file = 'materials/'.$material->file;
		if(!Storage::exists($file))
			abort(404);
		$content = Storage::get($file);
    	$type = Storage::mimeType($file);
    	if($request->is('admin/*'))
    		return response()->make($content,200)->header('Content-Type',$type);
    	return response()->make($content,200)->header('Content-Type',$type);
    	/*return response()->make($content,200)->withHeaders([
    		'Content-Type'=>'application/force-download',
    		'Content-Disposition'=>'attachment',
    		'filename'=>$material->title.'.'.$material->extension,
    		'Connection'=>'close'
    	]);*/
	}
}