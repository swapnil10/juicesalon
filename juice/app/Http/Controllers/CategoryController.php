<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\VideoCategory;

use Validator;

//use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{
    public function store(Request $request){
        //$request->enabled =0;
        $this->validate($request, [
        'title' => 'bail|required|max:255|unique:videoCategory'
        ]);
        $category = new VideoCategory;
        $category->title = $request->title;
        $category->parentId = 0;
        if($category->save())
            return response()->json(['id'=>$category->id,'title'=>$category->title]);

        return response()->json(['error'=>"Something went wrong."]);
    }
    public function show(Request $request,$id=null){
        // if($id == 1)
        //     return response()->json(['error'=>'Category not found']);

        if(!$request->is('admin/*'))
            return $this->showTreeView($id);

        if($id != null || $id != "" || is_int($id)){
            if(!$category = VideoCategory::find($id))
                return response()->json(['error'=>'Category not found.']);
            return response()->json($category);
        }
        $category = VideoCategory::all();
        return response()->json($category);
    }

    private function showTreeView($id){
        if($id == null){
            $category = VideoCategory::where("parentId",0)->get(); //Parent
            
            return response()->json($category);
        }

        if(!$category = VideoCategory::find($id))
            return response()->json(['error'=>'Category not found.']);
            
        $childCat = VideoCategory::where("parentId",$category->id)->get();
        return response()->json($childCat);
    }

    public function update(Request $request,$id){
        if(!$category = VideoCategory::find($id))
            return response()->json(['error'=>'Category not found.']);

        if($category->name == $request->name)
           $this->validate($request, ['title' => 'bail|required|max:255']); 
        else
            $this->validate($request, ['title' => 'bail|required|max:255|unique:videoCategory']);
        
        $category->title = $request->title;
        if($category->save())
            return response()->json(['id'=>$category->id,'title'=>$category->title]);

        return response()->json(['error'=>"Something went wrong."]);
    }
    public function destroy($id){
        if(!$category = VideoCategory::find($id))
            return response()->json(['error'=>'Category not found.']);
        if(!$category->delete())
            return response()->json(['error'=>'Unable to delete this category.']);
        if(VideoCategory::where('parentId',$id)->count())
            if(!VideoCategory::where('parentId',$id)->delete())
                return response()->json(['error'=>'Unable to delete child videoCategory']);
        return response()->json(['success'=>'true']);
    }
    public function updateParent($id,$parentId){
        if(!$category = VideoCategory::find($id))
            return response()->json(['error'=>'Category not found.']);
        if($parentId)
            if(!VideoCategory::find($parentId))
                return response()->json(['error'=>'Parent category not found.']);

        $category->parentId = $parentId;
        if($category->save())
            return response()->json(['id'=>$category->id,'title'=>$category->title,'updated'=>'true']);

        return response()->json(['error'=>"Something went wrong."]);
    }
}
