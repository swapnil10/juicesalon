<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use DB;


class ShippingController extends Controller{

    public function show($id=null){
        $ship = DB::table('shipping')->first();
        return response()->json(['shipping'=>$ship->amount]);
    }

    public function update(Request $request){
        $this->validate($request,[
        	'amount' => 'required|numeric',
        ]);
        DB::table('shipping')->update(['amount'=>$request->amount]);
        return response()->json(['success'=>true]);
    }
}