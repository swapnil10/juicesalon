<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

// use Intervention\Image\Facades\Image;
use Storage;
use App\Models\StaticImages;
use App\Exceptions\CustomException;
use Exception;

class StaticImageController extends Controller{
	
    public function store(Request $request){
	    $warnings = [];$warningFileName = [];
	    $extAllow = ['jpg','jpeg','png'];
	    //return response()->json($request->all());
	    if(!$request->hasFile('image'))
	    	throw new CustomeException('Invalid File or it may be corrupted', 1);

	    foreach ($request->image as $k => $v) {
			$img = $request->file('image.'.$k);

	        $originalName = explode('.',$img->getClientOriginalName())[0];

	        $originalExt = $img->getClientOriginalExtension();

	        if(filesize($img) > 5242880){

	            array_push($warnings, 'File must not be greater than 5MB');

	            array_push($warningFileName, $originalName);

	            continue;

	        }



	        if(!in_array($originalExt, $extAllow)){

	            array_push($warnings, 'File must be an image file');

	            array_push($warningFileName, $originalName);

	            continue;

	        }
	        $newName = md5_file($img).'.'.$originalExt;
	        $newName = strtolower($newName);

	        if(Storage::exists('staticImages/'.$newName)){
			 	array_push($warnings, 'Oops! This file already exist');
	            array_push($warningFileName, $originalName);
	            continue;
	        }
	        Storage::put('staticImages/'.$newName,file_get_contents($img->getRealPath()));
	        $image = new StaticImages();
	        $image->name = $newName;
	        $image->save();
	    }

    	return response()->json(["warnings"=>$warnings,"file"=>$warningFileName]);

    }

    public function get($id){
    	if(!$image = StaticImages::find($id))
    		abort(404);
    	$file = 'staticImages/'.$image->name;
		if(!Storage::exists($file))
			abort(404);
		$content = Storage::get($file);
    	$type = Storage::mimeType($file);
    	return response()->make($content,200)->header('Content-Type',$type);
    }

    public function getAll(){
    	$skip = 0;
    	if(isset($_GET['offset'])){
    		if(is_numeric($_GET['offset']))
    			$skip = intval($_GET['offset']);
    	}

    	$image = StaticImages::select('id')->take(10)->skip($skip)->get();
    	return response()->json(['images'=>$image]);
    }

    public function delete($id){
    	if($id == 0)
			return;
		if(!$image = StaticImages::find($id))
    		throw new Exception("Image Not Found.", 1);

    	$file ='images/'.$image->name;
    	if(Storage::exists($file)){
    		Storage::delete($file);
    	}

    	$image->delete();

    	return response()->json(["success"=>true]);	
    }

}