<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Mail\Emails;
use Illuminate\Mail\Mailer;


class OrderUserController extends Controller{
	public function getUsers(){
		$users = User::all();
		return response()->json(["data"=>$users]);
	}

	public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->uid);

        $user->activated = true;

        $user->save();

        $this->activationRepo->deleteActivation($token);

        return $user;

    }
	public function getOrders(){	
		$orders = DB::table('orderview')->get();
		return response()->json($orders);
	}
	public function changeUserStatus($id){
		$user = DB::table('users')->where('id', $id)->value('status');
		$value = 0;
		if($user == 0){
			$value = 1;

		}
		if(!DB::table('users')->where('id', $id)->update(['status' => $value]))
			return response()->json("Something went wrong.");
		else
			return response()->json($value);
	}
	public function changeStatus(Request $request, Mailer $m){
		$this->validate($request,[
			'od_id'=>'required|numeric',
			'status'=>'required|numeric'
			]);
		$odId = $request->od_id;
		$status = $request->status;
		
		switch($status){
			case 1:
				$statusMessage = 'confirmed';
				break;
			case 2:
				$statusMessage = 'dispatched';
				break;
			case 3:
				$statusMessage = 'delivered';
				break;
			default:
				return false;
				break;
		}
		// $email = new Emails($m);
		
		// $product = DB::table('ORDERVIEW')->where('orderDetailId',$odId)->get();
		// $product = $product[0];
		// $product->statusMessage = $statusMessage;
		// $user = User::find($product->uid);

		// $email->send($user->email,null,"Order Notification","updateStatus",["user"=>$user,"product"=>$product]);
		DB::table('orders')->where('id',$odId)->update(['status'=>$status]);
		return response()->json(['status'=>'success','statusId'=>$status]);
	}
}
