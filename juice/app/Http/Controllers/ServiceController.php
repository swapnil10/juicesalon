<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\Service;

use Validator;

//use Illuminate\Support\Facades\DB;


class ServiceController extends Controller
{

    public function store(Request $request){
        $this->validate($request, [
        'title' => 'bail|required|max:255',
        'enabled' => 'required'
        ]);
        $service = new Service();
        $service->title = $request->title;
        $service->enabled = $request->enabled;
        if($service->save())
            return response()->json(['id'=>$service->id,'name'=>$service->title]);
        
        return response()->json(['error'=>"Something went wrong1."]);
    }

    public function show(Request $request,$id=null){
        if($id != null || $id != "" || is_int($id)){
            if(!$service = Service::find($id))
                return response()->json(['error'=>'Service not found.']);
            if($request->is('admin/api/*'))
                return response()->json($service);
            else{
                if(!$service->enabled)
                   return response()->json(['error'=>'Service not found.']);
                return response()->json($service); 
            }
        }
        if($request->is('admin/api/*'))
            $service = Service::all();
        else
            $service = Service::where('enabled',1)->get();
        return response()->json($service);
    }

    public function update(Request $request,$id){

        if(!$service = Service::find($id))
            return response()->json(['error'=>'Service not found.']);
        if($service->title == $request->title){
           $this->validate($request, [
            'title' => 'bail|required|max:255',
            'enabled' => 'required'
        ]); 
        }else{
            $this->validate($request, [
            'title' => 'bail|required|max:255',
            'enabled' => 'required'
            ]);
        }
        $service->title = $request->title;
        $service->enabled = $request->enabled;
        if($service->save())
            return response()->json(['id'=>$service->id,'name'=>$service->title]); 
        return response()->json(['error'=>"Something went wrong."]);
    }

    public function destroy($id){
        if(!$service = Service::find($id))
            return response()->json(['error'=>'Service not found.']);
        if(!$service->delete())
            return response()->json(['error'=>'Unable to delete this Service.']);
        return response()->json(['success'=>'true']);
    }
}
