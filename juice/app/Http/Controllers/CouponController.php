<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\Coupon;

use Validator;

class CouponController extends Controller{
	
	public function show($code=null){
		if($code == null)
			return response()->json(Coupon::all());
		if(!$coupon = Coupon::find($code))
			return response()->json(['error'=>'No coupon found.']);
		return response()->json($coupon);
	}
	
	public function store(Request $request){
		$this->validate($request,[
			'code' => 'bail|required|max:10|unique:coupons,code',
			'discountType' => 'required|boolean',
			'amount' => 'required|numeric',
			'startDate' => 'required|date',
			'expiryDate' => 'required|date',
			'enabled' => 'required|boolean'
			]);
		$coupon = new Coupon;
		$coupon->code = $request->code;
		$coupon->discountType = $request->discountType;
		$coupon->amount = $request->amount;
		$coupon->startDate = $request->startDate;
		$coupon->expiryDate = $request->expiryDate;
		$coupon->enabled = $request->enabled == 'on'?1:0;
		if(!$coupon->save())
			return response()->json(['error'=>'Coupons Adding Failed.']);
		return response()->json(['id'=>$coupon->code]);
	}
	public function update(Request $request, $code){
		if(!$coupon = Coupon::find($code))
			return response()->json(['error'=>'No product found.']);
		$this->validate($request,[
			'discountType' => 'required|integer',
			'amount' => 'required|numeric',
			'startDate' => 'required|date',
			'expiryDate' => 'required|date',
			'enabled' => 'required|boolean'
		]);
		$coupon->discountType = $request->discountType;
		$coupon->amount = $request->amount;
		$coupon->startDate = $request->startDate;
		$coupon->expiryDate = $request->expiryDate;
		$coupon->enabled = $request->enabled?1:0;
		if(!$coupon->save())
			return response()->json(['error'=>'Coupons Update Failed.']);
		return response()->json(['id'=>$coupon->code]);
	}
	public function assoc($code, Request $request){
		if(!$coupon = Coupon::find($code))
			return response()->json(['error'=>'No product found.']);
		for($i=0;$i < sizeof($request->ptid);$i++){
			$this->validate($request,[
				'ptid.'.$i =>'required|integer',
				],[],['ptid.'.$i=>'ProductType']);
			if(!ProductType::find($request->input('ptid.'.$i)))
				return response()->json(['error'=>'Product type not found.']);
		}
		if(!$coupon->association()->sync($request->input('ptid')))
			return response()->json(['error'=>'Some product type may not be updated.']);
		return response()->json(['success'=>'true']);
	}
}