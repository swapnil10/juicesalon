<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Orders;
use App\Models\Product;
use App\Models\Tax;
use App\Models\Coupon;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Mail\Emails;
use Carbon\Carbon;
use App\Lib\Payment;
use Validator;

class OrderController extends Controller{
	private static $orderData = [];
	private $orderId;
	private $orderTotalPrice = 0;
	private $orderSubTotal = 0;
	private $couponDiscount = 0;
	private $productPrice = [];
	private $productSubPrice =[];
	private $shipping = 0;

	public function __construct(Request $request){
		if(!Auth::check())
			$this->init($request->cookie('token'));
		else
			$this->init(Auth::id());
	}

	// Initialize Cart
	private function init($token){
		//return response()->json($token);
		$orderData = Orders::where('uid',$token)->where('status',0);
		if($orderData->count() < 1){
			self::createCart($token);
			return;
		}
		self::$orderData = $orderData->first();
		return;
	}
	
	// Count Total Cart Product
	public function countProduct(){
		$order = self::$orderData;
		$count = DB::table('orderDetails')->where('orderId',$order->id)->count();
		return response()->json(['count'=>$count]);
	}
	// Create Cart
	public static function createCart($uid){
		if(Orders::where('uid',$uid)->where('status',0)->count() > 0)
			return;
		$order = new Orders();
		$order->status = 0;
		$order->uid = $uid;
		$order->save();
		self::$orderData = Orders::find($order->id);
	}
	//Merge Cart
	public static function mergeCart($token){
        $authOrder = Orders::where('uid',Auth::id())->where('status',0);
        $tokenOrder = Orders::where('uid',$token)->where('status',0);
        if($authOrder->count() > 0){
            if($tokenOrder->count() > 0){
            	$id = $authOrder->first()->id;
                DB::table('orderDetails')->where('orderId',$tokenOrder->first()->id)->update(['orderID'=>$id]);
               $tokenOrder->delete();
            }
        }
	}

	public function userOrders(){
		if(!Auth::check())
			return response()->json(["error"=>"Unauthorised Access"]);
		$id = Auth::id();
		$orders = DB::table('ORDERVIEW')->where('uid',$id)->get();
		return response()->json($orders);
	}

	//Check If Product already exist in cart return boolean
	private function checkIfProductExist($request){
		$orderD = DB::table('orderDetails')->where('productId',$request->productId)->where('orderId',self::$orderData->id);
		if($orderD->count() < 1)
			return false;
		return true;
	}

	// If product exist for this order with combination of variant then do this stuff
	private function doStuffWithProduct($request){
		DB::table('orderDetails')->where('productId',$request->productId)->where('orderId',self::$orderData->id)->increment('quantity',1);
		return response()->json(["success"=>true]);
	}

	public function test(){
		
		$order = Orders::with(['orderDetails','orderDetails.products','coupon'])->find(self::$orderData->id);
		return dd($order);
	}
	// Calculate Price
	private function calculatePrice(){
		$order = Orders::with(['orderDetails','orderDetails.products','coupon'])->find(self::$orderData->id);
		if(sizeof($order->orderDetails) < 1)
			return; // No product

		$isValidCoupon = false;

		if($order->coupon != null){
			$coupons = $order->coupon;
			$isValidCoupon = true;
			$expiry = Carbon::parse($coupons->expiryDate." 23:59:59");
			$start = Carbon::parse($coupons->startDate." 00:00:01");
			$today = Carbon::now();
			if($today->gt($expiry))
				$isValidCoupon = false;
			if($today->lt($start))
				$isValidCoupon = false;
		}

		$productsDetails = [];$orderDetails = [];
		$quantity = 0;$i=0;
		foreach($order->orderDetails as $od){
			$product = $od->products;
			$this->productPrice[$od->id] = $product->price;
			$productsDetails[$product->id] = $this->productPrice[$od->id]*$od->quantity;
			$this->productSubPrice[$od->id] = $productsDetails[$product->id];
			$this->orderSubTotal+= $productsDetails[$product->id];
			$this->orderTotalPrice+=$productsDetails[$product->id];
		}
		if($isValidCoupon){
			if($coupons->discountType==1){
				$this->couponDiscount = $this->orderTotalPrice * ($coupons->amount/100);
				$this->orderTotalPrice = $this->orderTotalPrice - $this->couponDiscount;
			}else{
				$this->couponDiscount = $coupons->amount;
				$this->orderTotalPrice = $this->orderTotalPrice - $this->couponDiscount;
			}
		}
		
		$this->shipping = DB::table('shipping')->first()->amount;
		$this->orderTotalPrice+= $this->shipping;
		return;
	}
	// Add Product To Cart
	public function addProduct(Request $request){
		$this->validate($request,[
			'productId'=>'bail|required|integer'
		]);
		
		if(!$product = Product::find($request->productId))
			return response()->json(["error"=>"Product not found."]);

		if($product->enabled == 0)
			return response()->json(["error"=>"Product not found."]);

		if($this->checkIfProductExist($request))
			return $this->doStuffWithProduct($request);
		DB::table('orderDetails')->insert([
			'orderId'=>self::$orderData->id,
			'productId'=>$request->productId,
			'quantity'=>1
		]);
		return response()->json(["success"=>true]);
		// $order = Orders::find(self::$orderData->id)->products()->attach($request->productId,['quantity'=>1]);
		
	}

	// Delete Product From Cart
	public function removeProduct($orderDetailId){
		DB::table('orderDetails')->where('orderId',self::$orderData->id)->where('id',$orderDetailId)->delete();
		return response()->json(["success"=>true]);
	}

	// Update Product Quantity
	public function updateQuantity(Request $request,$orderDetailId){
		$this->validate($request,[
			'quantity'=>'bail|required|integer'
		]);
		if($request->quantity < 1)
			return response()->json(['error'=>"Quantity must be equal or greater than 1"]);	
		$orderD = DB::table('orderDetails')->where('orderId',self::$orderData->id)->where('id',$orderDetailId);
		//return response()->json($orderD->get());
		$orderDetails = $orderD->first();
		if($orderDetails == null)
			return response()->json(["error"=>"No such product found."]);
		
		$orderD->update([
			'quantity'=>$request->quantity
		]);

		return response()->json(['quantity'=>$request->quantity]);
	}

	// Add Order Address
	public function addAddress(Request $request){
		//return response()->json($request->all());
		$this->validate($request,[
			"address"=>"bail|required|min:20",
			"pinCode"=>"bail|required|digits:6",
			"city"=>"bail|required",
			"state"=>"bail|required",
			"phone"=>"bail|required|digits:10"
		]);
		if(Auth::check()){
			$user = Auth::user();
			if(($address = UserAddress::where("userId",$user->id)->first()) === null)
				$address = new UserAddress();
			$address->city = $request->city;
			$address->state = $request->state;
			$address->pinCode = $request->pinCode;
			$address->address = $request->address;
			$address->userId = Auth::id();
			$address->guestId = 0;
			$address->userId = $user->id;
			$user->phone = $request->phone;
			$user->save();
			if($address->save())
				return response()->json(["success"=>true]);
			return response()->json(["error"=>"Something went wrong"]);
		}else{
			/*if(!$request->has('guestId'))
				return response()->json(["error"=>"Something not proper"]);*/
			return $this->guestAddress($request);
		}
	}

	
	// Fetch Final Price For This Order
	public function finalPrice(){
		$this->calculatePrice();
		return response()->json(["grandTotal"=>round($this->orderTotalPrice,2),"subTotal"=>round($this->orderSubTotal,2),"discount"=>round($this->couponDiscount,2),"couponCode"=>self::$orderData->couponCode,"productPrice"=>$this->productPrice,"productSubTotal"=>$this->productSubPrice,"shipping"=>$this->shipping]);
	}

	// Coupons Add
	public function coupon($cc){
		if(!$coupon = Coupon::find($cc))
			return response()->json(["error"=>"Coupon code is invalid"]);
		if(!$coupon->enabled)
			return response()->json(["error"=>"Invalid Coupon Code."]);
		$expiry = Carbon::parse($coupon->expiryDate." 23:59:59");
		$start = Carbon::parse($coupon->startDate." 00:00:01");
		$today = Carbon::now();
		if($today->gt($expiry))
			return response()->json(["error"=>"Coupon code is expired"]);
		if($today->lt($start))
			return response()->json(["error"=>"This coupon code is currently not available."]);
		
		$orders = Orders::find(self::$orderData->id);
		$orders->couponCode = $coupon->code;
		if($orders->save())
			return response()->json(["success"=>true]);
		
		return response()->json(["error"=>"This coupon code is invalid for your cart item."]);
	}
	// Coupon Remove
	public function removeCoupon(){
		$order = Orders::find(self::$orderData->id);
		$order->couponCode = '';
		$order->save();
		return response()->json(["success"=>true]);
	}

	// Get Cart Product
	public function cartProducts($orderDetailId = null){
		//$orderDetails =DB::table('orderDetails')->where('orderId',self::$orderData->id);
		return response()->json(DB::select('select productName,price,orderDetailId,productId,productImage,quantity from ORDERVIEW where id=:id',['id'=>self::$orderData->id]));
	}

	//Confirm Order
	private function confirmOrder(){
		$orders = self::$orderData;
		$orders->status = 1;
		$res = $orders->save();
		self::$orderData = Orders::find($order->id);
		$email = new Emails();
		$user = Auth::user();
		$email->send($user->email,'orders@juicesalon.com',"Order Successfully Placed","userOrderConfirmed",["user"=>$user]);
		return response()->json(['success'=>true]);
	}


	public function payment(){
		
		$order = self::$orderData;
		$data = ["currency"=>"INR"];
		//$this->calculatePrice();
		$data['amount'] = 3000; // $this->orderTotalPrice*100;
		$data['receipt'] = $order->id;
		$pay = new Payment();
		$user = Auth::user();
		$razorPayOrderId = $pay->createOrder($data);
		if($razorPayOrderId === false)
			return response()->json(['error'=>'Something went wrong']);

		$order->razorPayOrderId = $razorPayOrderId;
		$order->save();
		self::$orderData = $order;
		$data['razarPayOrderId'] = $razorPayOrderId;
		unset($data['receipt']);
		unset($data['INR']);
		unset($data['amount']);
		$data["prefill"]  = [
		    "name"       => $user->fullName,
		    "email"      => $user->email,
		    "contact"    => $user->mobile
	    ];
	    $data["key"] = Payment::$key;
	    //$addressObj = $user->address;
	    //$address = $addressObj->address.' '.$addressObj->city.' - '.$addressObj->pinCode.' '.$addressObj->state;
	    $address = "User Address";
	    $data['notes'] = [
		    "address"           => $address,
		    "merchant_order_id" => $order->id
		];
		return view('payment-page')->with(['data'=>$data]);
	}

	public function paymentResponse(Request $request){
		$pay = new Payment();
		if(empty($request->razorpay_payment_id))
			return response()->json(['error'=>'Something went wrong']);
		$razorPayOrderId = self::$orderData->razorPayOrderId;
		$attributes = [
			'razorpay_payment_id'=> $request->razorpay_payment_id,
			'razorpay_order_id' => $razorPayOrderId,
			'razorpay_signature'=> $request->razorpay_signature
		];
		if($pay->checkSignature($attributes)){
			$this->confirmOrder();
			return response()->json(['success'=>true]);
		}

		return response()->json(['error'=>'Oops! Payment Transaction Failed.']);
	}

	/************For Admin Only*****************/
	
	// Get All Confirmed Orders
	public static function getAll(){
		$skip = 0;
		if(isset($_GET['offset'])){
			if(is_integer($_GET['offset']))
				$skip = $_GET['offset'];
		}
		$orders = Orders::where('status','!=',0)->skip($skip)->take(50)->get();
		return response()->json($orders);
	}

	// Get Order Detail
	public static function getOrderDetail($id){
		$order = Orders::find($id);
		if(!$order)
			return response()->json(['error'=>'Order not found.']);
		if($order->status == 0)
			return response()->json(['error'=>'Order not found.']);
		$orderD = DB::table('ORDERVIEW')->where('id',$order->id)->select('orderDetailId','productName','productImage','price','quantity','productId')->get();
		return response()->json($orderD);
	}

	// Change Order Status
	public static function changeStatus(Request $request,$id){
		
		Validator::make((array)$request,[
			'status'=>'required|integer'
		]);

		$status = $request->status;
		
		$order = Orders::find($id);

		if(!$order)
			return response()->json(['error'=>'Order not found.']);
		if($order->status == 0)
			return response()->json(['error'=>'Order not found']);

		switch($status){
			case 1:
				$statusMessage = 'confirmed';
				break;
			case 2:
				$statusMessage = 'dispatched';
				break;
			case 3:
				$statusMessage = 'delivered';
				break;
			default:
				return response()->json(['error'=>'Invalid Status']);
				break;
		}
		$order->status = $request->status;
		$order->save();
		return response()->json(['status'=>'success','statusId'=>$status]);
	} 
}
