<?php

namespace App\Http\Controllers\AdminAuth;

use App\Models\Admin;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $redirectAfterLogout = 'admin/login';
    protected $redirectTo = '/admin';
    protected $guard = 'admin';
    public function showLoginForm(){
        return view('admin.login');
	}

    
	/*public function showRegistrationForm()
	{
    	return view('admin.auth.register');
	} */
    
}
