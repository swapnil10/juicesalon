<?php

namespace App\Http\Controllers\AdminAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'bail|required|max:255|exists:admins,email',
            'password' => 'required|min:6',
        ]);
        $email = $request->input('email');
        $password = $request->input('password');
        if (Auth::guard('admin')->attempt(['email' => $email, 'password' => $password])){
            $admin = Auth::guard('admin')->user();
            $request->session()->put(['adminName'=>$admin->name]);
            return response()->json(["success"=>true]);
        }
        return response()->json(["error"=>"Credential do not match our records."]);
    }
}