<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\VideoCategory;

use App\Http\Controllers\ImageController;

use DB;

use Validator;

use Exception;

//use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    public function store(Request $request){
        $this->validate($request, [
            'title' => 'bail|required|max:255',
            'hsn' => 'bail|max:10',
            'description'=>'bail|required',
            'tax'=>'bail|required',
            'type'=>'bail|required|numeric|exists:producttype,id',
            'price'=>'bail|required|numeric',
            'enabled' => 'required|boolean'
        ]);
        $url = "";
        $vCategory = 0;
        if($request->type == 1){
            $this->validate($request,[
                'webclip'=>'required|url',
                'category'=>'bail|required|integer|exists:videoCategory,id'
            ]);
            $url = $request->webclip;
            $vCategory = $request->category;
        }
        $product = new Product;
        $product->title = $request->title;
        $product->hsn = $request->hsn;
        $product->description = $request->description;
        $product->productTypeId = $request->type;
        $product->price = $request->price;
        $product->webclip = $url;
        $product->tax = $request->tax;
        $product->category = $vCategory;
        $product->enabled = $request->enabled;
        if($product->save())
            return response()->json(['id'=>$product->id,'name'=>$product->title]);
        
        return response()->json(['error'=>"Something went wrong."]);
    }

    public function show(Request $request,$id){
        if(!$product = Product::find($id))
            return response()->json(['error'=>'Product not found.']);
        if($request->is('admin/*'))
            return response()->json($product);
        if($product->enabled != 1)
            return response()->json(['error'=>'Product not found.']);
        return response()->json($product);
    }

    public function showAll(Request $request){
        if($request->is('admin/*'))
            return response()->json(Product::all());

        return $this->getAllProducts();
    }

    public function update(Request $request,$id){
        if(!$product = Product::find($id))
            return response()->json(['error'=>'Product not found.']);
        
        $this->validate($request, [
            'title' => 'bail|required|max:255',
            'hsn' => 'bail|required|max:10',
            'description'=>'bail|required',
            'tax' => 'bail|required',
            'type'=>'bail|required|numeric|exists:producttype,id',
            'price'=>'bail|required|numeric',
            'enabled' => 'required|boolean'
        ]);

        $url = "";
        $vCategory = 0;
        if($request->type == 1){
            $this->validate($request,[
                'webclip'=>'required|url',
            ]);
            $url = $request->webclip;
        }

        $product->title = $request->title;
        $product->description = $request->description;
        $product->productTypeId = $request->type;
        $product->price = $request->price;
        $product->tax = $request->tax;
        $product->hsn = $request->hsn;
        $product->enabled = $request->enabled;
        $product->webclip = $url;
        if($product->save())
            return response()->json(['id'=>$product->id,'name'=>$product->title]); 
        return response()->json(['error'=>"Something went wrong."]);
    }

    public function destroy($id){
        if(!$product = Product::find($id))
            return response()->json(['error'=>'Product type not found.']);
        if(!$product->delete())
            return response()->json(['error'=>'Unable to delete this product type.']);
        return response()->json(['success'=>true]);
    }

    public function addImage(Request $request){

        $this->validate($request,[
            'id'=> 'bail|required|integer',
            'image'=>'bail|required|mimes:jpeg,jpg,png|max:5000'
        ]);

        

        $res = [];
        try{
            if(!$product = Product::find($request->id))
                throw new Exception("No Product Found", 1);
                
            $save = ImageController::save($request,'image');
            if($save === false)
                throw new Exception("Image Adding failed", 1);
            ImageController::destroy($product->imageId);
            $product->imageId = $save;
            $product->save();
            $res = ["success"=>true];
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }

        return response()->json($res);
    }

    public function destroyImage($id){
        $res = [];
        try{
            if(!$product = Product::find($id))
                throw new Exception("No Product Found", 1);
            ImageController::destroy($product->imageId);
            $product->imageId = 0;
            $product->save();
            $res = ["success"=>true];
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }
        return response()->json($res);
    }

    // For Front End Website
    private function getAllProducts(){
        $skip = 0;
        if(isset($_GET['offset'])){
            if(!empty($_GET['offset'])){
                $skip = $_GET['offset'];
            }
        }
        $product = Product::where('enabled',1)->where('imageId','>',0)->where('productTypeId','!=',1)->take(30)->skip($skip);
        if(isset($_GET['productType'])){
            if(!empty($_GET['productType']))
                $product = $product->where('productTypeId',$_GET['productType']);
            
        }
        if(isset($_GET['orderBy'])){
            if($_GET['orderBy'] == 1)
                $product = $product->orderBy('price','ASC');
            else if($_GET['orderBy'] == 0)
                $product = $product->orderBy('price','DESC');
        }
        return response()->json($product->get());
    }


    public function getAllVideoProducts($categoryId){
        $cat = VideoCategory::find($categoryId);
        if(!$cat)
            return response()->json(['error'=>'Invalid Category']);
        $products = Product::where('enabled',1)->where('category',$categoryId)->where('productTypeId',1)->get();
        
        return response()->json(['products'=>$products,'category'=>$cat->title]);
    }

}
