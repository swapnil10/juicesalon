<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\Subservice;
use App\Models\Service;

use Validator;

//use Illuminate\Support\Facades\DB;


class SubserviceController extends Controller
{

    public function store(Request $request){
        $this->validate($request, [
        'title' => 'bail|required|max:255',
        'enabled' => 'required',
        'service' => 'required|exists:services,id',
        'desc' => 'required',
        'price' => 'required'
        ]);
        $service = new Subservice;
        $service->title = $request->title;
        $service->serviceId = $request->service;
        $service->description = $request->desc;
        $service->price = $request->price;
        $service->enabled = $request->enabled;
        if($service->save())
            return response()->json(['id'=>$service->id,'name'=>$service->title]);
        
        return response()->json(['error'=>"Something went wrong1."]);
    }

    public function show(Request $request,$id=null){
        if($id != null || $id != "" || is_int($id)){
            if(!$service = Subservice::find($id))
                return response()->json(['error'=>'Service not found.']);
            if($request->is('admin/api/*'))
                return response()->json($service);
            else{
                if(!$service->enabled)
                   return response()->json(['error'=>'Service not found.']);
                return response()->json($service); 
            }
        }
        if($request->is('admin/api/*'))
            $service = Subservice::all();
        else
            $service = Subservice::where('enabled',1)->get();
        return response()->json($service);
    }

    public function getSubServices($id){
        if(!$service = Service::find($id))
            return response()->json(['error'=>'Service not found.']);
        if(!$service->enabled)
            return response()->json(['error'=>'Service not found.']);
        $subServices = Subservice::where('enabled',1)->where('serviceId',$service->id)->get();
        return response()->json($subServices);
    }

    public function update(Request $request,$id){

        if(!$service = Subservice::find($id))
            return response()->json(['error'=>'Service not found.']);
        if($service->title == $request->title){
           $this->validate($request, [
            'title' => 'bail|required|max:255',
            'enabled' => 'required',
            'service' => 'required|exists:services,id',
            'desc' => 'required',
            'price' => 'required'
        ]); 
        }else{
            $this->validate($request, [
            'title' => 'bail|required|max:255',
            'enabled' => 'required',
            'service' => 'required|exists:services,id',
            'desc' => 'required',
            'price' => 'required'
            ]);
        }
        $service->title = $request->title;
        $service->serviceId = $request->service;
        $service->description = $request->desc;
        $service->price = $request->price;
        $service->enabled = $request->enabled;
        if($service->save())
            return response()->json(['id'=>$service->id,'name'=>$service->title]); 
        return response()->json(['error'=>"Something went wrong."]);
    }

    public function destroy($id){

        if(!$service = Subservice::find($id))
            return response()->json(['error'=>'Sub Service not found.']);
        if(!$service->delete())
            return response()->json(['error'=>'Unable to delete this Sub Service.']);
        return response()->json(['success'=>'true']);
    }
}
