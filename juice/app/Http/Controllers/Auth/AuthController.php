<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Lib\SMS;
use DB;
use Auth;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/sign-in';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }


    private function generateOTP(){
        $otp = mt_rand(10000,99999);
        $temp = DB::table('tempUser')->where('otp',$otp)->count();
        if($temp > 0)
            return $this->generateOTP();
       return $otp;
    }

    public function sendOTP(Request $request){
        $this->validate($request,[
            'phone'=>'required|numeric|digits:10',
            'email'=>'required|email',
            'password'=>'required|min:6|max:15|confirmed',
            'fullName'=>'required'
        ],['confirmed'=>'Password do not match'],[]);
        $user = User::where('phone',$request->phone)->count();
        if($user > 0)
            return response()->json(['error'=>'Phone No. already taken.']);

        $user = User::where('email',$request->email)->count();
        if($user > 0)
            return response()->json(['error'=>'Email address already taken.']);

        $otp = $this->generateOTP();

        $smsText = 'Your OTP is '.$otp;

        //Ucomment this two lines
        $sms = new SMS();
        if(!$sms->send($request->mobile,$smsText))
            return response()->json(['error'=>'Something went wrong.']);

        DB::table('tempUser')->where('phone',$request->phone)->delete();
        DB::table('tempUser')->insert([
            'phone'=>$request->phone,
            'otp'=>$otp,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'fullName'=>$request->fullName
        ]);

        return response()->json(['phone'=>$request->phone]);
    }

    public function register(Request $request){
        $this->validate($request,[
            'phone'=>'required|numeric|digits:10',
            'otp'=>'required|numeric|digits:5'
        ]);

        $tempUser = DB::table('tempUser')->where('phone',$request->phone)->where('otp',$request->otp)->first();
        if($tempUser === null)
            return response()->json(['error'=>'Invalid OTP']);
        $user = new User();
        $user->fullName = $tempUser->fullName;
        $user->email = $tempUser->email;
        $user->phone = $tempUser->phone;
        $user->password = $tempUser->password;
        $user->save();
        DB::table('tempUser')->where('phone',$request->phone)->where('otp',$request->otp)->delete();
        Auth::loginUsingId($user->id);
        return response()->json(['success'=>true]);
    }

    public function login(Request $request){
        $this->validate($request,[
            "phone"=>'required|numeric|digits:10',
            "password"=>'required|min:6|max:15'
        ]);
         if(Auth::attempt(['phone' => $request->phone, 'password' => $request->password]))
            return response()->json(['success'=>true]);
        return response()->json(['error'=>'Invalid phone or password']);
    }

    public function forgotPasswordOTP(Request $request){
        $this->validate($request,[
            'phone'=>'required|numeric|digits:10'
        ]);

        $user = User::where('phone',$request->phone)->count();
        if($user < 1)
            return response()->json(['error'=>'Invalid phone.']);
        $otp = $this->generateOTP();
        DB::table('tempUser')->where('phone',$request->phone)->delete();
        DB::table('tempUser')->insert(['phone'=>$request->phone,'otp'=>$otp]);
        return response()->json(['phone'=>$request->phone]);
    }

    public function forgotPassword(Request $request){
        $this->validate($request,[
            'phone'=>'required|numeric|digits:10',
            'otp'=>'required|numeric|digits:5',
            'password'=>'required|min:6|max:15|confirmed'
        ]);

        
        if(DB::table('tempUser')->where('phone',$request->phone)->where('otp',$request->otp)->count() < 1)
            return response()->json(['error'=>'Invalid phone or otp']);

        $user = User::where('phone',$request->phone)->first();
        if($user === null)
            return response()->json(['error'=>'Invalid phone.']);

        $this->changePassword($user,$request->password);
        return response()->json(['success'=>true]);
    }

    private function changePassword($user,$password){
        $user->password = bcrypt($password);
        $user->save();
    }
}
