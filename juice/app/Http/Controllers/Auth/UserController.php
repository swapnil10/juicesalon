<?php
	
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\User;
use App\Models\Address;

class UserController extends Controller{
	
	public static function fetchAddress($userId){
		$address = Address::where('userId',$userId)->first();
		if($address === null)
			return false;
		return $address;
	}

	public function getAddress(Request $request,$id=null){
		if(!$request->is('admin/api/*'))
			$id = Auth::id();

		$address = self::fetchAddress($id);
		if($address === false)
			return response()->json([]);

		return response()->json($address);
	}

	public function get(Request $request,$id=null){
		if(!$request->is('admin/api/*'))
			$id = Auth::id();
		$user = User::find($id);
		if(!$user)
			return response()->json(['error'=>'User Not Found']);
		return response()->json($user);
	}

	public function getAllUsers(){
		$skip = 0;
		if(isset($_GET['offset'])){
			if(is_integer($_GET['offset']))
				$skip = $_GET['offset'];
		}

		$user = User::select('id','fullName','phone','email','emailVerify','createdAt','isFranchisee')->skip($skip)->take(50)->get();
		return $user;
	}

	public function isFranchiseeConvert(Request $request,$id){
		$this->validate($request,[
			'status'=>'bail|required|boolean'
		]);
		$user = User::find($id);
		if(!$user)
			return response()->json(['error'=>'User Not Found']);
		$user->isFranchisee = $request->status;
		$user->save();
		return response()->json(['success'=>true]);
	}
	public function address(Request $request){
		$this->validate($request,[
			'address'=>'required',
			'pinCode'=>'required|numeric|digits:6',
			'state'=>'required',
			'city'=>'required'
		]);

		$userId = Auth::id();
		$address = self::fetchAddress($userId);
		if($address === false)
			$address = new Address();
		$address->address = $request->address;
		$address->city = $request->city;
		$address->pinCode = $request->pinCode;
		$address->state = $request->state;
		$address->userId = $userId;
		$address->save();
		return response()->json(['success'=>true]);
	}


}
?>