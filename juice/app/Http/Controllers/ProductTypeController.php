<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\ProductType;

use Validator;

//use Illuminate\Support\Facades\DB;


class ProductTypeController extends Controller
{

    public function store(Request $request){
        $this->validate($request, [
        'title' => 'bail|required|max:255|unique:producttype',
        'enabled' => 'required'
        ]);
        $type = new ProductType;
        $type->title = $request->title;
        $type->enabled = $request->enabled;
        if($type->save())
            return response()->json(['id'=>$type->id,'name'=>$type->title]);
        
        return response()->json(['error'=>"Something went wrong."]);
    }

    public function show(Request $request,$id=null){
        if($id != null || $id != "" || is_int($id)){
            if(!$type = ProductType::find($id))
                return response()->json(['error'=>'Product type not found.']);
            if($request->is('admin/api/*'))
                return response()->json($type);
            else{
                if(!$type->enabled)
                   return response()->json(['error'=>'Product type not found.']);
                return response()->json($type); 
            }
        }
        if($request->is('admin/api/*'))
            $type = ProductType::all();
        else
            $type = ProductType::where('enabled',1)->get();
        return response()->json($type);
    }

    public function update(Request $request,$id){
        if($id == 1)
            return response()->json(["error"=>"Product Type not found."]);

        if(!$type = ProductType::find($id))
            return response()->json(['error'=>'Product type not found.']);
        if($type->title == $request->title){
           $this->validate($request, [
            'title' => 'bail|required|max:255',
            'enabled' => 'required'
        ]); 
        }else{
            $this->validate($request, [
            'title' => 'bail|required|max:255|unique:producttype',
            'enabled' => 'required'
            ]);
        }
        $type->title = $request->title;
        $type->enabled = $request->enabled;
        if($type->save())
            return response()->json(['id'=>$type->id,'name'=>$type->title]); 
        return response()->json(['error'=>"Something went wrong."]);
    }

    public function destroy($id){
        if($id == 1)
            return response()->json(["error"=>"Product Type not found."]);

        if(!$type = ProductType::find($id))
            return response()->json(['error'=>'Product type not found.']);
        if(!$type->delete())
            return response()->json(['error'=>'Unable to delete this product type.']);
        return response()->json(['success'=>'true']);
    }
}
