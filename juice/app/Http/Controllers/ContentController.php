<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller{
/*********************Franchisee**********************************/
   public function franchise(Request $request){
        $this->validate($request, [
            'whydes' => 'required',
            'whycri' =>'required',
            'image'=>'numeric|exists:staticImages,id'
        ]);

        DB::table('franchisee_content')
            ->update(['why'=>$request->whydes,'criteria'=>$request->whycri,'imageId'=>$request->image]);
        return response()->json(["success"=>"true"]);
    }

    public function getFranchise(){
        $content = DB::table('franchisee_content')->first();
        return response()->json($content);
    }

    // public function imageFranchise(Request $request){
    //     $res = [];
    //     $franchise = DB::table('franchisee_content')->first();
    //     try{
    //         if($franchise->imageId != 0)
    //             ImageController::destroy($franchise->imageId);

    //         $image = ImageController::save($request,'image');
    //         $res = ['success'=>DB::table('franchisee_content')->update(["imageId"=>$image])];

    //     }catch(Exception $e){
    //         $res = ["error"=>$e->getMessage()];
    //     }
    //     return response()->json($res);
    // }

    // public function imageRemoveFranchise(){
    //     $franchise = DB::table('franchisee_content')->first();
    //     $res = [];
    //     try{
    //         ImageController::destroy($franchise->imageId);
    //         DB::table('franchisee_content')->update(["imageId"=>0]);
    //         $res = ["success"=>"true"];
    //     }catch(Exception $e){
    //         $res = ["error"=>$e->getMessage()];
    //     }
    //     return response()->json($res);
    // }

/**********************GET LOOK***********************************/
    public function getlook(Request $request){
        $this->validate($request, [
            'name' => 'bail|required',
            'description' =>'bail|required',
        ]);
        if(!$request->name || !$request->description)
            return response()->json(["error"=>"Something went wrong"]);
        $look = DB::table("getthelookpage");
        
        $look->update(["getLook"=>$request->name,"darkHeader"=>$request->description]);
        return response()->json(["success"=>"true"]);
    }
    public function showGetLook(){
        $look = DB::table("getthelookpage")->first();
        return response()->json($look);
    }

/********************Services********************************/
    public function service(Request $request){
        $this->validate($request, [
            'service' => 'required',
        ]);
        if(!$request->service)
            return response()->json(["error"=>"Empty fields not allowed"]);

        $service = DB::table('servicepage')->update(['header'=>$request->service]);
        return response()->json(['success'=>true]);
    }

    public function getService(){
        $service = DB::table('servicepage')->first();
        return response()->json($service);
    }

}
