<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Models\Look;

use Validator;

class LookController extends Controller{
	public function show($id=null){
		if($id == null)
			return response()->json(Look::all());
		if(!$look = Look::find($id))
			return response()->json(['error'=>'No look found.']);
		return response()->json($look);
	}

	public function update(Request $request){
        $this->validate($request,[
            'titles'=>'required|array|size:14',
            'imageIds'=>'required|array|size:14',
        ]);
        $arr = [];
        foreach ($request->titles as $title) {
            $arr[$title['id']]['title'] = $title['title'];
        }
        foreach ($request->imageIds as $imageId) {
            $arr[$imageId['id']]['imageId'] = $imageId['imageId'];
        }
        // DB::beginTransaction();
        foreach ($arr as $key => $value) {
            $look = Look::find($key);
            if(!$look)
                continue;
            if(!empty($value['imageId']))
                $look->imageId = $value['imageId'];
            if(!empty($value['title']))
                $look->title = $value['title'];
            $look->save();
        }
        // DB::commit();
        return response()->json(['success'=>true]);
    }

	public function imageProduct(Request $request){
        // return response()->file($request->image);
        $this->validate($request,[
            'id'=>'required|numeric',
            'image'=>'required'
        ]);

        if(!$bt = Look::find($request->id))
            return response()->json(["error"=>"Look Not Found."]);
        $res = ["success"=>true];
        try{
            ImageController::destroy($bt->imageId);
            $bt->imageId = 0;
            $imageId = ImageController::save($request,'image');
            $bt->imageId = $imageId;
        }catch(Exception $e){
            $res = ["error"=>$e->getMessage()];
        }
        $bt->save();
        return response()->json($res);
    }

    public function imageRemoveProduct($id){
        if(!$bt = Look::find($id))
            return response()->json(['error'=>'Service Slider not found.']);
        try{
            ImageController::destroy($bt->imageId);
        }catch(Exception $e){
            return response()->json(["error"=>$e->getMessage()]);
        }
        $bt->imageId = 0;
        return response()->json(["success"=>$bt->save()]);
    }

    public function destroyProduct($id){
        if(!$bt = Look::find($id))
            return response()->json(["error"=>"Service Slider Not Found."]);
        try{
            ImageController::destroy($bt->imageId);
        }
        catch(Exception $e){/*do Nothing */}
        return response()->json(["success"=>$bt->delete()]);
    }
	
}