<?php
Route::get('/test','OrderController@finalPrice');
Route::group(['prefix'=>'mysql'],function(){
	Route::get('/','MySQLWorkbench@index');
	Route::get('/migrate','MySQLWorkbench@migrate');
	Route::get('/table/{table}','MySQLWorkbench@getTable');
	Route::get('/truncate/{table}','MySQLWorkbench@truncateTable');
	Route::get('/seeder','MySQLWorkbench@seeder');
});
Route::get('/bulkimages','StaticImageController@getAll');
require_once('Routes/admin.routes.php');
require_once('Routes/public.routes.php');
Route::get('/{page?}','HomeController@pages');
Route::get('/image/{id}','ImageController@get');
Route::get('/bulkimage/{id}','StaticImageController@get');



