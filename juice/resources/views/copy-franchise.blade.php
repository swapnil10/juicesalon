@extends('partials.template')

@section('title','Franchise')

@section('meta')
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="/assets/admin/plugins/image-picker/image-picker.css">
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-common-extra.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-core.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-reset.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-sidebar.css'>
<link rel='stylesheet' href='/assets/admin/css/css/create-ui/css/create-ui.css'>
<link rel='stylesheet' href='/assets/admin/css/css/midgard-notifications/midgardnotif.css'>
<style>
  .image_picker_image{
    height: 100px !important;
    width: 100px !important;
</style>
@endsection

@section('custom-page-header')
<!-- Franchise Page Header -->
<header class="section-header franchise-title-header">
  
  <div class="bg-overlay" style="background-color: #ffff3d; position: absolute; left: 0;
  right: 0; bottom: 0; top: 0; opacity: .9;"></div>

  <div class="container" style="position: relative; padding-bottom: 1em; padding-top: 1em;">
      <h1 class="h1 text-center">Franchise</h1>
  </div>
</header>
@endsection

@section('main')

  <main class="main-section page-main-area article"  about="/my/article" id="franchise_page">
     <!-- Section 1: Why Juice -->    
     <section class="section-main franchise-why-juice" id="section_why_juice">
       <div class="container" style="position: relative;">
         <!-- decoration -->
          <div class="decorate swirl" style="position: absolute; right: 20px; top: -4em;user-select: none;-webkit-user-drag: none; ">
                  <img src="img/decorations/swirl-horizontal_120px.png" width="100">
          </div>
          <!-- decoration end -->

         <div class="row">
           <div class="col-md-5">
             <div class="why-juice-text franchise-text-col">
               <h2><b>Why Juice?</b></h2>
               <br>
               <p id="why-us"></p>
               <i style="opacity: 0.50; color: #4c4c4c;">Click here to edit..</i>
             </div>
           </div>
           
           <div class="col-md-1 visible-md visible-lg"></div>

           <div class="col-md-5">
             <div class="image-container">
               <img id="franchisee-image" class="img-responsive" alt="Juice Salon photo">
               <a id="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
               <i style="opacity: 0.50; color: #4c4c4c;">Click "Choose Image" to edit..</i>
             </div>
           </div>

           <div class="col-md-1 visible-md visible-lg"></div>
         </div>
       </div>
     </section>



     <!-- Section 1: Why Juice -->    
     <section class="section-main franchise-juice-support" id="section_juice_support">
         <div class="container" style="position: relative; padding-bottom: 2em;">

           <!-- decoration -->
           <div id="decor_xG56" class="decorate dots" style="position: absolute; bottom: -71px; left: 20px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                   <img src="img/decorations/dots-horizontal_120px.png" width="84">
           </div>
           <!-- decoration end -->

           <h3>Juice Support</h3>
           <br>

           <div class="row juice-support-links">
               <div class="col-md-7">
                 
                 <div class="col-md-4 support-link-col">
                   <ul class="support-links">
                     <li><a href="#">Site Identification</a></li>
                     <li><a href="#">Rental / Lease Agreement</a></li>
                     <li><a href="#">Product Procurement</a></li>
                   </ul>
                 </div>

                 <div class="col-md-4 support-link-col">
                   <ul class="support-links">
                     <li><a href="#">Site Identification</a></li>
                     <li><a href="#">Rental / Lease Agreement</a></li>
                     <li><a href="#">Product Procurement</a></li>
                   </ul>
                 </div>

                 <div class="col-md-4 support-link-col">
                   <ul class="support-links">
                     <li><a href="#">Site Identification</a></li>
                     <li><a href="#">Rental / Lease Agreement</a></li>
                     <li><a href="#">Product Procurement</a></li>
                   </ul>
                 </div>

               </div>
           </div>

         </div>
     </section>


     <!-- Section 3: Criteria -->    
     <section class="section-main franchise-criteria" id="section_criteria">
       <div class="container" style="position: relative; padding-top: 2em;">

         <div class="row">
           <div class="col-md-5">
             <div class="criteria-text franchise-text-col">
               <h2><b>Criteria</b></h2>
               <br>
               <p id="criteria"></p>
               <i style="opacity: 0.50; color: #4c4c4c;">Click here to edit..</i>
             </div>
           </div>
         </div>
       </div>
     </section>


     <!-- Franchise Section 3 - Enquiry Form -->
     <section class="section-main franchise-enquiry" id="section_enquiry_form">
       <div class="container">
         <h3>Enquiry Form</h3>

         <div class="enquiry-form-box">
             <form class="inner enquiry-form-box-inner form-general" id='franchise-form' style="width: 80%; margin: 0 auto;">
               <div class="row form-row">

                   <!-- 1st form column -->
                   <div class="form-col col-md-6">

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="full_name">Full Name</label></p>
                         <input type="text" id="full_name" name="name" placeholder="Your full name.." required="true">
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="email_id">Email ID</label></p>
                         <input type="email" id="email_id" name="email" placeholder="Your email id.." required="true">
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="contact_number">Contact number</label></p>
                         <input type="tel" id="contact_number" name="number" placeholder="Contact number" required="true">
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="select_city">City interested</label></p>
                         <select id="select_city" name="city">
                           <option value="0">Select City</option>
                           <option value="Mumbai">Mumbai</option>
                           <option value="New Delhi">New Delhi</option>
                           <option value="Hyderabad">Hyderabad</option>
                           <option value="Banglore">Bangalore</option>
                         </select>
                       </div>
                     </div>

                   </div>

                   <!-- 2nd Form column -->
                   <div class="form-col col-md-6">

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="ownPlace">Do you own your space suitable for Salon?</label></p>
                         
                         <div class="radio-option-group">
                           <div class="radio-item">
                             <input type="radio" name="ownPlace" value="1" checked id="own_place_YES">
                             <label for="own_place_YES">Yes</label>
                           </div>

                           <div class="radio-item">
                             <input type="radio" name="ownPlace" value="0" id="own_place_NO">
                             <label for="own_place_NO">No</label>
                           </div>
                         </div>
                         
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p>
                           <label for="space_size">What is the size of the space? (in Sq. ft.)</label>
                         </p>
                         <input type="text" id="space_size" name="size" placeholder="Enter size of the space.." required="true">
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p>
                           <label for="found_salon">How did you come to know about this salon?</label>
                         </p>
                         <input type="text" id="found_salon" name="foundSalon" placeholder="A short description.." required="true">
                       </div>
                     </div>


                     <div class="form-col-item">
                       <div class="input-block">
                         <p>
                           <label for="comment_message">Comment / Message</label>
                         </p>
                         <textarea  id="comment_message" name="message" class="comment-message-box" required="true"></textarea>
                       </div>
                     </div>
                     
                   </div>
                   <b><p class="status"></p></b>
               </div>
             </form>
         </div>
         <div class="submit-btn-box text-center">
           <button class="links-btn btn-shadow" name="enquiryForm" id="submit_enquiry" style="width: 200px;">Submit</button>
         </div>

       </div>
     </section>
  </main>
  <!--modal-->
  <div class="modal fade" id="images-modal" role="dialog" style="width: 625px;">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Image</h4>
        </div>
        <div class="modal-body" style="height: 550px;overflow: auto;">
          <select id="image-select" class="image-picker show-html"></select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!--end Modal-->
@endsection

@section('script')

<script type="text/javascript">
    initImages();
    $("#open-all-images").click(function(){
    $("#images-modal").modal('show');
    return false;
  });

  function initImages(ofs=0){
      $.get('/bulkimages?offset='+ofs,function(images){
          if(!images.images.length){
            $("#image-select").imagepicker()
            return false;
          } 
          $.each(images.images,function(i,v){
            var tmp = "<option data-img-src='/bulkimage/"+v.id+"' value='"+v.id+"'>"+v.id+"</option>";
            $("#image-select").append(tmp);
          }); 
          ofs = ofs + 10;
          initImages(ofs);

          $("#image-select").on('change',function(){
          var imageId = $(this).val();
          $("#franchisee-image").attr("data-id",imageId);
          $("#images-modal").modal('hide');
          if(imageId == 0 || imageId == "" || imageId == 'undefined'){
            $("#franchisee-image").addClass('hide');
          }else{
            $("#ifranchisee-image").removeClass('hide');
            $("#franchisee-image").attr('src','/bulkimage/'+imageId);
          }

          });
        });
  }
</script>
<script type="text/javascript" src="/assets/admin/plugins/image-picker/image-picker.js"></script>
<script type="text/javascript" src="js/franchise.js"></script>
<script type="text/javascript" src='http://code.jquery.com/ui/1.9.2/jquery-ui.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/underscore-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/backbone-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/vie-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/jquery.tagsinput.min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/jquery.rdfquery.min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/annotate-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/rangy-core-1.2.3.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/hallo-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/aloha/lib/require.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/aloha/lib/aloha-full.min.js', data-aloha-plugins='common/ui,common/format,common/link,common/image,extra/sourceview'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/create-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/contentblocks.js'></script>
<script type="text/javascript" src="/assets/admin/js/create.js"></script>


<script type="text/javascript">

// Prepare VIE
var v = new VIE();
v.use(new v.RdfaService());

// Load Create.js with your VIE instance
$("body").midgardCreate({
  vie: v
});
</script>
@endsection