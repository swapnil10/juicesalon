<?php
   if(!Auth::check())
    die(redirect('/sign-in'));
  $user = Auth::user();
  if(!$user->isFranchisee){
    die(redirect('/my-account'));
  }
?>
@extends('partials.template')

@section('title','Franchise Owner')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')


<main class="main-section page-main-area" id="account_page">

     <header class="section-header account-page-header" style="background-color: #ccc; padding-bottom: 0; padding-top: 0;">

               <!-- OwlCarousel Start -->
               <div class="owl-carousel account-main-slider slider-simple" id="account_page_slider" style="height: 320px; opacity: 0;">

                   <!-- Slide Item 1 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('images/sliders/grand-creation-salon-hair-extensions-slide.jpg');"></div>

                     
                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>

                   <!-- Slide Item 2 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('images/sliders/clinic-image-2040.jpg');"></div>

                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>

                   <!-- Slide Item 3 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('images/sliders/iStock_000020818374Large.jpg');"></div>

                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>


               </div>
               <!-- OwlCarousel End -->

     </header>


     <!-- Content -->
     <section class="section-content">
         <div class="container">


             <div class="account-list ac-videos-list section-list">
               <header class="account-title-box title-box">
                   <!-- back button to go to screen 1 -->
                   <div class="header-col"></div>

                   <h2 class="account-title title">Franchise</h2>
               </header>

               <!-- control buttons stripe -->  
               <aside id="action_btns_stripe">  
                   <div class="row">
                       <div class="container">
                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-left">
                                   <a class="icon-btn" href="../account.html" id="goToAccount_1">
                                       <img src="../img/icons/go-back-left-arrow.svg" width="28">
                                   </a>
                               </div>
                           </div>

                           <div class="col-md-4 col-sm-12"></div>

                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-right">
                                   <!-- Cart to show the added items - wow! -->
                                   <div class="header-col">
                                      <div class="options-block">
                                          <a title="Checkout" href="#cart" class="options-btn cart-btn links-btn" data-label="Cart" data-target="#checkout_popup">
                                            <span class="sr-only">Cart</span>
                                            <i class="fa fa-shopping-cart hidden"></i>
                                            <img src="../img/icons/shopping-cart.svg" width="16" style="display: inline-block;">
                                            <span class="peg cartItemCount" data-items="3">5</span>
                                          </a>

                                          <div class="popup-dialog go-left" id="checkout_popup">
                                            <div class="bg-overlay"></div>
                                            <div class="inner">
                                              <header class="popup-header">
                                                <h5 class="pull-left">
                                                    <img src="../img/icons/shopping-cart.svg" width="16" style="display: inline-block; margin-right: 10px;">  
                                                    <span>Cart</span>
                                                </h5>
                                                <h5 class="close-btn text-right pull-right">
                                                  <a href="#" data-target="#checkout_popup"><i class="fa fa-close"></i></a>
                                                </h5>
                                                <div class="clearfix"></div>
                                              </header>
                                              
                                              <!-- Cart details -->
                                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. </p>
                                            </div>
                                          </div>

                                      </div>
                                   </div>
                               </div>
                           </div>

                       </div>
                   </div>
               </aside>

               <!-- Content for Franchise Page -->
               <div class="acc-franchise-items">
                 <div class="inner row">
                   
                 </div>
               </div>

             </div>



         </div>
     </section>
</main>

<div id="templatess" style="display: none;">
  <div class="col-md-6 acc-franchise-item-col">
    <!-- Franchise Account Item -->
     <article class="acc-franchise-item">
       <div class="inner-body">
         <div class="icon">
           <div class="icon-box">
             <img src="/images/icons/file.svg" width="128">
             <span id="extension"></span>
           </div>
         </div>

         <div class="text">
             <div class="text-box">
               <h3 id="title"></h3>
               <p id="description"></p>
             </div>

             <div class="button">
                <a target="_blank" href="#" id="download" class="links-btn btn-inverted"><span>Download</span></a>
             </div>
          </div>
       </div>
     </article>
   </div>
</div>
@endsection


@section('script')
<script type="text/javascript">
  cartCount();

  $.get('/api/materials',function(data){
    if(data.error)
      return false;
    $.each(data,function(i,v){
      var a = $("#templatess > .acc-franchise-item-col:first").clone().appendTo(".acc-franchise-items > .inner.row");
      a.find("#extension").html(v.extension);
      a.find("#title").html(v.title);
      a.find("#description").html(v.description);
      a.find("#download").attr('href','/api/materials/'+v.id+'/getFile');
    });
  });
</script>
@endsection