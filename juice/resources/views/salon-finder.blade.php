@extends('partials.template')

@section('title','Find a JUICE Salon')

@section('meta')
@endsection

@section('style')
@endsection


@section('main')
<!--
@ Salon Finder Page
-->
<main class="main-section page-main-area" id="salon_finder">

    <!-- Image stripe -->
    <div class="bg-image-stripe" style="background-image: url('images/salon_3.JPG'); ">
        
    </div>

    <!-- Content -->
    <section class="section-content">
    <div class="section-content container">
       
        <div class="row">
            <section class="col-md-12" id="salonFinderContainer">

            <!-- decoration -->
            <div class="decorate swirl" style="position: absolute;left: -20px;top: 60px;user-select: none;-webkit-user-drag: none; ">
                    <img src="img/decorations/swirl-horizontal_120px.png" width="100">
            </div>
            <!-- decoration end -->

            <!-- Store locator box -->
                <div class="salon-finder-box">
                    <header class="salon-finder-header">
                      <div class="header-inner">
                        <div class="row">
                            <div class="search-header-blocks col-md-4 col-xs-12">
                                <div class="salon-finder-label text-left">
                                    
                                    <div class="title">       
                                      <h3 style="font-size: 1.15em; margin: 0 auto; display: flex; justify-content: center; align-items: center; font-weight: 400;">
                                        <img src="img/icons/map-marker-white.svg" width="32" style="margin-right: 20px;">
                                        <span>Find a Juice Salon near you</spam>
                                      </h3>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="search-header-blocks col-md-5 col-xs-8">
                                <div class="salon-finder-input" id="pac_card">
                                    <!--<form id="mapsLocationSearch" name="maps_location_search" method="get" action="">-->
                                        <input class="controls" id="locationName" type="text" name="place" placeholder="Enter your city..">
                                        <input id="locationRadius" name="radius" value="50" type="hidden">
                                    <!--</form>-->

                                    <!-- 
                                    USING Google's Autocomplete Instead
                                    <div id="typeAheadLocation" class="typeahead-list" style="display: block;">
                                        <ul>
                                            <li data-locationName="New York">New York</li>
                                            <li data-locationName="New Delhi">New Delhi</li>
                                        </ul></div> 
                                    -->
                                </div>
                            </div>
                            <div class="search-header-blocks col-md-3 col-xs-4">
                                <div class="salon-location-submit">
                                    <button type="submit" class="btn-yellow-box" id="location_search_submit">Search</button>
                                </div>
                            </div>
                        </div>
                      </div>
                    </header>
                    <!-- Google Map Salon Locator Canvas -->
                    <div class="google-map-container" id="googlemaps_SalonFinder" style="height: 460px; background-color: #333;"></div>

                    <!-- Salon List based on the searched location  -->
                    <div class="salon-list-container">
                        <div class="salon-list" id="salonLocationList">

                            <!-- salon list item container and content -->
                            <!-- <div class="col-sm-6 col-md-4 salon-location-item animated fadeIn" id="juice_salon_id_1">
                                <div class="inner">
                                    <h3 class="salon-name">Juice Andheri</h3>
                                    <p class="salon-address">
                                        345 Block B R Road,<br>
                                        Near Sai Mandir<br>
                                        Andheri East<br>
                                        Mumbai, Maharashtra<br>
                                        440033<br>
                                    </p>
                                </div>
                            </div> -->

                        </div>
                    </div>



                </div>

            </section>
        </div>
    </div>
    </section>


</main> <!-- main end -->
@endsection

@section('script')
<!-- Page specific JS File -->
<script src="js/salon-locator.js"></script>
<script src="https:/developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script async defer src="https:/maps.googleapis.com/maps/api/js?key=AIzaSyA3IzSGgAVTcz1CzkfJycSxLASGgLLmGqI&libraries=places,geometry&callback=initMap"></script>

@endsection