<?php
	$db = 'Tables_in_';
	
	if(isset($_SERVER['RDS_DB_NAME']))
		$db = $db.$_SERVER['RDS_DB_NAME'];
	else
		$db = $db."juicesalon";
?>
<!DOCTYPE html>
<html>
<head>
	<title>MySQL Workbench</title>
</head>
<body>
	
		<div style="height: 600px;width: 100%;overflow: auto;">
			<center>
				<h4><a title="Refresh table" href="/mysql/migrate?token={{$_GET['token']}}">Refresh</a></h4>
				<h2>All Table Names</h2>
				@foreach($tables as $table)
					<p>
						<a href="/mysql/table/{{$table->{$db} }}?token={{$_GET['token']}}">
						{{$table->{$db} }}
						</a>
					</p>
				@endforeach
			</center>
		</div>
</body>
</html>