<!DOCTYPE html>
<html>
<head>
	<title>MySQL-Table-{{$name}}</title>
</head>
<body>
	<center>
		<h1>Table - {{$name}}</h1>
		<h4><a href="/mysql/truncate/{{$name}}?token={{$_GET['token']}}">Truncate Table</a></h4>
		<h4><a href="/mysql?token={{$_GET['token']}}">MySQL Workbench</a></h4>
		<table>
			<thead>
				<tr>
					@foreach($columns as $column)
						<th>{{$column}}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				@foreach($rows as $row)
					<tr>
						@foreach($columns as $column)
							<td>{{$row->$column}}</td>
						@endforeach
					</tr>
				@endforeach
			</tbody>
		</table>
	</center>
</body>
</html>