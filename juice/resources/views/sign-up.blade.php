@if(Auth::check())
<script type="text/javascript">
	location.href="{{url('/')}}";
</script> 
@endif
@extends('partials.template')

@section('title','Sign Up')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')

<main class="main-section page-main-area" id="sign_up_page">
	
	<!-- Log-in / Sign-up Page Header -->
	<header class="sign-page-header">

		<!-- Image stripe -->
		<div class="bg-image-stripe parallax-bg-image" style="background-image: url('images/salon_3.JPG'); height: 180px;">
			<div class="bg-stretch"></div>
		</div>

	</header>


	<!-- Sign-up Section -->
	<section class="section-main sign-up-section" id="sign_up_section">
		<div class="container">

			<div class="row">
				
				<div class="col-md-2"></div>

				<div class="col-md-8">
					<!-- SIGN-UP FORM -->
					<h3><b class="signup-head">Create an account</b></h3>

					<div id='step1' class="form-container-box font-2 animated fadeInUp">
						<div class="inner">
							<form class="form-general sign-in-form register-form user-form" id='signUpForm' name="signUpForm">
								<div class="form-fields-box">
									<div class="form-close-btn">
										<a class="cancel-button" href="/sign-in" ></a>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-row">
												<label for="full_name">Full Name*</label><br>
												<input type="text" name="fullName" placeholder='eg. John Doe' id="full_name" required />
											</div>

											<div class="form-row">
												<label for="email_id">Email ID*</label><br>
												<input type="email" name="email" placeholder="Valid Email Id" id="email_id" required />
											</div>

											<div class="form-row">
												<label for="email_id">Mobile Number*</label><br>
												<input type="tel" name="phone" placeholder='Without country code' id="mobile_number" required />
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-row">
												<label for="new_password">Password*</label><br>
												<input type="password" name="password" placeholder='Strong Password' id="new_password" required />
											</div>

											<div class="form-row">
												<label for="new_password_confirm">Re-enter Password*</label><br>
												<input type="password" name="password_confirmation" placeholder='Confirm Password' id="new_password_confirm" required />
											</div>
										</div>
									</div>
									<p class='status'></p>
								</div>

								<div class="form-outer-field">
									<input class="form-btn" type="submit" name="signUpForm-btn" value="Proceed" />
								</div>
							</form>
						</div>
					</div>

					<div id='step2' style='display: none;' class="form-container-box font-2 animated fadeInUp">
						<i><p style="color:green;">We've sent an email, kindy check your inbox/spam to verify your email.</p></i>
						<div class="inner">
							<form class="form-general sign-in-form register-form user-form" id='OTPForm' name="OTPForm">
								<div class="form-fields-box">
									<div class="row">
										<div class="col-md-6">
											<div class="form-row">
												<input type='hidden' name="phone" id='hidden-data' />
												<label for="full_name">5-digit OTP*</label><br>
												<input type="text" name="otp" placeholder='5 digit OTP' id="otp" />
											</div>
										</div>
									</div>
								</div>
								<p class="otp-status"></p>
								<div class="form-outer-field">
									<input class="form-btn" type="submit" name="OTPForm-btn" value="Confirm" />
								</div>
							</form>
						</div>
					</div>

				</div>

				<div class="col-md-2"></div>

			</div>
		</div>
	</section>


</main> <!-- # log page main end -->

@endsection

@section('script')
<script type="text/javascript">
	var url1 = "{{url('/api/auth')}}";
	$(document).on('submit','#signUpForm',function(){
		$('.status').fadeOut();
		var data = $('#signUpForm').serializeArray();
		$.ajax({
			url: url1+'/send-otp',
			type: 'POST',
			data: data,
			success: function(data){
				$('#hidden-data').val(data.phone);
				$('.signup-head').html('Verfiy your details');
				$('#step1').fadeOut();
				$('#step2').fadeIn();
			},
			error: function(jqXhr){
				if(jqXhr.status == 422){
		           var errors = jqXhr.responseJSON;
		           $.each(errors,function(k,v){
		                $('.status').html(v[0]).fadeIn().css({'color':'red'});
		                return false;
		           });
		          return false;
		        }
			}
		});
		return false;
	});

	$(document).on('submit','#OTPForm',function(){
		$('.otp-status').fadeOut();
		var data = $('#OTPForm').serializeArray();
		$.ajax({
			url: url1+'/register',
			type: 'POST',
			data: data,
			success: function(data){
				if(data.error){
					$('.otp-status').html(data.error).fadeIn().css({'color':'red'});
		            return false;
				}
				window.location.href='/';
			},
			error: function(jqXhr){
				if(jqXhr.status == 422){
		           var errors = jqXhr.responseJSON;
		           $.each(errors,function(k,v){
		                $('.otp-status').html(v[0]).fadeIn().css({'color':'red'});
		                return false;
		           });
		          return false;
		        }
			}
		});
		return false;
	});
</script>
@endsection