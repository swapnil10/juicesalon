@extends('partials.template')
@section('title','Range of Make Up Products, Beauty Tips, Fashion Trends Online in India')
@section('meta')
@endsection
@section('style')
@endsection

@section('custom-page-header')
<!-- 
@ Main Page Carousel 
-->
<section class="home-page-slider" id="slider_container_home">

      <!-- OwlCarousel Start -->
      <div class="owl-carousel home-main-slide" id="home_slide_1" style="height: 360px; opacity: 0;">
          
      </div>
      <!-- OwlCarousel End -->
</section> 
@endsection
@section('main')
  <main class="main-section page-main-area" id="home_page_content">
  <!-- Section 1 : Images cards and text -->
  <section class="section-main cards featured-cards">
      <div class="container-fluid">
          <!-- Feature Box #1 / Style 5 -->
          <div class="col-md-6 col-sm-12">

              <article class="featured-boxes style--5">
                <div class="inner row">
                  
                  <div class="col col-xs-5 nopadding">
                    <div class="image-container">
                           <img id="leftimage" src="images/featured/featured_look_1.jpg">
                      </div>
                  </div>
                  
                  <div class="col col-xs-7 summary-box nopadding text-right">
                    <div class="summary-box-inner">
                        <p id="leftcontent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.  rhoncus quis cursus sed, egestas ut nulla.</p>
                        <p class="floating-btn"><b><a class="links-btn btn-shadow__yellow" href="#">View all looks</a></b></p>
                    </div>
                  </div>
                </div>
              </article>

          </div>

          <!-- Feature Box #2 / Style 2 -->
          <div class="col-md-6 col-sm-12" style="position: relative;">
               <!-- decoration -->
               <div id="decor_xG591" class="decorate dots visible-md visible-lg" style="position: absolute; left: 10px; top: 100px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                               <img src="img/decorations/dots_50px.png" width="32">
                       </div>
               <!-- decoration end -->

              <article class="featured-boxes style--2" style="max-width: 400px;">
                <div class="inner row">
                  <div class="column-text col col-xs-6">
                      <header class="title-box">
                        <h3 id="rightheading">Launching the LYN conditioner soon</h3>
                      </header>
                      <p class="summary" id="rightcontent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.  </p>
                      
                    <p><b><a class="link-highlighted" href="#">Subscribe to the newsletter</a></b></p>
                  </div>
                  
                  <div class="column-image col col-xs-6">
                    <div class="image-container text-center">
                           <img id="rightimage" src="images/featured/featured_product_1.png">
                      </div>
                  </div>
                      
                </div>
              </article>
          </div>

      </div>
  </section>

  <!-- Section 2 : Images cards and text -->
  <section class="section-main cards featured-cards">
      <div class="container-fluid">
          <div class="row">

              <div class="col-md-6 col-sm-12">
                  <!-- Feature Box #3 / Style 1 -->
                  <article class="featured-boxes style--1" style="max-width: 400px;">
                    <div class="inner">
                      <div class="bg-half yellow"></div>
                      
                      <header class="title-box">
                        <h3><a href="#" id="leftheading">You should do this today yourself</a></h3>
                      </header>
                      
                      <div class="image-container">
                        <img id="leftimage1" src="images/featured/featured_look_2.jpg">
                      </div>
                      
                      <p id="leftcontent1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
                      
                    </div>
                  </article>

              </div>
              


              <!-- Feature Box #2 -->

              <div class="col-md-6 col-sm-12">

                  <!-- Feature Box #4 / Style 3 -->
                  <article class="featured-boxes style--3">
                    <div class="inner row">
                      <div class="col col-xs-5 nopadding">
                        <div class="image-container">
                               <img id="rightimage1" src="images/featured/featured_look_8.jpg">
                          </div>
                      </div>
                      <div class="col col-xs-7 summary-box nopadding text-right">
                        <div class="summary-box-inner">
                            <p id="rightcontent1">Try these new haircuts from our look book.<br>Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai it's discounted so I guess you are more interest.</p>
                            <p><b><a class="link-highlighted" href="#">Find out more</a></b></p>
                        </div>
                      </div>
                    </div>
                  </article>
                  
              </div>

          </div>
      </div>
  </section>



  <!-- Get the Look - Top Picks -->
  <section class="section-main look-page-section" style="background-color: #ffff3d; position: relative; padding: 0;">

      <div class="container" style="position: relative; padding-bottom: 1em; padding-top: 3em;">
          <!-- decoration -->
          <div id="decor_xG56" class="decorate dots" style="position: absolute; right: 20px; top: -16.5px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                  <img src="img/decorations/dots-horizontal_120px.png" width="84">
          </div>
          <!-- decoration end -->

          <div class="row">

            <div class="col-md-4 col-xs-12" >
              <div class="page-description-header">
                <div class="media">

                  <div style="display: flex; align-items: center;">
                       <h4 id="leftheading1">Our picks for the haircuts you need to rock this monsoon</h4>
                       <img  style="padding: 0px" src="img/icons/scissors.svg" alt="Scissors Cutting Hair" width="32">

                       <div class="clearfix"></div>
                  </div>

                  <div class="media-body">
                    <p id="leftcontent2">Nunc tincidunt quis dui sed efficitur. Cras lo sed varius consectetur, metus mauris ullamcorper tortor, in euismod lectus nunc ac lorem. </p>
                    <p>
                        <a href="get-the-look.html" title="Get the Look!" class="links-btn btn-shadow small pull-right"><b>Get the look</b></a>
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-1 col-lg-1 visible-md visible-lg"></div>

            <div class="col-md-7 col-xs-12">
              <div class="row">
                <div class="animated fadeIn image-card none col-md-4 col-xs-6 text-center">
                  <div class="inner-container">
                    <img style="margin: 0 auto;" id="rightimage2" src="images/get-the-look/look-1-is.png" width="150" height="180" class="img-responsive">
                    <p class="caption-image-card text-left" style="max-width: 165px; margin: 0 auto;">This hairstyle that</p>
                  </div>  
                </div>

                <div class="animated fadeIn image-card none col-md-4 col-xs-6 text-center">
                  <div class="inner-container">
                    <img style="margin: 0 auto;" id="rightimage3" src="images/get-the-look/look-2-is.png" width="150" height="180" class="img-responsive">
                    <p class="caption-image-card text-left" style="max-width: 165px; margin: 0 auto;">This hairstyle that</p>
                  </div>  
                </div>

                <div class="animated fadeIn image-card none col-md-4 col-xs-12 text-center">
                  <div class="inner-container">
                    <img style="margin: 0 auto;" id="rightimage4" src="images/get-the-look/look-3-is.png" width="150" height="180" class="img-responsive">
                    <p class="caption-image-card text-left" style="max-width: 165px; margin: 0 auto;">This hairstyle that</p>
                  </div>  
                </div>

              </div>
            </div>

          </div>
      </div>
  </section>




  <!-- Section 3 : Images cards and text -->
  <section class="section-main cards featured-cards">
      <div class="container-fluid">
          <!-- Feature Box #5 / Style 4-->
          <div class="col-md-6 col-sm-12">
              <article class="featured-boxes style--4">
                <div class="inner row">
                  <div class="col col-xs-5 nopadding">
                    <div class="image-container">
                           <img id="leftimage5" src="images/featured/featured_look_6.jpg">
                      </div>
                  </div>
                  <div class="col col-xs-7 summary-box nopadding text-right">
                    <div class="summary-box-inner">
                        <p id="leftcontent3">Try these new haircuts from our look book.<br>
Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai it's discounted so I guess you are more interest.</p>
                        <p><b><a class="links-btn btn-shadow" href="#">View all looks</a></b></p>
                    </div>
                  </div>
                </div>
              </article>
          </div>

          <!-- Feature Box #6 / Style 3 -->

          <div class="col-md-6 col-sm-12">
              <article class="featured-boxes style--3">
                <div class="inner row">
                  
                  <div class="col col-xs-7 summary-box nopadding text-left">
                    <div class="summary-box-inner">
                        <p id="rightcontent2">Try these new haircuts from our look book.<br>
Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai it's discounted so I guess you are more interest.</p>
                        <p><b><a class="link-highlighted" href="#">Find out more</a></b></p>
                    </div>
                  </div>
                  
                  <div class="col col-xs-5 nopadding">
                    <div class="image-container">
                           <img id="rightimage5" src="images/featured/featured_look_5.jpg">
                      </div>
                  </div>
                </div>
              </article>
          </div>

      </div>
  </section>



  <!-- Section 4 : Images cards and text -->
  <section class="section-main cards featured-cards">
      <div class="container-fluid">
          <!-- Feature Box #7 / Style 6 -->
          <div class="col-md-6 col-sm-12">
              
              <article class="featured-boxes style--6">
                <div class="inner row">
                  <div class="bg"></div>
                  
                  <div class="col col-md-6 col-sm-6 col-xs-12 nopadding">
                    <div class="image-container">
                           <img id="leftimage6" src="images/featured/featured_look_7.jpg">
                    
                      </div>
                  </div>
                  
                  <div class="col col-md-6 col-sm-6 col-xs-12 summary-box nopadding text-right">
                    <div class="summary-box-inner" style="padding-top: 2em;">
                        <p id="leftcontent4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.  rhoncus quis cursus sed, egestas ut nulla.</p>
                        <p class="floating-btn"><b><a class="links-btn btn-shadow__yellow" href="#">View all looks</a></b></p>
                    </div>
                  </div>
                  
                </div>
              </article>
          </div>

          <!-- Feature Box #8 / Style 2 -->
          <div class="col-md-6 col-sm-12" style="position: relative;">
            <!-- decoration -->
            <div id="decor_xG592" class="decorate dots visible-md visible-lg" style="position: absolute; left: 10px; top: 60px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                            <img src="img/decorations/dots_50px.png" width="32">
                    </div>
            <!-- decoration end -->
              <article class="featured-boxes style--2">
                <div class="inner row">
                  <div class="column-image col col-xs-12">
                    <div class="image-container text-center">
                           <img id="rightimage6" src="images/featured/featured_product_2.png">
                      </div>
                  </div>
                  
                  <div class="column-text col col-xs-12 text-right">
                      <header class="title-box text-left">
                        <h3 id="rightheading1">Launching the LYN conditioner next week. Here's why you need to buy it.</h3>
                      </header>
                      <p class="summary" id="rightcontent3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.  </p>
                      
                    <p><b><a class="link-highlighted" href="#">Subscribe to the newsletter</a></b></p>
                  </div>
                      
                </div>
              </article>
          </div>
      </div>
  </section>


<!-- Experts Section - Images Cards  -->
<section class="section-main images-cards experts-section" style="background-color: #111; color: #fff;">
    <div class="container">
        
        <div class="col-md-2">
            <div class="text-col">
                <!-- decoration -->
                <div class="decoration swirl">
                    <img style="width: 100%; max-width: 100px; margin-bottom: 3em;" src="img/decorations/swirl-decorate-white-200.png">
                </div>
                <!-- decoration -->

                <p id="leftcontent5">Our experts say that random lorem ipsum hair tips and tricks. This is what they have to say.
Click on their pic to learn more about them.</p>


                <p style="margin-top: 2em;">
                    <a class="links-btn btn-shadow__yellow" href="#">View all looks</a>
                </p>

            </div>
        </div>

        <div class="col-md-10 experts-image-cards">
            <div class="row">
                <div class="col-md-4">
                    <div class="image-card">
                        <div class="image-container">
                            <img id="rightimage7" src="images/experts/expert-1.jpg">
                        </div>

                        <div class="expert-details">
                            <div class="inner">
                                <h3 id="heading1">Lorem Ipsum Doler</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="image-card">
                        <div class="image-container">
                            <img id="rightimage8" src="images/experts/expert-2.jpg">
                        </div>

                        <div class="expert-details">
                            <div class="inner">
                                <h3 id="heading2">Lorem Ipsum Doler</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="image-card">
                        <div class="image-container">
                            <img id="rightimage9" src="images/experts/expert-3.jpg">
                        </div>

                        <div class="expert-details">
                            <div class="inner">
                                <h3 id="heading3">Lorem Ipsum Doler</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>



<!-- Section 5 : Images cards and text -->
<section class="section-main cards featured-cards">
  <div class="container-fluid">
      <!-- Feature Box #9 / Style 1 -->
      <div class="col-md-6 col-sm-12">
          <article class="featured-boxes style--1" style="max-width: 400px;">
            <div class="inner">
              <div class="bg-half yellow"></div>
              
              <header class="title-box">
                <h3 id="leftheading2"><a href="#">Why you need to do this before getting that done everyday.</a></h3>
              </header>
              
              <div class="image-container">
                <img id="leftimage7" src="images/featured/featured_look_3.jpg">
              </div>
              
              <p id="leftcontent6">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
              
            </div>
          </article>
      </div>

      <!-- Feature Box #10 / Style -->
      <div class="col-md-6 col-sm-12">
                <article class="featured-boxes style--7">
                  <div class="inner row row-eq-height">
            
                    <div class="bg"></div>
                     
                    <div class="col col-xs-6 summary-box nopadding">
                      <header class="title-box">
                          <h2><a href="#" id="rightheading2">Get some this and that for your skin.</a></h2>
                        <div class="arrow-icon" style="bottom: .5em;">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="32px" height="32px" viewBox="0 0 408 408" style="enable-background:new 0 0 408 408;" xml:space="preserve"> <g> <g id="arrow-back"> <path d="M408,178.5H96.9L239.7,35.7L204,0L0,204l204,204l35.7-35.7L96.9,229.5H408V178.5z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>
                        </div>

                    </header>

                      <div class="summary-box-inner">
                          <p id="rightcontent4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.</p>
                          <p class="floating-link"><b><a class="links-btn btn-shadow__yellow" href="#">View all looks</a></b></p>
                      </div>
                    </div>
                    
                    <div class="col col-xs-6 nopadding">
                      <div class="image-container">
                             <img id="rightimage10" src="images/featured/featured_look_4.jpg">
                        </div>
                    </div>
                    
                    
                  </div>
                </article>
      </div>
  </div>
</section>
</main>
<div id="template-slider" style="display: none;">
  <div class="owl-item" style="float: none;">
    <div class="bg-image-slide" id="image-slider"></div>
    <div class="slide-inner">
        <div class="bubble-container" ></div>
        <div class="text-col" data-animation="animated fadeInLeft">
            <p class="medium" id="description"></p>
            <p class="btn-container"><a id="link" href="#products" class="action-link">View Products</a> </p>
        </div>
    </div>
  </div>
</div>
@endsection

@section('instagram-feed')
@include('partials.instagram-feed')
@endsection
@section('script')
<script type="text/javascript">
$.get("api/homepage",function(data){
    console.log(data);
    
    $("#leftcontent").html(data[0].leftcontent);
    $("#leftimage").attr("src","/bulkimage/"+data[0].leftimage).attr("data-lookId",data[0].leftimage);
    $("#rightheading").html(data[0].rightheading);
    $("#rightcontent").html(data[0].rightcontent);
    $("#rightimage").attr("src","/bulkimage/"+data[0].rightimage).attr("data-lookId",data[0].rightimage);
    $("#rightheading1").html(data[0].rightheading1);
    $("#leftheading").html(data[0].leftheading);
    $("#leftimage1").attr("src","/bulkimage/"+data[0].leftimage1).attr("data-lookId",data[0].leftimage1);
    $("#leftcontent1").html(data[0].leftcontent1);
    $("#rightimage1").attr("src","/bulkimage/"+data[0].rightimage1).attr("data-lookId",data[0].rightimage1);
    $("#rightcontent1").html(data[0].rightcontent1);
    $("#leftheading1").html(data[0].leftheading1);
    $("#leftcontent2").html(data[0].leftcontent2);
    $("#rightimage2").attr("src","/bulkimage/"+data[0].rightimage2).attr("data-lookId",data[0].rightimage2);
    $("#rightimage3").attr("src","/bulkimage/"+data[0].rightimage3).attr("data-lookId",data[0].rightimage3);
    $("#rightimage4").attr("src","/bulkimage/"+data[0].rightimage4).attr("data-lookId",data[0].rightimage4);
    $("#leftimage5").attr("src","/bulkimage/"+data[0].leftimage5).attr("data-lookId",data[0].leftimage5);
    $("#leftcontent3").html(data[0].leftcontent3);
    $("#rightimage5").attr("src","/bulkimage/"+data[0].rightimage5).attr("data-lookId",data[0].rightimage5);
    $("#rightcontent2").html(data[0].rightcontent2);
    $("#leftimage6").attr("src","/bulkimage/"+data[0].leftimage6).attr("data-lookId",data[0].leftimage6);
    $("#leftcontent4").html(data[0].leftcontent4);
    $("#rightheading2").html(data[0].rightheading2);
    $("#rightimage6").attr("src","/bulkimage/"+data[0].rightimage6).attr("data-lookId",data[0].rightimage6);
    $("#rightcontent3").html(data[0].rightcontent3);
    $("#leftcontent5").html(data[0].leftcontent5);
    $("#rightimage7").attr("src","/bulkimage/"+data[0].rightimage7).attr("data-lookId",data[0].rightimage7);
    $("#rightimage8").attr("src","/bulkimage/"+data[0].rightimage8).attr("data-lookId",data[0].rightimage8);
    $("#rightimage9").attr("src","/bulkimage/"+data[0].rightimage9).attr("data-lookId",data[0].rightimage9);
    $("#leftheading2").html(data[0].leftheading2);
    $("#leftcontent6").html(data[0].leftcontent6);
    $("#leftimage7").attr("src","/bulkimage/"+data[0].leftimage7).attr("data-lookId",data[0].leftimage7);
    $("#rightcontent4").html(data[0].leftcontent6);
    $("#rightimage10").attr("src","/bulkimage/"+data[0].rightimage10).attr("data-lookId",data[0].rightimage10);
    $("#heading1").html(data[0].heading1);
    $("#heading2").html(data[0].heading2);  
    $("#heading3").html(data[0].heading3);
  });
  $.get(api+"slider/home",function(data){
    $.each(data,function(i,v){
      template = $("#template-slider > .owl-item").clone();
      template.find('#description').html(v.content);
      template.find('#link').attr("href",v.link);
      template.find('#image-slider').css("background-image","url('/image/"+v.imageId+"')");
      template.appendTo("#home_slide_1");
    });
    console.log("IN");
    slider();
  });

  function slider(){
    $("#home_slide_1").on("initialized.owl.carousel", function(e){
      var $homeCarouselSlider = $("#home_slide_1");
          $homeCarouselSlider.css({'height': 'initial'});
          $homeCarouselSlider.animate({'opacity': 1});
    });
    var homepageCarouselMain = $("#home_slide_1").owlCarousel({
        loop:true,
        margin: 0,
        nav:true,
        dots:true, 
        items: 1,
        loop: false,
        responsive:{
            0:{
                items:1
            },

            600:{
                items:1
            },

            1000:{
                items:1
            }
        }
    });
    homepageCarouselMain.on("changed.owl.carousel", function(e){
      var $animatingElems = $(e.relatedTarget.$element).find("[data-animation ^= 'animated']");
      doAnimations( $animatingElems );
    });
  }
  function doAnimations( elems ) {
    //Cache the animationend event in a variable
    var animEndEv = 'webkitAnimationEnd animationend';
    
    elems.each(function () {
      var $this = $(this),
        $animationType = $this.attr('data-animation');

      $this.addClass($animationType).one(animEndEv, function () {
        $this.removeClass($animationType);
      });
    });
  }
</script>
@endsection
