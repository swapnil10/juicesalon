<?php
   if(!Auth::check())
    die(redirect('/sign-in'));
  $user = Auth::user();
?>
@extends('partials.template')

@section('title','Account')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')

<main class="main-section page-main-area" id="account_page">

     <header class="section-header account-page-header" style="background-color: #ccc; padding-bottom: 0; padding-top: 0;">

               <!-- OwlCarousel Start -->
               <div class="owl-carousel account-main-slider slider-simple" id="account_page_slider" style="height: 320px; opacity: 0;">

                   <!-- Slide Item 1 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('images/sliders/grand-creation-salon-hair-extensions-slide.jpg');"></div>

                     
                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>

                   <!-- Slide Item 2 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('images/sliders/clinic-image-2040.jpg');"></div>

                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>

                   <!-- Slide Item 3 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('images/sliders/iStock_000020818374Large.jpg');"></div>

                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>


               </div>
               <!-- OwlCarousel End -->

     </header>
     <!-- Content -->
     <section class="section-content">
         <div class="container">
             <div class="account-list section-list">
                 <header class="account-title-box title-box">
                     <!-- back button to go to screen 1 -->
                     <div class="header-col">

                     </div>

                     <h2 class="account-title title">My Account</h2>

                 </header>

               <!-- control buttons stripe -->  
               <aside id="action_btns_stripe">  
                   <div class="row">
                       <div class="container">
                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-left">
                                   <a class="icon-btn" href="/" id="goToServices_1">
                                       <img src="img/icons/go-back-left-arrow.svg" width="28">
                                   </a>
                               </div>
                           </div>

                           <div class="col-md-4 col-sm-12"></div>

                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-right">
                                   <!-- Cart to show the added items - wow! -->
                                   <div class="header-col">
                                      <div class="options-block">
                                          <a title="Checkout" href="#cart" class="options-btn cart-btn links-btn" data-label="Cart" data-target="#checkout_popup">
                                            <span class="sr-only">Cart</span>
                                            <i class="fa fa-shopping-cart hidden"></i>
                                            <img src="img/icons/shopping-cart.svg" width="16" style="display: inline-block;">
                                            <span class="peg cartItemCount" data-items="3">0</span>
                                          </a>

                                          <div class="popup-dialog go-left" id="checkout_popup">
                                            <div class="bg-overlay"></div>
                                            <div class="inner">
                                              <header class="popup-header">
                                                <h5 class="pull-left">
                                                    <img src="img/icons/shopping-cart.svg" width="16" style="display: inline-block; margin-right: 10px;">  
                                                    <span>Cart</span>
                                                </h5>
                                                <h5 class="close-btn text-right pull-right">
                                                  <a href="#" data-target="#checkout_popup"><i class="fa fa-close"></i></a>
                                                </h5>
                                                <div class="clearfix"></div>
                                              </header>
                                              
                                              <!-- Cart details -->
                                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. </p>
                                            </div>
                                          </div>

                                      </div>
                                   </div>
                               </div>
                           </div>

                       </div>
                   </div>
               </aside>

               <div class="row screens" style="position: relative; padding: 2em 0; min-height: 320px;" id="screen_1">

                <!-- VIDEOS - MY ACCOUNT ITEMS -->
                <div class="list-col col-md-4 col-sm-6 col-xs-12">

                     <a href="videos" class="list-item"  title="My Videos..">
                         <span>Videos</span>
                     </a>
                </div>


                <!-- ORDERS - MY ACCOUNT ITEMS -->
                <div class="list-col col-md-4 col-sm-6 col-xs-12">
                     <a href="/my-orders" class="list-item"  title="My Orders">
                         <span>Orders</span>
                     </a>

                </div>   
                @if($user->isFranchisee)
                <!-- FRANCHISE - MY ACCOUNT ITEMS -->
                <div class="list-col col-md-4 col-sm-6 col-xs-12">
                     <a href="/franchise-owner" class="list-item" title="Franchise">
                         <span>Franchise</span>
                     </a>
                </div>  

                @endif
               </div>

             </div>
         </div>
     </section>
</main>
@endsection


@section('script')
<script type="text/javascript">
  cartCount();
</script>
@endsection
