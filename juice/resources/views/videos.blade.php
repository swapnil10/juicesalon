@extends('partials.template')

@section('title','Academy')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')
<main class="main-section page-main-area" id="account_page">

     <header class="section-header account-page-header" style="background-color: #ccc; padding-bottom: 0; padding-top: 0;">

               <!-- OwlCarousel Start -->
               <div class="owl-carousel account-main-slider slider-simple" id="account_page_slider" style="height: 320px; opacity: 0;">

                   <!-- Slide Item 1 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('/account-images/images/sliders/grand-creation-salon-hair-extensions-slide.jpg');"></div>

                     
                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>

                   <!-- Slide Item 2 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('/account-images/images/sliders/clinic-image-2040.jpg');"></div>

                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>

                   <!-- Slide Item 3 -->
                   <div class="owl-item" style="float: none;">
                     <div class="bg-image-slide" style="background-image: url('/account-images/images/sliders/iStock_000020818374Large.jpg');"></div>

                     <div class="container">
                       <div class="slide-inner">
                           <div class="text-col">
                               <h2 class="slide-title">Some big title here.</h2>
                               <p>A bit of information about the salon and the academy...</p>
                           </div>
                       </div>
                     </div>

                   </div>


               </div>
               <!-- OwlCarousel End -->

     </header>


     <!-- Content -->
     <section class="section-content">
         <div class="container">
             <div class="account-list ac-videos-list section-list">
               <header class="account-title-box title-box">
                   <!-- back button to go to screen 1 -->
                   <div class="header-col"></div>

                   <h2 class="account-title title">Videos</h2>
               </header>

               <!-- control buttons stripe -->  
               <aside id="action_btns_stripe">  
                   <div class="row">
                       <div class="container">
                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-left">
                                   <a class="icon-btn" href="/my-account" id="goToAccount_1">
                                       <img src="/img/icons/go-back-left-arrow.svg" width="28">
                                   </a>
                               </div>
                           </div>

                           <div class="col-md-4 col-sm-12"></div>

                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-right">
                                   <!-- Cart to show the added items - wow! -->
                                   <div class="header-col">
                                      <div class="options-block">
                                          <a title="Checkout" href="#cart" class="options-btn cart-btn links-btn" data-label="Cart" data-target="#checkout_popup">
                                            <span class="sr-only">Cart</span>
                                            <i class="fa fa-shopping-cart hidden"></i>
                                            <img src="../img/icons/shopping-cart.svg" width="16" style="display: inline-block;">
                                            <span class="peg cartItemCount" data-items="3">0</span>
                                          </a>

                                          <div class="popup-dialog go-left" id="checkout_popup">
                                            <div class="bg-overlay"></div>
                                            <div class="inner">
                                              <header class="popup-header">
                                                <h5 class="pull-left">
                                                    <img src="/img/icons/shopping-cart.svg" width="16" style="display: inline-block; margin-right: 10px;">  
                                                    <span>Cart</span>
                                                </h5>
                                                <div class="clearfix"></div>
                                              </header>
                                            </div>
                                          </div>

                                      </div>
                                   </div>
                               </div>
                           </div>

                       </div>
                   </div>
               </aside>
             </div>
         </div>
     </section>


     <!-- Content for Video Items -->
     <section class="section-content video-list-section" style="padding-top: 4em; padding-bottom: 4em;">
       <div class="container-fluid">
         <!-- Content for Videos Page -->
         <div class="acc-videos-items">
           
           <div class="row video-items-row video-items-row_1">
             <div class="col-md-4 col-sm-6 col-lg-3">
               <!-- Video Item -->
               <article class="acc-video-item" id="video_item_1">
                 <div class="video-thumbnail">
                   <div class="thumb-overlay"></div>
                   <div class="img-container">
                     <img src="/account-images/images/vids/vid_thumb_1_320px.jpg">
                   </div>
                 </div>

                 <div class="video-details">
                   <div class="video-timing"><span>3:20 min</span></div>
                   <h3 class="video-title">Hair</h3>
                   <div><a href="#" class="video-label"><span>Beginner Course</span></a></div>
                   <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus. Lorem ipsum dolor sit amet</p>
                 </div>
               </article>
             </div>

             <div class="col-md-4 col-sm-6 col-lg-3">
               <!-- Video Item -->
               <article class="acc-video-item" id="video_item_1">
                 <div class="video-thumbnail">
                   <div class="thumb-overlay"></div>
                   <div class="img-container">
                     <img src="account-images/images/vids/vid_thumb_1_320px.jpg">
                   </div>
                 </div>

                 <div class="video-details">
                   <div class="video-timing"><span>3:20 min</span></div>
                   <h3 class="video-title">Hair</h3>
                   <div><a href="#" class="video-label"><span>Beginner Course</span></a></div>
                   <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus</p>
                 </div>
               </article>
             </div>

             <!-- Responsive Separator -->
             <div class="hidden-lg hidden-md col-sm-12"></div>

             <div class="col-md-4 col-sm-6 col-lg-3">
               <!-- Video Item -->
               <article class="acc-video-item" id="video_item_1">
                 <div class="video-thumbnail">
                   <div class="thumb-overlay"></div>
                   <div class="img-container">
                     <img src="/account-images/images/vids/vid_thumb_1_320px.jpg">
                   </div>
                 </div>

                 <div class="video-details">
                   <div class="video-timing"><span>3:20 min</span></div>
                   <h3 class="video-title">Hair</h3>
                   <div><a href="#" class="video-label"><span>Beginner Course</span></a></div>
                   <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus Vivamus tortor magna, malesuada eget hendrerit vitae.</p>
                 </div>
               </article>
             </div>

             <div class="col-md-4 col-sm-6 col-lg-3">
               <!-- Video Item -->
               <article class="acc-video-item" id="video_item_1">
                 <div class="video-thumbnail">
                   <div class="thumb-overlay"></div>
                   <div class="img-container">
                     <img src="/account-images/images/vids/vid_thumb_1_320px.jpg">
                   </div>
                 </div>

                 <div class="video-details">
                   <div class="video-timing"><span>3:20 min</span></div>
                   <h3 class="video-title">Hair</h3>
                   <div><a href="#" class="video-label"><span>Beginner Course</span></a></div>
                   <p class="description">Vivamus tortor magna, malesuada eget hendrerit vitae, gravida eget ligula. Lorem ipsum dolor sit amet.</p>
                 </div>
               </article>
             </div>

           </div>


           <div class="row video-items-row video-items-row_1">
             <div class="col-md-4 col-sm-6 col-lg-3">
               <!-- Video Item -->
               <article class="acc-video-item" id="video_item_1">
                 <div class="video-thumbnail">
                   <div class="thumb-overlay"></div>
                   <div class="img-container">
                     <img src="/account-images/images/vids/vid_thumb_1_320px.jpg">
                   </div>
                 </div>

                 <div class="video-details">
                   <div class="video-timing"><span>3:20 min</span></div>
                   <h3 class="video-title">Hair</h3>
                   <div><a href="#" class="video-label"><span>Beginner Course</span></a></div>
                   <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus. Lorem ipsum dolor sit amet</p>
                 </div>
               </article>
             </div>

             <div class="col-md-4 col-sm-6 col-lg-3">
               <!-- Video Item -->
               <article class="acc-video-item" id="video_item_1">
                 <div class="video-thumbnail">
                   <div class="thumb-overlay"></div>
                   <div class="img-container">
                     <img src="/account-images/images/vids/vid_thumb_1_320px.jpg">
                   </div>
                 </div>

                 <div class="video-details">
                   <div class="video-timing"><span>3:20 min</span></div>
                   <h3 class="video-title">Hair</h3>
                   <div><a href="#" class="video-label"><span>Beginner Course</span></a></div>
                   <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus</p>
                 </div>
               </article>
             </div>

             <!-- Responsive Separator -->
             <div class="hidden-lg hidden-md col-sm-12"></div>

             <div class="col-md-4 col-sm-6 col-lg-3">
               <!-- Video Item -->
               <article class="acc-video-item" id="video_item_1">
                 <div class="video-thumbnail">
                   <div class="thumb-overlay"></div>
                   <div class="img-container">
                     <img src="/account-images/images/vids/vid_thumb_1_320px.jpg">
                   </div>
                 </div>

                 <div class="video-details">
                   <div class="video-timing"><span>3:20 min</span></div>
                   <h3 class="video-title">Hair</h3>
                   <div><a href="#" class="video-label"><span>Beginner Course</span></a></div>
                   <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus Vivamus tortor magna, malesuada eget hendrerit vitae.</p>
                 </div>
               </article>
             </div>

             <div class="col-md-4 col-sm-6 col-lg-3">
               <!-- Video Item -->
               <article class="acc-video-item" id="video_item_1">
                 <div class="video-thumbnail">
                   <div class="thumb-overlay"></div>
                   <div class="img-container">
                     <img src="/account-images/images/vids/vid_thumb_1_320px.jpg">
                   </div>
                 </div>

                 <div class="video-details">
                   <div class="video-timing"><span>3:20 min</span></div>
                   <h3 class="video-title">Hair</h3>
                   <div><a href="#" class="video-label"><span>Beginner Course</span></a></div>
                   <p class="description">Vivamus tortor magna, malesuada eget hendrerit vitae, gravida eget ligula. Lorem ipsum dolor sit amet.</p>
                 </div>
               </article>
             </div>

           </div>


         </div>
       </div>
     </section> <!-- # video items section end -->




</main>
@endsection