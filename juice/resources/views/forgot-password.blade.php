@if(Auth::check())
<script type="text/javascript">
	location.href="{{url('/')}}";
</script> 
@endif
@extends('partials.template')

@section('title','Forgot Password')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')
<main class="main-section page-main-area" id="log_in_page" style="overflow-x: hidden;">
	
	<!-- Log-in / Sign-up Page Header -->
	<header class="sign-page-header">

		<!-- Image stripe -->
		<div class="bg-image-stripe parallax-bg-image" style="background-image: url('images/salon_3.JPG'); height: 180px;">
			<div class="bg-stretch"></div>
		</div>

	</header>



	<!-- Log-in Section -->
	<section class="section-main log-in-section" id="log_in_section">
		<div class="container">

			<div class="row">
				
				<div class="col-md-3 hidden-sm hidden-xs"></div>

				<div class="col-md-6 col-sm-12 col-xs-12 ">
					<!-- LOG-IN FORM -->
					<h3><b>Forgot Password?</b></h3>

					<div id='step1' class="form-container-box font-2">
						<div class="inner">
							<form class="form-general sign-in-form register-form user-form" id="forgot1">
								<div class="form-fields-box">
									<div class="form-row">
										<label for="user_email_id">Mobile Number</label><br>
										<input type="text" name="phone" id="phone" placeholder="Your Mobile Number" />
									</div>
									<p class="forgot-status"></p>
								</div>

								<div class="form-outer-field">
									<input class="form-btn" type="submit" value="Send OTP" />
								</div>
							</form>
						</div>
					</div>

					<div id='step2' style='display: none;' class="form-container-box font-2">
						<div class="inner">
							<form class="form-general sign-in-form register-form user-form" id="forgot2">
								<div class="form-fields-box">
									<div class="form-row">
										<label for="user_email_id">5 - Digit OTP</label><br>
										<input type="hidden" name="phone" id="hidden-data" />
										<input type="text" name="otp" id="otp" placeholder="Your Mobile Number" required />
									</div>

									<div class="form-row">
										<label for="user_email_id">New Password</label><br>
										<input type="password" name="password" id="password" placeholder="Strong Password" required />
									</div>

									<div class="form-row">
										<label for="user_email_id">Re-enter Password</label><br>
										<input type="text" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" required />
									</div>
									<p class="forgot2-status"></p>
								</div>

								<div class="form-outer-field">
									<input class="form-btn" type="submit" value="Change Password" />
								</div>
							</form>
						</div>
					</div>

				</div>

				<div class="col-md-3 hidden-sm hidden-xs"></div>

			</div>
		</div>
	</section>

</main> <!-- # log page main end -->


@endsection

@section('script')
<script type="text/javascript">
	var url1 = "{{url('/api/auth')}}";
</script>
<script type="text/javascript">
	$(document).on('submit','#forgot1',function(){
		$('.forgot-status').fadeOut();
		var data = $('#forgot1').serializeArray();
		$.ajax({
			url: url1+'/forgot-password-otp',
			type: 'POST',
			data: data,
			success: function(data){
				if(data.error){
					 $('.forgot-status').html(data.error).fadeIn().css({'color':'red'});
		             return false;
				}
				$('#hidden-data').val(data.phone);
				$('#step1').fadeOut();
				$('#step2').fadeIn();
			},
			error: function(jqXhr){
				if(jqXhr.status == 422){
		           var errors = jqXhr.responseJSON;
		           $.each(errors,function(k,v){
		                $('.forgot-status').html(v[0]).fadeIn().css({'color':'red'});
		                return false;
		           });
		          return false;
		        }
			}
		});
		return false;
	});

	$(document).on('submit','#forgot2',function(){
		$('.forgot2-status').fadeOut();
		var data = $('#forgot2').serializeArray();
		$.ajax({
			url: url1+'/forgot-password',
			type: 'POST',
			data: data,
			success: function(data){
				if(data.error){
					 $('.forgot2-status').html(data.error).fadeIn().css({'color':'red'});
		             return false;
				}
				$('.forgot2-status').html('Password changed successfully, redirecting now..').fadeIn().css({'color':'green'});
				setTimeout(function(){
					window.location.href='//sign-in';
				},4200);
			},
			error: function(jqXhr){
				if(jqXhr.status == 422){
		           var errors = jqXhr.responseJSON;
		           $.each(errors,function(k,v){
		                $('.forgot2-status').html(v[0]).fadeIn().css({'color':'red'});
		                return false;
		           });
		          return false;
		        }
			}
		});
		return false;
	});
</script>
@endsection