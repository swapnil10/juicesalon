@extends('partials.template')
@section('title','Get The Look')
@section('meta')
@endsection
@section('style')
@endsection
@section('main')
  <main class="main-section page-main-area" id="get_the_look_page">
     <!-- GET THE LOOK Header -->
     <header class="section-header look-page-header" style="background-color: #ffff3d; position: relative; padding: 0;">

         <div class="container" style="position: relative; padding-bottom: 1em; padding-top: 3em;">

             <!-- decoration -->
             <div id="decor_xG1" class="decorate dots" style="position: absolute; right: 20px; bottom: -16.5px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                     <img src="img/decorations/dots-horizontal_120px.png" width="84">
             </div>
             <!-- decoration end -->

             <div class="row">

               <div class="col-md-5 col-xs-12" >
                 <div class="page-description-header">
                   <div class="media">
                     <h4>Get the Look</h4>
                     <div class="media-left">
                         <img class="media-object" style="padding: 10px" src="img/icons/scissors.svg" alt="Scissors Cutting Hair" width="72">
                     </div>
                     <div class="media-body" id="get-look-header">
                       
                     </div>
                   </div>

                   <p class="font-2" style="margin-top: 20px;"><b>Here are our top picks for the week</b></p>
                 </div>
               </div>

               <div class="col-md-7 col-xs-12">
                 <div class="row weekly-top">
                 </div>
               </div>
             </div>
         </div>
     </header>

     <!-- Image Cards Row Section 1 -->
     <section class="section-main image-cards-container" id="getTheLook_imageCards1" >
         <div class="container">

            <!-- decoration -->
            <div id="decor_xG2" class="decorate swirl" style="position: absolute; z-index:1; bottom: -9.5px; right: 20px; user-select: none;-webkit-user-drag: none; ">
                    <img src="img/decorations/swirl-horizontal_120px.png" width="84">
            </div>
            <!-- decoration end -->

             <div class="row image-cards-row level-one">

             </div> 
         </div> 
     </section>

     <!-- Image Cards Row Section 2 -->
     <section class="section-main image-cards-container" id="getTheLook_imageCards2">

         <div class="faux-bg visible-md visible-lg" style="position: absolute;top: 0;bottom: 0;left: 0;width: 30%;background: #000;"></div>

        <div class="container" style="position: relative;">

             <!-- decoration -->
             <div id="decor_xG3" class="decorate dots visible-md visible-lg" style="position: absolute; z-index:1; top:50%; right: -10px; margin-top: -42px; user-select: none;-webkit-user-drag: none; ">
                     <img src="img/decorations/dots_50px.png" width="35">
             </div>
             <!-- decoration end -->

            <div class="row image-cards-row">
                 <div class="col-md-4 col-xs-12 col-sm-12" id="imgCards2_text_col">
                   <div class="inner">
                     <div class="col-text-section" id="dark-header"> 
                     </div>
                   </div>
                 </div>

                <div class="col-md-8 col-xs-12 col-sm-12">
                  <div class="inner">
                      <div class="row image-cards--style-2 level-two">
                      </div>
                  </div>
                </div>
            </div>
        </div>

      </section>


     <!-- Image Cards Row Section 3 -->
     <section class="section-main image-cards-container" id="getTheLook_imageCards3">
         <div class="container">
             <div class="row image-cards-row level-three">
               
             </div> 
         </div> 
     </section>
    </div>

    <div class="weekly-top-template" style="display: none;">
      <div class="animated fadeIn image-card none col-md-4 col-xs-6 text-center">
        <div class="inner-container">
          <img style="margin: 0 auto;" width="150" height="180" alt='juice look' class="img-responsive">
          <p class="caption-image-card text-left" style="max-width: 165px; margin: 0 auto;"></p>
        </div>  
      </div>
    </div>

    <div class="level-one-template" style="display: none;">
      <div class="col-md-3  col-sm-6 col-xs-12 image-card-col">
        <div class="image-card yellow yellow text-center">
          <div class="inner-container">
              <img class="img-responsive" width="200">
              <p class="caption-image-card"><span class='look-name'>Hair on Shoulder Cut</span></p>
          </div>    
        </div>
      </div>
    </div>

    <div class="level-two-template" style="display: none;">
      <div class="col-md-4 col-xs-12 col-sm-6 image-card-col">
        <div class="image-card--style-2" tabindex="0">
          <div class="hover-text-content"><p class="text-center"></p></div>
          <img class="image-card-img img-responsive" alt="Juice Look">
        </div>
      </div>
    </div>

    <div class="level-three-template" style="display: none;">
      <div class="col-md-3  col-sm-6 col-xs-12 image-card-col">
        <div class="image-card text-center">
          <div class="inner-container">
              <img class="img-responsive" width="200">
              <p class="caption-image-card"><span class="look-name-third"></span></p>
          </div>    
        </div>
      </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function(){
    $.get('api/content/get-look',function(data){
      $("#get-look-header").html(data.getLook);
      $("#dark-header").html(data.darkHeader);
    });
  });

  $.get('admin/api/look',function(data){
    var k=0;
    //Level 0
    for(i=0;i<3;i++){
      var template = $('.weekly-top-template').find('.image-card').clone();
      template.find('img').attr('src','/bulkimage/'+data[i].imageId);
      template.find('p').html(data[i].title);
      $('.weekly-top').append(template);
    }

    //Level1 
    for(i=3;i<7;i++){
      var template = $('.level-one-template').find('.image-card-col').clone();
      template.find('img').attr('src','/bulkimage/'+data[i].imageId);
      template.find('.look-name').html(data[i].title);
      $('.level-one').append(template);
    }

    //Level2 
    for(i=7;i<10;i++){
      var template = $('.level-two-template').find('.image-card-col').clone();
      template.find('img').attr('src','/bulkimage/'+data[i].imageId);
      template.find('p').html(data[i].title);
      $('.level-two').append(template);
    }

    //Level3
    for(i=10;i<14;i++){
      var template = $('.level-three-template').find('.image-card-col').clone();
      template.find('img').attr('src','/bulkimage/'+data[i].imageId);
      template.find('.look-name-third').html(data[i].title);
      $('.level-three').append(template);
    }

    console.log(data);
  });
</script>
@endsection
