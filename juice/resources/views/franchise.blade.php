@extends('partials.template')

@section('title','Franchise')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')



  <main class="main-section page-main-area" id="franchise_page">
     <!-- Section 1: Why Juice -->    
     <section class="section-main franchise-why-juice" id="section_why_juice">
       <div class="container" style="position: relative;">
         <!-- decoration -->
          <div class="decorate swirl" style="position: absolute; right: 20px; top: -4em;user-select: none;-webkit-user-drag: none; ">
                  <img src="img/decorations/swirl-horizontal_120px.png" width="100">
          </div>
          <!-- decoration end -->

         <div class="row">
           <div class="col-md-5">
             <div class="why-juice-text franchise-text-col">
               <h2><b>Why Juice?</b></h2>
               <br>
               <p id="why-us"></p>
             </div>
           </div>

           <div class="col-md-1 visible-md visible-lg"></div>

           <div class="col-md-5">
             <div class="image-container">
               <img id="franchisee-image" class="img-responsive" alt="Juice Salon photo">
             </div>
           </div>

           <div class="col-md-1 visible-md visible-lg"></div>
         </div>
       </div>
     </section>



     <!-- Section 1: Why Juice -->    
     <section class="section-main franchise-juice-support" id="section_juice_support">
         <div class="container" style="position: relative; padding-bottom: 2em;">

           <!-- decoration -->
           <div id="decor_xG56" class="decorate dots" style="position: absolute; bottom: -71px; left: 20px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                   <img src="img/decorations/dots-horizontal_120px.png" width="84">
           </div>
           <!-- decoration end -->

           <h3>Juice Support</h3>
           <br>

           <div class="row juice-support-links">
               <div class="col-md-7">
                 
                 <div class="col-md-4 support-link-col">
                   <ul class="support-links">
                     <li><a href="#">Site Identification</a></li>
                     <li><a href="#">Rental / Lease Agreement</a></li>
                     <li><a href="#">Product Procurement</a></li>
                   </ul>
                 </div>

                 <div class="col-md-4 support-link-col">
                   <ul class="support-links">
                     <li><a href="#">Site Identification</a></li>
                     <li><a href="#">Rental / Lease Agreement</a></li>
                     <li><a href="#">Product Procurement</a></li>
                   </ul>
                 </div>

                 <div class="col-md-4 support-link-col">
                   <ul class="support-links">
                     <li><a href="#">Site Identification</a></li>
                     <li><a href="#">Rental / Lease Agreement</a></li>
                     <li><a href="#">Product Procurement</a></li>
                   </ul>
                 </div>

               </div>
           </div>

         </div>
     </section>


     <!-- Section 3: Criteria -->    
     <section class="section-main franchise-criteria" id="section_criteria">
       <div class="container" style="position: relative; padding-top: 2em;">

         <div class="row">
           <div class="col-md-5">
             <div class="criteria-text franchise-text-col">
               <h2><b>Criteria</b></h2>
               <br>
               <p id="criteria"></p>
             </div>
           </div>
         </div>
       </div>
     </section>


     <!-- Franchise Section 3 - Enquiry Form -->
     <section class="section-main franchise-enquiry" id="section_enquiry_form">
       <div class="container">
         <h3>Enquiry Form</h3>

         <div class="enquiry-form-box">
             <form class="inner enquiry-form-box-inner form-general" id='franchise-form' style="width: 80%; margin: 0 auto;">
               <div class="row form-row">

                   <!-- 1st form column -->
                   <div class="form-col col-md-6">

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="full_name">Full Name</label></p>
                         <input type="text" id="full_name" name="name" placeholder="Your full name.." required="true">
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="email_id">Email ID</label></p>
                         <input type="email" id="email_id" name="email" placeholder="Your email id.." required="true">
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="contact_number">Contact number</label></p>
                         <input type="tel" id="contact_number" name="number" placeholder="Contact number" required="true">
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="select_city">City interested</label></p>
                         <select id="select_city" name="city">
                           <option value="0">Select City</option>
                           <option value="Mumbai">Mumbai</option>
                           <option value="New Delhi">New Delhi</option>
                           <option value="Hyderabad">Hyderabad</option>
                           <option value="Banglore">Bangalore</option>
                         </select>
                       </div>
                     </div>

                   </div>

                   <!-- 2nd Form column -->
                   <div class="form-col col-md-6">

                     <div class="form-col-item">
                       <div class="input-block">
                         <p><label for="ownPlace">Do you own your space suitable for Salon?</label></p>
                         
                         <div class="radio-option-group">
                           <div class="radio-item">
                             <input type="radio" name="ownPlace" value="1" checked id="own_place_YES">
                             <label for="own_place_YES">Yes</label>
                           </div>

                           <div class="radio-item">
                             <input type="radio" name="ownPlace" value="0" id="own_place_NO">
                             <label for="own_place_NO">No</label>
                           </div>
                         </div>
                         
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p>
                           <label for="space_size">What is the size of the space? (in Sq. ft.)</label>
                         </p>
                         <input type="text" id="space_size" name="size" placeholder="Enter size of the space.." required="true">
                       </div>
                     </div>

                     <div class="form-col-item">
                       <div class="input-block">
                         <p>
                           <label for="found_salon">How did you come to know about this salon?</label>
                         </p>
                         <input type="text" id="found_salon" name="foundSalon" placeholder="A short description.." required="true">
                       </div>
                     </div>


                     <div class="form-col-item">
                       <div class="input-block">
                         <p>
                           <label for="comment_message">Comment / Message</label>
                         </p>
                         <textarea  id="comment_message" name="message" class="comment-message-box" required="true"></textarea>
                       </div>
                     </div>
                     
                   </div>
                   <b><p class="status"></p></b>
               </div>
             </form>
         </div>
         <div class="submit-btn-box text-center">
           <button class="links-btn btn-shadow" name="enquiryForm" id="submit_enquiry" style="width: 200px;">Submit</button>
         </div>

       </div>
     </section>
  </main>

@endsection


@section('script')
    <script type="text/javascript" src="js/franchise.js"></script>
@endsection

