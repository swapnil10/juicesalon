@if(Auth::check())
<script type="text/javascript">
	location.href="{{url('/')}}";
</script> 
@endif
@extends('partials.template')

@section('title','Sign In')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')
<main class="main-section page-main-area" id="log_in_page" style="overflow-x: hidden;">
	
	<!-- Log-in / Sign-up Page Header -->
	<header class="sign-page-header">

		<!-- Image stripe -->
		<div class="bg-image-stripe parallax-bg-image" style="background-image: url('images/salon_3.JPG'); height: 180px;">
			<div class="bg-stretch"></div>
		</div>

	</header>



	<!-- Log-in Section -->
	<section class="section-main log-in-section" id="log_in_section">
		<div class="container">

			<div class="row">
				
				<div class="col-md-3 hidden-sm hidden-xs"></div>

				<div class="col-md-6 col-sm-12 col-xs-12 ">
					<!-- LOG-IN FORM -->
					<h3><b>Sign In</b></h3>

					<div class="form-container-box font-2">
						<div class="inner">
							<form class="form-general sign-in-form register-form user-form" id="signInForm" name="signInForm">
								<div class="form-fields-box">
									<div class="form-row">
										<label for="user_email_id">Mobile Number*</label><br>
										<input type="text" name="phone" id="phone" placeholder="Your Mobile Number" required />
									</div>
									<div class="form-row">
										<label for="user_password">Password*</label><br>
										<input type="password" name="password" placeholder="Your Password" id="user_password" required />
									</div>
									<div class="form-row text-center">
										<p class="no-account form-text"><b>Don't have an account? <a href="{{url('sign-up')}}" id="sign_up_link">Sign up</a></b></p>
									</div>
									<div class="form-row text-center">
										<p class="no-account form-text"><b>Forgot Password? <a href="{{url('forgot-password')}}" id="sign_up_link">Click here</a></b></p>
									</div>
									<p class="login-status"></p>
								</div>

								<div class="form-outer-field">
									<input class="form-btn" type="submit" name="signInFormBtn" value="Login" />
								</div>
							</form>
						</div>
					</div>

				</div>

				<div class="col-md-3 hidden-sm hidden-xs"></div>

			</div>
		</div>
	</section>

</main> <!-- # log page main end -->


@endsection

@section('script')
<script type="text/javascript">
	var url1 = "{{url('/api/auth')}}";
	$(document).on('submit','#signInForm',function(){
		$('.login-status').fadeOut();
		var data = $('#signInForm').serializeArray();
		$.ajax({
			url: url1+'/login',
			type: 'POST',
			data: data,
			success: function(data){
				if(data.error){
					 $('.login-status').html(data.error).fadeIn().css({'color':'red'});
		             return false;
				}
				window.location.href='/';
			},
			error: function(jqXhr){
				if(jqXhr.status == 422){
		           var errors = jqXhr.responseJSON;
		           $.each(errors,function(k,v){
		                $('.login-status').html(v[0]).fadeIn().css({'color':'red'});
		                return false;
		           });
		          return false;
		        }
			}
		});
		return false;
	});
</script>
@endsection