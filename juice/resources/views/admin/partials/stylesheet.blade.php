<link rel="stylesheet" type="text/css" href="https://s.mlcdn.co/animate.css">
<link rel="stylesheet" href="{{ URL::asset('/assets/admin/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('/assets/admin/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ URL::asset('/assets/admin/css/_all-skins.min.css') }}">
  <!-- Pnotify -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/admin/plugins/notify/pnotify.custom.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/admin/plugins/select2/select2.min.css') }}">
  <!-- tagsInput -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/admin/css/taginput.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ URL::asset('/assets/admin/plugins/iCheck/flat/blue.css') }}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ URL::asset('/assets/admin/plugins/morris/morris.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ URL::asset('/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ URL::asset('/assets/admin/plugins/datepicker/datepicker3.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ URL::asset('/assets/admin/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ URL::asset('/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('/assets/admin/plugins/jquery-nice-select-1.1.0/css/nice-select.css') }}">
  @yield('styles')