  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ URL::asset('assets/admin/images/user.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Session::get('adminName') }}</p>
          <p><a href="/admin/logout">Logout</a></p>
        </div>
      </div>
      <!-- search form -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" style="padding-top:15px;">
        <li @if(Request::is('admin')) class="active" @endif>
            <a href="/admin"><i class="fa fa-bar-chart"></i> Dashboard</a>
        </li>

        <li @if(Request::is('admin/type')) class="active" @endif>
          <a href="/admin/type">
            <i class="fa fa-cube"></i> <span>Product Types</span>
          </a>
        </li>

        <li @if(Request::is('admin/category')) class="active" @endif>
          <a href="/admin/category">
            <i class="fa fa-play"></i> <span>Video Categories</span>
          </a>
        </li>

        <li @if(Request::is('admin/coupons')) class="active" @endif>
          <a href="/admin/coupons">
            <i class="fa fa-gift"></i> <span>Coupons</span>
          </a>
        </li>

        <li @if(Request::is('admin/product')) class="active" @endif>
          <a href="/admin/product">
            <i class="fa fa-barcode"></i> <span>Products</span>
          </a>
        </li>

        <li @if(Request::is('admin/services/*')) class="active treeview" @else class="treeview" @endif>
          <a href="#">
            <i class="fa fa-rocket"></i> <span>Services</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if(Request::is('admin/services/addService')) class="active" @endif><a href="/admin/services/addService"><i class="fa fa-circle-o"></i>Service</a></li>
            <li @if(Request::is('admin/services/addSubService')) class="active" @endif><a href="/admin/services/addSubService"><i class="fa fa-circle-o"></i>Sub Service</a></li>
          </ul>
        </li>


        <li @if(Request::is('admin/shipping')) class="active" @endif>
          <a href="/admin/shipping">
            <i class="fa fa-truck"></i> <span>Shipping</span>
          </a>
        </li>

        <li @if(Request::is('admin/users')) class="active" @endif>
          <a href="/admin/users">
            <i class="fa fa-users"></i> <span>Users</span>
          </a>
        </li>

        <li @if(Request::is('admin/orders')) class="active" @endif>
          <a href="/admin/orders">
            <i class="fa fa-shopping-bag"></i> <span>Orders</span>
          </a>
        </li>

        <li @if(Request::is('admin/franchise/*')) class="active treeview" @else class="treeview" @endif>
          <a href="#">
            <i class="fa fa-building-o"></i> <span>Franchise</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if(Request::is('admin/franchise/view')) class="active" @endif><a href="/admin/franchise/view"><i class="fa fa-circle-o"></i>View Enquiries</a></li>
            <li @if(Request::is('admin/franchise/upload')) class="active" @endif><a href="/admin/franchise/upload"><i class="fa fa-circle-o"></i>Upload Materials</a></li>
          </ul>
        </li> 

        <!-- <li @if(Request::is('admin/franchise/view')) class="active" @endif>
          <a href="/admin/franchise/view">
            <i class="fa fa-building-o"></i> <span>Franchise Enquiries</span>
          </a>
        </li> -->

        <li @if(Request::is('admin/location')) class="active" @endif>
          <a href="/admin/location">
            <i class="fa fa-map-marker"></i> <span>Locations</span>
          </a>
        </li>


        <li @if(Request::is('admin/website/*')) class="active treeview" @else class="treeview" @endif>
          <a href="#">
            <i class="fa fa-tv"></i> <span>Sliders & Images</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if(Request::is('admin/website/service-page-slider')) class="active" @endif><a href="/admin/website/service-page-slider"><i class="fa fa-circle-o"></i>Service Sliders</a></li>

            <li @if(Request::is('admin/website/home-page-slider')) class="active" @endif><a href="/admin/website/home-page-slider"><i class="fa fa-circle-o"></i>Home Sliders</a></li>

            <li @if(Request::is('admin/website/product-page-slider')) class="active" @endif><a href="/admin/website/product-page-slider"><i class="fa fa-circle-o"></i>Product Sliders</a></li>

            <li @if(Request::is('admin/website/academy-page-slider')) class="active" @endif><a href="/admin/website/academy-page-slider"><i class="fa fa-circle-o"></i>Academy Sliders</a></li>

            <li @if(Request::is('admin/website/imageupload')) class="active" @endif><a href="/admin/website/imageupload"><i class="fa fa-circle-o"></i>Bulk Image Upload</a></li>

            <li @if(Request::is('admin/website/imageremove')) class="active" @endif><a href="/admin/website/imageremove"><i class="fa fa-circle-o"></i>Delete Images</a></li>
          </ul>
        </li>

        <li @if(Request::is('admin/visual-editor/*')) class="active treeview" @else class="treeview" @endif>
          <a href="#">
            <i class="fa fa-pencil"></i> <span>Visual Editor</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li @if(Request::is('admin/visual-editor/HomePageContent')) class="active" @endif><a href="/admin/visual-editor/HomePageContent"><i class="fa fa-circle-o"></i>Home Page</a></li>
            <li @if(Request::is('admin/visual-editor/franchiseContent')) class="active" @endif><a href="/admin/visual-editor/franchiseContent"><i class="fa fa-circle-o"></i>Franchise Page</a></li>
            <li @if(Request::is('admin/visual-editor/GetTheLookContent')) class="active" @endif><a href="/admin/visual-editor/GetTheLookContent"><i class="fa fa-circle-o"></i>Get The Look Page</a></li>
            <li @if(Request::is('admin/visual-editor/servicesContent')) class="active" @endif><a href="/admin/visual-editor/servicesContent"><i class="fa fa-circle-o"></i>Services Page</a></li>
          </ul>
        </li>



  
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>