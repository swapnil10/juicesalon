@extends('admin.template')
@section('title','Dashboard')
@section('style')
@endsection
@section('content')
<div class="row">

        <div class="col-lg-3 col-xs-6">

          <!-- small box -->
          <a href="//admin/orders">
          <div class="small-box bg-aqua">

            <div class="inner">

              <h3>{{$orders}}</h3>



              <p>Total Orders</p>

            </div>

            <div class="icon">

              <i class="ion ion-bag"></i>

            </div>

            

          </div>
          </a>
        </div>

        <div class="col-lg-3 col-xs-6">

          <!-- small box -->
          <a href="//admin/users">
          <div class="small-box bg-yellow">

            <div class="inner">

              <h3 id="users">{{$user}}</h3>



              <p>Total Registered Users</p>

            </div>

            <div class="icon">

              <i class="ion ion-person-add"></i>

            </div>

            

          </div>
          </a>
        </div>

        <div class="col-lg-3 col-xs-6">

          <!-- small box -->
          <a href="//admin/franchise/view">
          <div class="small-box bg-aqua">

            <div class="inner">

              <h3>{{$franchise}}</h3>



              <p>Franchise Enquiries</p>

            </div>

            <div class="icon">

              <i class="ion ion-ios-lightbulb-outline"></i>

            </div>

            

          </div>
          </a>
        </div>

      </div>
@endsection
@section('script')
@endsection