@if (Auth::guard('admin')->check()) 
      <script type="text/javascript">
        location.href="/admin";
      </script> 
@endif
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Admin | Login Form</title>
  
  
  
      <style type="text/css">
      	* {
box-sizing: border-box;
}

*:focus {
	outline: none;
}
body {
font-family: Arial;
background-color: #3498DB;
padding: 50px;
}
.login {
margin: 20px auto;
width: 300px;
}
.login-screen {
background-color: #FFF;
padding: 20px;
border-radius: 5px
}

.app-title {
text-align: center;
color: #777;
}

.login-form {
text-align: center;
}
.control-group {
margin-bottom: 10px;
}

input {
text-align: center;
background-color: #ECF0F1;
border: 2px solid transparent;
border-radius: 3px;
font-size: 16px;
font-weight: 200;
padding: 10px 0;
width: 250px;
transition: border .5s;
}

input:focus {
border: 2px solid #3498DB;
box-shadow: none;
}

.btn {
  border: 2px solid transparent;
  background: #3498DB;
  color: #ffffff;
  font-size: 16px;
  line-height: 25px;
  padding: 10px 0;
  text-decoration: none;
  text-shadow: none;
  border-radius: 3px;
  box-shadow: none;
  transition: 0.25s;
  display: block;
  width: 250px;
  margin: 0 auto;
}

.btn:hover {
  background-color: #2980B9;
}

.login-link {
  font-size: 12px;
  color: #444;
  display: block;
	margin-top: 12px;
}
      </style>

  
</head>

<body>
  <body>
  <form id='adminlogin'>
  {{ csrf_field() }}
	<div class="login">
		<div class="login-screen">
			<div class="app-title">
				<h1>Login</h1>
			</div>

			<div class="login-form">
				<div class="control-group">
				<input type="email" name='email' class="login-field" placeholder="username" id="login-name">
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>

				<div class="control-group">
				<input type="password" name='password' class="login-field"  placeholder="password" id="login-pass">
				<label class="login-field-icon fui-lock" for="login-pass"></label>
				</div>

				<input type="submit" id='submitlogin' class="btn btn-primary btn-large btn-block" value="login" />
				<a class="login-link" href="#">Lost your password?</a>
			</div>
		</div>
	</div>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
@foreach($errors->all() as $messages)
console.log("{{$messages}}");
@endforeach
$(document).on('click','#submitlogin',function(){
  var req = $('#adminlogin').serializeArray();
  $.ajax({
    url:"{{ url('/admin/login') }}",
    type:"POST",
    data :req,
    statusCode:{
      403:function(){
          console.log("Email or Password is incorrect");
      },
      200:function(){
        location.href="/admin";
      }
  },
    error:function( jqXhr ) {
        console.log(jqXhr.status);
        if( jqXhr.status === 401 ) //redirect if not authenticated user.
            console.log("401");//$( location ).prop( 'pathname', 'auth/login' );
        if( jqXhr.status === 422 ) {
        //process validation errors here.
        var errors = jqXhr.responseJSON; //this will get the errors response data.
        //show them somewhere in the markup
        //e.g

        //errorsHtml = '<div class="alert alert-danger"><ul>';

        $.each( errors , function( key, value ) {
          console.log(value[0]);
           // errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
        });
        //errorsHtml += '</ul></di>';
            
        //$( '#form-errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
        } else {
            /// do some thing else
            console.log("else");
        }
    }
  });
  return false;
});
</script>
</body>
  
  
</body>
</html>
