@extends('admin.template')
@section('title',' Users')
@section('content')
@section('styles')<!-- DataTables -->
  <link rel="stylesheet" href="/assets/admin/plugins/datatables/dataTables.bootstrap.css">
@endsection

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Registered Users</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="user-table" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th># (id)</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Join Date</th>
                      <th>View</th>
                      <th>Is Franchisee?</th>
                    </tr>
                  </thead>
                  <tbody id="users-tbody"></tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>

      <div id="address-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">User Detail</h4>
            </div>
            <div class="modal-body">
              <table class="table">
                <thead>
                  <tr>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Pin Code</th>
                  </tr>
                </thead>
                <tbody id="tbody-address">
                  <tr>
                    <td id="address"></td>
                    <td id="city"></td>
                    <td id="state"></td>
                    <td id="pinCode"></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
      <!-- /.content -->
<div id="templates" style="display: none;">
  <table>
    <tbody>
      <tr id="user-tr-template">
        <td id="id"></td>
        <td id="name"></td>
        <td id="email"></td>
        <td id="phone"></td>
        <td id="join-date"></td>
        <td><button class="view btn btn-primary">View</button></td>
        <td id="is_franchisee"></td>
      </tr>
    </tbody>
  </table>
</div>
@endsection
@section('script')
<script src="/assets/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

  init();
  
  function init(offset=0){
    $.get(api+'/users?offset='+offset,function(data){
      if(data.error)
        return error(data.error);

      if(!data.length)
        return false;

      $.each(data,function(i,v){
        var a = $("#templates > table > tbody > tr#user-tr-template").clone().appendTo("tbody#users-tbody");
        a.attr("data-user",v.id);
        a.find("#id").html(v.id);
        a.find("#name").html(v.fullName);
        a.find("#email").html(v.email);
        a.find("#phone").html(v.phone);
        a.find("#join-date").html(v.created_at);
        a.find(".view").attr("data-user",v.id);

        if(v.isFranchisee == 1)
          a.find("#is_franchisee").html('Yes <button style="margin-left:10%;" data-user="'+v.id+'" class="btn btn-danger remove_is_franchisee"> Remove </button>');
        else
          a.find("#is_franchisee").html('No <button style="margin-left:10%;" data-user="'+v.id+'" class="btn btn-success convert_is_franchisee "> Convert </button>');
      });
    });
  }

  $(document).on('click','.btn.view',function(){
    var id = $(this).attr('data-user');
    var ths = $(this);
    ths.attr('disabled',true);
    ths.html('Processing...');
    $.get(api+'/users/'+id+'/address',function(data){
      console.log(data);
      if(data.error)
        return error(data.error);

      var a = $("tbody#tbody-address > tr");
      a.find("#address").html("").html(data.address);
      a.find("#city").html("").html(data.city);
      a.find("#pinCode").html("").html(data.pinCode);
      a.find("#state").html("").html(data.state);
      $("#address-modal").modal('show');
    }).complete(function(){
      ths.removeAttr('disabled');
      ths.html('View');
    });
    return false;
  });

  $(document).on('click',"button.userEnable",function(){
    var ths = $(this);
    var id = ths.attr("data-id");
    $.get(apiadd+"/userList/"+id+'/enable',function(data){
      if(data.error){
        new PNotify({
          'title':'Something went wrong.',
          'text':data.error,
          'type':'error'
        });
        return false;
      }
      if(data)
        enabledF(id,true);
      else
        enabledF(id,false);
    });

    return false;
  });

  function enabledF(id,flag){
    var thi = $("button.userEnable[data-id='"+id+"']");
    thi.removeClass("btn-success");
    thi.removeClass("btn-danger");
    if(flag){
      thi.addClass("btn-success");
      thi.html("<i class='fa fa-unlock'></i>&nbsp;&nbsp;&nbsp;Block");
    }
    else{
      thi.addClass("btn-danger");
      thi.html("<i class='fa fa-lock'></i>&nbsp;&nbsp;&nbsp;Unblock");
    }
  }

  $(document).on('click',"button.convert_is_franchisee",function(){
    var id = $(this).attr("data-user");
    var ths = $(this);
    $.post(api+'/users/'+id+'/is-franchisee',{status:1},function(data){
      if(data.error)
        return error(data.error);
      ths.parent("#is_franchisee").html('Yes <button style="margin-left:10%;" data-user="'+id+'" class="btn btn-danger remove_is_franchisee "> Remove </button>');
    });
  });

  $(document).on('click',"button.remove_is_franchisee",function(){
    var id = $(this).attr("data-user");
    var ths = $(this);
    $.post(api+'/users/'+id+'/is-franchisee',{status:0},function(data){
      if(data.error)
        return error(data.error);
      ths.parent("#is_franchisee").html('No <button style="margin-left:10%;" data-user="'+id+'" class="btn btn-success convert_is_franchisee "> Convert </button>');
    });
  });
</script>

@endsection
  <!-- ./wrapper -->
