@extends('admin.template')
@section('title','Add Category')
@section('styles')
<style type="text/css">
    select.variant{
      margin:10px 0;
    }
    .variant-div{
      margin-bottom:15px;
    }
    ol.dd-list {
      list-style-type: none;
    }
    .dd-item .dd-handle {
      position: relative;
      background-color: #fff;
      margin: 10px 0;
      padding: 10px;
      font-size: 1.6rem;
      font-weight: bold;
      color: #656565;
      border-radius: 3px;
      border-top: 3px solid #3C8DBC;
      box-shadow: 0 1px 1px rgba(0,0,0,0.1);
      cursor: all-scroll;
    }
    .dd-item .dd-delete{
      position: absolute;
      right: 0;
    }
    .dd-item .dd-delete:after{
      content: '\f00d';
      color: #dd4b39;
      position: absolute;
      right: 29px;
      top: -40px;
      font-family: FontAwesome;
      cursor: pointer;
    }
    .dd-item .dd-edit{
      position: absolute;
      right: 25px;
    }
    .dd-item .dd-edit:after{
      content: '\f044';
      color: #008000;
      position: absolute;
      right: 29px;
      top: -40px;
      font-family: FontAwesome;
      cursor: pointer;
    }
    .dd-item > button{
      display: none;
    }
    .dd-placeholder {
      border: 2px solid #00a65a;
      border-style: dashed;
      background-color:rgba(255,255,255,0.5);
      border-radius:3px;
    }
    .dd-dragel { 
      position: absolute; 
      pointer-events: none; 
      z-index: 9999; }
    .dd-empty { 
      margin: 5px 0; 
      padding: 0; 
      min-height: 30px; 
      background: #f2fbff; 
      border: 1px dashed #b6bcbf; 
      box-sizing: border-box; 
      -moz-box-sizing: border-box; 
    }
    .dd-empty { 
      border: 1px dashed #bbb; 
      min-height: 100px; 
      background-color: #e5e5e5;
      background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                        -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
      background-image: -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                        -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
      background-image: linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                        linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
      background-size: 60px 60px;
      background-position: 0 0, 30px 30px;
    }
    .modal{
      display:none;
    }
  </style>
@endsection
@section('headline','Category')
@section('content')
<div class="row">
<!-- left column -->
  <div class="col-md-8">
    <!-- general form elements -->
    <div class="box box-success">
      <!-- /.box-header -->
      <!-- form start -->
      <form id="category-add">
        <div class="box-body">
          <div class="col-xs-12 input-group form-group" style="margin-bottom:0px;">
            <input type="text" class="form-control form-data" name="name" id="name" placeholder="new category name" autocomplete="false" style="border:0;">
            <span class="input-group-btn">
              <input type="submit" class="btn bg-olive btn-flat" id="add" value=" Go! " />
            </span>
          </div>
        </div>
      </form>
    </div>
    <!-- /.box -->


  </div>
  <!--/.col (left) -->

</div>
<!-- /.row -->
<div class="row">
  <div class="col-md-8 dd">
    <ol class="dd-list category-list">
    </ol>
  </div>
</div>
@endsection
@section('section')
<!-- <section class="content">
      <div class="row">
        <div class="col-md-8 dd">
          <ol class="dd-list category-list">
          </ol>
        </div>
      </div>
      
    </section> -->
    <!-- /.content -->

<section class="content">
  <div class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Update Category name</h4>
        </div>
        <form id="update-category">
          <div class="modal-body">
                <input type="text" class="form-control input-lg" name="catName" id="catName" placeholder="Category name?" autocomplete="false">
          </div>
          <div class="modal-footer">
            <input type="button" id="update-btn" class="btn btn-primary update" value='Save changes'/>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</section>
@endsection
@section('script')
<script src="/assets/admin/plugins/jQueryNestable/jquery.nestable.js"></script>
<script src="/assets/admin/js/custom/category.add.js"></script>
@endsection