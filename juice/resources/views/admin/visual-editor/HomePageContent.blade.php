@extends('admin.template')
@section('title','Get The Look Content')
@section('styles')
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-common-extra.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-core.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-reset.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-sidebar.css'>
<link rel='stylesheet' href='/assets/admin/css/css/create-ui/css/create-ui.css'>
<link rel='stylesheet' href='/assets/admin/css/css/midgard-notifications/midgardnotif.css'>

<style>
  @media only screen and (max-width: 1500px) and (min-width : 992px){
      .img-box{padding:10px !important;}
    }
  .brand-image{width: 130px;height: 130px;}
  .dropzone{padding:0;border:none;}
  .dz-preview:not(.dz-processing) .dz-progress{opacity: 0;}
  .error{
    background-color: #d9534f;
    color:#fff;
    padding:5px;
    margin:5px 0;
    font-size: 1.3rem;
    display:none;
  }
  .nice-select{border:0!important;height: 27px;line-height: 10px;}
  [contentEditable=true]:empty:not(:focus):before{
    content:attr(data-text);
    color:#9e9e9e;
  }
  [contentEditable=true]{height:100px;outline: none;}
</style>
@endsection
@section('content')
<div class="row">
         <!-- left column -->
        <!--/.col (left) -->

        <!-- left column -->
        <!-- <div class="col-lg-6 col-md-7 brand-details-section">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Get The Look Page</div>
            </div>
          </div>
          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Get The Look</div>
            </div>
            <div class="box-body pad">
              <textarea class="textarea no-border" form="brand-form" name="description" contenteditable="true" id="unecom-look" placeholder="Content for heading" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
            </div>
          </div>
          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Dark Heading</div>
            </div>
             <div class="box-body pad">
              <textarea class="textarea no-border" form="brand-form" name="criteria" contenteditable="true" id="unecom-description" placeholder="This will go at the dark background" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
            </div>
          </div>
          <div class="col-sm-12 no-padding">
            <button class="btn btn-primary pull-right" id="save-btn">update</button>
          </div>
        </div> -->
          <div class="col-md-12">
            <div class="box box-primary article">
              <div class="box-header with-border">
               Page View of Get the Look
               <center><button class="btn btn-success" id="add-btn">Update</button></center>
              </div>
              <div class="box-body text-center">
                 <iframe width="100%" height="1000px" id="iframe" src="/copy-index"> </iframe>
                 <div class="col-sm-12 no-padding">
              </div>
              </div>
              
            </div>
          </div>
        <!-- /.row -->
         
        
</div>
@endsection
@section('script')
<script type="text/javascript">
</script>

<script type="text/javascript">
var uri = apiadd+'/contents/get-look';
// function resetForm(){
//     $('#unecom-look').data('wysihtml5').editor.setValue('');
//     $('#unecom-description').data('wysihtml5').editor.setValue('');
//     return false;
// }

// $.get(uri,function(data){
//   if(data.error)
//     return error(data.error);
//   $('#unecom-look').data('wysihtml5').editor.setValue(data.getLook);
//   $('#unecom-description').data('wysihtml5').editor.setValue(data.darkHeader);
// });

$('#add-btn').click(function(){
    var src = $('#iframe').contents().find("body").html();
    var leftcontent = $(src).find("#leftcontent").html();
    
    var leftimage = $(src).find("#leftimage").attr("data-lookId");
    var rightheading = $(src).find("#rightheading").html();
    var rightcontent = $(src).find("#rightcontent").html();
    var rightimage = $(src).find("#rightimage").attr("data-lookId");
    var rightheading1 = $(src).find("#rightheading1").html();
    var leftheading = $(src).find("#leftheading").html();
    var leftimage1 = $(src).find("#leftimage1").attr("data-lookId");
    var leftcontent1 = $(src).find("#leftcontent1").html();
    var rightimage1 = $(src).find("#rightimage1").attr("data-lookId");
    var rightcontent1 = $(src).find("#rightcontent1").html();
    var leftheading1 = $(src).find("#leftheading1").html();
    var leftcontent2 = $(src).find("#leftcontent2").html();
    var rightimage2 = $(src).find("#rightimage2").attr("data-lookId");
    var rightimage3 = $(src).find("#rightimage3").attr("data-lookId");
    var rightimage4 = $(src).find("#rightimage4").attr("data-lookId");
    var leftimage5 = $(src).find("#leftimage5").attr("data-lookId");
    var leftcontent3 = $(src).find("#leftcontent3").html();
    var rightimage5 = $(src).find("#rightimage5").attr("data-lookId");
    var rightcontent2 = $(src).find("#rightcontent2").html();
    var leftimage6 = $(src).find("#leftimage6").attr("data-lookId");
    var leftcontent4 = $(src).find("#leftcontent4").html();
    var rightimage6 = $(src).find("#rightimage6").attr("data-lookId");
    var rightcontent3 = $(src).find("#rightcontent3").html();
    var leftcontent5 = $(src).find("#leftcontent5").html();
    var rightimage7 = $(src).find("#rightimage7").attr("data-lookId");
    var rightimage8 = $(src).find("#rightimage8").attr("data-lookId");
    var rightimage9 = $(src).find("#rightimage9").attr("data-lookId");
    var leftheading2 = $(src).find("#leftheading2").html();
    var leftcontent6 = $(src).find("#leftcontent6").html();
    var leftimage7 = $(src).find("#leftimage7").attr("data-lookId");
    var rightheading2 = $(src).find("#rightheading2").html();
    var rightcontent4 = $(src).find("#rightcontent4").html();
    var rightimage10 = $(src).find("#rightimage10").attr("data-lookId");
    var heading1 = $(src).find("#heading1").html();
    var heading2 = $(src).find("#heading2").html();
    var heading3 = $(src).find("#heading3").html();

    var data = {leftcontent:leftcontent,leftimage:leftimage,rightheading:rightheading,rightcontent:rightcontent,rightimage:rightimage,rightheading1:rightheading1,leftheading:leftheading,leftimage1:leftimage1,leftcontent1:leftcontent1,rightimage1:rightimage1,rightcontent1:rightcontent1,leftheading1:leftheading1,leftcontent2:leftcontent2,rightimage2:rightimage2,rightimage3:rightimage3,rightimage4:rightimage4,leftimage5:leftimage5,leftcontent3:leftcontent3,rightimage5:rightimage5,rightcontent2:rightcontent2,leftimage6:leftimage6,leftcontent4:leftcontent4,rightheading2:rightheading2,rightimage6:rightimage6,rightcontent3:rightcontent3,leftcontent5:leftcontent5,rightimage7:rightimage7,rightimage8:rightimage8,rightimage9:rightimage9,leftheading2:leftheading2,leftcontent6:leftcontent6,leftimage7:leftimage7,rightcontent4:rightcontent4,rightimage10:rightimage10,heading1:heading1,heading2:heading2,heading3:heading3};
    console.log(data);
  $.ajax({
        url:'/api/homepage',
        type : 'PATCH',
        data : data,
        success : function(data){
          console.log(data);
          if(data.error){
          new PNotify({
              title: 'Something went wrong',
              text:  data.error,
              type: 'error',
          });
          return false;
        }
        new PNotify({
          title:'Content Updated',
          text:'Content Update Success',
          type:'success'
        });
        }
      });
    });
</script>
@endsection