@extends('admin.template')
@section('title','Services Content')
@section('styles')

@endsection
@section('content')
<div class="row">
         <!-- left column -->
        <!--/.col (left) -->

        <!-- left column -->
        <!-- <div class="col-lg-6 col-md-7 brand-details-section">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Services Page</div>
            </div>
          </div>
          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Header</div>
            </div>
            <div class="box-body pad">
              <div class="box-body image-desc"  name="" id=""  data-text=""></div>
              <textarea class="textarea no-border" form="brand-form" name="description" contenteditable="true" id="unecom-service" placeholder="Content for header" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
            </div>
          </div>
          <div class="col-sm-12 no-padding">
            <button class="btn btn-primary pull-right" id="save-btn">update</button>
          </div>
        </div> -->
        <div class="col-md-12">
            <div class="box box-primary">
              <div class="fallback">
                
              </div>
              <div class="box-header with-border">
               Page View of Services
                <center><button class="btn btn-success" id="save-btn">Update</button></center>
              </div>
              <div class="box-body text-center">
                <iframe width="100%" height="1000px" id="iframe" src="/copy-services">
                </iframe>
              </div>
            </div>
          </div>
        <!-- /.row -->
         
        
</div>
@endsection
@section('script')
<script type="text/javascript">
var uri = apiadd+'/contents/service-page-header-content';
// $.get(uri,function(data){
//   $('#unecom-service').data('wysihtml5').editor.setValue(data.header);
// });

$('#save-btn').click(function(){
  var formData = {};
  var src = $('#iframe').contents().find("body").html();
  var service = $(src).find("#header-content").html();
  if(!service.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }

  formData['service'] = service;
  console.log(formData);
  
  $.ajax({
    url:uri,
    type:'PATCH',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.success){
        var notice = new PNotify({
        title: 'success',
        text: 'Content Uploaded Successfully',
        type: 'success',
      });
        return false;
      }
      if(data.error)
        return error(data.error);
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          $.each( errors , function( key, value ) {
            return error(value[0]);
          });
          return false;
        }
        return error("");
    }
  });
});
</script>
@endsection