@extends('admin.template')
@section('title','Get The Look Content')
@section('styles')
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-common-extra.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-core.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-reset.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-sidebar.css'>
<link rel='stylesheet' href='/assets/admin/css/css/create-ui/css/create-ui.css'>
<link rel='stylesheet' href='/assets/admin/css/css/midgard-notifications/midgardnotif.css'>

<style>
  @media only screen and (max-width: 1500px) and (min-width : 992px){
      .img-box{padding:10px !important;}
    }
  .brand-image{width: 130px;height: 130px;}
  .dropzone{padding:0;border:none;}
  .dz-preview:not(.dz-processing) .dz-progress{opacity: 0;}
  .error{
    background-color: #d9534f;
    color:#fff;
    padding:5px;
    margin:5px 0;
    font-size: 1.3rem;
    display:none;
  }
  .nice-select{border:0!important;height: 27px;line-height: 10px;}
  [contentEditable=true]:empty:not(:focus):before{
    content:attr(data-text);
    color:#9e9e9e;
  }
  [contentEditable=true]{height:100px;outline: none;}
</style>
@endsection
@section('content')
<div class="row">
         <!-- left column -->
        <!--/.col (left) -->

        <!-- left column -->
        <!-- <div class="col-lg-6 col-md-7 brand-details-section">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Get The Look Page</div>
            </div>
          </div>
          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Get The Look</div>
            </div>
            <div class="box-body pad">
              <textarea class="textarea no-border" form="brand-form" name="description" contenteditable="true" id="unecom-look" placeholder="Content for heading" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
            </div>
          </div>
          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Dark Heading</div>
            </div>
             <div class="box-body pad">
              <textarea class="textarea no-border" form="brand-form" name="criteria" contenteditable="true" id="unecom-description" placeholder="This will go at the dark background" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
            </div>
          </div>
          <div class="col-sm-12 no-padding">
            <button class="btn btn-primary pull-right" id="save-btn">update</button>
          </div>
        </div> -->
          <div class="col-md-12">
            <div class="box box-primary article">
              <div class="box-header with-border">
               Page View of Get the Look
               <center><button class="btn btn-success" id="add-btn">Update</button></center>
              </div>
              <div class="box-body text-center">
                 <iframe width="100%" height="1000px" id="iframe" src="/copy-get-the-look"> </iframe>
                 <div class="col-sm-12 no-padding">
              </div>
              </div>
              
            </div>
          </div>
        <!-- /.row -->
         
        
</div>
@endsection
@section('script')
<script type="text/javascript">
</script>

<script type="text/javascript">
var uri = apiadd+'/contents/get-look';
// function resetForm(){
//     $('#unecom-look').data('wysihtml5').editor.setValue('');
//     $('#unecom-description').data('wysihtml5').editor.setValue('');
//     return false;
// }

// $.get(uri,function(data){
//   if(data.error)
//     return error(data.error);
//   $('#unecom-look').data('wysihtml5').editor.setValue(data.getLook);
//   $('#unecom-description').data('wysihtml5').editor.setValue(data.darkHeader);
// });

$('#add-btn').click(function(){

  var formData = {};
  var src = $('#iframe').contents().find("body").html();
  var name = $(src).find("#get-look-header").html();
  var description = $(src).find("#dark-header").html();
  var lookTitles = [];
  $(src).find('.lookTitle').each(function(){
    var lookId = $(this).attr('data-lookId');
    var title = $(this).text();
    lookTitles.push({title:title,id:lookId});
  });
  var imageIds = [];
  $(src).find('.lookImage').each(function(){
    var lookId = $(this).attr('data-lookId');
    var imageId = $(this).attr('data-imageId');
    imageIds.push({imageId:imageId,id:lookId});
  });

  $.ajax({
    url:'/admin/api/look',
    type : 'PATCH',
    data : {titles:lookTitles,imageIds:imageIds},
    success : function(data){
      console.log(data);
    }
  });
  if(!name.length || !description.length){
    var notice = new PNotify({
        title: 'Empty Fields',
        text: 'All fields are required',
        type: 'error',
    });
    return false;
  }

  formData['name'] = name;
  formData['description'] =description;
  console.log(formData);
  $.ajax({
    url:uri,
    type:'PATCH',
    data:formData,
    success:function(data){
      console.log(data);
      if(data.success){
        var notice = new PNotify({
        title: 'success',
        text: 'Content Uploaded Successfully',
        type: 'success',
      });
        return false;
      }
      if(data.error)
        return error(data.error);
    },
    error:function(jqXhr){
      if(jqXhr.status == 422){
          var errors = jqXhr.responseJSON;
          $.each( errors , function( key, value ) {
            return error(value[0]);
          });
          return false;
        }
        return error("");
    }
  });
});
</script>
@endsection