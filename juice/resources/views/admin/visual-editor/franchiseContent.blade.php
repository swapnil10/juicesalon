@extends('admin.template')
@section('title','Franchise Content')
@section('styles')
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-common-extra.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-core.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-reset.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-sidebar.css'>
<link rel='stylesheet' href='/assets/admin/css/css/create-ui/css/create-ui.css'>
<link rel='stylesheet' href='/assets/admin/css/css/midgard-notifications/midgardnotif.css'>

<style>
  @media only screen and (max-width: 1500px) and (min-width : 992px){
      .img-box{padding:10px !important;}
    }
  .brand-image{width: 130px;height: 130px;}
  .dropzone{padding:0;border:none;}
  .dz-preview:not(.dz-processing) .dz-progress{opacity: 0;}
  .error{
    background-color: #d9534f;
    color:#fff;
    padding:5px;
    margin:5px 0;
    font-size: 1.3rem;
    display:none;
  }
  .nice-select{border:0!important;height: 27px;line-height: 10px;}
  [contentEditable=true]:empty:not(:focus):before{
    content:attr(data-text);
    color:#9e9e9e;
  }
  [contentEditable=true]{height:100px;outline: none;}
</style>
@endsection
@section('content')
<div class="row">


          <div class="col-md-12">
            <div class="box box-primary article">
              <div class="box-header with-border">
               Page View of Franchise
               <center><button class="btn btn-success" id="add-btn">Update</button></center>
              </div>
              <div class="box-body text-center">
                 <iframe width="100%" height="1000px" id="iframe" src="/copy-franchise"> </iframe>
              </div>
              
            </div>
          </div>
        <!-- /.row -->
         <!-- Variants -->
        <div class="unecom-template" style="display:none;">
          <ul class="unecom-brands">
            <li class="brand-item" id="brand-list-template">
              <a href="javascript:void(0)"><i class="fa fa-gift"></i><span class="brand"></span></a>
            </li>
          </ul>
        </div>
        
</div>

<div style="display: none;">
  <div id="dz-preview-template">
        <div class="dz-preview dz-file-preview">
          <div class="dz-image">
            <img data-dz-thumbnail="">
          </div>
          <div class="dz-details">
            <div class="dz-size text-center" data-dz-size></div>
            <div class="dz-filename text-center"><span data-dz-name></span></div>
          </div>
          <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
          <div class="dz-success-mark">    
            <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Check</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>      </g>    </svg>  
          </div>
          <div class="dz-error-mark">    
            <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Error</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>        </g>      </g>    </svg>  
          </div>
          <div class="dz-error-message"><span data-dz-errormessage></span></div>
        </div>
      </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
  $('select').select2();
</script>
<script src='/assets/admin/plugins/dropzone/dropzone.js'></script>
<!-- <script src="assets/admin/js/custom/content.js"></script> -->
<script type="text/javascript">
  
    $("#add-btn").click(function(){
      var src = $('#iframe').contents().find("body").html();
      var whydes = $(src).find("#why-us").html();
      var whycri = $(src).find("#criteria").html();
      var image = $(src).find("#franchisee-image").attr("data-id");
      if(!whydes.length || !whycri.length){
        var notice = new PNotify({
            title: 'Empty Fields',
            text: 'All fields are required',
            type: 'error',
        });
        return false;
      }
      var data = {whydes:whydes,whycri:whycri,image:image};
      console.log(data);
      $.ajax({
        url:apiadd+'/contents/franchise',
        type : 'PATCH',
        data : {whydes:whydes,whycri:whycri,image:image},
        success : function(data){
          console.log(data);
          if(data.error){
          new PNotify({
              title: 'Something went wrong',
              text:  data.error,
              type: 'error',
          });
          return false;
        }
        new PNotify({
          title:'Content Updated',
          text:'Content Update Success',
          type:'success'
        });
        }
      });
    });
    // var src = $('#iframe').contents().find("body").html();
    // var why = $(src).find("#why-us").html();
    // var criteria = $(src).find("#criteria").html();
    
  
    
  
</script>
@endsection