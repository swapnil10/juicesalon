@extends('admin.template')
@section('title',' Shipping Amount')
@section('content')
<div class="row">
        <!-- left column -->
        <div class="col-md-offset-3 col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Update</h3>
            </div>
          </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" name="shipping" id="shipping">
              <div class="box box-solid">
                  <div class="box-header with-border">
                    <div class="col-md-12 box-title no-padding">Shipping Amount
                    </div>
                  </div>
                  <div class="box-body">
                    <input type="number" class="form-control no-border" id="amount" name="amount" placeholder="Shipping Amount">
                  </div>
              </div>
              <div class="box box-solid">
                <button class="btn btn-primary pull-right" id="save-btn">Save</button>
              </div>
            </form>
        </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
  
  var amount = $("form#shipping").find("input#amount");
  $.get(api+'/shipping',function(data){
    if(data.error)
      error(data.error);
    amount.val(data.shipping);
  });
  $(document).on('click','#save-btn',function(e){
    e.preventDefault();
    var a = amount.val();
    $.ajax({
      url : api+'/shipping',
      data : {amount:a},
      type : 'PATCH',
      success:function(data){
        if(data.error)
          return error(data.error);
        notify('Success','Shipping amount was updated','success');
      },
      error : function(err){
        if(err.status == 422){
          return error("Amount is invalid");
        }
      }
    });
  });

</script>
@endsection