@extends('admin.template')
@section('title','View/Edit Locations')
@section('styles')
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
@endsection
@section('content')
<div class="row">
         <!-- left column -->
         <div class="col-lg-3 col-md-5" id="brands-section">

          <div class="box box-primary">
            <div class="box-header">
              <div class="col-md-12 box-title no-padding">
                <input type="text" class="search form-control no-border" placeholder="Search your Locations">
              </div>
            </div>
          </div>
          <div class="box box-solid">
            <div class="box-body no-padding">
              <ul class="list brands-collection nav nav-pills nav-stacked">
                <li class="brand-item active" data-id="add">
                  <a href="javascript:void(0)"><i class="fa fa-plus"></i><span class="brand add">Add Location</span></a>
                </li>
              </ul>
            </div>
          </div>
          <div class="text-center"><span class="total-brands">Loading</span> Locations</div>
        </div>
        <!--/.col (left) -->

        <!-- left column -->
        <div class="col-lg-6 col-md-7 brand-details-section">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Location Details</div>
            </div>
          </div>
          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Name</div>
            </div>
            <div class="box-body">
              <input type="hidden" name="id" id="location-id">
              <input type="text" class="form-control no-border" name="name" id="name" placeholder="What is your location called as?">
            </div>
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Address</div>
            </div>
            <div class="box-body pad">
              <!-- <div class="box-body image-desc"  name="" id=""  data-text=""></div> -->
              <textarea class="textarea no-border" form="brand-form" name="address" contenteditable="true" id="unecom-address" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
            </div>
          </div>
          
          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Phone/Mobile</div>
            </div>
            <div class="box-body">
              <input type="text" class="form-control no-border" name="phone" id="phone">
            </div>
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Email</div>
            </div>
            <div class="box-body">
              <input type="text" class="form-control no-border" name="email" id="email">
            </div>
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Website</div>
            </div>
            <div class="box-body">
              <input type="text" class="form-control no-border" name="website" id="website">
            </div>
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Longitude</div>
            </div>
            <div class="box-body">
              <input type="text" class="form-control no-border" name="longitude" id="longitude">
            </div>
          </div>

          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Latitude</div>
            </div>
            <div class="box-body">
              <input type="text" class="form-control no-border" name="latitude" id="latitude">
            </div>
          </div>

          <div class="col-sm-12 no-padding">
            <button class="btn btn-success pull-right" id="add-btn">Add</button>
            <button class="btn btn-primary pull-right hide" id="save-btn">Save</button>
            <button class="btn btn-danger pull-right hide" id="delete-btn" style="margin-right:10px;">Delete</button>
          </div>
          
        </div>
  
        <!-- /.row -->
         <!-- Variants -->
        <div class="unecom-template" style="display:none;">
          <ul class="unecom-brands">
            <li class="brand-item" id="brand-list-template">
              <a href="javascript:void(0)"><i class="fa fa-gift"></i><span class="brand"></span></a>
            </li>
          </ul>
        </div>
        
</div>
@endsection
@section('script')
<script type="text/javascript">
	$('select').select2();
</script>
<script src="/assets/admin/js/custom/locations.js"></script>
@endsection