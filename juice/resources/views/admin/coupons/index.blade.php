@extends('admin.template')

@section('title','Coupons')

@section('styles')

@endsection

@section('content')

<div class="row">



<!-- left column -->

<div class="col-lg-3 col-md-5" id="coupons-section">

  <div class="box box-primary">

    <div class="box-header">

      <div class="col-md-12 box-title no-padding">

        <input type="text" class="search form-control no-border" id="Search" placeholder="Search your coupons">

      </div>

    </div>

  </div>

  <div class="box box-solid">

    <div class="box-body no-padding">

      <ul class="list brands-collection nav nav-pills nav-stacked">

        <li class="active add-list-ui">

          <a href="#"><i class="fa fa-plus"></i>Add new Coupon</a>

        </li>

      </ul>

    </div>

  </div>

  <div class="text-center"><span class="total-brands">Loading</span> Coupons</div>

</div>

<!--/.col (left) -->



<!-- left column -->



<!-- left column -->

<div class="col-md-6 col-md-7">

  <!-- general form elements -->

  <div class="box box-primary">

    <div class="box-header with-border">

      <h3 class="box-title">Quick Add</h3>

    </div>

    <!-- /.box-header -->

    <!-- form start -->

    <form role="form" name="coupons" id="coupon">

      </div>

      <div class="box box-solid">

          <div class="box-header with-border">

            <div class="col-md-12 box-title no-padding">Coupon Code</div>

          </div>

          <div class="box-body">

            <input type="text" class="form-control no-border" id="code" name="code" placeholder="Enter coupon code">

          </div>

      </div>



      <div class="box box-solid">

          <div class="box-header with-border">

            <div class="col-md-12 box-title no-padding">Discount Type</div>

          </div>

          <div class="box-body ">

            <select id="discountType" class="form-control no-border" name="discountType">

            <option value="0">Percentage</option>

            <option value="1">Flat Amount</option>

          </select>

          </div>

      </div>



      <div class="box box-solid">

          <div class="box-header with-border">

            <div class="col-md-12 box-title no-padding">Amount</div>

          </div>

          <div class="box-body">

          <input type="text" class="form-control no-border" id="amount" name="amount" placeholder="% or amount">

      </div>

      </div>



      <div class="box box-solid">

          <div class="box-header with-border">

            <div class="col-md-12 box-title no-padding">Start Date</div>

          </div>

          <div class="box-body">

            <input type="date" class="form-control no-border" id="startDate" name="startDate" placeholder="Year-Month-Date">                  </div>

      </div>



      <div class="box box-solid">

        <div class="box-header with-border">

          <div class="col-md-12 box-title no-padding">End Date</div>

        </div>

        <div class="box-body">

          <input type="date" class="form-control no-border" id="expiryDate" name="expiryDate" placeholder="Year-Month-Date">                  

        </div>

      </div>

      <div class="box box-solid">

        <div class="box-header with-border">

          <div class="col-md-12 box-title no-padding">Enabled? <input class="pull-right" type="checkbox" id="enabled" name="enabled"></div>

        </div>

      </div>



      <div class="box box-solid">

        <div class="brand-add-btn-group">

          <button class="btn btn-primary pull-right" id="add-btn">Add</button>

        </div>

        <div class="brand-edit-btn-group hide">

          <button class="btn btn-primary pull-right" id="save-btn">Save</button>

        </div>

      </div>



    </form>

  

  <!-- /.box -->

</div>

<!--/.col (left) -->

  <div class="col-md-3 right-panel hide">

    <div class="box box-primary">

      <div class="box-header">Associated Categories</div>

      <div class="box-body no-padding" style="max-height: 400px;overflow: auto;">

        <form id="brand-assoc-form">

          <ul class="list brands-associated nav nav-pills nav-stacked">

            

          </ul>

        </form>

      </div>

    </div>

    <!-- <div class="col-md-12" >

      <button class="btn btn-primary pull-right" id="save-brand-assoc">Save association</button>

    </div> -->

  </div>

</div>

<div class="unecom-template" style="display:none;">

      <ul class="unecom-brands">

        <li id="brand-list-template">

          <a href="javascript:void(0)"><i class="fa fa-shopping-bag"></i><span class="brand"></span></a>

        </li>

        <li id="brand-associated-template">

          <a href="javascript:void(0)"><i class="fa fa-shopping-bag"></i><span class="brand"></span><input type="checkbox" name="brand" class="pull-right"></a>

        </li>

      </ul>

    </div>

@endsection

@section('script')

<script type="text/javascript" src='/assets/admin/js/custom/coupons.add.js'></script>

@endsection