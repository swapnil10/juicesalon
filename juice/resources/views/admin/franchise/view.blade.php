@extends('admin.template')
@section('title','Enquiries')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin-left:10px !important;">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <h1>
        Franchise Enquiry
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Enquiry</li>
      </ol>
    </section> -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="nav-tabs-custom">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li id="franchise-btn" role="presentation" class="active">
                  <a href="#franchise" aria-controls="franchise" role="tab" data-toggle="tab">All Enquiries</a>
                </li>
              </ul>
            
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="franchise">
                  <table id="franchise-table" class="table table-bfranchiseed table-hover">
                    <thead>
                      <tr>
                        <th>Sr No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>City</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="franchise-body"></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div class="unecom-template" style="display:none;">
    </div>
    <!-- unecom-template -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->
<div class="franchise-template" style="display:none;">
  <table>
    <tbody>
      <tr role="row" class="odd">
        <td class="sorting_1 franchiseId"></td>
        <td class="name"></td>
        <td class="email"></td>
        <td class="mobile"></td>
        <td class="city"></td>
        <td><a class="action" ><input type="button" class="btn btn-primary btn-xs action_btn" value="View Details" style="min-width: 56px"></button></a></td>
      </tr>
    </tbody>
  </table>
</div>

<div id="modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3">
            <label style="font-size: 16px; color: #153242;">Name: </label>
          </div>
          <div class="col-md-9">
            <p id="name"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <label style="font-size: 16px; color: #153242;">Email: </label>
          </div>
          <div class="col-md-9">
            <p id="email"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <label style="font-size: 16px; color: #153242;">Mobile: </label>
          </div>
          <div class="col-md-9">
            <p id="mobile"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <label style="font-size: 16px; color: #153242;">Size: </label>
          </div>
          <div class="col-md-9">
            <p id="size"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <label style="font-size: 16px; color: #153242;">How did they come to know?</label>
          </div>
          <div class="col-md-9">
            <p id="comeToknow"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <label style="font-size: 16px; color: #153242;">City: </label>
          </div>
          <div class="col-md-9">
            <p id="city"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <label style="font-size: 16px; color: #153242;">Message: </label>
          </div>
          <div class="col-md-9">
            <p id="message"></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  //franchise
  $.ajax({
    url: apiadd+"/franchise",
    success:function(data1){
      $.each(data1,function(i,franchise){
          var template3 = $('.franchise-template >table > tbody > .odd').clone();
          template3.attr("id",franchise.id);
          template3.find('.franchiseId').html(franchise.id);
          template3.find('.name').html(franchise.name);
          template3.find('.email').html(franchise.email);
          template3.find('.mobile').html(franchise.mobile);
          template3.find('.city').html(franchise.city);
          $("#franchise-body").append(template3);
      });
    },
    error:function(e){
      console.log(e);
    }
  });

$(document).on('click', '.action_btn', function (event) {
  var id = $(this).closest("tr").attr("id");
  $('#modal').modal('show'); 
  $.ajax({
    url: apiadd+"/franchise/"+id,
    success:function(franchise){
          $(".modal-title").html(franchise.name+"'s Enquiry");
          $('#name').html(franchise.name);
          $('#email').html(franchise.email);
          $('#mobile').html(franchise.mobile);
          $('#size').html(franchise.size);
          $('#comeToknow').html(franchise.comeToKnw);
          $('#city').html(franchise.city);
          $('#message').html(franchise.message);
    },
    error:function(e){
      console.log(e);
    }
  });
});
</script>
@endsection
<!-- jQuery 2.2.0 -->

