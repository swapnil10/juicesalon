
@extends('admin.template')
@section('title','Variant')
@section('styles')
  <style type="text/css">
    .error{
      background-color: #d9534f;
      color:#fff;
      padding:5px;
      margin:5px 0;
      font-size: 1.3rem;
      display:none;
    }
    .edit{
      contenteditable:true;
    }
    .success{
      background-color: #fff;
      color:green;
      padding:5px;
      margin:5px 0;
      font-size: 1.3rem;
      display:none;
    }
    .update{
      color:blue; 
      cursor:pointer;
    }
    .delete{
      position: absolute;
      right: 15px;
      top: 13px;
      cursor:pointer;
    }
   .unecom-template{
    display:none;
  }
  .error{
    background-color: #d9534f;
    color:#fff;
    padding:5px;
    margin:5px 0;
    font-size: 1.3rem;
    display:none;
  }
  .edit{
    contenteditable:true;
  }
  .success{
    background-color: #fff;
    color:green;
    padding:5px;
    margin:5px 0;
    font-size: 1.3rem;
    display:none;
  }
  .no-border{border: 0 !important;}
  .search{border: none;outline: none;padding: 0;}
  .image-desc{
    outline: none;
    min-height: 150px;
  }
  [contentEditable=true]:empty:not(:focus):before{
    content:attr(data-text);
    color:#9e9e9e;
  }
  .textarea{outline: none;}
  .brands-collection{max-height: 42rem;overflow: auto;}

  .dz-preview{display: none;}
  img.circle{border-radius: 50%;}
  .reset{
    margin-right: 2rem;
  }
</style>
@endsection
@section('content')
<div class="row">



        <!-- left column -->

        <div class="col-lg-3 col-md-5" id="variants-section">

          <div class="box box-primary">

            <div class="box-header">

              <div class="col-md-12 box-title no-padding">

                <input type="text" class="search form-control no-border" id="Search" placeholder="Search your variants">

              </div>

            </div>

          </div>

          <div class="box box-solid">

            <div class="box-body no-padding">

              <ul class="list brands-collection nav nav-pills nav-stacked">

                <li class="active add-list-ui">

                  <a href="#"><i class="fa fa-plus"></i>Add new Variant</a>

                </li>

              </ul>

            </div>

          </div>

          <div class="text-center"><span class="total-brands">Loading</span> Variant</div>

        </div>

        <!--/.col (left) -->



        <!-- left column -->



        <!-- left column -->

        <div class="col-md-6 col-md-7">

          <!-- general form elements -->

          <div class="box box-primary">

            <div class="box-header with-border">

              <h3 class="box-title">Quick Add</h3>

            </div>

            <!-- /.box-header -->

            <!-- form start -->

            <form role="form" class="variantForm" name="variant" id="variantId">

              </div>

              <div class="box box-solid">

                  <div class="box-header with-border">

                    <div class="col-md-12 box-title no-padding">Variant Name</div>

                  </div>

                  <div class="box-body">

                    <input type="text" class="form-control no-border" id="name" name="name" placeholder="What is your variant called as ?">

                  </div>

              </div>



              <div class="box box-solid">

                <div class="brand-add-btn-group">

                  <button class="btn btn-primary pull-right" id="add-btn">Add</button>

                </div>

                <div class="brand-edit-btn-group hide">

                  <button class="btn btn-success pull-right" id="save-btn">Save</button>

                </div>

              </div>



            </form>

          

          <!-- /.box -->

        </div>



        <!-- <div class="col-lg-3 col-md-7 col-md-offset-5 col-lg-offset-0">

      <div class="hide" id="codes-section">

            <div class="box box-solid">

              <div class="box-body">

                <div class="col-xs-12 input-group form-group" style="margin-bottom:0px;">

                  <input type="text" class="form-control form-data" name="name" id="code" placeholder="New Variants" autocomplete="false" >

                  <span class="input-group-btn">

                    <input type="submit" class="btn bg-olive btn-flat" id="add-code" value=" Add " />

                  </span>

                </div>

                <input type="hidden" id="variantId">

                <button type="button" class="col-xs-1 btn btn-flat btn-success pull-right" id="add">Add</button>

              </div>

            </div>

            <div class="box box-solid">

              <div class="box-header with-border">

                <div class="col-md-12 box-title no-padding">All your Variants</div>

              </div>

              <div class="box-body no-padding">

                <ol class="list codes-collection nav nav-pills nav-stacked" type="I"></ol>

              </div>

            </div>

          </div> 

        </div> -->

</div>



<div class="unecom-template" style="display:none;">

      <ul class="unecom-brands">

        <li id="brand-list-template">

          <a href="javascript:void(0)"><i class="fa fa-shopping-bag"></i><span class="brand"></span></a>

        </li>

        <li id="code-item">

          <a href="javascript:void(0)"><span class="title"></span><i class="variant-delete fa fa-times pull-right"></i></a>

        </li>

      </ul>

</div>
@endsection
@section('script')
<script type="text/javascript" src='/assets/admin/js/custom/variant.add.js'></script>
@endsection
