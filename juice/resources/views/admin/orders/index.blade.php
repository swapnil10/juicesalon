@extends('admin.template')
@section('title','View Orders')
@section('styles')
<style type="text/css">
  th,td{
    align-items: center;
  }
</style>
@endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin-left:10px !important;">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <h1>
        Orders
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
      </ol>
    </section> -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="nav-tabs-custom">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li id="orders-btn" role="presentation" class="active">
                  <a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">All Orders</a>
                </li>
              </ul>
            
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="orders">
                  <table id="orders-table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Order#</th>
                        <th>Applied Coupon Code</th>
                        <th>Razor Pay Order Id</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Completed At</th>
                      </tr>
                    </thead>
                    <tbody id="orders-body"></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div id="unecom-template" style="display:none;">
      <div id="orders-template">
        <table>
          <tbody>
            <tr role="row" class="odd">
              <td id="orderId"></td>
              <td id="couponCode"></a></td>
              <td id="razorPayOrderId"></td>
              <td id="status"></td>
              <td><a id="action" ><input type="button" class="btn btn-primary btn-xs action_btn" style="min-width: 56px"></button></a></td>
              <td id="completed_at"></td>
            </tr>
            <tr id="order-detail-template" role="row">
              <td id="productName"></td>
              <td id="price"></a></td>
              <td id="quantity"></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- unecom-template -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->
<div id="orders-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Information</h4>
      </div>
      <div class="modal-body">
        <h2>Order Details</h2>
        <table class="table">
          <thead>
            <tr>
              <th colspan="4">Order Detail</th>
            </tr>
          </thead>
          <thead>
            <tr>
              <th>Product Name</th>
              <th>Price</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody id="order-detail"></tbody>
        </table>
        <h2>User Detail</h2>
        <table class="table">
          <thead>
            <tr>
              <th colspan="4">Address</th>
            </tr>
          </thead>
          <thead>
            <tr>
              <th>Address</th>
              <th>City</th>
              <th>State</th>
              <th>Pin Code</th>
            </tr>
          </thead>
          <tbody id="tbody-address">
            <tr>
              <td id="address"></td>
              <td id="city"></td>
              <td id="state"></td>
              <td id="pinCode"></td>
            </tr>
          </tbody>
        </table>
        <table class="table">
          <thead>
            <tr>
              <th colspan="4">Contact Information</th>
            </tr>
          </thead>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
            </tr>
          </thead>
          <tbody id="tbody-user">
            <tr>
              <td id="id"></td>
              <td id="fullName"></td>
              <td id="email"></td>
              <td id="phone"></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
function init(offset=0){
  $.ajax({
    url: api+"/order?offset="+offset,
    success:function(data){
      if(data.error)
        return error(data.error);

      $.each(data,function(i,order){
          var a = $('#orders-template >table > tbody > .odd').clone();
          a.attr("data-orderId",order.id).attr("data-userId",order.uid);
          a.find("#couponCode").html("<i>None</i>");
          if(order.couponCode != "")
            a.find("#couponCode").html(order.couponCode);
          a.find("#razorPayOrderId").html(order.razorPayOrderId);
          a.find("#orderId").html(order.id);
          a.find("#completed_at").html(order.completed_at);
          switch(order.status){
            case "1":
              a.find('#status').html('Confirmed').css({"color":"blue"});
              a.find('.action_btn').attr("value","Dispatch");
              a.find('.action_btn').attr("od_id",order.id);
              a.find('.action_btn').attr("data-status",order.status);
              break;
            case "2":
              a.find('#status').html('Dispatched').css({"color":"orange"});
              a.find('.action_btn').attr("value","Deliver");
              a.find('.action_btn').attr("od_id",order.id);
              a.find('.action_btn').attr("data-status",order.status);
              break;
            case "3":
              a.find('#status').html('Delivered').css({"color":"green"});
              a.find('.action_btn').hide();
              break;
          }
          $("#orders-body").append(a);
      });

      if(data.length == 50){
        offset = offset+50;
        init(offset);
        return false;
      }
      // $(document).ready(function() {
      //   $('#orders-table').DataTable( {
      //       "paging":   true,
      //       "ordering": false,
      //       "searching": true
      //   } );
      // });
      return false;
    },
    error:function(e){
      return error("Something went wrong.");
    }
  });
}
//Orders
init();

$(document).on('click', '.action_btn', function (event) {
  var value = $(this).attr("value");
  var od_id = $(this).attr("od_id");
  var status = $(this).attr("data-status");
  var parent = $(this);
  status = parseInt(status)+1;
  var obj = [];
  obj = {od_id:od_id,status:status};
  var json = JSON.stringify(obj);
  if (confirm("Are you sure you want to "+value) == true) {
    $.ajax({
      url: api+'/order/'+od_id+'/change-status',
      type: 'POST',
      data: json,
      contentType: 'application/json; charset=utf-8',
      success: function (response) {
        if (response.error)
          return error(response.error);
        else
          changeButton(parent,response.statusId);
      },
      error: function(jqXhr) {
        if(jqXhr.status == 422)
          console.log(jqXhr.responseJSON);
      }
    }); 
    return false;
  }
});

$(document).on('click','tr.odd',function(){
  var userId = $(this).attr("data-userId");
  var orderId = $(this).attr("data-orderId");
  $.get(api+"/order/"+orderId,function(orders){
    $("#order-detail").html("");
    if(orders.error)
      return error(orders.error);

    $.each(orders,function(i,v){
      var a = $("#unecom-template").find("#order-detail-template").clone().appendTo("#order-detail");
      a.find("#productName").html(v.productName);
      a.find('#price').html(v.price);
      a.find('#quantity').html(v.quantity);
    });
    
  });

  $.get(api+'/users/'+userId,function(user){
    if(user.error)
      return error(user.error);

    var a = $("#tbody-user");
    a.find("#fullName").html("").html(user.fullName);
    a.find("#id").html("").html(user.id);
    a.find("#email").html("").html(user.email);
    a.find("#phone").html("").html(user.phone);
  });

  $.get(api+'/users/'+userId+'/address',function(address){
    var a = $("#tbody-address");
    a.find("#address").html("").html(address.address);
    a.find("#city").html("").html(address.city);
    a.find("#state").html("").html(address.state);
    a.find("#pinCode").html("").html(address.pinCode);
  });
  $("#orders-modal").modal('show');
});

function changeButton(button,statusId){
  parent = button.closest("td").prev('td');
  switch(statusId){
    case 0:
      parent.html('Pending').css({"color":"red"});
      button.attr("value","Confirm");
      button.attr("data-status",statusId);

      break;
    case 1:
      parent.html('Confirmed').css({"color":"blue"});
      button.attr("value","Dispatch");
      button.attr("data-status",statusId);
      break;
    case 2:
      parent.html('Dispatched').css({"color":"orange"});
      button.attr("value","Deliver");
      button.attr("data-status",statusId);
      break;
    case 3:
      parent.html('Delivered').css({"color":"green"});
      button.hide();
      break;
  }
}
</script>
@endsection
<!-- jQuery 2.2.0 -->

