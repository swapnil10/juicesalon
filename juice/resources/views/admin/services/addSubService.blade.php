@extends('admin.template')

@section('title','Sub Service')

@section('styles')

@endsection

@section('content')

<div class="row">



<!-- left column -->

<div class="col-lg-3 col-md-5" id="subservice-section">

  <div class="box box-primary">

    <div class="box-header">

      <div class="col-md-12 box-title no-padding">

        <input type="text" class="search form-control no-border" id="Search" placeholder="Search Sub Service">

      </div>

    </div>

  </div>

  <div class="box box-solid">

    <div class="box-body no-padding">

      <ul class="list brands-collection nav nav-pills nav-stacked">

        <li class="active add-list-ui">

          <a href="#"><i class="fa fa-plus"></i>Add new sub service</a>

        </li>

      </ul>

    </div>

  </div>

  <div class="text-center"><span class="total-brands">Loading</span> sub services</div>

</div>

<!--/.col (left) -->



<!-- left column -->



<!-- left column -->

<div class="col-md-6 col-md-7">

  <!-- general form elements -->

  <div class="box box-primary">

    <div class="box-header with-border">

      <h3 class="box-title">Quick Add</h3>

    </div>

    <!-- /.box-header -->

    <!-- form start -->

    <form role="form" name="subservices" id="subservices">

      </div>

      <div class="box box-solid">

          <div class="box-header with-border">

            <div class="col-md-12 box-title no-padding">Title</div>

          </div>

          <div class="box-body">
          	<input type="hidden" name="id" id="id">
            <input type="text" class="form-control no-border" id="title" name="title" placeholder="Enter subservice title">

          </div>

      </div>

      <div class="box box-solid">
        <div class="box-header with-border">
          <div class="col-md-12 box-title no-padding">Select Service</div>
        </div>
        <div class="box-body">
          <select class="unecom-form-data form-control no-border form-data" name="service" id="unecom-service" style="width:100%">
            <option value="0" selected disabled>Select Service Type</option>
          </select>
        </div>
      </div>
      <div class="box box-solid">
        <div class="box-header with-border">
          <div class="col-md-12 box-title no-padding">Describe the Sub Service</div>
        </div>
        <div class="box-body pad">
          <!-- <div class="box-body image-desc"  name="" id=""  data-text=""></div> -->
          <textarea class="textarea no-border" form="brand-form" name="description" contenteditable="true" id="unecom-description" placeholder="Short description about this sub service" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
        </div>
      </div>
      <div class="box box-solid">

          <div class="box-header with-border">

            <div class="col-md-12 box-title no-padding">Starting from</div>

          </div>

          <div class="box-body">
            <input type="text" class="form-control no-border" id="price" name="price" placeholder="Enter the starting from price">
          </div>

      </div>

      <div class="box box-solid">

        <div class="box-header with-border">

          <div class="col-md-12 box-title no-padding">Enabled? <input class="pull-right" type="checkbox" id="enabled" name="enabled"></div>

        </div>

      </div>



      <div class="box box-solid">

        <div class="brand-add-btn-group">

          <button class="btn btn-primary pull-right" id="add-btn">Add</button>

        </div>

        <div class="brand-edit-btn-group hide">

          <button class="btn btn-primary pull-right" id="save-btn">Save</button>

        </div>

      </div>



    </form>

  

  <!-- /.box -->

</div>

<!--/.col (left) -->


</div>

<div class="unecom-template" style="display:none;">
  <ul class="unecom-brands">
    <li id="brand-list-template">
      <a href="javascript:void(0)"><i class="fa fa-shopping-bag"></i><span class="brand"></span></a>
    </li>
  </ul>
</div>

@endsection

@section('script')

<script type="text/javascript" src='/assets/admin/js/custom/subservices.js'></script>

@endsection