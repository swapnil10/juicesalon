@extends('admin.template')

@section('title','Service')

@section('styles')

@endsection

@section('content')

<div class="row">



<!-- left column -->

<div class="col-lg-3 col-md-5" id="services-section">

  <div class="box box-primary">

    <div class="box-header">

      <div class="col-md-12 box-title no-padding">

        <input type="text" class="search form-control no-border" id="Search" placeholder="Search Services">

      </div>

    </div>

  </div>

  <div class="box box-solid">

    <div class="box-body no-padding">

      <ul class="list brands-collection nav nav-pills nav-stacked">

        <li class="active add-list-ui">

          <a href="#"><i class="fa fa-plus"></i>Add new service</a>

        </li>

      </ul>

    </div>

  </div>

  <div class="text-center"><span class="total-brands">Loading</span> services</div>

</div>

<!--/.col (left) -->



<!-- left column -->



<!-- left column -->

<div class="col-md-6 col-md-7">

  <!-- general form elements -->

  <div class="box box-primary">

    <div class="box-header with-border">

      <h3 class="box-title">Quick Add</h3>

    </div>

    <!-- /.box-header -->

    <!-- form start -->

    <form role="form" name="services" id="service">

      </div>

      <div class="box box-solid">

          <div class="box-header with-border">

            <div class="col-md-12 box-title no-padding">Title</div>

          </div>

          <div class="box-body">
          	<input type="hidden" name="id" id="id">
            <input type="text" class="form-control no-border" id="title" name="title" placeholder="Enter Service title">

          </div>

      </div>

      <div class="box box-solid">

        <div class="box-header with-border">

          <div class="col-md-12 box-title no-padding">Enabled? <input class="pull-right" type="checkbox" id="enabled" name="enabled"></div>

        </div>

      </div>



      <div class="box box-solid">

        <div class="brand-add-btn-group">

          <button class="btn btn-primary pull-right" id="add-btn">Add</button>

        </div>

        <div class="brand-edit-btn-group hide">

          <button class="btn btn-primary pull-right" id="save-btn">Save</button>

        </div>

      </div>



    </form>

  

  <!-- /.box -->

</div>

<!--/.col (left) -->


</div>

<div class="unecom-template" style="display:none;">
  <ul class="unecom-brands">
    <li id="brand-list-template">
      <a href="javascript:void(0)"><i class="fa fa-rocket"></i><span class="brand"></span></a>
    </li>
  </ul>
</div>

@endsection

@section('script')

<script type="text/javascript" src='/assets/admin/js/custom/services.js'></script>

@endsection