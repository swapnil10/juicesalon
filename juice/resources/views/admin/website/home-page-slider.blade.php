@extends('admin.template')
@section('title',' Slider')
@section('styles')
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
@endsection
@section('content')
<div class="row">

  <!-- left column -->
  <div class="col-lg-3 col-md-5" id="variants-section">
    <div class="box box-primary">
      <div class="box-header">
        <div class="col-md-12 box-title no-padding">
          <input type="text" class="search form-control no-border" id="Search" placeholder="Search your Slider Content">
        </div>
      </div>
    </div>
    <div class="box box-solid">
      <div class="box-body no-padding">
        <ul class="list brands-collection nav nav-pills nav-stacked">
          <li class="active add-list-ui">
            <a href="#"><i class="fa fa-plus"></i>Add new Home page Slider</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="text-center"><span class="total-brands">Loading</span> Slider Content</div>
  </div>
  <!--/.col (left) -->

  <!-- left column -->

  <!-- left column -->
  <div class="col-md-6 col-md-7">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Quick Add</h3>
      </div>
    </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form">
        <input type="hidden" name="id" id="id">
        <div class="box box-solid">
          <div class="box-header with-border">
            <div class="col-md-12 box-title no-padding">Content</div>
          </div>
          <div class="box-body pad">
            <!-- <div class="box-body image-desc"  name="" id=""  data-text=""></div> -->
            <textarea class="textarea no-border" readonly form="brand-form" name="description" contenteditable="true" id="unecom-content" placeholder="Short description about this slider" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
          </div>
      	</div>

        <div class="box box-solid">
          <div class="box-header with-border">
            <div class="col-md-12 box-title no-padding">Link</div>
          </div>
          <div class="box-body pad">
            <!-- <div class="box-body image-desc"  name="" id=""  data-text=""></div> -->
            <input type="text" name="link" id="link" class="form-control no-border" placeholder="URL">
            
          </div>
        </div>

        <div class="box box-solid">
          <div class="brand-add-btn-group">
            <button class="btn btn-success pull-right" id="add-btn">Add</button>
          </div>
          <div class="brand-edit-btn-group hide">
            <button class="btn btn-primary pull-right" id="save-btn">Save</button>
            <button class="btn btn-danger pull-right" id="delete-btn" style="margin-right:10px;">Delete</button>
          </div>
        </div>

      </form>
      
      <!-- /.box -->
    </div>
  

  <div class="col-md-3">
    <div class="box box-primary dropzone" id="brand-image">
      <div class="fallback">
        <input name="file" type="file" multiple />
      </div>
      <div class="box-header with-border">
        Images - Just Drag n Drop (1900 x 380 px)
      </div>
      <div class="box-body text-center img-container dropzone-previews">
        
      </div>
    </div>
  </div>
</div>
<div class="unecom-template" style="display:none;">
      <ul class="unecom-brands">
        <li id="brand-list-template">
          <a href="javascript:void(0)"><i class="fa fa-shopping-bag"></i><span class="brand"></span></a>
        </li>
      </ul>
</div>

<div style="display: none;">
	<div id="dz-preview-template">
        <div class="dz-preview dz-file-preview">
          <div class="dz-image">
            <img data-dz-thumbnail="">
          </div>
          <div class="dz-details">
            <div class="dz-size text-center" data-dz-size></div>
            <div class="dz-filename text-center"><span data-dz-name></span></div>
          </div>
          <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
          <div class="dz-success-mark">    
            <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Check</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>      </g>    </svg>  
          </div>
          <div class="dz-error-mark">    
            <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Error</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>        </g>      </g>    </svg>  
          </div>
          <div class="dz-error-message"><span data-dz-errormessage></span></div>
        </div>
      </div>
</div>
@endsection
@section('script')
<script src='/assets/admin/plugins/dropzone/dropzone.js'></script>
<script src="/assets/admin/js/custom/slider-home.js"></script>
@endsection