@extends('admin.template')
@section('title','Home Slider')
@section('styles')
<link rel="stylesheet" type="text/css" href="//assets/admin/plugins/image-picker/image-picker.css">
<style>
  .image_picker_image{
    height: 100px !important;
    width: 100px !important;
</style>
</style>
@endsection
@section('content')
<div class="row">
         <!-- left column -->
         <div class="col-lg-3 col-md-5" id="brands-section">

          <div class="box box-primary">
            <div class="box-header">
              <div class="col-md-12 box-title no-padding">
                <input type="text" class="search form-control no-border" placeholder="Search your Slider">
              </div>
            </div>
          </div>
          <div class="box box-solid">
            <div class="box-body no-padding">
              <ul class="list brands-collection nav nav-pills nav-stacked">
                <li class="brand-item active" data-id="add">
                  <a href="javascript:void(0)"><i class="fa fa-plus"></i><span class="brand add">Add a Slider</span></a>
                </li>
              </ul>
            </div>
          </div>
          <div class="text-center"><span class="total-brands">Loading</span> Sliders</div>
        </div>
        <!--/.col (left) -->

        <!-- left column -->
        <div class="col-lg-6 col-md-7 brand-details-section">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding">Sliders Details</div>
              <input type="hidden" name="id" id="slider-id">
              <input type="hidden" name="image" id="image">
            </div>
          </div>
          <div class="box box-solid">
          <div class="box-header with-border">
            <div class="col-md-12 box-title no-padding">Content</div>
          </div>
          <div class="box-body pad">
            <!-- <div class="box-body image-desc"  name="" id=""  data-text=""></div> -->
            <textarea class="textarea no-border" readonly form="brand-form" name="description" contenteditable="true" id="content" placeholder="Short description about this product" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
          </div>
          </div>
          <div class="box box-solid">
          <div class="box-header with-border">
            <div class="col-md-12 box-title no-padding">Link Title</div>
          </div>
          <div class="box-body pad">
            <!-- <div class="box-body image-desc"  name="" id=""  data-text=""></div> -->
            <input type="text" name="link-title" id="link-title" class="form-control no-border" placeholder="Link Title">
          </div>
          </div>
          <div class="box box-solid">
          <div class="box-header with-border">
            <div class="col-md-12 box-title no-padding">Link</div>
          </div>
          <div class="box-body pad">
            <input type="text" name="link" id="link" class="form-control no-border" placeholder="Link">
          </div>
          </div>
          <div class="box box-solid">
            <div class="box-header with-border">
              <div class="col-md-12 box-title no-padding"><p id="image-text">Image not selected!</p><img class="hide" id="image-preview" style="height: 100px;width:100px;" /> <a id="open-all-images" style="float: right !important;">Choose Image</a></div>
            </div>
        </div>
          <div class="col-sm-12 no-padding">
            <button class="btn btn-success pull-right" id="add-btn">Add</button>
            <button class="btn btn-primary pull-right hide" id="save-btn">Save</button>
            <button class="btn btn-danger pull-right hide" id="delete-btn" style="margin-right:10px;">Delete</button>
          </div>
        </div>
        <!-- /.row -->
         <!-- Variants -->
        <div class="unecom-template" style="display:none;">
          <ul class="unecom-brands">
            <li class="brand-item" id="brand-list-template">
              <a href="javascript:void(0)"><i class="fa fa-gift"></i><span class="brand"></span></a>
            </li>
          </ul>
        </div>
        
</div>
 <!--modal-->
  <div class="modal fade" id="images-modal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Image</h4>
        </div>
        <div class="modal-body" style="height: 550px;overflow: auto;">
          <select id="image-select" class="image-picker show-html"></select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!--end Modal-->
@endsection
@section('script')
<script type="text/javascript">
	$('select').select2();
</script>
<script type="text/javascript" src="//assets/admin/plugins/image-picker/image-picker.js"></script>
<script src="//assets/admin/js/custom/homeslider.js"></script>
@endsection