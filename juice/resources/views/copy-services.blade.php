@extends('partials.template')

@section('title','Services at JUICE Salon')

@section('meta')
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="/assets/admin/plugins/image-picker/image-picker.css">
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-common-extra.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-core.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-reset.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-sidebar.css'>
<link rel='stylesheet' href='/assets/admin/css/css/create-ui/css/create-ui.css'>
<link rel='stylesheet' href='/assets/admin/css/css/midgard-notifications/midgardnotif.css'>
<style>
  .image_picker_image{
    height: 100px !important;
    width: 100px !important;
</style>
@endsection

@section('main')
<!--
@ Service Page - Style 1
-->

<main class="main-section page-main-area article" id="services_page" about="/my/article">

     <header class="section-header service-page-header">
         <div class="container">
              <div class="row">
                  <div class="services-offered-banner col-md-4 col-sm-12">
                      <div class="inner">
                          <h4>Services Offered</h4>
                          
                          <div class="media" >
                            <div class="media-left">
                              <a href="#">
                                <img class="media-object" style="padding: 10px" src="img/icons/scissors-cutting-hair-white.svg" alt="Scissors Cutting Hair" width="72">
                              </a>
                            </div>
                            <div class="media-body">
                              <p style="color: #ccc; font-weight: 300;" id="header-content"></p>
                              <i style="opacity: 0.50;">Click here to edit...</i>
                            </div>
                          </div>

                      </div>
                  </div>
        
                  <div class="services-promotion-slide col-md-7 col-sm-12">
                      <!-- Carousel Start -->
                      <div class="owl-carousel" id="service_header_slider">



                      </div>
                      <!-- Carousel End -->
                  </div>

              </div>
         </div>
     </header>


     <!-- Content -->
     <section class="section-content">
         <div class="container">
             <div class="services-list">
                 <header class="services-title-box">
                     <h2 class="service-title">Choose from our variety of services</h2>
                 </header>

               <!-- control buttons stripe -->  
               <aside id="action_btns_stripe">  
                   <div class="row">
                       <div class="container">
                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-left" style="display: none;">
                                   <a class="icon-btn" href="#screen1" id="goScreen1">
                                       <img src="img/icons/go-back-left-arrow.svg" width="28">
                                   </a>
                               </div>
                           </div>

                           <div class="col-md-4 col-sm-12"></div>

                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-right">
                                   <!-- Cart to show the added items - wow! -->
                                   <div class="header-col">
                                      <div class="options-block">
                                          <a title="Checkout" href="#cart" class="options-btn cart-btn links-btn" data-label="Cart" data-target="#checkout_popup">
                                            <span class="sr-only">Cart</span>
                                            <i class="fa fa-shopping-cart hidden"></i>
                                            <img src="img/icons/shopping-cart.svg" width="16" style="display: inline-block;">
                                            <span class="peg cartItemCount" data-items="3">5</span>
                                          </a>

                                          <div class="popup-dialog go-left" id="checkout_popup">
                                            <div class="bg-overlay"></div>
                                            <div class="inner">
                                              <header class="popup-header">
                                                <h5 class="pull-left">
                                                    <img src="img/icons/shopping-cart.svg" width="16" style="display: inline-block; margin-right: 10px;">  
                                                    <span>Cart</span>
                                                </h5>
                                                <h5 class="close-btn text-right pull-right">
                                                  <a href="#" data-target="#checkout_popup"><i class="fa fa-close"></i></a>
                                                </h5>
                                                <div class="clearfix"></div>
                                              </header>
                                              
                                              <!-- Cart details -->
                                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. </p>
                                            </div>
                                          </div>

                                      </div>
                                   </div>
                               </div>
                           </div>

                       </div>
                   </div>
               </aside>

                 <div class="row" style="padding: 2em 0;" id="services">
                 <!-- Service Item -->
                 <!-- <div class="services-list-col col-md-3 col-sm-6 col-xs-12">
                     <a href="#" class="service-item" data-target="#haircuts" title="Haircuts">
                         <span>Haircuts</span>
                     </a>

                     <div class="service-infowindow" id="haircuts" hidden>
                       <div class="inner">
                         <h3>Hair cuts <a href="#" class="close-infowindow"><i class="fa fa-close"></i></a></h3>
                         <div class="services-item-info row">
                             <article class="services-item-block col-md-6 col-xs-12">
                                 <div class="inner">
                                     <h4>Lorem Ipsum Doler</h4>
                                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus</p>
                                     <div class="price">
                                         <span>Starts at Rs. 200.00 /-</span>
                                     </div>
                                 </div>
                             </article>

                         </div>
                       </div>
                     </div> 

                 </div> -->  


                  </div>

             </div>
         </div>
     </section>
</main>
@endsection
<div id="slider-template" hidden>
  <div class="owl-item" style="float: none;">
    <div class="bg-layer-set">
        <div class="bg-slide-image" id="image"></div>
          <div class="bg-overlay-color"></div>
        </div>
        <div class="inner">
          <div class="text-col">
              <p data-animation="animated fadeInLeft" id="data"></p>
          </div>
        </div>
    </div>
</div>
<div id="service-template" hidden="">
  <div class="services-list-col col-md-3 col-sm-6 col-xs-12 main-services">
    <a href="#" class="service-item" id="target">
      <span class="title"></span>
    </a>
      <div class="service-infowindow" id="id" hidden>
        <div class="inner">
          <h3><span class="title"></span> <a href="#" class="close-infowindow"><i class="fa fa-close"></i></a></h3>
            <div class="services-item-info row">
                             
            </div>
        </div>
      </div>
  </div> 
</div>
<div id="subService-template" hidden>
  <article class="services-item-block col-md-6 col-xs-12 subservice">
    <div class="inner">
      <h4 id="title">Lorem Ipsum Doler</h4>
      <p id="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus</p>
      <div class="price">
        <span>Starts at Rs. <span id="price">200.00</span> /-</span>
      </div>
    </div>
  </article>
</div>
@section('script')
<script type="text/javascript" src="/assets/admin/plugins/image-picker/image-picker.js"></script>
<script type="text/javascript">
  $.get("api/content/service-page-header-content",function(data){
    $("#header-content").html(data.header).attr("property","header-content");
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
      $.get("api/slider/service",function(data){
        $.each(data,function(i,v){
          if(v.imageId!=0){
            template = $("#slider-template .owl-item").clone();
            template.find('#data').html(v.content);
            var imageUrl = "image/"+v.imageId+".png";
            template.find("#image").css('background-image','url(' + imageUrl + ')');
            template.appendTo("#service_header_slider");
          }
        }); 
        slider();
      });
      $.get("api/services",function(data){
        $.each(data,function(i,v){
            template = $("#service-template .main-services").clone();
            template.find("#target").attr('data-target','#'+v.id);
            template.find("#target").attr('title',v.title);
            template.find("#id").attr('id',v.id);
            template.find(".title").html(v.title);
            $.get("api/services/sub-services/"+v.id,function(subservice){
              $.each(subservice,function(k,j){
                template1 = $("#subService-template .subservice").clone();
                template1.find("#title").html(j.title);
                template1.find("#description").html(j.description);
                template1.find("#price").html(j.price);
                template1.appendTo($("#"+v.id).find('.services-item-info'));
              })
            });
            template.appendTo("#services");
            
        });
      });
  });



  function slider(){
    $("#service_header_slider").on("initialized.owl.carousel", function(e){
   var $servicePromoSlider = $("#service_header_slider");
       $servicePromoSlider.css({'height': 'initial'});
       $servicePromoSlider.animate({'opacity': 1});
  });


  var servicePromoSliderMain = $("#service_header_slider").owlCarousel({
      loop:true,
      margin: 0,
      nav: false,
      dots:true, 
      items: 1,
      loop: false,
      responsive:{
          0:{
              items:1
          },

          600:{
              items:1
          },

          1000:{
              items:1
          }
      }
  });


  servicePromoSliderMain .on("changed.owl.carousel", function(e){
   var $animatingElems = $(e.relatedTarget.$element).find("[data-animation ^= 'animated']");
   doAnimations( $animatingElems );
  });
  function doAnimations( elems ) {
    //Cache the animationend event in a variable
    var animEndEv = 'webkitAnimationEnd animationend';
    
    elems.each(function () {
      var $this = $(this),
        $animationType = $this.attr('data-animation');

      $this.addClass($animationType).one(animEndEv, function () {
        $this.removeClass($animationType);
      });
    });
  }
  }
</script>
<script type="text/javascript" src='http://code.jquery.com/ui/1.9.2/jquery-ui.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/underscore-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/backbone-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/vie-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/jquery.tagsinput.min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/jquery.rdfquery.min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/annotate-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/rangy-core-1.2.3.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/hallo-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/aloha/lib/require.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/aloha/lib/aloha-full.min.js', data-aloha-plugins='common/ui,common/format,common/link,common/image,extra/sourceview'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/create-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/contentblocks.js'></script>
<script type="text/javascript" src="/assets/admin/js/create.js"></script>
<script type="text/javascript">
 var v = new VIE();
v.use(new v.RdfaService());

// Load Create.js with your VIE instance
$("body").midgardCreate({
  vie: v
});
</script>
@endsection