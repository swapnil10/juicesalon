@extends('partials.template')
@section('title','Get The Look')
@section('meta')
@endsection
@section('style')
@endsection
@section('main')
@if(isset($data))

<form action="{{url('payment/response')}}" name="myform" method="POST">
  <script 
src="https:/checkout.razorpay.com/v1/checkout.js" 
data-key="{{$data['key']}}"
data-name="Juice Salon"
data-prefill.name="{{$data['prefill']['name']}}" 
data-prefill.email="{{$data['prefill']['email']}}" 
data-notes.shopping_order_id="2"
data-order_id="{{$data['razarPayOrderId']}}"

></script>
{{csrf_field()}}

  
</form>
@else
<script type="text/javascript">
  setTimeout(function(){
    window.location.href = "/";
  },1000);
</script>
@endif
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function(){
    var a = $("form[name='myform']").find(".razorpay-payment-button");
    
    a.trigger('click');
    a.hide();
  });
</script>
@endsection()