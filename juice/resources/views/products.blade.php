@extends('partials.template')

@section('title','Products - Shampoo, Description, Description..')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')
<!--
@ Service Page - Style 1
-->

   <main class="main-section page-main-area" id="products_page">

     <header class="section-header product-page-header" id="products_page_slide" style="height: 320px; opacity: 0;">

         <div class="products-slide-background">
           <div id="solid_yellow_bg">
             <div class="container" style="position: relative; height: 100%;">
               <div id="pink_oval_layer" class="visible-md visible-lg" data-animation="animated zoomIn"></div>
             </div>
               <div id="pink_oval_layer_2" class="visible-sm visible-xs"></div>
           </div>
         </div>


         <div class="container-fluid">
             <!-- 
             @ PRODUCT PAGE OFFERS AND SHOWCASE CAROUSEL
             -->
             <div class="owl-carousel products-slide" id="products_slide_1">
             </div>
             <!-- owl carousel end -->

             
         </div>
     </header>


     <!-- Content -->
     <section class="section-content">
         <div class="container">
             <header class="main-section-header">
               <h2 class="title-box products-page-title text-center">Products</h2>
             </header>
             
             <div class="row">
               <!-- Filter and Cart Column -->
               <div class=" col-md-3 col-sm-3 col-lg-2">
                 <div class="product-page-options">
                   <div class="inner">
                     
                     <div class="options-block">
                       
                         <a title="Filter products" href="#filter" class="options-btn filter-btn links-btn" data-label="Filter" data-target="#filter_popup"> 
                           <span class="sr-only">Filter</span>
                           <i class="fa fa-filter hidden"></i>
                           <img src="/img/icons/filter-results-button.svg" width="16" style="display: inline-block;">
                         </a>
                       
                       
                       <div class="popup-dialog" id="filter_popup">
                         <div class="bg-overlay"></div>
                         <div class="inner">    
                           <header class="popup-header">
                             <h5 class="pull-left"> 
                                 <img src="/img/icons/filter-results-button.svg" width="16" style="display: inline-block; margin-right: 10px;">  
                                 <span>Filter</span>
                             </h5>
                             <h5 class="close-btn pull-right">
                               <a href="#" data-target="#filter_popup"><i class="fa fa-close"></i></a>
                             </h5>
                             <div class="clearfix"></div>
                           </header>
                           
                           <!-- Filter popup inner details -->
                           <div class="filter-screen">
                             
                             <div class="selected-filters">
                               <a class="filter-tag hide" id="product-type-filter-tag" href="#">Shampoo <i class="fa fa-close"></i></a>
                               <a class="filter-tag" id="order-filter-tag" href="#">High-to-Low <i class="fa fa-close"></i></a>
                             </div>
                             
                             <form name="product_filters" class="form-group form-general">

                                 
                               <div class="input-block form-row">
                                 <label class="_z " for="productTypeFilter">Product Type: </label>
                                 <select name="product_type" id="productTypeFilter">
                                   <option value="" disabled="" selected>Select Product Type</option>
                                   
                                 </select>
                               </div>
                                 
                                 
                               <div class="input-block col">
                                 <label class="_z" for="priceSortFilter">Price: </label>
                                 <select name="price_sort" id="priceSortFilter">
                                   <option value="1" selected="">High to Low</option>
                                   <option value="0">Low to High</option>
                                 </select>
                               </div>
                               
                               <div class="input-block col">
                                 <label class="_z"></label>
                                 <button type="submit" id="produceFilterSubmit" class="filter-submit-button ">
                                   <i class="fa fa-spinner fa-pulse fa-fw" style="margin-right: 4px;"></i> <span>Apply</span>
                                 </button>
                               </div>
             
                             </form>
                             
                           </div>
                           <!-- Filter popup inner details -->
                         </div>
                       </div>
                       
                     </div>
                     
                     
                     <div class="options-block">

                         <a title="Checkout" href="#cart" class="options-btn cart-btn links-btn" data-label="Cart" data-target="#checkout_popup">
                           <span class="sr-only">Cart</span>
                           <i class="fa fa-shopping-cart hidden"></i>
                           <img src="/img/icons/shopping-cart.svg" width="16" style="display: inline-block;">
                           <span class="peg cartItemCount" data-items="3">5</span>
                         </a>
                       
                       
                       <div class="popup-dialog" id="checkout_popup">
                         <div class="bg-overlay"></div>
                         <div class="inner">

                           <header class="popup-header">
                             <h5 class="pull-left">
                                 <img src="/img/icons/shopping-cart.svg" width="16" style="display: inline-block; margin-right: 10px;">  
                                 <span>Cart</span>
                             </h5>
                             <h5 class="close-btn text-right pull-right">
                               <a href="#" data-target="#checkout_popup"><i class="fa fa-close"></i></a>
                             </h5>
                             <div class="clearfix"></div>
                           </header>
                           
                           <!-- Cart details -->
                           
                           <!-- The cart box - put all the necessary form here and use the checkout button to submit the form -->
                           <div class="cart-box">
                             <div class="cart-box-inner" id="cart-container">
                             </div>
                           </div> <!-- #cart box end -->

                         </div>
                       </div>
                       
                     </div>
                     
                     
                     
                     
                     
                     
                   </div>
                 </div>
               </div>
               
               <!-- product listing column -->
               <div class="col-md-9 col-sm-9 col-lg-10 product-list">
                 <div class="inner">
                   
                 </div>
                 <div class="inner product-list-inner grid-product product-container">
                  
                   <!-- Single Product Item -->
                    
                 </div>
               </div>
             </div>
         </div> 
     </section>

     
    </main>
<div id="slider-template" hidden>
  <div class="owl-item" style="float: none;">
    <div class="product-slide-inner slide_item_1">
      <div class="container">
        <div class="row">
          <div class="col-md-5 text-col">
            <p class="big" data-animation="animated fadeInUp" id="description"></p>
            <p class="big" data-animation="animated fadeInDown"><a href="#buynow" id="link" class="link-highlighted inverted buy-now-btn"><b>Buy now</b></a></p>
          </div>
          <div class="col-md-2 visible-md visible-lg offset-col"></div>
          <div class="col-md-4 image-col">
            <div class="image-container">
              <img class="img-responsive" width="320" data-animation="" id="image" >
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="unecom-template" style="display: none;">
        <article class="product-box col product">
         <div class="product-image">
           <a href="" class="overlay-link"></a>
           <!-- images must be a square, more than or same as 240px -->
           <img src="" class="image">
         </div>
         <div class="product-details">
           <h3><a href="" class="overlay-link title"></a></h3>
           <p class="description"></p>
           <div class="price">
             <p>Rs.<span id="price"></span></p>
           </div>
           <div class="add">
             <button class="add-to-cart">Add to Cart</button>
           </div>
         </div>
       </article>
</div>
@endsection
@section('script')
<script type="text/javascript" src="/js/custom/product.js"></script>
@endsection