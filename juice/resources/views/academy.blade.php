@extends('partials.template')

@section('title','Academy')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')
<main class="main-section page-main-area" id="academy_page">
     <header class="section-header academy-page-header" style=" background-color: #ccc; position: relative; padding-bottom: 0; padding-top: 0;">

               <!-- OwlCarousel Start -->
               <div class="owl-carousel academy-main-slide" id="academy_page_slider" style="height: 320px; opacity: 0;">
               </div>
               <!-- OwlCarousel End -->

     </header>


     <!-- Content -->
     <section class="section-content">
         <div class="container">
             <div class="academy-list section-list">
                 <header class="academy-title-box title-box">
                     <!-- back button to go to screen 1 -->
                     <div class="header-col">
                         
                     </div>

                     <h2 class="academy-title title">Choose from our variety of our services</h2>

                     
                 </header>

               <!-- control buttons stripe -->  
               <aside id="action_btns_stripe">  
                   <div class="row">
                       <div class="container">
                           <div class="col-md-4 col-sm-12">
                               <div class="action-btn pull-left" style="display: none;">
                                   <a class="icon-btn" href="#screen1" id="goScreen1">
                                       <img src="/img/icons/go-back-left-arrow.svg" width="28">
                                   </a>
                               </div>
                           </div>

                           <div class="col-md-4 col-sm-12"></div>

                           <div class="col-md-4 col-sm-12">
                            <div class="product-page-options">
                              <div class="inner">
                             <div class="options-block">

                                 <a title="Checkout" href="#cart" class="options-btn cart-btn links-btn" data-label="Cart" data-target="#checkout_popup">
                                   <span class="sr-only">Cart</span>
                                   <i class="fa fa-shopping-cart hidden"></i>
                                   <img src="/img/icons/shopping-cart.svg" width="16" style="display: inline-block;">
                                   <span class="peg cartItemCount" data-items="3">5</span>
                                 </a>
                               <div class="popup-dialog" id="checkout_popup">
                                 <div class="bg-overlay"></div>
                                 <div class="inner">

                                   <header class="popup-header">
                                     <h5 class="pull-left">
                                         <img src="/img/icons/shopping-cart.svg" width="16" style="display: inline-block; margin-right: 10px;">  
                                         <span>Cart</span>
                                     </h5>
                                     <h5 class="close-btn text-right pull-right">
                                       <a href="#" data-target="#checkout_popup"><i class="fa fa-close"></i></a>
                                     </h5>
                                     <div class="clearfix"></div>
                                   </header>
                                   
                                   <!-- Cart details -->
                                   
                                   <!-- The cart box - put all the necessary form here and use the checkout button to submit the form -->
                                   <div class="cart-box">
                                     <div class="cart-box-inner" id="cart-container">
                                     </div>
                                   </div> <!-- #cart box end -->

                                 </div>
                               </div>
                             </div>
                              </div>
                            </div>
                           </div>

                       </div>
                   </div>
               </aside>

               <div class="row screens" style="position: relative; padding: 2em 0; min-height: 320px;" id="category-box">
                <div><a class="back" href="#" style="text-decoration:null;margin-left: 3%;">Back</a></div>
                <div class="sub-infowindow infowindow active" id="product-box" style="margin-top:157px;">
                   <div class="inner">
                     <h3 id="product-category-name"></h3>
                     <div class="item-info row" id="product-inner"></div>
                   </div>
                </div>
               </div>

             </div>
         </div>
     </section>
    </main>
  <div id="templates" style="display: none;">

    <div id="category-box-template">
      <div class="list-col col-md-4 col-sm-6 col-xs-12">
         <a class="list-item infowindow-btn">
             <span id="category-title"></span>
         </a>
      </div>
    </div>

    <div id="product-box-template">
      <article class="item-block col-md-6 col-xs-12">
          <div class="inner">
              <h4 id="product-title"></h4>
              <p id="product-description"></p>
              <div class="add-to-cart item-add-button">
                  <a id="add-video" class="links-btn btn-inverted"><span>Add to cart</span></a>
              </div>
          </div>
      </article>
    </div>
    <div id="page-slider-template">
      <div class="owl-item" style="float: none;">
       <div class="bg-image-slide" id="image-slider"></div>

       
       <div class="container">
         <div class="slide-inner">
             <div class="text-col" id="description">
                 
             </div>
         </div>
       </div>
      </div>
   </div>
  </div>

@endsection

@section('script')
<script type="text/javascript" src="/js/custom/academy.js"></script>
@endsection