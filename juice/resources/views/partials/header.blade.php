<!-- 
@ main header and navbar @ for all pages 
-->
<header class="main-header" id="headerMain">
    <nav class="navbar" id="navbarMain">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">

          <a id="book_now_1" class="pull-right" href="/sign-in">
              <span>Sign In</span>
          </a>

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-main" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="open-icon">
                <img src="/img/icons/menu.svg" width="16">
            </span>
            <span class="close-icon">
                <img src="/img/icons/cancel.svg" width="16">
            </span>
          </button>

          
          <a class="navbar-brand" href="/" title="Juice">
            <img alt="Brand" src="/images/logo.png" width="115px">
          </a>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-navbar-main">
        
          <ul class="nav navbar-nav navbar-right">
            <li @if(Request::is('services')) class="active" @endif><a class="nav-link" href="/services"><span>Services</span> <span class="sr-only">(current)</span></a></li>
            <li @if(Request::is('salon-finder')) class="active" @endif><a class="nav-link" href="/salon-finder"><span>Salon Finder</span></a></li>
            <li @if(Request::is('products')) class="active" @endif><a class="nav-link" href="/products"><span>Products</span></a></li>
            <li @if(Request::is('get-the-look')) class="active" @endif><a class="nav-link" href="/get-the-look"><span>Get the look</span></a></li>
            <li @if(Request::is('academy')) class="active" @endif><a class="nav-link" href="/academy"><span>Academy</span></a></li>
            <li @if(Request::is('franchise')) class="active" @endif><a class="nav-link" href="/franchise"><span>Franchise</span></a></li>
            @if(!Auth::check())
            <li><a class="nav-btn hidden-xs" href="/sign-in">Sign In</a></li>
            @else
            <li @if(Request::is('my-account')) class="active" @endif><a class="nav-link" href="/my-account"><span>My Account</span></a></li>
            <li><a class="nav-btn hidden-xs" href="/api/auth/logout">Sign Out</a></li>
            @endif
          </ul>

        </div><!-- /.navbar-collapse -->

      </div><!-- /.container-fluid -->
    </nav>
</header>