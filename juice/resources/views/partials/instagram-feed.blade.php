<!--
@ Instagram Slide Section
-->
<section class="content-section instagram-feed" id="instagramFeed">
    <div class="instagram-feed-container container" style="position: relative;">

        <!-- decoration -->
        <div id="decor_xG59" class="decorate dots" style="position: absolute; right: 20px; top: -3.5em; z-index:1; user-select: none;-webkit-user-drag: none; ">
                <img src="img/decorations/dots-horizontal_120px.png" width="84">
        </div>
        <!-- decoration end -->

        <div class="instagram-feed-box">
          <div class="inner">
            <header class="ig-feed-header">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <h4 class="ig-box-title">
                            <i class="fa fa-instagram fa-fw fa-2x" style="margin-right: 10px;"></i>
                            <span>Find out what our happy customers have to say :)</span>
                        </h4>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <a href="#post-your-pic" class="links-btn btn-shadow__yellow pull-right">Post your pic</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </header>


            <div class="instagram-feed-carousel">
                <div class="ig-feed-boxes owl-carousel owl-theme">
                    <article class="ig-box owl-item">
                        <img class="photo" src="images/customers/ig-customer-1.jpg" width="232" data-animation="animated slideInRight">
                        <a class="ig-handle" href="#">@jane_doe</a>
                        <p class="ig-caption">Lovin' this cut <a href="https:/www.instagram.com/explore/tags/thanksjuice/" class="hashtag">#thanksjuice</a> <a href="#" class="hashtag">#other</a></p>
                    </article>

                    <article class="ig-box owl-item">
                        <img class="photo" src="images/customers/ig-customer-2.jpg" width="232" data-animation="animated slideInRight">
                        <a class="ig-handle" href="#">@jonnadoe</a>
                        <p class="ig-caption">Hair game on point.</p>
                    </article>

                    <article class="ig-box owl-item">
                        <img class="photo" src="images/customers/ig-customer-3.jpg" width="232" data-animation="animated slideInRight">
                        <a class="ig-handle" href="#user">@cheryl_doe</a>
                        <p class="ig-caption">Getting the perfect make-over for my wedding. <a href="https:/www.instagram.com/explore/tags/torescue/" class="hashtag">#juicetorescue</a></p>
                    </article>

                    <article class="ig-box owl-item">
                        <img class="photo" src="images/customers/ig-customer-4.jpg" width="232" data-animation="animated slideInRight">
                        <a class="ig-handle" href="#user">@janejanexx</a>
                        <p class="ig-caption">Lorem Ipsum Doler Sit Amet <a href="https:/www.instagram.com/explore/tags/torescue/" class="hashtag">#juicetorescue</a></p>
                    </article>

                    <article class="ig-box owl-item">
                        <img class="photo" src="images/customers/ig-customer-5.jpg" width="232" data-animation="animated slideInRight">
                        <a class="ig-handle" href="#user">@_lysa</a>
                        <p class="ig-caption">Integer ex lacus, lobortis quis consequat in. </p>
                    </article>

                    <article class="ig-box owl-item">
                        <img class="photo" src="images/customers/ig-customer-2.jpg" width="232" data-animation="animated slideInRight">
                        <a class="ig-handle" href="#user">@somename</a>
                        <p class="ig-caption">Suspendisse potenti. Donec lorem metus, blandit sit amet sagittis in, semper ac neque.  <a href="https:/www.instagram.com/explore/tags/salon/" class="hashtag">#juicesalon</a></p>
                    </article>

                </div>

                <div class="slider-navigation ">
                    <div class="icon-container left-icon">
                        <a href="#" class="ig-slide-nav" data-slide="prev">
                           <img style="transform: rotateY(180deg);" src="img/icons/right-arrow-forward.svg" width="32">
                        </a>
                    </div>

                    <div class="icon-container right-icon">
                        <a href="#" class="ig-slide-nav" data-slide="next">
                           <img src="img/icons/right-arrow-forward.svg" width="32"> 
                        </a>
                    </div>
                </div>
            </div>

          </div> <!-- .inner end -->
        </div> <!-- .instagreem-feed-box end -->

    </div>    
</section>