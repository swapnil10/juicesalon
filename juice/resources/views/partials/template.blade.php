<!doctype html>
<html class="no-js" lang="">
@include('partials.head')
<body>
@include('partials.header')
@yield('custom-page-header')
<!-- 
@ User greet, links and cart 
-->

<!-- 
<nav class="user-toolbar">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 pull-left">
                <span>Good evening [[User's name]]</span>
            </div>

            <div class="col-sm-3 pull-right">
                <a href="#" title="Add products to your cart" class="cart"> <i class="cart-icon"></i> <span>1 item(s)</span></a>
            </div>
        </div>
    </div>
</nav>
-->

@yield('main')

@yield('instagram-feed')
@include('partials.footer')
</body>
</html>