<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title') | JUICE Salon </title>
    <meta name="description" content="Make up products, beauty tips and videos, fashion trends in Mumbai, India">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/styles.css">
    @yield('style')
    <script src="/js/vendor/modernizr-2.8.3.min.js"></script>
</head>