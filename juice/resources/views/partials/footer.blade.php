

    <div id="global-templates" style="display: none;">
        <div id="cart-container-template">
            <article class="item-block product-block" id="cart_product_1">

             <div class="row">
               <div class="col-xs-3 product-cart-image">
                 <div class="img-container">
                   <div class="background-oval"></div>
                   <img class="img-responsive">
                 </div>
               </div>

               <div class="col-xs-7 product-detail">
                <h3 class="product-title"></h3>
                <table class="price-discounts-table table">
                  <tbody>
                    <tr class="tr_quantity">
                      <td class="tr_label">Quantity</td>
                      <td class="tr_value"><input class="item-quantity" type="number" disabled="" value="0"></td>
                    </tr>
                    <tr class="tr_price">
                      <td class="tr_label">Price</td>
                      <td class="tr_value">Rs. <span class="value" id="price">0</span></td>
                    </tr>

                    

                    <tr class="tr_product_total big">
                      <td class="tr_label">Total</td>
                      <td class="tr_value">Rs. <span class="value" id="total">0</span></td>
                    </tr>
                  </tbody>
                </table>
               </div>

               <div class="col-xs-2 product-edit">
                <div class="item-edit-options">
                  <ul>
                    <li><a class="product-cart-item-edit edit-item" href="#edit"><i class="fa fa-fw fa-pencil"></i></a></li>
                    <li><a class="product-cart-item-edit delete-item" href="#delete"><i class="fa fa-fw fa-trash-o"></i></a></li>
                  </ul>
                </div>
               </div>

             </div>
           </article>
        </div>
        <!-- <div id="cart-container-checkout-template">
            <article class="item-block">
             <div class="row">
               <div class="col-xs-3"></div>
               <div class="col-xs-7">
                 <!-- checkout button 
                 <div class="item-block checkout-button">
                   <a href="/api/order/payment" class="links-btn cart-checkout-btn">Checkout</a>
                 </div>

               </div>
             </div>
           </article>
        </div> -->
    </div>

    <!-- Styles -->
    <link href="https:/fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700|Oxygen:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="/css/animate.min.css">  
    <link rel="stylesheet" href="/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/owl.theme.default.css">

    <!-- Scripts and Resources -->
    <script src="/js/vendor/jquery-1.12.0.min.js"></script>
    <script src="/js/vendor/bootstrap.js"></script>
    <script src="/js/plugins.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/custom/global-function.js"></script>
    @yield('script')