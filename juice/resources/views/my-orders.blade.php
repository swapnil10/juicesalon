<?php
   if(!Auth::check())
    die(redirect('/sign-in'));
  $user = Auth::user();
?>
@extends('partials.template')

@section('title','Academy')

@section('meta')
@endsection

@section('style')
@endsection

@section('main')  


<!-- Franchise Page Header -->
<header class="section-header myorders-title-header">
  <div class="bg-overlay" style="background-color: #ffff3d; position: absolute; left: 0;
  right: 0; bottom: 0; top: 0; opacity: .9;"></div>

  <div class="container" style="position: relative; padding-bottom: 1em; padding-top: 1em;">
      <h1 class="h1 text-center font-2">My Orders</h1>
  </div>
</header>


<main class="main-section page-main-area" id="my_orders">
  
  <!-- Order History List -->
  <section class="section-main my-orders-list">
    <div class="container">

      <!-- Item 1 -->
      <article class="order-item order-item-big">
        <div class="inner">
          <div class="product-image">
            <div class="img-container">
              <div class="bg-oval"></div>
              <img src="../images/product-images/shampoo_2_240px.png">
            </div>
          </div>
          <div class="product-details">
            <div class="product-details-container">
              <h3 class="product-title"><a href="#"><span>LYN Hair Shampoo</span></a></h3>

              <table class="price-discounts-table table">
                <tbody>
                  <tr class="tr_quantity">
                    <td class="tr_label">Quantity</td>
                    <td class="tr_value"><span class="value">1</span></td>
                  </tr>
                  <tr class="tr_price">
                    <td class="tr_label">Price</td>
                    <td class="tr_value">Rs. <span class="value">889</span></td>
                  </tr>

                  <tr class="tr_discount">
                    <td class="tr_label">Discount</td>
                    <td class="tr_value">Rs. <span class="value">334</span> (5%)</td>
                  </tr>

                  <tr class="tr_product_total ">
                    <td class="tr_label">Total</td>
                    <td class="tr_value">Rs. <span class="value">588</span></td>
                  </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </article>


      <!-- Item 2 -->
      <article class="order-item order-item-big">
        <div class="inner">
          <div class="product-image">
            <div class="img-container">
              <div class="bg-oval"></div>
              <img src="../images/product-images/shampoo_2_240px.png">
            </div>
          </div>
          <div class="product-details">
            <div class="product-details-container">
              <h3 class="product-title"><a href="#"><span>LYN Hair Shampoo</span></a></h3>
              <table class="price-discounts-table table">
                <tbody>
                  <tr class="tr_quantity">
                    <td class="tr_label">Quantity</td>
                    <td class="tr_value"><span class="value">1</span></td>
                  </tr>
                  <tr class="tr_price">
                    <td class="tr_label">Price</td>
                    <td class="tr_value">Rs. <span class="value">889</span></td>
                  </tr>

                  <tr class="tr_discount">
                    <td class="tr_label">Discount</td>
                    <td class="tr_value">Rs. <span class="value">334</span> (5%)</td>
                  </tr>

                  <tr class="tr_product_total ">
                    <td class="tr_label">Total</td>
                    <td class="tr_value">Rs. <span class="value">588</span></td>
                  </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </article>

    </div>
  </section>


</main>

