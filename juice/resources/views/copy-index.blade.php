@extends('partials.template')
@section('title','Range of Make Up Products, Beauty Tips, Fashion Trends Online in India')
@section('meta')
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="/assets/admin/plugins/image-picker/image-picker.css">
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-common-extra.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-core.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-reset.css'>
<link rel='stylesheet' href='/assets/admin/css/css/aloha/css/aloha-sidebar.css'>
<link rel='stylesheet' href='/assets/admin/css/css/create-ui/css/create-ui.css'>
<link rel='stylesheet' href='/assets/admin/css/css/midgard-notifications/midgardnotif.css'>
<style>
  .image_picker_image{
    height: 100px !important;
    width: 100px !important;
</style>
@endsection

@section('custom-page-header')
<!-- 
@ Main Page Carousel 
-->
<section class="home-page-slider" id="slider_container_home">

      <!-- OwlCarousel Start -->
      <div class="owl-carousel home-main-slide" id="home_slider_1" style="height: 360px; opacity: 0;">

          <!-- Slide Item 1 -->
          <div class="owl-item" style="float: none;">
            <div class="bg-image-slide" style="background-image: url('images/sliders/grand-creation-salon-hair-extensions-slide.jpg');" ></div>
            <div class="slide-inner">
                <div class="bubble-container" ></div>
                <div class="text-col" data-animation="animated fadeInLeft">
                    <p class="medium"><b>Monsoon Madness.</b><br><b>Get 50% off on select products.</b></p>
                    <p class="btn-container"><a href="#products" class="action-link">View Products</a> </p>
                </div>
            </div>
          </div>

          <!-- Slide Item 2 -->
          <div class="owl-item" style="float: none;">
            <div class="bg-image-slide" style="background-image: url('images/sliders/grand-creation-salon-hair-extensions-slide.jpg');" ></div>
            <div class="slide-inner"> 

                <div class="bubble-container bubble-right"></div>
                <div class="filler" style="flex-grow: 1;"></div>
                <div class="text-col right-align" data-animation="animated fadeInRight">
                    <p class="medium"><b>Monsoon Madness.</b><br><b>Get 50% off on select products.</b></p>
                    <p class="btn-container"><a href="#products" class="action-link">View Products</a> </p>
                </div>

            </div>
          </div>


          <!-- Slide Item 3 -->
          <div class="owl-item" style="float: none;">
            <div class="bg-image-slide" style="background-image: url('images/sliders/grand-creation-salon-hair-extensions-slide.jpg');" ></div>
            <div class="slide-inner">
                <div class="bubble-container"></div>
                <div class="text-col" data-animation="animated fadeInLeft">
                    <p class="medium"><b>Monsoon Madness.</b><br><b>Get 50% off on select products.</b></p>
                    <p class="btn-container"><a href="#products" class="action-link">View Products</a> </p>
                </div>

            </div>
          </div>

      </div>
      <!-- OwlCarousel End -->
</section> 
@endsection

@section('main')
  <main class="main-section page-main-area" id="home_page_content article" about="/my/article">
  <!-- Section 1 : Images cards and text -->
  <section class="section-main cards featured-cards">
      <div class="container-fluid">
          <!-- Feature Box #1 / Style 5 -->
          <div class="col-md-6 col-sm-12">

              <article class="featured-boxes style--5">
                <div class="inner row">
                  
                  <div class="col col-xs-5 nopadding">
                    <div class="image-container">
                           <img data-position="left" data-count='' id="leftimage" src="images/featured/featured_look_1.jpg">
                           <a data-position="left" data-count='' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
               <i style="opacity: 0.50; color: #4c4c4c;">Click "Choose Image" to edit..</i>
                      </div>
                  </div>
                  
                  <div class="col col-xs-7 summary-box nopadding text-right">
                    <div class="summary-box-inner">
                        <p property="content" id="leftcontent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.  rhoncus quis cursus sed, egestas ut nulla.</p>
                        <i style="color: #4c4c4c;">Click above to edit..</i>
                        <p class="floating-btn"><b><a class="links-btn btn-shadow__yellow" href="#">View all looks</a></b></p>
                    </div>
                  </div>
                </div>
              </article>

          </div>

          <!-- Feature Box #2 / Style 2 -->
          <div class="col-md-6 col-sm-12" style="position: relative;">
               <!-- decoration -->
               <div id="decor_xG591" class="decorate dots visible-md visible-lg" style="position: absolute; left: 10px; top: 100px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                               <img src="img/decorations/dots_50px.png" width="32">
                       </div>
               <!-- decoration end -->

              <article class="featured-boxes style--2" style="max-width: 400px;">
                <div class="inner row">
                  <div class="column-text col col-xs-6">
                      <header class="title-box">
                        <h3 property="heading" id="rightheading">Launching the LYN conditioner soon</h3>
                      </header>
                      <p property="content" class="summary" id="rightcontent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.  </p>
                      <i style="color: #4c4c4c;">Click above to edit..</i>
                  </div>
                  
                  <div class="column-image col col-xs-6">
                    <div class="image-container text-center">
                           <img data-position="right" data-count='' id="rightimage" src="images/featured/featured_product_1.png">
                           <a data-position="right" data-count='' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                        <i style="opacity: 0.50; color: #4c4c4c;">Click "Choose Image" to edit..</i>
                      </div>
                  </div>
                      
                </div>
              </article>
          </div>

      </div>
  </section>

  <!-- Section 2 : Images cards and text -->
  <section class="section-main cards featured-cards">
      <div class="container-fluid">
          <div class="row">

              <div class="col-md-6 col-sm-12">
                  <!-- Feature Box #3 / Style 1 -->
                  <article class="featured-boxes style--1" style="max-width: 400px;">
                    <div class="inner">
                      <div class="bg-half yellow"></div>
                      
                      <header class="title-box">
                        <h3><a id="leftheading" href="#" property="heading2">You should do this today yourself</a></h3>
                      </header>
                      
                      <div class="image-container">
                        <img data-position="left" data-count='1' id="leftimage1" src="images/featured/featured_look_2.jpg">
                        <a data-position="left" data-count='1' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                        <i style="opacity: 0.50; color: #4c4c4c;">Click "Choose Image" to edit..</i>
                      </div>
                      
                      <p property="content" id="leftcontent1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
                      <i style="color: #4c4c4c;">Click above to edit..</i>
                    </div>
                  </article>

              </div>
              


              <!-- Feature Box #2 -->

              <div class="col-md-6 col-sm-12">

                  <!-- Feature Box #4 / Style 3 -->
                  <article class="featured-boxes style--3">
                    <div class="inner row">
                      <div class="col col-xs-5 nopadding">
                        <div class="image-container">
                               <img data-position="right" data-count='1' id="rightimage1" src="images/featured/featured_look_8.jpg">
                               <a data-position="right" data-count='1' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                            <i style="opacity: 0.50; color: #4c4c4c;">Click "Choose Image" to edit..</i>
                          </div>
                      </div>
                      <div class="col col-xs-7 summary-box nopadding text-right">
                        <div class="summary-box-inner">
                            <p id="rightcontent1" property="content">Try these new haircuts from our look book.<br>Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai it's discounted so I guess you are more interest.</p>
                            <i style="color: #4c4c4c;">Click above to edit..</i>
                        </div>
                      </div>
                    </div>
                  </article>
                  
              </div>

          </div>
      </div>
  </section>



  <!-- Get the Look - Top Picks -->
  <section class="section-main look-page-section" style="background-color: #ffff3d; position: relative; padding: 0;">

      <div class="container" style="position: relative; padding-bottom: 1em; padding-top: 3em;">
          <!-- decoration -->
          <div id="decor_xG56" class="decorate dots" style="position: absolute; right: 20px; top: -16.5px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                  <img src="/img/decorations/dots-horizontal_120px.png" width="84">
          </div>
          <!-- decoration end -->

          <div class="row">

            <div class="col-md-4 col-xs-12" >
              <div class="page-description-header">
                <div class="media">

                  <div style="display: flex; align-items: center;">
                       <h4 property="heading" id="leftheading1">Our picks for the haircuts you need to rock this monsoon</h4>
                       <img  style="padding: 0px" src="/img/icons/scissors.svg" alt="Scissors Cutting Hair" width="32">

                       <div class="clearfix"></div>
                  </div>

                  <div class="media-body">
                    <p property="content" id="leftcontent2">Nunc tincidunt quis dui sed efficitur. Cras lo sed varius consectetur, metus mauris ullamcorper tortor, in euismod lectus nunc ac lorem. </p>
                    <i style="color: #4c4c4c;">Click above to edit..</i>
                    <p>
                        <a href="get-the-look.html" title="Get the Look!" class="links-btn btn-shadow small pull-right"><b>Get the look</b></a>
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-1 col-lg-1 visible-md visible-lg"></div>

            <div class="col-md-7 col-xs-12">
              <div class="row">
                <div class="animated fadeIn image-card none col-md-4 col-xs-6 text-center">
                  <div class="inner-container">
                    <img data-position="right" data-count='2' style="margin: 0 auto;" id="rightimage2" src="images/get-the-look/look-1-is.png" width="150" height="180" class="img-responsive">
                    <p class="caption-image-card text-left" style="max-width: 165px; margin: 0 auto;">This hairstyle that</p>
                     <a data-position="right" data-count='2' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                  </div>  
                </div>

                <div class="animated fadeIn image-card none col-md-4 col-xs-6 text-center">
                  <div class="inner-container">
                    <img data-position="right" data-count='3' style="margin: 0 auto;" id="rightimage3" src="images/get-the-look/look-2-is.png" width="150" height="180" class="img-responsive">
                    <p class="caption-image-card text-left" style="max-width: 165px; margin: 0 auto;">This hairstyle that</p>
                    <a data-position="right" data-count='3' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                  </div>  
                </div>

                <div class="animated fadeIn image-card none col-md-4 col-xs-12 text-center">
                  <div class="inner-container">
                    <img data-position="right" data-count='4' style="margin: 0 auto;" id="rightimage4" src="images/get-the-look/look-3-is.png" width="150" height="180" class="img-responsive">
                    <p class="caption-image-card text-left" style="max-width: 165px; margin: 0 auto;">This hairstyle that</p>
                    <a data-position="right" data-count='4' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                  </div>  
                </div>

              </div>
            </div>

          </div>
      </div>
  </section>




  <!-- Section 3 : Images cards and text -->
  <section class="section-main cards featured-cards">
      <div class="container-fluid">
          <!-- Feature Box #5 / Style 4-->
          <div class="col-md-6 col-sm-12">
              <article class="featured-boxes style--4">
                <div class="inner row">
                  <div class="col col-xs-5 nopadding">
                    <div class="image-container">
                           <img data-position="left" data-count='5' id="leftimage5" src="images/featured/featured_look_6.jpg">
                           <a data-position="left" data-count='5' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                      </div>
                  </div>
                  <div class="col col-xs-7 summary-box nopadding text-right">
                    <div class="summary-box-inner">
                        <p id="leftcontent3" property="content">Try these new haircuts from our look book.<br>
Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai it's discounted so I guess you are more interest.</p>
                        <i style="color: #4c4c4c;">Click above to edit..</i>
                        <p><b><a class="links-btn btn-shadow" href="#">View all looks</a></b></p>
                    </div>
                  </div>
                </div>
              </article>
          </div>

          <!-- Feature Box #6 / Style 3 -->

          <div class="col-md-6 col-sm-12">
              <article class="featured-boxes style--3">
                <div class="inner row">
                  
                  <div class="col col-xs-7 summary-box nopadding text-left">
                    <div class="summary-box-inner">
                        <p property="content" id="rightcontent2">Try these new haircuts from our look book.<br>
                         Inspired by some random thing this is perfect for round faces so ya you need to rock this cut oh by the wai it's discounted so I guess you are more interest.</p>
                         <i style="color: #4c4c4c;">Click above to edit..</i>
                    </div>
                  </div>
                  
                  <div class="col col-xs-5 nopadding">
                    <div class="image-container">
                           <img data-position="right" data-count='5' id="rightimage5" src="images/featured/featured_look_5.jpg">
                           <a data-position="right" data-count='5' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                      </div>
                  </div>
                </div>
              </article>
          </div>

      </div>
  </section>



  <!-- Section 4 : Images cards and text -->
  <section class="section-main cards featured-cards">
      <div class="container-fluid">
          <!-- Feature Box #7 / Style 6 -->
          <div class="col-md-6 col-sm-12">
              
              <article class="featured-boxes style--6">
                <div class="inner row">
                  <div class="bg"></div>
                  
                  <div class="col col-md-6 col-sm-6 col-xs-12 nopadding">
                    <div class="image-container">
                           <img data-position="left" data-count='6' id="leftimage6" src="images/featured/featured_look_7.jpg">

                      </div>
                  </div>
                  <div class="col col-md-6 col-sm-6 col-xs-12 summary-box nopadding text-right">
                    <div class="summary-box-inner" style="padding-top: 2em;">
                    <a data-position="left" data-count='6' class="open-all-images" href="javascript:void(0)">Choose Image</a>
                        <p property="content" id="leftcontent4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.  rhoncus quis cursus sed, egestas ut nulla.</p>
                        <i style="color: #4c4c4c;">Click above to edit..</i>
                        <p class="floating-btn"><b><a class="links-btn btn-shadow__yellow" href="#">View all looks</a></b></p>
                    </div>
                  </div>
                </div>
              </article>
          </div>

          <!-- Feature Box #8 / Style 2 -->
          <div class="col-md-6 col-sm-12" style="position: relative;">
            <!-- decoration -->
            <div id="decor_xG592" class="decorate dots visible-md visible-lg" style="position: absolute; left: 10px; top: 60px; z-index:1; user-select: none;-webkit-user-drag: none; ">
                            <img src="img/decorations/dots_50px.png" width="32">
                    </div>
            <!-- decoration end -->
              <article class="featured-boxes style--2">
                <div class="inner row">
                  <div class="column-image col col-xs-12">
                    <div class="image-container text-center">
                           <img data-position="right" data-count='6' id="rightimage6" src="images/featured/featured_product_2.png">
                           <a data-position="right" data-count='6' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                      </div>
                  </div>
                  
                  <div class="column-text col col-xs-12 text-right">
                      <header class="title-box text-left">
                        <h3 property="heading" id="rightheading1">Launching the LYN conditioner next week. Here's why you need to buy it.</h3>
                      </header>
                      <p class="summary" property="content" id="rightcontent3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.  </p>
                      <i style="color: #4c4c4c;">Click above to edit..</i>
                  </div>
                      
                </div>
              </article>
          </div>
      </div>
  </section>


<!-- Experts Section - Images Cards  -->
<section class="section-main images-cards experts-section" style="background-color: #111; color: #fff;">
    <div class="container">
        
        <div class="col-md-2">
            <div class="text-col">
                <!-- decoration -->
                <div class="decoration swirl">
                    <img style="width: 100%; max-width: 100px; margin-bottom: 3em;" src="img/decorations/swirl-decorate-white-200.png">
                </div>
                <!-- decoration -->

                <p property="content" id="leftcontent5">Our experts say that random lorem ipsum hair tips and tricks. This is what they have to say.
Click on their pic to learn more about them.</p>
                <i style="color: #4c4c4c;">Click above to edit..</i>
                <p style="margin-top: 2em;">
                    <a class="links-btn btn-shadow__yellow" href="#">View all looks</a>
                </p>

            </div>
        </div>

        <div class="col-md-10 experts-image-cards">
            <div class="row">
                <div class="col-md-4">
                    <div class="image-card">
                        <div class="image-container">
                            <img data-position="right" data-count='7' id="rightimage7" src="images/experts/expert-1.jpg">
                        </div>
                        <div class="expert-details">
                            <div class="inner">
                                <h3 property="heading" id="heading1">Lorem Ipsum Doler</h3>
                                <i style="color: #4c4c4c;">Click above to edit..</i>
                            </div>
                        </div>
                    </div>
                    <a data-position="right" data-count='7' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                </div>
                <div class="col-md-4">
                    <div class="image-card">
                        <div class="image-container">
                            <img data-position="right" data-count='8' id="rightimage8" src="images/experts/expert-2.jpg">
                        </div>
                        <div class="expert-details">
                            <div class="inner">
                                <h3 property="heading" id="heading2">Lorem Ipsum Doler</h3>
                                <i style="color: #4c4c4c;">Click above to edit..</i>
                            </div>
                        </div>
                    </div>
                    <a data-position="right" data-count='8' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                </div>
                <div class="col-md-4">
                    <div class="image-card">
                        <div class="image-container">
                            <img data-position="right" data-count='9' id="rightimage9" src="images/experts/expert-3.jpg">
                        </div>

                        <div class="expert-details">
                            <div class="inner">
                                <h3 property="heading" id="heading3">Lorem Ipsum Doler</h3>
                                <i style="color: #4c4c4c;">Click above to edit..</i>
                            </div>
                        </div>
                    </div>
                    <a data-position="right" data-count='9' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                </div>
            </div>
        </div>

    </div>
</section>



<!-- Section 5 : Images cards and text -->
<section class="section-main cards featured-cards">
  <div class="container-fluid">
      <!-- Feature Box #9 / Style 1 -->
      <div class="col-md-6 col-sm-12">
          <article class="featured-boxes style--1" style="max-width: 400px;">
            <div class="inner">
              <div class="bg-half yellow"></div>
              
              <header class="title-box">
                <h3 property="heading" id="leftheading2"><a href="#">Why you need to do this before getting that done everyday.</a></h3>
              </header>
              
              <div class="image-container">
                <img data-position="left" data-count='7' id="leftimage7" src="images/featured/featured_look_3.jpg">
                <a data-position="left" data-count='7' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
              </div>
              
              <p property="content" id="leftcontent6">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
              <i style="color: #4c4c4c;">Click above to edit..</i>
              
            </div>
          </article>
      </div>

      <!-- Feature Box #10 / Style -->
      <div class="col-md-6 col-sm-12">
                <article class="featured-boxes style--7">
                  <div class="inner row row-eq-height">
            
                    <div class="bg"></div>
                     
                    <div class="col col-xs-6 summary-box nopadding">
                      <header class="title-box">
                          <h2><a href="#" property="heading" id="rightheading2">Get some this and that for your skin.</a></h2>
                        <div class="arrow-icon" style="bottom: .5em;">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="32px" height="32px" viewBox="0 0 408 408" style="enable-background:new 0 0 408 408;" xml:space="preserve"> <g> <g id="arrow-back"> <path d="M408,178.5H96.9L239.7,35.7L204,0L0,204l204,204l35.7-35.7L96.9,229.5H408V178.5z"></path> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>
                        </div>

                    </header>

                      <div class="summary-box-inner">
                          <p property="content" id="rightcontent4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris lectus, rhoncus quis cursus sed, egestas ut nulla.</p>
                          <i style="color: #4c4c4c;">Click above to edit..</i>
                          <p class="floating-link"><b><a class="links-btn btn-shadow__yellow" href="#">View all looks</a></b></p>
                      </div>
                    </div>
                    
                    <div class="col col-xs-6 nopadding">
                      <div class="image-container">
                             <img data-position="right" data-count='10' id="rightimage10" src="images/featured/featured_look_4.jpg">
                             <a data-position="right" data-count='10' class="open-all-images" href="javascript:void(0)" style="float: right !important;">Choose Image</a>
                        </div>
                    </div>
                    
                    
                  </div>
                </article>
      </div>
  </div>
</section>
</main>

<!--modal-->
  <div class="modal fade" id="images-modal" role="dialog" style="width: 625px;">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Image</h4>
        </div>
        <div class="modal-body" style="height: 550px;overflow: auto;">
          <select id="image-select" class="image-picker show-html"></select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!--end Modal-->


@endsection

@section('instagram-feed')
@include('partials.instagram-feed')
@endsection

@section('script')
<script type="text/javascript">
    initImages();
    var lookId = 0;
    var target;
    var iamInPosition = "";
    var iamInCount = "";
    var preventMe = false;
     $(document).on('click','.open-all-images',function(e){
        e.preventDefault();
        $('#images-modal').modal('show');
        var position = $(this).attr('data-position');
        var count = $(this).attr('data-count');
        var imageId = $("body").find("img[data-position='"+position+"'][data-count='"+count+"']").attr('data-lookId');

        preventMe = true;
        $("#image-select").val(imageId).trigger('change');
        preventMe = false;

        iamInPosition = position;
        iamInCount = count;
  });

     $(document).on('change','#image-select',function(e){
        e.preventDefault();
        if(preventMe)
          return false;
        var imgId = $("#image-select").find("option:selected").val();
        $("img[data-position='"+iamInPosition+"'][data-count='"+iamInCount+"']").attr('src','/bulkimage/'+imgId).attr('data-lookId',imgId);
    });

  function initImages(ofs=0){
      $.get('/bulkimages?offset='+ofs,function(images){
          if(!images.images.length){
            $("#image-select").imagepicker()
            return false;
          } 
          $.each(images.images,function(i,v){
            var tmp = "<option data-img-src='/bulkimage/"+v.id+"' value='"+v.id+"'>"+v.id+"</option>";
            $("#image-select").append(tmp);
          }); 
          ofs = ofs + 10;
          initImages(ofs);
        });
  }

  $.get("api/homepage",function(data){
    $("#leftcontent").html(data[0].leftcontent);
    $("#leftimage").attr("src","/bulkimage/"+data[0].leftimage).attr("data-lookId",data[0].leftimage);
    $("#rightheading").html(data[0].rightheading);
    $("#rightcontent").html(data[0].rightcontent);
    $("#rightimage").attr("src","/bulkimage/"+data[0].rightimage).attr("data-lookId",data[0].rightimage);
    $("#leftheading").html(data[0].leftheading);
    $("#leftimage1").attr("src","/bulkimage/"+data[0].leftimage1).attr("data-lookId",data[0].leftimage1);
    $("#leftcontent1").html(data[0].leftcontent1);
    $("#rightimage1").attr("src","/bulkimage/"+data[0].rightimage1).attr("data-lookId",data[0].rightimage1);
    $("#rightcontent1").html(data[0].rightcontent1);
    $("#leftheading1").html(data[0].leftheading1);
    $("#leftcontent2").html(data[0].leftcontent2);
    $("#rightimage2").attr("src","/bulkimage/"+data[0].rightimage2).attr("data-lookId",data[0].rightimage2);
    $("#rightimage3").attr("src","/bulkimage/"+data[0].rightimage3).attr("data-lookId",data[0].rightimage3);
    $("#rightimage4").attr("src","/bulkimage/"+data[0].rightimage4).attr("data-lookId",data[0].rightimage4);
    $("#leftimage5").attr("src","/bulkimage/"+data[0].leftimage5).attr("data-lookId",data[0].leftimage5);
    $("#leftcontent3").html(data[0].leftcontent3);
    $("#rightimage5").attr("src","/bulkimage/"+data[0].rightimage5).attr("data-lookId",data[0].rightimage5);
    $("#rightcontent2").html(data[0].rightcontent2);
    $("#leftimage6").attr("src","/bulkimage/"+data[0].leftimage6).attr("data-lookId",data[0].leftimage6);
    $("#leftcontent4").html(data[0].leftcontent4);
    $("#rightheading1").html(data[0].rightheading1);
    $("#rightimage6").attr("src","/bulkimage/"+data[0].rightimage6).attr("data-lookId",data[0].rightimage6);
    $("#rightcontent3").html(data[0].rightcontent3);
    $("#leftcontent5").html(data[0].leftcontent5);
    $("#rightimage7").attr("src","/bulkimage/"+data[0].rightimage7).attr("data-lookId",data[0].rightimage7);
    $("#rightimage8").attr("src","/bulkimage/"+data[0].rightimage8).attr("data-lookId",data[0].rightimage8);
    $("#rightimage9").attr("src","/bulkimage/"+data[0].rightimage9).attr("data-lookId",data[0].rightimage9);
    $("#leftheading2").html(data[0].leftheading2);
    $("#leftcontent6").html(data[0].leftcontent6);
    $("#leftimage7").attr("src","/bulkimage/"+data[0].leftimage7).attr("data-lookId",data[0].leftimage7);
    $("#rightheading2").html(data[0].rightheading2);
    $("#rightcontent4").html(data[0].leftcontent6);
    $("#rightimage10").attr("src","/bulkimage/"+data[0].rightimage10).attr("data-lookId",data[0].rightimage10);
    $("#heading1").html(data[0].heading1);
    $("#heading2").html(data[0].heading2);  
    $("#heading3").html(data[0].heading3);
  });

</script>
<script type="text/javascript" src="/assets/admin/plugins/image-picker/image-picker.js"></script>
<script type="text/javascript" src='http://code.jquery.com/ui/1.9.2/jquery-ui.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/underscore-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/backbone-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/vie-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/jquery.tagsinput.min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/jquery.rdfquery.min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/annotate-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/rangy-core-1.2.3.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/hallo-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/aloha/lib/require.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/aloha/lib/aloha-full.min.js', data-aloha-plugins='common/ui,common/format,common/link,common/image,extra/sourceview'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/create-min.js'></script>
<script type="text/javascript" src='/assets/admin/js/scripts/contentblocks.js'></script>
<script type="text/javascript" src="/assets/admin/js/create.js"></script>


<script type="text/javascript">

// Prepare VIE
var v = new VIE();
v.use(new v.RdfaService());

// Load Create.js with your VIE instance
$("body").midgardCreate({
  vie: v
});
</script>
@endsection
